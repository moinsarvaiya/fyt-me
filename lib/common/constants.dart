import 'package:flutter/cupertino.dart';
import 'package:fytme/data/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'config/general.dart';
part 'config/storage.dart';
