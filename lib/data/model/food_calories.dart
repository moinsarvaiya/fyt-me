class FoodCalories {
  int _id;
  int _userId;
  String _calorieIntakeDate;
  int _caloriesIntakeGoal;
  List<EatingTime> _breakfast;
  List<EatingTime> _lunch;
  List<EatingTime> _dinner;
  List<EatingTime> _snacks;
  CaloriesBreakdownData _caloriesBreakdownData;

  int get id => _id;

  int get userId => _userId;

  String get calorieIntakeDate => _calorieIntakeDate;

  int get caloriesIntakeGoal => _caloriesIntakeGoal;

  List<EatingTime> get breakfast => _breakfast;

  List<EatingTime> get lunch => _lunch;

  List<EatingTime> get dinner => _dinner;

  List<EatingTime> get snacks => _snacks;

  CaloriesBreakdownData get caloriesBreakdownData => _caloriesBreakdownData;

  FoodCalories(
      {int id,
      int userId,
      String calorieIntakeDate,
      int caloriesIntakeGoal,
      List<EatingTime> breakfast,
      List<EatingTime> lunch,
      List<EatingTime> dinner,
      List<EatingTime> snacks,
      CaloriesBreakdownData caloriesBreakdownData}) {
    _id = id;
    _userId = userId;
    _calorieIntakeDate = calorieIntakeDate;
    _caloriesIntakeGoal = caloriesIntakeGoal;
    _breakfast = breakfast;
    _lunch = lunch;
    _dinner = dinner;
    _snacks = snacks;
    _caloriesBreakdownData = caloriesBreakdownData;
  }

  FoodCalories.fromJson(dynamic json) {
    _id = json["id"];
    _userId = json["user_id"];
    _calorieIntakeDate = json["calorie_intake_date"];
    _caloriesIntakeGoal = json["calories_intake_goal"];
    if (json["breakfast"] != null) {
      _breakfast = [];
      json["breakfast"].forEach((v) {
        _breakfast.add(EatingTime.fromJson(v));
      });
    }
    if (json["lunch"] != null) {
      _lunch = [];
      json["lunch"].forEach((v) {
        _lunch.add(EatingTime.fromJson(v));
      });
    }
    if (json["dinner"] != null) {
      _dinner = [];
      json["dinner"].forEach((v) {
        _dinner.add(EatingTime.fromJson(v));
      });
    }
    if (json["snacks"] != null) {
      _snacks = [];
      json["snacks"].forEach((v) {
        _snacks.add(EatingTime.fromJson(v));
      });
    }
    _caloriesBreakdownData = json["calories_breakdown_data"] != null ? CaloriesBreakdownData.fromJson(json["calories_breakdown_data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["user_id"] = _userId;
    map["calorie_intake_date"] = _calorieIntakeDate;
    map["calories_intake_goal"] = _caloriesIntakeGoal;
    if (_breakfast != null) {
      map["breakfast"] = _breakfast.map((v) => v.toJson()).toList();
    }
    if (_lunch != null) {
      map["lunch"] = _lunch.map((v) => v.toJson()).toList();
    }
    if (_dinner != null) {
      map["dinner"] = _dinner.map((v) => v.toJson()).toList();
    }
    if (_snacks != null) {
      map["snacks"] = _snacks.map((v) => v.toJson()).toList();
    }
    if (_caloriesBreakdownData != null) {
      map["calories_breakdown_data"] = _caloriesBreakdownData.toJson();
    }
    return map;
  }
}

class EatingTime {
  String _consumedFood;
  String _quantityOfFood;
  double _consumedCalories;
  double _consumedFats;
  double _consumedCarbohydrates;
  double _consumedProteins;

  String get consumedFood => _consumedFood;

  String get quantityOfFood => _quantityOfFood;

  double get consumedCalories => _consumedCalories;

  double get consumedFats => _consumedFats;

  double get consumedCarbohydrates => _consumedCarbohydrates;

  double get consumedProteins => _consumedProteins;

  EatingTime(
      {String consumedFood,
      String quantityOfFood,
      double consumedCalories,
      double consumedFats,
      double consumedCarbohydrates,
      double consumedProteins}) {
    _consumedFood = consumedFood;
    _quantityOfFood = quantityOfFood;
    _consumedCalories = consumedCalories;
    _consumedFats = consumedFats;
    _consumedCarbohydrates = consumedCarbohydrates;
    _consumedProteins = consumedProteins;
  }

  EatingTime.fromJson(dynamic json) {
    _consumedFood = json['consumed_food'];
    _quantityOfFood = json['quantity_of_food'];
    _consumedCalories = json['consumed_calories'];
    _consumedFats = json['consumed_fats'];
    _consumedCarbohydrates = json['consumed_carbohydrates'];
    _consumedProteins = json['consumed_proteins'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['consumed_food'] = _consumedFood;
    map['quantity_of_food'] = _quantityOfFood;
    map['consumed_calories'] = _consumedCalories;
    map['consumed_fats'] = _consumedFats;
    map['consumed_carbohydrates'] = _consumedCarbohydrates;
    map['consumed_proteins'] = _consumedProteins;
    return map;
  }
}

class CaloriesBreakdownData {
  String _totalConsumedCalories;
  String _totalBreakfastCalories;
  String _totalLunchCalories;
  String _totalDinnerCalories;
  String _totalSnacksCalories;
  String _totalBreakfastFats;
  String _totalLunchFats;
  String _totalDinnerFats;
  String _totalSnacksFats;
  String _totalBreakfastProteins;
  String _totalLunchProteins;
  String _totalDinnerProteins;
  String _totalSnacksProteins;
  String _totalBreakfastCarbohydrates;
  String _totalLunchCarbohydrates;
  String _totalDinnerCarbohydrates;
  String _totalSnacksCarbohydrates;

  String get totalConsumedCalories => _totalConsumedCalories;

  String get totalBreakfastCalories => _totalBreakfastCalories;

  String get totalLunchCalories => _totalLunchCalories;

  String get totalDinnerCalories => _totalDinnerCalories;

  String get totalSnacksCalories => _totalSnacksCalories;

  String get totalBreakfastFats => _totalBreakfastFats;

  String get totalLunchFats => _totalLunchFats;

  String get totalDinnerFats => _totalDinnerFats;

  String get totalSnacksFats => _totalSnacksFats;

  String get totalBreakfastProteins => _totalBreakfastProteins;

  String get totalLunchProteins => _totalLunchProteins;

  String get totalDinnerProteins => _totalDinnerProteins;

  String get totalSnacksProteins => _totalSnacksProteins;

  String get totalBreakfastCarbohydrates => _totalBreakfastCarbohydrates;

  String get totalLunchCarbohydrates => _totalLunchCarbohydrates;

  String get totalDinnerCarbohydrates => _totalDinnerCarbohydrates;

  String get totalSnacksCarbohydrates => _totalSnacksCarbohydrates;

  CaloriesBreakdownData(
      {String totalConsumedCalories,
      String totalBreakfastCalories,
      String totalLunchCalories,
      String totalDinnerCalories,
      String totalSnacksCalories,
      String totalBreakfastFats,
      String totalLunchFats,
      String totalDinnerFats,
      String totalSnacksFats,
      String totalBreakfastProteins,
      String totalLunchProteins,
      String totalDinnerProteins,
      String totalSnacksProteins,
      String totalBreakfastCarbohydrates,
      String totalLunchCarbohydrates,
      String totalDinnerCarbohydrates,
      String totalSnacksCarbohydrates}) {
    _totalConsumedCalories = totalConsumedCalories;
    _totalBreakfastCalories = totalBreakfastCalories;
    _totalLunchCalories = totalLunchCalories;
    _totalDinnerCalories = totalDinnerCalories;
    _totalSnacksCalories = totalSnacksCalories;
    _totalBreakfastFats = totalBreakfastFats;
    _totalLunchFats = totalLunchFats;
    _totalDinnerFats = totalDinnerFats;
    _totalSnacksFats = totalSnacksFats;
    _totalBreakfastProteins = totalBreakfastProteins;
    _totalLunchProteins = totalLunchProteins;
    _totalDinnerProteins = totalDinnerProteins;
    _totalSnacksProteins = totalSnacksProteins;
    _totalBreakfastCarbohydrates = totalBreakfastCarbohydrates;
    _totalLunchCarbohydrates = totalLunchCarbohydrates;
    _totalDinnerCarbohydrates = totalDinnerCarbohydrates;
    _totalSnacksCarbohydrates = totalSnacksCarbohydrates;
  }

  CaloriesBreakdownData.fromJson(dynamic json) {
    _totalConsumedCalories = json['total_consumed_calories'].toString();
    _totalBreakfastCalories = json['total_breakfast_calories'].toString();
    _totalLunchCalories = json['total_lunch_calories'].toString();
    _totalDinnerCalories = json['total_dinner_calories'].toString();
    _totalSnacksCalories = json['total_snacks_calories'].toString();
    _totalBreakfastFats = json['total_breakfast_fats'].toString();
    _totalLunchFats = json['total_lunch_fats'].toString();
    _totalDinnerFats = json['total_dinner_fats'].toString();
    _totalSnacksFats = json['total_snacks_fats'].toString();
    _totalBreakfastProteins = json['total_breakfast_proteins'].toString();
    _totalLunchProteins = json['total_lunch_proteins'].toString();
    _totalDinnerProteins = json['total_dinner_proteins'].toString();
    _totalSnacksProteins = json['total_snacks_proteins'].toString();
    _totalBreakfastCarbohydrates = json['total_breakfast_carbohydrates'].toString();
    _totalLunchCarbohydrates = json['total_lunch_carbohydrates'].toString();
    _totalDinnerCarbohydrates = json['total_dinner_carbohydrates'].toString();
    _totalSnacksCarbohydrates = json['total_snacks_carbohydrates'].toString();
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['total_consumed_calories'] = _totalConsumedCalories;
    map['total_breakfast_calories'] = _totalBreakfastCalories;
    map['total_lunch_calories'] = _totalLunchCalories;
    map['total_dinner_calories'] = _totalDinnerCalories;
    map['total_snacks_calories'] = _totalSnacksCalories;
    map['total_breakfast_fats'] = _totalBreakfastFats;
    map['total_lunch_fats'] = _totalLunchFats;
    map['total_dinner_fats'] = _totalDinnerFats;
    map['total_snacks_fats'] = _totalSnacksFats;
    map['total_breakfast_proteins'] = _totalBreakfastProteins;
    map['total_lunch_proteins'] = _totalLunchProteins;
    map['total_dinner_proteins'] = _totalDinnerProteins;
    map['total_snacks_proteins'] = _totalSnacksProteins;
    map['total_breakfast_carbohydrates'] = _totalBreakfastCarbohydrates;
    map['total_lunch_carbohydrates'] = _totalLunchCarbohydrates;
    map['total_dinner_carbohydrates'] = _totalDinnerCarbohydrates;
    map['total_snacks_carbohydrates'] = _totalSnacksCarbohydrates;
    return map;
  }
}
