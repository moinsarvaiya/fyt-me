import 'package:fytme/common/constants.dart';

class User {
  int userId;
  String username;
  String email;
  String password;
  String confirmPassword;
  String firstName;
  String lastName;
  String city;
  String fcmToken;
  String country;
  String countryCode;
  String zipCode;
  String contactNumber;
  String gender;
  String accountType;
  String profilePicture;
  String dateOfBirth;
  int hourlyRate;
  List<String> specialty;
  String experience;
  String bio;
  String instructorStatus;
  String address;
  String token;
  bool auth;
  bool isVideoTraining;
  bool isClientTravels;
  bool isProfileCompleted;
  bool isQuestionsAnswered;
  String postCode;
  String coordinates;
  List<int> imagePathBytes;
  var imagePath;
  bool isEmailVerified;

  User({
    this.instructorStatus,
    this.isEmailVerified,
    this.bio,
    this.coordinates,
    this.postCode,
    this.token,
    this.userId,
    this.username,
    this.accountType,
    this.password,
    this.email,
    this.confirmPassword,
    this.address,
    this.auth,
    this.city,
    this.fcmToken,
    this.contactNumber,
    this.country,
    this.countryCode,
    this.dateOfBirth,
    this.experience,
    this.firstName,
    this.gender,
    this.hourlyRate,
    this.lastName,
    this.profilePicture,
    this.specialty,
    this.zipCode,
    this.isProfileCompleted,
    this.isQuestionsAnswered,
    this.imagePath,
    this.imagePathBytes,
    this.isVideoTraining,
    this.isClientTravels,
  });

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["email"] = email;
    map["password"] = password;
    map["fcm_token"] = fcmToken;
    return map;
  }

  Map mapOfCreateProfileInstructor() {
    var map = new Map<String, dynamic>();
    if (username != null) {
      map['username'] = username;
    }
    if (userId != null) {
      map['user_id'] = userId;
    }
    if (firstName != null) {
      map['first_name'] = firstName;
    }
    if (lastName != null) {
      map['last_name'] = lastName;
    }
    if (city != null) {
      map['city'] = city;
    }
    if (fcmToken != null) {
      map['fcm_token'] = fcmToken;
    }
    if (postCode != null) {
      map['postcode'] = postCode;
    }
    if (country != null) {
      map['country'] = country;
    }
    if (contactNumber != null) {
      map['contact_number'] = contactNumber;
    }
    if (countryCode != null) {
      map['contact_number'] = countryCode;
    }
    if (gender != null) {
      map['gender'] = gender;
    }
    if (dateOfBirth != null) {
      map['date_of_birth'] = dateOfBirth;
    }

    if (hourlyRate != null) {
      map['hourly_rate'] = hourlyRate;
    }
    if (specialty != null) {
      map['specialty'] = specialty;
    }
    if (experience != null) {
      map['experience'] = experience;
    }
    map['address'] = address;

    if (imagePath != null) {
      map['profile_picture'] = imagePath;
    }
    if (coordinates != null) {
      map['location_coordinates'] = coordinates;
    }
    if (bio != null) {
      map['bio'] = bio;
    }
    if (Storage().currentUser.accountType == 'instructor') {
      map['is_video_training'] = isVideoTraining;
      map['is_travel_to_client'] = isClientTravels;
    }
    return map;
  }

  Map mapOfCreateProfileCustomer() {
    var map = new Map<String, dynamic>();
    if (userId != null) {
      map['user_id'] = userId;
    }
    if (firstName != null) {
      map['first_name'] = firstName;
    }
    if (lastName != null) {
      map['last_name'] = lastName;
    }
    if (city != null) {
      map['city'] = city;
    }
    if (fcmToken != null) {
      map['fcm_token'] = fcmToken;
    }
    if (postCode != null) {
      map['postcode'] = postCode;
    }
    if (country != null) {
      map['country'] = country;
    }
    if (contactNumber != null) {
      map['contact_number'] = contactNumber;
    }
    if (countryCode != null) {
      map['contact_number'] = countryCode;
    }
    if (gender != null) {
      map['gender'] = gender;
    }
    if (isVideoTraining) {
      map['is_video_training'] = isVideoTraining;
    }
    if (isClientTravels) {
      map['is_travel_to_client'] = isClientTravels;
    }

    if (dateOfBirth != null) {
      map['date_of_birth'] = dateOfBirth;
    }

    if (address != null) {
      map['address'] = address;
    }
    if (coordinates != null) {
      map['location_coordinates'] = coordinates;
    }

    if (imagePath != null) {
      map['profile_picture'] = imagePath;
    }
    return map;
  }

  factory User.fromProfile(Map data) {
    print('data[' ']');
    print(data['specialty'].runtimeType);
    List<dynamic> _listDy = data['specialty'];
    List<String> specialty = [];
    if (_listDy != null) {
      if (_listDy.isNotEmpty) {
        _listDy.forEach((element) {
          specialty.add(element);
        });
      }
    }
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    //String authToken =  prefs.getString('authToken');
    String authToken = '';
    if (data['auth_token'] != null) {
      if (data['auth_token']['access_token'] != null) {
        authToken = data['auth_token']['access_token'];
      } else {
        authToken = data['auth_token'];
      }
    }
    return User(
      instructorStatus: data['instructor_status'],
      bio: data['bio'],
      isEmailVerified: data['is_email_verified'],
      token: authToken,
      userId: data['id'] != null ? data['id'] : data['user_id'],
      email: data['email'],
      username: data['username'],
      firstName: data['first_name'],
      lastName: data['last_name'],
      city: data['city'],
      fcmToken: data['fcm_token'],
      postCode: data['postcode'],
      country: data['country'],
      contactNumber: data['contact_number'] == null
          ? ''
          : data['contact_number'] is String
              ? data['contact_number']
              : '${data['contact_number']['number']}',
      //: '${data['contact_number']['country_code']}${data['contact_number']['number']}',
      countryCode: data['contact_number'] == null
          ? ''
          : data['contact_number'] is String
              ? data['contact_number']
              : data['contact_number']['country_code'],
      gender: data['gender'],
      accountType: data['account_type'],
      profilePicture: data['profile_picture'] != null
          ? data['profile_picture'].toString().contains('https://dev.fytme.co.uk')
              ? data['profile_picture']
              : "https://dev.fytme.co.uk${data['profile_picture']}"
          : null,
      dateOfBirth: data['date_of_birth'],
      isVideoTraining: data['is_video_training'],
      isClientTravels: data['is_travel_to_client'],
      hourlyRate: data['hourly_rate'],
      specialty: specialty,
      experience: data['experience'],
      address: data['address'],
      coordinates: data['location_coordinates'] == null
          ? ''
          : '${data['location_coordinates']['coordinates'][0]}, ${data['location_coordinates']['coordinates'][1]}',
      isProfileCompleted: data['is_profile_complete'],
      isQuestionsAnswered: data['is_questions_answered'],
    );
  }
}
