class Calories {
  int _id;
  String _calorieIntakeDate;
  int _calorieIntakeGoal;
  double _totalConsumedCalories = 0.0;
  double _totalConsumedProteins;
  double _totalConsumedFats;
  double _totalConsumedCarbohydrates;
  int _userId;

  int get id => _id;

  String get calorieIntakeDate => _calorieIntakeDate;

  int get calorieIntakeGoal => _calorieIntakeGoal;

  double get totalConsumedCalories => _totalConsumedCalories;

  double get totalConsumedProteins => _totalConsumedProteins;

  double get totalConsumedFats => _totalConsumedFats;

  double get totalConsumedCarbohydrates => _totalConsumedCarbohydrates;

  int get userId => _userId;

  Calories(
      {int id,
      String calorieIntakeDate,
      int calorieIntakeGoal,
      double totalConsumedCalories,
      double totalConsumedProteins,
      double totalConsumedFats,
      double totalConsumedCarbohydrates,
      int userId}) {
    _id = id;
    _calorieIntakeDate = calorieIntakeDate;
    _calorieIntakeGoal = calorieIntakeGoal;
    _totalConsumedCalories = totalConsumedCalories;
    _totalConsumedProteins = totalConsumedProteins;
    _totalConsumedFats = totalConsumedFats;
    _totalConsumedCarbohydrates = totalConsumedCarbohydrates;
    _userId = userId;
  }

  Calories.fromJson(dynamic json) {
    _id = json["id"];
    _calorieIntakeDate = json["calorie_intake_date"];
    _calorieIntakeGoal = json["calorie_intake_goal"];
    _totalConsumedCalories = json["total_consumed_calories"];
    _totalConsumedProteins = json["total_consumed_proteins"];
    _totalConsumedFats = json["total_consumed_fats"];
    _totalConsumedCarbohydrates = json["total_consumed_carbohydrates"];
    _userId = json["user_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["calorie_intake_date"] = _calorieIntakeDate;
    map["calorie_intake_goal"] = _calorieIntakeGoal;
    map["total_consumed_calories"] = _totalConsumedCalories;
    map["total_consumed_proteins"] = _totalConsumedProteins;
    map["total_consumed_fats"] = _totalConsumedFats;
    map["total_consumed_carbohydrates"] = _totalConsumedCarbohydrates;
    map["user_id"] = _userId;
    return map;
  }
}
