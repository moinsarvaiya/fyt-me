class UnreadCount {
  int _count;
  List<int> _latestIds;

  int get count => _count;

  List<int> get latestIds => _latestIds;

  UnreadCount({int count, List<int> latestIds}) {
    _count = count;
    _latestIds = latestIds;
  }

  UnreadCount.fromJson(dynamic json) {
    _count = json["count"];
    _latestIds = json["latest_ids"] != null ? json["latest_ids"].cast<int>() : [];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["count"] = _count;
    map["latest_ids"] = _latestIds;
    return map;
  }
}
