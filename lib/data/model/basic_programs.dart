class BasicPrograms {
  String _programName;
  String _picture;

  String get programName => _programName;

  String get picture => _picture;

  BasicPrograms({String programName, String picture}) {
    _programName = programName;
    _picture = picture;
  }

  BasicPrograms.fromJson(dynamic json) {
    _programName = json["program_name"];
    _picture = json["picture"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["program_name"] = _programName;
    map["picture"] = _picture;
    return map;
  }
}
