class Review {
  int id;
  ReviewByUserInfo reviewByUserInfo;
  ReviewByUserInfo reviewToUserInfo;
  int rating;
  String comments;

  Review({this.id, this.reviewByUserInfo, this.reviewToUserInfo, this.rating, this.comments});

  Review.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reviewByUserInfo = json['review_by_user_info'] != null ? new ReviewByUserInfo.fromJson(json['review_by_user_info']) : null;
    reviewToUserInfo = json['review_to_user_info'] != null ? new ReviewByUserInfo.fromJson(json['review_to_user_info']) : null;
    rating = json['rating'];
    comments = json['comments'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.reviewByUserInfo != null) {
      data['review_by_user_info'] = this.reviewByUserInfo.toJson();
    }
    if (this.reviewToUserInfo != null) {
      data['review_to_user_info'] = this.reviewToUserInfo.toJson();
    }
    data['rating'] = this.rating;
    data['comments'] = this.comments;
    return data;
  }
}

class ReviewByUserInfo {
  int id;
  String username;
  String email;

  ReviewByUserInfo({this.id, this.username, this.email});

  ReviewByUserInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['email'] = this.email;
    return data;
  }
}
