class CalenderData {
  List<String> _dates;
  List<CalenderEvents> _data;

  List<String> get dates => _dates;

  List<CalenderEvents> get data => _data;

  CalenderData({List<String> dates, List<CalenderEvents> data}) {
    _dates = dates;
    _data = data;
  }

  CalenderData.fromJson(dynamic json) {
    _dates = json["dates"] != null ? json["dates"].cast<String>() : [];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(CalenderEvents.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["dates"] = _dates;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class CalenderEvents {
  int _id;
  String _startDate;
  String _status;
  String _startTime;
  String _endTime;
  int _totalHours;
  String _instructorName;
  String _clientName;
  String _modeOfTraining;
  int _totalPrice;

  int get id => _id;

  String get startDate => _startDate;

  String get status => _status;

  String get startTime => _startTime;

  String get endTime => _endTime;

  int get totalHours => _totalHours;

  String get instructorName => _instructorName;

  String get clientName => _clientName;

  String get modeOfTraining => _modeOfTraining;

  int get totalPrice => _totalPrice;

  CalenderEvents(
      {int id,
      String startDate,
      String status,
      String startTime,
      String endTime,
      int totalHours,
      String instructorName,
      String clientName,
      String modeOfTraining,
      int totalPrice}) {
    _id = id;
    _startDate = startDate;
    _status = status;
    _startTime = startTime;
    _endTime = endTime;
    _totalHours = totalHours;
    _instructorName = instructorName;
    _clientName = clientName;
    _modeOfTraining = modeOfTraining;
    _totalPrice = totalPrice;
  }

  CalenderEvents.fromJson(dynamic json) {
    _id = json["id"];
    _startDate = json["start_date"];
    _status = json["status"];
    _startTime = json["start_time"];
    _endTime = json["end_time"];
    _totalHours = json["total_hours"];
    _instructorName = json["instructor_name"];
    _clientName = json["client_name"];
    _modeOfTraining = json["mode_of_training"];
    _totalPrice = json["total_price"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["start_date"] = _startDate;
    map["status"] = _status;
    map["start_time"] = _startTime;
    map["end_time"] = _endTime;
    map["total_hours"] = _totalHours;
    map["instructor_name"] = _instructorName;
    map["client_name"] = _clientName;
    map["mode_of_training"] = _modeOfTraining;
    map["total_price"] = _totalPrice;
    return map;
  }
}
