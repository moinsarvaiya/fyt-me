class DashboardModel {
  DashboardModel({
    String totalEarnings,
    String name,
    String profilePicture,
    int noOfReviews,
    String specialities,
    String ratings,
    List<Data> data,
  }) {
    _totalEarnings = totalEarnings;
    _name = name;
    _profilePicture = profilePicture;
    _noOfReviews = noOfReviews;
    _specialities = specialities;
    _ratings = ratings;
    _data = data;
  }

  DashboardModel.fromJson(dynamic jso) {
    _totalEarnings = jso['total_earnings'].toString();
    _name = jso['name'];
    _profilePicture = jso['profile_picture'];
    _noOfReviews = jso['no_of_reviews'];
    String s1 = jso['specialities'];
    String s2 = s1.replaceAll('[', '');
    String s3 = s2.replaceAll(']', '');
    _specialities = s3.replaceAll("'", '');
    _ratings = jso['ratings'].toString();
    _data = [];
    if (jso['data'] != null) {
      jso['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  String _totalEarnings;
  String _name;
  String _profilePicture;
  int _noOfReviews;
  String _specialities;
  String _ratings;
  List<Data> _data;

  String get totalEarnings => _totalEarnings;

  String get name => _name;

  String get profilePicture => _profilePicture;

  int get noOfReviews => _noOfReviews;

  String get specialities => _specialities;

  String get ratings => _ratings;

  List<Data> get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total_earnings'] = _totalEarnings;
    map['name'] = _name;
    map['profile_picture'] = _profilePicture;
    map['no_of_reviews'] = _noOfReviews;
    map['specialities'] = _specialities;
    map['ratings'] = _ratings;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  Data({
    int userId,
    String modeOfTraining,
    String profilePicture,
    String clientName,
    String paymentDate,
    String paymentTime,
    String transactionType,
    int amount,
  }) {
    _userId = userId;
    _modeOfTraining = modeOfTraining;
    _profilePicture = profilePicture;
    _clientName = clientName;
    _paymentDate = paymentDate;
    _paymentTime = paymentTime;
    _transactionType = transactionType;
    _amount = amount;
  }

  Data.fromJson(dynamic json) {
    _userId = json['user_id'];
    _modeOfTraining = json['mode_of_training'];
    _profilePicture = json['profile_picture'];
    _clientName = json['client_name'];
    _paymentDate = json['payment_date'];
    _paymentTime = json['payment_time'];
    _transactionType = json['transaction_type'];
    _amount = json['amount'];
  }

  int _userId;
  String _modeOfTraining;
  String _profilePicture;
  String _clientName;
  String _paymentDate;
  String _paymentTime;
  String _transactionType;
  int _amount;

  int get userId => _userId;

  String get modeOfTraining => _modeOfTraining;

  String get profilePicture => _profilePicture;

  String get clientName => _clientName;

  String get paymentDate => _paymentDate;

  String get paymentTime => _paymentTime;

  String get transactionType => _transactionType;

  int get amount => _amount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['user_id'] = _userId;
    map['mode_of_training'] = _modeOfTraining;
    map['profile_picture'] = _profilePicture;
    map['client_name'] = _clientName;
    map['payment_date'] = _paymentDate;
    map['payment_time'] = _paymentTime;
    map['amount'] = _amount;
    return map;
  }
}
