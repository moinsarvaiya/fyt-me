import 'package:fytme/expertise.dart';

class InstructorModel {
  int _id;
  String _firstName;
  String _lastName;
  List<String> _specialty;
  String _address;
  LocationCoordinates _locationCoordinates;
  String _profilePicture;
  int _ratings;

  int get id => _id;

  String get firstName => _firstName;

  String get lastName => _lastName;

  List<String> get specialty => _specialty;

  String get address => _address;

  LocationCoordinates get locationCoordinates => _locationCoordinates;

  String get profilePicture => _profilePicture;

  int get ratings => _ratings;

  InstructorModel(
      {int id,
      String firstName,
      String lastName,
      List<String> specialty,
      String address,
      LocationCoordinates locationCoordinates,
      String profilePicture,
      int ratings}) {
    _id = id;
    _firstName = firstName;
    _lastName = lastName;
    _specialty = specialty;
    _address = address;
    _locationCoordinates = locationCoordinates;
    _profilePicture = profilePicture;
    _ratings = ratings;
  }

  InstructorModel.fromJson(dynamic json) {
    List<dynamic> _listDy = json['specialty'];
    List<String> specialty = [];
    if (_listDy != null) {
      if (_listDy.isNotEmpty) {
        _listDy.forEach((element) {
          specialty.add(mapAbleList[element]);
        });
      }
    }

    int finalRate = 0;
    if (json['ratings'] != null) {
      if (json['ratings'] is int) {
        finalRate = json['ratings'];
      } else {
        double rate = json['ratings'];
        finalRate = rate.toInt();
      }
    }

    _id = json['id'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _specialty = specialty;
    _address = json['address'];
    _locationCoordinates = json['location_coordinates'] != null
        ? LocationCoordinates.fromJson(json['location_coordinates'])
        : LocationCoordinates(coordinates: [0.0, 0.0]);
    _profilePicture = json['profile_picture'];
    _ratings = finalRate;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['first_name'] = _firstName;
    map['last_name'] = _lastName;
    map['specialty'] = _specialty;
    map['address'] = _address;
    if (_locationCoordinates != null) {
      map['location_coordinates'] = _locationCoordinates.toJson();
    }
    map['profile_picture'] = _profilePicture;
    map['ratings'] = _ratings;
    return map;
  }
}

class LocationCoordinates {
  List<double> _coordinates;

  List<double> get coordinates => _coordinates;

  LocationCoordinates({List<double> coordinates}) {
    _coordinates = coordinates;
  }

  LocationCoordinates.fromJson(dynamic json) {
    _coordinates = json['coordinates'] != null ? json['coordinates'].cast<double>() : [0.0, 0.0];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['coordinates'] = _coordinates;
    return map;
  }
}
