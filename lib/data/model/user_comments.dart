class UserComments {
  int _totalLikes = 0;
  int _totalComments = 0;
  bool _isPictureLikedByUser = false;
  List<UserList> _userList = [];
  List<Data> _data = [];

  int get totalLikes => _totalLikes;

  int get totalComments => _totalComments;

  bool get isPictureLikedByUser => _isPictureLikedByUser;

  List<UserList> get userList => _userList;

  List<Data> get data => _data;

  UserComments({int totalLikes, int totalComments, List<UserList> userList, List<Data> data}) {
    _totalLikes = totalLikes;
    _totalComments = totalComments;
    _isPictureLikedByUser = isPictureLikedByUser;
    _userList = userList;
    _data = data;
  }

  UserComments.fromJson(dynamic json) {
    _totalLikes = json["total_likes"];
    _totalComments = json["total_comments"];
    _isPictureLikedByUser = json["is_picture_liked_by_user"] != null ? json["is_picture_liked_by_user"] : false;

    _userList = [];
    if (json["user_list"] != null) {
      json["user_list"].forEach((v) {
        _userList.add(UserList.fromJson(v));
      });
    }

    _data = [];
    if (json["data"] != null) {
      json["data"].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["total_likes"] = _totalLikes;
    map["total_comments"] = _totalComments;
    map["is_picture_liked_by_user"] = _isPictureLikedByUser;
    if (_userList != null) {
      map["user_list"] = _userList.map((v) => v.toJson()).toList();
    }
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Data {
  int _id;
  int _userImagesObjId;
  int _imageId;
  String _comment;
  CommentedBy _commentedBy;

  int get id => _id;

  int get userImagesObjId => _userImagesObjId;

  int get imageId => _imageId;

  String get comment => _comment;

  CommentedBy get commentedBy => _commentedBy;

  Data({int id, int userImagesObjId, int imageId, String comment, CommentedBy commentedBy}) {
    _id = id;
    _userImagesObjId = userImagesObjId;
    _imageId = imageId;
    _comment = comment;
    _commentedBy = commentedBy;
  }

  Data.fromJson(dynamic json) {
    _id = json["id"];
    _userImagesObjId = json["user_images_obj_id"];
    _imageId = json["image_id"];
    _comment = json["comment"];
    _commentedBy = json["commented_by"] != null ? CommentedBy.fromJson(json["commented_by"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["user_images_obj_id"] = _userImagesObjId;
    map["image_id"] = _imageId;
    map["comment"] = _comment;
    if (_commentedBy != null) {
      map["commented_by"] = _commentedBy.toJson();
    }
    return map;
  }
}

class CommentedBy {
  String _fullName;
  String _profilePicture;

  String get fullName => _fullName;

  String get profilePicture => _profilePicture;

  CommentedBy({String fullName, String profilePicture}) {
    _fullName = fullName;
    _profilePicture = profilePicture;
  }

  CommentedBy.fromJson(dynamic json) {
    _fullName = json["full_name"];
    _profilePicture = json["profile_picture"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["full_name"] = _fullName;
    map["profile_picture"] = _profilePicture;
    return map;
  }
}

class UserList {
  String _fullName;
  String _profilePicture;

  String get fullName => _fullName;

  String get profilePicture => _profilePicture;

  UserList({String fullName, String profilePicture}) {
    _fullName = fullName;
    _profilePicture = profilePicture;
  }

  UserList.fromJson(dynamic json) {
    _fullName = json["full_name"];
    _profilePicture = json["profile_picture"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["full_name"] = _fullName;
    map["profile_picture"] = _profilePicture;
    return map;
  }
}
