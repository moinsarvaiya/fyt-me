import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum EmailValidationError { empty, invalidFormat, validFormat, remoteError }

class Email extends FormzInput<String, EmailValidationError> {
  final EmailValidationError externalValidationError;

  const Email.pure({
    this.externalValidationError,
  }) : super.pure('');

  const Email.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  EmailValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return EmailValidationError.empty;
    } else if (Validators().emailSubmitValidator.isValid(value ?? '') != true) {
      return EmailValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
