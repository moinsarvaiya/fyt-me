import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum PasswordValidationError { empty, invalidFormat, validFormat, remoteError }

class Password extends FormzInput<String, PasswordValidationError> {
  final PasswordValidationError passwordValidationError;

  const Password.pure({
    this.passwordValidationError,
  }) : super.pure('');

  const Password.dirty({
    this.passwordValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  PasswordValidationError validator(String value) {
    if (passwordValidationError != null) {
      return passwordValidationError;
    } else if (value?.isEmpty == true) {
      return PasswordValidationError.empty;
    } else if (Validators().passwordRegisterSubmitValidator.isValid(value ?? '') != true) {
      return PasswordValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
