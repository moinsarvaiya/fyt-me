import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum UserNameValidationError { empty, invalidFormat, validFormat }

class UserName extends FormzInput<String, UserNameValidationError> {
  final UserNameValidationError externalValidationError;

  const UserName.pure({
    this.externalValidationError,
  }) : super.pure('');

  const UserName.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  UserNameValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return UserNameValidationError.empty;
    } else if (Validators().userNameValidator.isValid(value ?? '') != true) {
      return UserNameValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
