import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum CountryValidationError { empty, invalidFormat, validFormat }

class CountryName extends FormzInput<String, CountryValidationError> {
  final CountryValidationError externalValidationError;

  const CountryName.pure({
    this.externalValidationError,
  }) : super.pure('');

  const CountryName.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  CountryValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return CountryValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return CountryValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
