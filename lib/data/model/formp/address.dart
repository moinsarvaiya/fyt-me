import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum AddressValidationError { empty, invalidFormat, validFormat }

class Address extends FormzInput<String, AddressValidationError> {
  final AddressValidationError externalValidationError;

  const Address.pure({
    this.externalValidationError,
  }) : super.pure('');

  const Address.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  AddressValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return AddressValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return AddressValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
