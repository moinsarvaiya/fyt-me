import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum CityValidationError { empty, invalidFormat, validFormat }

class CityName extends FormzInput<String, CityValidationError> {
  final CityValidationError externalValidationError;

  const CityName.pure({
    this.externalValidationError,
  }) : super.pure('');

  const CityName.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  CityValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return CityValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return CityValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
