import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum LastNameValidationError { empty, invalidFormat, validFormat }

class LastName extends FormzInput<String, LastNameValidationError> {
  final LastNameValidationError externalValidationError;

  const LastName.pure({
    this.externalValidationError,
  }) : super.pure('');

  const LastName.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  LastNameValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return LastNameValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return LastNameValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
