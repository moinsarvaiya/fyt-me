import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum DobValidationError { empty, invalidFormat, validFormat }

class Dob extends FormzInput<String, DobValidationError> {
  final DobValidationError externalValidationError;

  const Dob.pure({
    this.externalValidationError,
  }) : super.pure('');

  const Dob.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  DobValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return DobValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return DobValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
