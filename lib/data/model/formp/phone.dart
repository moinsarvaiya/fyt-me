import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum ContactNumberValidationError { empty, invalidFormat, validFormat }

class PhoneNumber extends FormzInput<String, ContactNumberValidationError> {
  final ContactNumberValidationError externalValidationError;

  const PhoneNumber.pure({
    this.externalValidationError,
  }) : super.pure('');

  const PhoneNumber.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  ContactNumberValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return ContactNumberValidationError.empty;
    } else if (Validators().phoneNumberSubmitValidator.isValid(value ?? '') != true) {
      return ContactNumberValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
