import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum FirstNameValidationError { empty, invalidFormat, validFormat }

class FirstName extends FormzInput<String, FirstNameValidationError> {
  final FirstNameValidationError externalValidationError;

  const FirstName.pure({
    this.externalValidationError,
  }) : super.pure('');

  const FirstName.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  FirstNameValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return FirstNameValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return FirstNameValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
