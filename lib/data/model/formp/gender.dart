import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum GenderValidationError { empty, invalidFormat, validFormat }

class Gender extends FormzInput<String, GenderValidationError> {
  final GenderValidationError externalValidationError;

  const Gender.pure({
    this.externalValidationError,
  }) : super.pure('');

  const Gender.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  GenderValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return GenderValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return GenderValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
