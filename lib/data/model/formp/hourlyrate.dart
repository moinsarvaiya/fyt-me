import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum HoutrlyRateValidationError { empty, invalidFormat, validFormat }

class HourlyRate extends FormzInput<String, HoutrlyRateValidationError> {
  final HoutrlyRateValidationError externalValidationError;

  const HourlyRate.pure({
    this.externalValidationError,
  }) : super.pure('');

  const HourlyRate.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  HoutrlyRateValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return HoutrlyRateValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return HoutrlyRateValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
