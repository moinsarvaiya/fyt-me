import 'package:formz/formz.dart';
import 'package:fytme/common/config/validator.dart';

enum PostCodeValidationError { empty, invalidFormat, validFormat }

class PostCode extends FormzInput<String, PostCodeValidationError> {
  final PostCodeValidationError externalValidationError;

  const PostCode.pure({
    this.externalValidationError,
  }) : super.pure('');

  const PostCode.dirty({
    this.externalValidationError,
    String value = '',
  }) : super.dirty(value);

  @override
  PostCodeValidationError validator(String value) {
    if (externalValidationError != null) {
      return externalValidationError;
    } else if (value?.isEmpty == true) {
      return PostCodeValidationError.empty;
    } else if (Validators().nonEmpty.isValid(value ?? '') != true) {
      return PostCodeValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
