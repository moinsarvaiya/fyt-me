import 'package:fytme/common/constants.dart';

class Faq {
  String fullName;
  String email;
  String phoneNumber;
  String subject;
  String message;

  Faq();

  Map toMap() {
    var map = new Map<String, dynamic>();
    var countryCode = Storage().userProfile.countryCode;
    map['full_name'] = fullName;
    map['email'] = email;
    map['phone_number'] = countryCode + phoneNumber;
    map['subject'] = subject;
    map['message'] = message;
    return map;
  }
}
