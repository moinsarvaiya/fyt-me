import 'package:equatable/equatable.dart';

enum StateStatuses { loading, success, failure, navigateToNext, invalid, sharing, sharingSuccess, sharingError, downloadDon }

class StateStatus extends Equatable {
  final StateStatuses status;
  final String message;
  final double progress;
  final int progressPercent;
  final Map<String, dynamic> errors;

  const StateStatus({
    this.progressPercent = 0,
    this.status = StateStatuses.loading,
    this.progress = 0.0,
    this.message = '',
    this.errors = const <String, dynamic>{},
  });

  StateStatus copyWith({StateStatuses status, String message, Map<String, dynamic> errors, double progress, int progressPercent}) {
    return StateStatus(
        progress: progress ?? this.progress,
        status: status ?? this.status,
        message: message ?? this.message,
        errors: errors ?? this.errors,
        progressPercent: progressPercent ?? this.progressPercent);
  }

  @override
  List<Object> get props => [
        status,
        message,
        errors,
        progress,
        progressPercent,
      ];

  @override
  bool get stringify => true;
}
