import 'package:equatable/equatable.dart';

class UserImages extends Equatable {
  final int id;
  final int memberId;
  final String image1;
  final String image2;
  final String image3;
  final String image4;
  final String image5;
  final String image6;
  final bool isImageEdited;
  final String createdOn;

  const UserImages(
      {this.id = 0,
      this.memberId = 0,
      this.image1,
      this.image2,
      this.image3,
      this.image4,
      this.image5,
      this.image6,
      this.isImageEdited = false,
      this.createdOn});

  UserImages copyWith(
      {int id,
      int memberId,
      String image1,
      String image2,
      String image3,
      String image4,
      String image5,
      String image6,
      bool isImageEdited,
      String createdOn}) {
    return UserImages(
      id: id ?? this.id,
      memberId: memberId ?? this.memberId,
      image1: image1,
      image2: image2,
      image3: image3,
      image4: image4,
      image5: image5,
      image6: image6,
      isImageEdited: isImageEdited ?? this.isImageEdited,
      createdOn: createdOn ?? this.createdOn,
    );
  }

  @override
  List<Object> get props => [id, memberId, image1, image2, image3, image4, image5, image6, isImageEdited, createdOn];

  @override
  bool get stringify => true;
}
