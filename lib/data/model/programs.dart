class Programs {
  int _id;
  List<String> _programName;
  String _picture;
  bool _isActive;
  String _createdAt;
  int _userId;

  int get id => _id;

  List<String> get programName => _programName;

  String get picture => _picture;

  bool get isActive => _isActive;

  String get createdAt => _createdAt;

  int get userId => _userId;

  Programs({int id, List<String> programName, String picture, bool isActive, String createdAt, int userId}) {
    _id = id;
    _programName = programName;
    _picture = picture;
    _isActive = isActive;
    _createdAt = createdAt;
    _userId = userId;
  }

  Programs.fromJson(dynamic json) {
    _id = json["id"];
    _programName = json["program_name"] != null ? json["program_name"].cast<String>() : [];
    _picture = json["picture"];
    _isActive = json["is_active"];
    _createdAt = json["created_at"];
    _userId = json["user_id"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["program_name"] = _programName;
    map["picture"] = _picture;
    map["is_active"] = _isActive;
    map["created_at"] = _createdAt;
    map["user_id"] = _userId;
    return map;
  }
}
