import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

//Custom class in project directory
class CustomWidgets {
  CustomWidgets._();

  static void buildSuccessSnackBar(BuildContext context, String message) {
    if (message.isNotEmpty) {
      Flushbar(
        icon: Icon(
          Icons.check,
          color: Colors.white,
        ),
        message: message,
        messageColor: Colors.white,
        duration: Duration(seconds: 2),
        flushbarPosition: FlushbarPosition.TOP,
        backgroundColor: Colors.green,
      )..show(context);
    }
  }

  static void buildErrorSnackBar(BuildContext context, String message) {
    if (message.isNotEmpty) {
      Flushbar(
        icon: Icon(Icons.error),
        message: message,
        messageColor: Colors.white,
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
        flushbarPosition: FlushbarPosition.TOP,
      )..show(context);
    }
  }
}
