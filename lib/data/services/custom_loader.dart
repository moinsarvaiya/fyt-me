import 'package:flutter/material.dart';

class CustomLoader {
  CustomLoader();

  Future<void> show(BuildContext context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              height: MediaQuery.of(context).size.height * .1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                  ),
                ),
              ),
            ),
          );
        });
  }

  void hide(BuildContext context) {
    Navigator.pop(context);
  }
}
