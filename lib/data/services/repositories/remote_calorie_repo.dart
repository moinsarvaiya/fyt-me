import 'dart:convert';
import 'dart:io';

import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/calories.dart';
import 'package:fytme/data/model/food_calories.dart';
import 'package:fytme/data/repositories/abstracts/calorie_repository.dart';
import 'package:http/http.dart' as http;

import '../../serveraddress.dart';

class RemoteCalorieRepository extends CalorieRepository {
  @override
  Future<List<Calories>> getCalorieData({String token, int userId}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}calorie-data/?user_id=$userId';
      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      print('getCalorieData ==========n${response.statusCode}${response.body}');
      if (response.statusCode == 200) {
        List _list = jsonDecode(response.body);
        List<Calories> _listModel;
        if (_list.isNotEmpty) {
          _listModel = _list.map((e) => Calories.fromJson(e as Map)).toList();
        } else {
          _listModel = [];
        }
        return _listModel;
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<FoodCalories>> getFoodCalorieData({String token, int userId, String calorieDate}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}food-calories/?user_id=$userId&calorie_date=$calorieDate';
      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      print('getFoodData ==========n${response.statusCode}${response.body}');
      if (response.statusCode == 200) {
        List _list = jsonDecode(response.body);
        print(response.body);
        List<FoodCalories> _listModel;
        if (_list.isNotEmpty) {
          _listModel = _list.map((e) => FoodCalories.fromJson(e as Map)).toList();
        } else {
          _listModel = [];
        }
        return _listModel;
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> createFoodCalorie(
      {String token, int userId, int calorieObjId, String typeOfMeal, String consumedFood, String quantityOfFood}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}food-calories/';
      final requestBody = jsonEncode({
        'calorie_obj_id': calorieObjId,
        'type_of_meal': typeOfMeal.toLowerCase(),
        'consumed_food': consumedFood,
        'quantity_of_food': quantityOfFood
      });
      final client = new http.Client();
      final response = await client.post(Uri.parse(url),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $token',
          },
          body: requestBody);
      print('${response.statusCode}${response.body}');
      var reBody = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return reBody['msg'];
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> createCalorie({String token, int userId, String calorieIntakeDate, int calorieGoal, int id}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final client = new http.Client();
      var response;
      if (id != 0) {
        final String url = '${ServerAddress.api_base_url}calorie-data/$id/';
        response = await client.patch(Uri.parse(url),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer $token',
            },
            body: json.encode({
              'calorie_intake_date': calorieIntakeDate,
              'calorie_intake_goal': calorieGoal,
              'user_id': Storage().currentUser.userId,
            }));
        print('${response.statusCode}${response.body}');
        var reBody = jsonDecode(response.body);
        if (response.statusCode == 200) {
          return reBody.toString();
        } else {
          if (reBody.containsKey("calorie_intake_goal")) {
            throw reBody['calorie_intake_goal'].toString();
          } else {
            throw reBody['detail'].toString();
          }
        }
      } else {
        final String url = '${ServerAddress.api_base_url}calorie-data/';
        response = await client.post(Uri.parse(url),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer $token',
            },
            body: json.encode({
              'calorie_intake_date': calorieIntakeDate,
              'calorie_intake_goal': calorieGoal,
              'user_id': Storage().currentUser.userId,
            }));
        print('${response.statusCode}${response.body}');
        var reBody = jsonDecode(response.body);
        if (response.statusCode == 201) {
          return reBody.toString();
        } else {
          if (reBody.containsKey("calorie_intake_goal")) {
            throw reBody['calorie_intake_goal'].toString();
          } else {
            throw reBody['detail'].toString();
          }
        }
      }
    } catch (err) {
      rethrow;
    }
  }
}
