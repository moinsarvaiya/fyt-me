import 'dart:convert';
import 'dart:io';

import 'package:fytme/data/model/review.dart';
import 'package:fytme/data/repositories/abstracts/review_repository.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:http/http.dart' as http;

class RemoteReviewRepository extends ReviewRepository {
  @override
  Future<List<Review>> getInstructorReview({String token, int id}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        print('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-reviews/?user_id=$id';
      final client = new http.Client();
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      print('getReviews ====== ${response.body}');

      if (response.statusCode == 200) {
        List _list = jsonDecode(response.body);
        List<Review> _listModel;
        if (_list.isNotEmpty) {
          _listModel = _list.map((e) => Review.fromJson(e as Map)).toList();
        } else {
          _listModel = [];
        }
        return _listModel;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      print(err);
      rethrow;
    }
  }
}
