import 'dart:convert';
import 'dart:io';

import 'package:fytme/data/model/dashboard_model.dart';
import 'package:fytme/data/model/search_instructor_model.dart';
import 'package:fytme/data/repositories/abstracts/home_repository.dart';
import 'package:http/http.dart' as http;

import '../../../common/constants.dart';
import '../../serveraddress.dart';

class RemoteHomeRepository extends HomeRepository {
  @override
  Future<List<InstructorModel>> searchInstructor({String query}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}instructor-search/?$query';
      final client = new http.Client();
      final response = await client.get(Uri.parse(url));
      printLog('total instructor loaded');
      print('$url\n${response.body}');
      print(response.body);
      if (response.statusCode == 200) {
        List _list = jsonDecode(response.body);
        _list = _list.where((i) => i["location_coordinates"] != null).toList();

        List<InstructorModel> _listModel;
        if (_list.isNotEmpty) {
          _listModel = _list.map((e) => InstructorModel.fromJson(e as Map)).toList();
        } else {
          _listModel = [];
        }
        return _listModel;
      } else {
        throw Exception();
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<DashboardModel> dashboardResult({String query, String token}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}instructor-dashboard/?$query';
      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        },
      );

      print('$url\n${response.body}');
      if (response.statusCode == 200) {
        return DashboardModel.fromJson(jsonDecode(response.body));
      } else {
        throw Exception();
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> withdrawalResult({String token, String amount}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}instructor-dashboard/';
      Map<String, dynamic> data = {'amount': amount};
      final client = new http.Client();
      final response = await client.post(Uri.parse(url),
          headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          },
          body: json.encode(data));

      print('$url\n${response.body}');
      if (response.statusCode == 200) {
        return response.body.toString();
      } else {
        throw Exception(response.body.toString());
      }
    } catch (err) {
      rethrow;
    }
  }
}
