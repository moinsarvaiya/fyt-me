import 'dart:convert';
import 'dart:io';

import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/user.dart';
import 'package:fytme/data/repositories/abstracts/user_repository.dart';
import 'package:http/http.dart' as http;

import '../../serveraddress.dart';

class RemoteUserRepository extends UserRepository {
  @override
  Future<String> changePassword(String token, String oldPassword, String newPassword) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}change-password/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode({
          "new_password": newPassword,
          "old_password": oldPassword,
        }),
      );
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> verificationEmailForResetPassword(int userId, String otp, String newPassword) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}verify-otp-reset-password/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          "id": userId,
          "otp": otp,
          "new_password": newPassword,
        }),
      );

      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> confirmRegistration(String email, String otp) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}confirm-registration/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          "email": email,
          "otp": otp,
        }),
      );
      printLog(' ${response.body}');
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw new Exception(response.body);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<int> forgotPassword(String email) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}forgot-password/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          "email": email,
        }),
      );

      printLog(response.statusCode);
      if (response.statusCode == 200) {
        return jsonDecode(response.body)['user'];
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      printLog('forgot-password/ $err');
      rethrow;
    }
  }

  @override
  Future<String> login({User user}) async {
    printLog(user.toMap());
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}login/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(user.toMap()),
      );
      print('fcm token ${user.toMap().toString()}');
      if (response.statusCode == 200) {
        print('login ${response.body}');
        return response.body;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> register({User user}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}register/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(
          {
            "email": user.email,
            "username": user.username,
            "password": user.password,
            "confirm_password": user.confirmPassword,
            "fcm_token": user.fcmToken,
          },
        ),
      );
      print('${response.body}');
      if (response.statusCode == 200) {
        print('login ${response.body}');
        return response.body;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      printLog('----------------- $err');
      rethrow;
    }
  }

  @override
  Future<String> setAccountType({String accountType, String userId, String token}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-account-type/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode({
          "account_type": accountType,
          "user_id": userId,
        }),
      );
      if (response.statusCode == 201) {
        return response.body;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> resendOtp({int userId}) async {
    printLog(userId);
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}resend-otp';
      printLog(url);
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          "user_id": userId,
        }),
      );
      printLog('resendOtp ${response.statusCode}');
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      printLog('resendOtp $err');
      rethrow;
    }
  }

  @override
  Future<String> socialSignUp({String provider, String accessToken}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}social-signup/';
      printLog(url);
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({"provider": provider, "access_token": accessToken}),
      );
      printLog('socialSignUp ${response.statusCode}');
      if (response.statusCode == 200) {
        print('socialSignUp ${response.body}');
        return response.body;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      printLog('resendOtp $err');
      rethrow;
    }
  }
}
