import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/calender_data.dart';
import 'package:fytme/data/model/programs.dart';
import 'package:fytme/data/model/user.dart';
import 'package:fytme/data/model/user_comments.dart';
import 'package:http/http.dart' as http;

import '../../repositories/abstracts/profile_respository.dart';
import '../../serveraddress.dart';

class RemoteProfileRepository extends ProfileRepository {
  @override
  Future<String> createProfile({User user, String token, String userType}) async {
    final result = await InternetAddress.lookup("google.com");
    if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
      throw Exception;
    }
    var dio = Dio();
    dio.options.contentType = Headers.jsonContentType;
    try {
      Map data = user.mapOfCreateProfileInstructor();
      if (data['profile_picture'] != null) {
        data['profile_picture'] = await MultipartFile.fromFile(
          user.imagePath,
        );
      }
      printLog(data);
      String url;
      if (userType == 'instructor') {
        url = '${ServerAddress.api_base_url}instructor-profile-create/';
      } else {
        url = '${ServerAddress.api_base_url}customer-profile-create/';
      }
      var formData = FormData.fromMap(data);
      printLog(formData.fields);
      var response = await dio.post(
        url,
        data: formData,
        options: Options(
          contentType: Headers.jsonContentType,
          responseType: ResponseType.json,
          validateStatus: (_) => true,
          headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          },
        ),
      );
      printLog(response.statusCode);
      if (response.statusCode == 201) {
        return jsonEncode(response.data);
      } else {
        throw Exception(response.data);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<User> getProfile({String token, int userId}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-profile/$userId/';
      final client = new http.Client();
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('getProfile ====== ${response.body}');

      if (response.statusCode == 200) {
        User user;
        user = User.fromProfile(jsonDecode(response.body));
        return user;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }

  @override
  Future<User> updateProfile({User user, String token, int userId}) async {
    printLog(user.mapOfCreateProfileInstructor());
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-profile/$userId/';
      final client = new http.Client();
      final response = await client.patch(Uri.parse(url),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $token',
          },
          body: jsonEncode(user.mapOfCreateProfileInstructor()));
      if (response.statusCode == 200) {
        User user;
        print(response.body);
        user = User.fromProfile(jsonDecode(response.body));
        return user;
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<User> updateProfileImage({User user, String token, int userId}) async {
    printLog(user.mapOfCreateProfileInstructor());
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      var dio = Dio();
      dio.options.contentType = Headers.jsonContentType;
      Map data = user.mapOfCreateProfileInstructor();
      if (data['profile_picture'] != null) {
        data['profile_picture'] = await MultipartFile.fromFile(
          user.imagePath,
        );
      }
      final String url = '${ServerAddress.api_base_url}user-profile/$userId/';
      var formData = FormData.fromMap(data);
      printLog(formData.fields);
      var response = await dio.patch(url,
          data: formData,
          options: Options(
            contentType: Headers.jsonContentType,
            responseType: ResponseType.json,
            validateStatus: (_) => true,
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            },
          ));
      if (response.statusCode == 200) {
        printLog('Profile update data ===========');
        User user;
        user = User.fromProfile((response.data));
        return user;
      } else {
        throw Exception(response.data);
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> submitInstructorVerificationForm({Map<String, dynamic> data, String token}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      var dio = Dio();
      dio.options.contentType = Headers.jsonContentType;
      final String url = '${ServerAddress.api_base_url}instructor-verification/';
      var formData = FormData.fromMap(data);
      var response = await dio.post(url,
          data: formData,
          options: Options(
            contentType: Headers.jsonContentType,
            responseType: ResponseType.json,
            validateStatus: (_) => true,
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            },
          ));
      printLog('Profile update data =========== Profile update data ===========');
      print(response.statusCode);
      print(response.data.runtimeType);
      if (response.statusCode == 201) {
        printLog('Profile update data ===========');
        if (response.data.runtimeType == String) {
          return response.data;
        } else {
          return jsonEncode(response.data);
        }
      } else {
        if (response.data.runtimeType == String) {
          throw Exception(response.data);
        } else {
          throw Exception(jsonEncode(response.data));
        }
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> submitQuizQuestionForm({Map<String, dynamic> data, String token}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      final String url = '${ServerAddress.api_base_url}capture-customer-responses/';

      print("Data ::::: $data");

      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode(data),
      );

      printLog('Profile update data =========== Profile update data ===========');
      print(response.statusCode);
      if (response.statusCode == 201) {
        return response.body;
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<UserImages> getUserImages({String token, int userId}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      final String url = '${ServerAddress.api_base_url}user-images/?user_id=$userId';

      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      printLog('User Images data =========== User Images data ===========');
      print(response.body);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body).isNotEmpty) {
          final responseBody = jsonDecode(response.body)[0];
          return parseUserImageResponse(responseBody, false);
        } else
          return UserImages();
      } else {
        throw Exception;
      }
    } catch (err) {
      throw Exception;
    }
  }

  @override
  Future<UserImages> uploadUserImage({File imagePath, String token, int userId, String selIndex, String requestType, int id}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      String reqType = id == 0 ? "POST" : "PUT";
      String url = '${ServerAddress.api_base_url}user-images/$id/';
      if (reqType == 'POST') {
        url = '${ServerAddress.api_base_url}user-images/';
      }
      var request = http.MultipartRequest(reqType, Uri.parse(url));
      Map<String, String> headers = {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      };
      request.headers.addAll(headers);
      request.fields['member'] = '$userId';
      if (imagePath != null) {
        request.files.add(await http.MultipartFile.fromPath('image_$selIndex', imagePath.path));
      }
      print('$reqType $id image_$selIndex');

      var response = await request.send();
      final res = await http.Response.fromStream(response);
      print(res.body);
      final responseBody = jsonDecode(res.body);
      print(res.statusCode);
      if (response.statusCode != 200 && responseBody == null) {
        return responseBody['msg'];
      } else {
        if (response.statusCode == 200 || response.statusCode == 201) {
          if (responseBody.isNotEmpty) {
            if (reqType == 'POST') {
              return UserImages(
                  id: 0,
                  memberId: 0,
                  image1: null,
                  image2: null,
                  image3: null,
                  image4: null,
                  image5: null,
                  image6: null,
                  isImageEdited: true,
                  createdOn: null);
            } else {
              return parseUserImageResponse(responseBody, true);
            }
          } else {
            return UserImages();
          }
        } else {
          return UserImages();
        }
      }
    } catch (err) {
      rethrow;
    }
  }

  UserImages parseUserImageResponse(dynamic responseBody, bool isImageEdited) {
    return UserImages(
        id: responseBody["id"],
        memberId: responseBody["member_id"],
        image1: responseBody["image_1"],
        image2: responseBody["image_2"],
        image3: responseBody["image_3"],
        image4: responseBody["image_4"],
        image5: responseBody["image_5"],
        image6: responseBody["image_6"],
        isImageEdited: isImageEdited,
        createdOn: responseBody["created_on"]);
  }

  @override
  Future<UserImages> deleteUserImage({String token, int userId, String selIndex, int id}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      final String url = '${ServerAddress.api_base_url}user-images/$id/';

      var request = http.MultipartRequest('PATCH', Uri.parse(url));
      Map<String, String> headers = {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      };
      request.headers.addAll(headers);
      request.fields['member'] = '$userId';
      request.fields['image_$selIndex'] = '';
      print('$id image_$selIndex');

      var response = await request.send();
      final res = await http.Response.fromStream(response);
      print(res.body);
      final responseBody = jsonDecode(res.body);
      if (response.statusCode != 201 && responseBody == null) {
        return responseBody['msg'];
      } else {
        if (response.statusCode == 200) {
          if (responseBody.isNotEmpty)
            return parseUserImageResponse(responseBody, true);
          else
            return UserImages();
        } else {
          return UserImages();
        }
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<Programs> getPrograms({String token, int userId}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      final String url = '${ServerAddress.api_base_url}programs/?user_id=$userId';

      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      printLog('program data =========== program data ===========');
      print(response.body);
      if (response.statusCode == 200) {
        List<dynamic> _list = jsonDecode(response.body);
        if (_list.isNotEmpty) {
          return Programs.fromJson(jsonDecode(response.body)[0]);
        } else {
          return Programs(id: 0, programName: []);
        }
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<CalenderData> getCalenderData({String token, int userId, String startTime, String endTime}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      final String url = '${ServerAddress.api_base_url}calender-data/?user_id=$userId&start_time=$startTime&end_time=$endTime';

      final client = new http.Client();
      final response = await client.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      printLog('calender data =========== calender data ===========\n$url');
      print(response.body);
      if (response.statusCode == 200) {
        final responseBody = jsonDecode(response.body);
        /*List<CalenderData> _listModel;
        if (responseBody.isNotEmpty) {
          List _list = jsonDecode(response.body);
          if (_list.isNotEmpty) {
            _listModel =
                _list.map((e) => CalenderData.fromJson(e as Map)).toList();
          } else {
            _listModel = [];
          }
        } else {
          _listModel = [];
        }*/
        return CalenderData.fromJson(responseBody);
      } else {
        throw Exception;
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<UserComments> getUserComments({int userId, String token, int imageId}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-comments/?user_id=$userId&image_id=$imageId';
      print(url);
      final client = new http.Client();
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('getUserComments ====== ${response.body}');

      if (response.statusCode == 200) {
        return UserComments.fromJson(jsonDecode(response.body));
      } else {
        throw Exception(response.body);
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }
}
