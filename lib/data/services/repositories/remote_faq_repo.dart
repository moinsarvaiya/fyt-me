import 'dart:convert';

import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/faq.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:http/http.dart' as http;

Future<bool> sendContactUs(Faq faq) async {
  final String url = '${ServerAddress.api_base_url}contact-us/';
  final client = new http.Client();
  var token = Storage().currentUser.token;
  final response = await client.post(
    Uri.parse(url),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    },
    body: json.encode(faq.toMap()),
  );

  print("response : ${response.body}");

  if (response.statusCode == 201) {
    return true;
  }
  return false;
}
