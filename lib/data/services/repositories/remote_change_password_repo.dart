import 'dart:convert';
import 'dart:io';

import 'package:fytme/common/constants.dart';
import 'package:fytme/data/repositories/abstracts/change_password_repository.dart';
import 'package:http/http.dart' as http;

import '../../serveraddress.dart';

class RemoteChangePasswordRepository extends ChangePasswordRepository {
  @override
  Future<String> changePassword({String newPassword, String oldPassword, String token}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}change-password/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode({
          "new_password": newPassword,
          "old_password": oldPassword,
        }),
      );
      printLog(response.statusCode);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw new Exception(response.body);
      }
    } catch (err) {
      rethrow;
    }
  }
}
