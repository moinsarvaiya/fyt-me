import 'dart:convert';
import 'dart:io';

import 'package:fytme/common/constants.dart';
import 'package:fytme/data/repositories/abstracts/proposal_repository.dart';
import 'package:http/http.dart' as http;

import '../../serveraddress.dart';

class RemoteProposalRepository extends ProposalRepository {
  @override
  Future<String> sendProposal(String token, int instructor, String startDate, String startTimestamp, String endTimestamp, List<String> clientFriends,
      String modeOfTraining) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}send-proposal/';
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode({
          "instructor": instructor,
          "start_date": startDate,
          "start_timestamp": startTimestamp,
          "end_timestamp": endTimestamp,
          "client_friends": clientFriends,
          "mode_of_training": modeOfTraining.toLowerCase(),
        }),
        /* body: clientFriends.length > 0
            ? json.encode({
                "instructor": instructor,
                "start_date": startDate,
                "start_timestamp": startTimestamp,
                "end_timestamp": endTimestamp,
                "client_friends": clientFriends,
                "mode_of_training": modeOfTraining.toLowerCase(),
              })
            : json.encode({
                "instructor": instructor,
                "start_date": startDate,
                "start_timestamp": startTimestamp,
                "end_timestamp": endTimestamp,
                "mode_of_training": modeOfTraining.toLowerCase(),
              }),*/
      );
      print(response.body);
      print(response.statusCode);
      var reBody = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return reBody.toString();
      } else {
        throw reBody['error'].toString();
      }
    } catch (err) {
      rethrow;
    }
  }
}
