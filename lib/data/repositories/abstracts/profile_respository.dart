import 'dart:io';

import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/calender_data.dart';
import 'package:fytme/data/model/programs.dart';
import 'package:fytme/data/model/user_comments.dart';

import '../../model/user.dart';

abstract class ProfileRepository {
  Future<String> createProfile({User user, String token, String userType});

  Future<User> getProfile({String token, int userId});

  Future<User> updateProfile({User user, String token, int userId});

  Future<User> updateProfileImage({User user, String token, int userId});

  Future<UserImages> getUserImages({String token, int userId});

  Future<UserImages> uploadUserImage({File imagePath, String token, int userId, String selIndex, String requestType, int id});

  Future<UserImages> deleteUserImage({String token, int userId, String selIndex, int id});

  Future<Programs> getPrograms({String token, int userId});

  Future<CalenderData> getCalenderData({String token, int userId, String startTime, String endTime});

  Future<String> submitInstructorVerificationForm({Map<String, dynamic> data, String token});

  Future<String> submitQuizQuestionForm({Map<String, dynamic> data, String token});

  Future<UserComments> getUserComments({int userId, String token, int imageId});
}
