abstract class ChangePasswordRepository {
  Future<String> changePassword({String newPassword, String oldPassword, String token});
}
