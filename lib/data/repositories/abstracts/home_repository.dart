import 'package:fytme/data/model/dashboard_model.dart';
import 'package:fytme/data/model/search_instructor_model.dart';

abstract class HomeRepository {
  Future<List<InstructorModel>> searchInstructor({String query});

  Future<DashboardModel> dashboardResult({String query, String token});

  Future<String> withdrawalResult({String token, String amount});
}
