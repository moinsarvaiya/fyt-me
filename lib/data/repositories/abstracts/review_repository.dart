import 'package:fytme/data/model/review.dart';

abstract class ReviewRepository {
  Future<List<Review>> getInstructorReview({String token, int id});
}
