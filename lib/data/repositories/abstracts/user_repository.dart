import 'package:fytme/data/model/user.dart';

abstract class UserRepository {
  Future<String> login({User user});

  Future<String> register({User user});

  Future<String> confirmRegistration(String email, String otp);

  Future<int> forgotPassword(String email);

  Future<String> verificationEmailForResetPassword(int userId, String otp, String newPassword);

  Future<String> changePassword(String token, String oldPassword, String newPassword);

  Future<String> setAccountType({String accountType, String userId, String token});

  Future<String> resendOtp({int userId});

  Future<String> socialSignUp({String provider, String accessToken});
}
