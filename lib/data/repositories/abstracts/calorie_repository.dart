import 'package:fytme/data/model/calories.dart';
import 'package:fytme/data/model/food_calories.dart';

abstract class CalorieRepository {
  Future<List<Calories>> getCalorieData({String token, int userId});

  Future<List<FoodCalories>> getFoodCalorieData({String token, int userId, String calorieDate});

  Future<String> createCalorie({String token, int userId, String calorieIntakeDate, int calorieGoal, int id});

  Future<String> createFoodCalorie({String token, int userId, int calorieObjId, String typeOfMeal, String consumedFood, String quantityOfFood});
}
