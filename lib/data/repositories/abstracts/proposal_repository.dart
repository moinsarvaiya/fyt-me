abstract class ProposalRepository {
  Future<String> sendProposal(
      String token, int instructor, String startDate, String startTimestamp, String endTimestamp, List<String> clientFriends, String modeOfTraining);
}
