import 'package:flutter/material.dart';

class ProfileButtonWidget extends StatefulWidget {
  final Widget icon;
  final double size;

  const ProfileButtonWidget({Key key, this.icon, this.size}) : super(key: key);

  @override
  _ProfileButtonWidgetState createState() => _ProfileButtonWidgetState();
}

class _ProfileButtonWidgetState extends State<ProfileButtonWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: widget.size,
      icon: widget.icon,
      onPressed: () {
        Navigator.of(context).pushReplacementNamed('/Pages', arguments: 0);
      },
    );
  }
}
