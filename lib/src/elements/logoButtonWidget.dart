import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';

import '../helpers/app_config.dart' as config;

class LogoButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (Storage().userProfile.accountType != 'instructor') {
          Navigator.of(context).pushNamed('/Home');
        }
      },
      child: Image.asset(
        "assets/img/splash/logo.png",
        height: config.App(context).appHeight(3),
      ),
    );
  }
}
