import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_event.dart';
import 'package:fytme/bloc/authentication/authentication_state.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/home/search_trainers.dart';
import 'package:fytme/src/pages/top_instructors.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../helpers/app_config.dart' as config;

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends StateMVC<DrawerWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: <Widget>[
          Container(
            // height: config.App(context).appHeight(22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: config.App(context).appHeight(5),
                ),
                ListTile(
                  leading: IconButton(
                    iconSize: 40,
                    icon: Image.asset(
                      "assets/img/icons/menu1.png",
                    ),
                    onPressed: () {},
                  ),
                  title: Text(
                    "${Storage().userProfile.firstName ?? ""} ${Storage().userProfile.lastName ?? ""}",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  // trailing: UserImage(radius: 10,),
                ),
                SizedBox(
                  height: config.App(context).appHeight(2),
                ),
                Storage().userProfile.accountType != 'instructor'
                    ? Container(
                        decoration: BoxDecoration(
                          color: config.Colors().secondDarkColor(1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: TextButton(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              "Search Instructors Near Me",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 11,
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchTrainer(
                                          isLoggedIn: true,
                                        )));
                            // Navigator.of(context).pushReplacementNamed('/Pages', arguments: 4);
                          },
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: config.App(context).appHeight(2),
                ),
              ],
            ),
          ),
          ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.white,
              ),
              title: Text(
                Storage().userProfile.accountType == 'instructor' ? 'Wallet' : "Main Page",
                style: Theme.of(context).textTheme.headline4,
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchTrainer(
                              isLoggedIn: true,
                            )));
              }),
          Storage().userProfile.accountType != 'instructor'
              ? ListTile(
                  leading: SvgPicture.asset("assets/img/icons/Instructors Icon.svg"),
                  title: Text(
                    "Top Instructors",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TopInstructorsWidget(parentScaffoldKey: new GlobalKey<ScaffoldState>()),
                      ),
                    );
                  },
                )
              : Container(),
          ExpansionTile(
            leading: Icon(
              Icons.security,
              color: Colors.white,
            ),
            // iconColor: Colors.white,
            // collapsedIconColor: Colors.white,
            title: Text(
              "Account Settings",
              style: Theme.of(context).textTheme.headline4,
            ),
            childrenPadding: EdgeInsets.only(left: config.App(context).appWidth(15)),
            children: [
              ListTile(
                leading: SvgPicture.asset("assets/img/icons/Account.svg"),
                title: Text(
                  "Edit Profile",
                  style: Theme.of(context).textTheme.headline4,
                ),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/EditProfile');
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.security,
                  color: Colors.white,
                ),
                title: Text(
                  "Change Password",
                  style: Theme.of(context).textTheme.headline4,
                ),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/ChangePassword');
                },
              ),

              ListTile(
                leading: Icon(
                  Icons.delete_forever_outlined,
                  color: Colors.white,
                ),
                title: Text(
                  "Delete Account",
                  style: Theme.of(context).textTheme.headline4,
                ),
                onTap: () {
                  showAlertDialog(context);
                },
              ),
              (Storage().currentUser.accountType == 'instructor' && Storage().userProfile.instructorStatus == null)
                  ? ListTile(
                      leading: Icon(
                        Icons.domain_verification_outlined,
                        color: Colors.white,
                      ),
                      title: Text(
                        "Verification",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        if (Storage().userProfile.instructorStatus != null) {
                          CustomWidgets.buildSuccessSnackBar(context, 'Your profile already verified');
                        } else {
                          Navigator.pushNamed(context, '/Verification');
                        }
                      },
                    )
                  : Container(),
            ],
          ),
          ListTile(
              leading: SvgPicture.asset("assets/img/icons/comm-17_chat.svg"),
              title: Text(
                "Messaging",
                style: Theme.of(context).textTheme.headline4,
              ),
              onTap: () {
                Navigator.of(context).pushNamed('/ChatUsers');
              }),
          Storage().userProfile.accountType != 'instructor'
              ? ListTile(
                  leading: SvgPicture.asset("assets/img/icons/muscular_strength.svg"),
                  title: Text(
                    "Programs",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/Programs');
                  },
                )
              : Container(),
          ListTile(
            leading: SvgPicture.asset("assets/img/icons/About Us Icon.svg"),
            title: Text(
              "About Us",
              style: Theme.of(context).textTheme.headline4,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/About');
            },
          ),
          ListTile(
            leading: SvgPicture.asset("assets/img/icons/How It Works Icon.svg"),
            title: Text(
              "How It Works",
              style: Theme.of(context).textTheme.headline4,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/HowItWorks');
            },
          ),
          ListTile(
            leading: SvgPicture.asset("assets/img/icons/aerobic_endurance.svg"),
            title: Text(
              "Calorie Tracker",
              style: Theme.of(context).textTheme.headline4,
            ),
            onTap: () {
              Navigator.pushNamed(context, '/Calorie');
            },
          ),
          BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
              return ListTile(
                leading: SvgPicture.asset("assets/img/icons/Sign Out Icon.svg"),
                title: Text(
                  "Sign Out",
                  style: Theme.of(context).textTheme.headline4,
                ),
                onTap: () {
                  context.read<AuthenticationBloc>().add(LoggedOut());
                  Navigator.of(context).pushNamed('/Login');
                },
              );
            },
          ),
          SizedBox(
            height: config.App(context).appHeight(4),
          ),
          ListTile(
            leading: SvgPicture.asset("assets/img/icons/accept.svg"),
            title: Text(
              "Terms & Conditions",
              style: Theme.of(context).textTheme.headline4,
            ),
            onTap: () {
              Navigator.pushNamed(context, '/TermsAndConditions');
            },
          ),
          ListTile(
            leading: SvgPicture.asset("assets/img/icons/question.svg"),
            title: Text(
              "Help",
              style: Theme.of(context).textTheme.headline4,
            ),
            onTap: () {
              Navigator.pushNamed(context, "/ContactUs");
            },
          ),
        ],
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      child: Text("No"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Yes"),
      onPressed:  () {
        context.read<AuthenticationBloc>().add(DeleteAccount());
        Navigator.of(context).pushNamed('/Login');
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Delete Account?"),
      content: Text("Are you sure want to delete this account?",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
