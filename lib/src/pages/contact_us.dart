import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../bloc/faq_controller.dart';
import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class ContactUsWidget extends StatefulWidget {
  @override
  _ContactUsWidgetState createState() => _ContactUsWidgetState();
}

class _ContactUsWidgetState extends StateMVC<ContactUsWidget> {
  FaqController _con;
  var countryCode = Storage().currentUser.countryCode;

  @override
  Widget build(BuildContext context) {
    _con = FaqController(context);
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _con.scaffoldKey,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
            iconSize: 40,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            "CONTACT US",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        body: Stack(
          children: [
            Positioned(
              top: 0,
              child: Image.asset(
                'assets/img/splash/splash.png',
                fit: BoxFit.cover,
                width: config.App(context).appWidth(100),
              ),
            ),
            Container(
              height: config.App(context).appHeight(100),
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
                child: Form(
                  key: _con.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: config.App(context).appHeight(22),
                      ),
                      Text(
                        "Your Name",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) => _con.faq.fullName = input,
                        validator: (input) => input.isEmpty == true ? "Required this field" : null,
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          suffixIcon: Icon(
                            Icons.person,
                            color: Colors.white,
                          ),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Text(
                        "Mail",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (input) => _con.faq.email = input,
                        validator: (input) => !input.contains('@') ? 'Should be a valid email' : null,
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          suffixIcon: Icon(
                            Icons.mail,
                            color: Colors.white,
                          ),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Text(
                        "Phone",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.phone,
                        onSaved: (input) => _con.faq.phoneNumber = input,
                        validator: (input) => input.isEmpty == true ? "Required this field" : null,
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          suffixIcon: Icon(
                            Icons.phone_iphone,
                            color: Colors.white,
                          ),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Text(
                        "Subject",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) => _con.faq.subject = input,
                        validator: (input) => input.isEmpty == true ? "Required this field" : null,
                        obscureText: false,
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          suffixIcon: Icon(
                            Icons.subject,
                            color: Colors.white,
                          ),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Text(
                        "Message",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) => _con.faq.message = input,
                        validator: (input) => input.isEmpty == true ? "Required this field" : null,
                        maxLines: 8,
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          hintStyle: Theme.of(context).textTheme.headline4,
                          hintText: "write here...",
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(5),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: TextButton(
                          onPressed: () {
                            print("clicking send button");
                            _con.sendContactUs();
                          },
                          child: Container(
                            width: config.App(context).appWidth(40),
                            padding: EdgeInsets.symmetric(
                              vertical: config.App(context).appHeight(2),
                              horizontal: 20,
                            ),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Text(
                              "Send",
                              style: Theme.of(context).textTheme.subtitle1,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(5),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
