import 'package:flutter/material.dart';

import '../helpers/app_config.dart' as config;

class PrivacyAndPolicyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
          iconSize: 40,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "PRIVACY POLICY",
              style: Theme.of(context).textTheme.headline1,
            ),
          ],
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          horizontal: config.App(context).appWidth(10),
          vertical: config.App(context).appHeight(3),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Introduction",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "Welcome to the FYTme Website (fytme.co.uk) (the “Website”) and FYTme application (the App) owned and operated by SPJ Group 77 Lower Camden Street ,Dublin, D02 XE80, Ireland. By using the App and/or the Website, you agree to be bound by terms of use (the Terms) set forth below, together with the Privacy Policy accessible in the App and the Website, which is governed by English law. These Terms and the Privacy Policy affect your legal rights and obligations so please read them carefully. If you do not agree to these Terms and the Privacy Policy, you must immediately log off the Website and App and may immediately discontinue use of the Website and the App. If you have any questions, please email admin@fytme.co.uk We reserve the right to update these Terms from time to time at our discretion. If we do so, and the changes substantially affect your rights or obligations, we shall notify you if we have your email address. Otherwise, you are responsible for regularly reviewing these Terms so that you are aware of any changes to them. For ease, within these Terms, when we refer to a client we mean a person who wishes to use the FYTme services provided within the Website and the App, Reference to Instructors (including you, your) include Fitness Instructors, Yoga Instructors, Personal Trainers, and all guests that use the Website and App. When we refer to a user we mean and instructor or a client using the website and/or the app.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "The Website and App",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "The Website and App here referred as (FYTme platform) allows instructors and clients to enter their weekly availability, "
              "clients to see the courses they’re interested in, and their location preferences in order to be matched. "
              "The FYTme platform enables instructors to verify clients and vice versa;"
              " payments to be made by clients to instructors; and supports schedule management for both instructors and clients."
              " All instructors available on the FYTme platform hold relevant qualifications to the service they provide, have undergone identification verification, have public liability insurance and a first aid award. This include a cardio-pulmonary resuscitation certificate (CPR)."
              " All instructors available on the FYTme platform have one or equivalent of the following:\n "
              "• Level 2 Certificate in Fitness Instructing – Gym, \n"
              "• Level 2 Diploma in Health, Fitness, and Exercise Instruction, \n"
              "• Level 2 Diploma in Instructing Exercise and Fitness,\n "
              "• Level 3 Diploma in Fitness Instructing and Personal Training, \n"
              "• Level 3 Diploma in Personal Training Please note the above list is not exhaustive and is presented as a guideline only, instructors may have different qualifications or equivalent depending on the type of service they provide. The FYTme platform encourages all instructors to be members of a professional organisation, such as the Register of Exercise Professionals (REPs) or National Register of Personal Trainers (NRPT) or Yoga Alliance Professionals (YAP) We encourage all users to communicate solely through the FYTme platform. FYTme reserves the right to remove profiles and content at our sole discretion. We do not provide coaching services and similarly, cannot be held liable for the quality of service a client may/may not receive following the use of our platform. If you are an instructor or client and wish to register on the FYTme platform, you must provide us with certain mandatory information about you. We shall process personal data in accordance with the terms of our Privacy Policy. Once you have registered, you can follow our step by step process to finish creating your account. Please note that whilst we have prepared this step by step process with care, it is up to each instructor to ensure that their terms of business are suitable for their purposes and comply with all applicable laws and regulations.7",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Registration and Use of the App and Website",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "You agree that you are responsible for all activities that occur under your FYTme platform account. You may only register using your own details. All information you provide to us on registration must be accurate and complete and we ask that you keep your information up to date by making any necessary changes within your account. You must keep confidential your password to access the FYTme platform, and if you believe that there has been any unauthorised use of your account, please contact us immediately. When you use the FYTme platform you must comply with all applicable laws. In particular, you shall not assist a third party to (a) try to undermine, damage or disrupt the security of the FYTme platform, associated software, computing systems or networks; (b) act in a way which could risk overloading, impairing or damaging the FYTme platform and supporting infrastructure; (c) attempt to gain unauthorised access to any materials or other parts of our infrastructure; (d) attempt to modify, disassemble, copy or adapt any computer programs used to deliver the FYTme platform (except strictly to the extent that you are permitted to do so under applicable law not capable of exclusion); (e) sell, resell, duplicate, reproduce or create any secondary works from any part of the FYTme platform; (f) make for any purpose including error correction, any modifications, adaptations, additions or enhancements to the FYTme platform; (g)reproduce, redistribute, sell, create derivative works from, decompile ,reverse engineer, or disassemble all or part of the FYTme platform attempt to gain access to the source code; and/or(h) build a product competitive to the FYTme platform otherwise using similar ideas, features, functions of graphics as FYTme platform.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Payment",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "The FYTme platform is free to download/access and use to create an account. In order to request and receive payments on the FYTme platform, an instructor must register with our payment service provider, currently Stripe. Payments to instructors for training sessions are made through a direct debit mandate set up within the FYTme platform. Instructors agree that we charge a 10% (plus VAT) platform and service fee for transactions processed through our platform. Stripe is responsible for receiving payments from clients and making payments to instructors. We have no responsibility for Stripe’s acts or omissions. Instructors are self-employed and do not act as our employees. Instructors are responsible for making appropriate PAYEE deductions for tax and national insurance contributions.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Cancellation Process",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "Training sessions can only be cancelled via the FYTme platform. If a session is cancelled by the client 48 hours before it is due to take place, the client will get a full refund. If the session is cancelled by the client between 24 and 48 hours before it is due to take place, the client will receive 50% of their fee. For cancellations that occur with less than 24 hour’s notice, the client will be charged the full fee. We have this policy in place to ensure that instructors have sufficient time to reschedule cancelled sessions. If at any moment leading up to the Training session the instructor cancels, the client will receive a full refund.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "FYTme platform Content",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "The copyright in all material contained in the FYTme platform including all information, data, text, images, and all source code and other software is owned by or licensed to us— all rights are reserved. The FYTme platform may contain links to websites operated by third parties. We do not have any influence or control over any such third-party websites and we are not responsible for and do not endorse any third party websites or their availability or content. An instructor may use the FYTme platform to add feedback and notes in respect of a client and their personal experience. This content can be viewed by the client and instructor in question and no other user of the FYTme platform. However, you agree that you shall not (a) include any content that is deliberately dishonest or false; (b) is offensive, hateful or inflammatory; and/or (c) include any copyrights, database rights, trade marks or other intellectual property rights that do not belong to you, unless you have the written consent of the owner of such rights.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Review Policy",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "After every training session that takes place, we encourage instructors and clients to rate each other on a five-star scale and provide feedback on how they found the experience. The feedback provides valuable information for our community to make informed bookings, so we expect our users to provide honest, unbiased and relevant comments. Any comments deemed to be inflammatory, abusive, or focuses on anything outside the scope of your experience of the training session, will not be allowed. If we receive a report on a review that violates this policy, it may be removed, and the responsible account could be suspended or permanently deactivated. If a user’s star rating is lower than the overall average or they receive too many negative reviews, FYTme reserves the right to investigate, and to deactivate accounts where they don’t comply with our ambition to deliver high quality experiences, in a safe and respectful environment.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Liability and Disclaimer",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "We warrant that the FYTme platform will be of satisfactory quality, fit for purpose and as described. But, and subject to these warranties, to the maximum extent permitted by law we disclaim all liability whatsoever, whether arising in contract, tort (incl. negligence) under statute or otherwise in relation to the FYTme platform. You agree that the FYTme platform has not been created specifically for you. If you notice any errors within the FYTme platform, for example in respect of any training schedule, or payments made or due, please notify us promptly and we shall investigate the error as soon as possible. To the extent permitted by law, this is your only remedy in respect of any error. Any content within the FAQs, blogs or otherwise within the FYTme platform, including your account, is for information only. It has not been prepared for any particular individual and should not be interpreted as legal or tax advice that you can rely on. If you are a client, we shall only be liable to you for any direct damages that you can evidence were caused by our demonstrable failure to comply with these Terms. We will not be liable for damage which you could have avoided by following our advice to download and install an update offered to you for free or for damage caused by you failing to correctly follow instructions about use or anything in the documentation on the FYTme platform. If you are an instructor, then you agree that we shall not be liable to you for (a) loss of profits; (b) loss of business; (c) loss or corruption of data or information; (d) business interruption; (e) loss of or wasted staff or management time; (f) any kind of special, indirect, consequential loss or pure economic loss whether or not you advise us of the possibility of these losses. Nothing in these Terms shall be construed as excluding or limiting our liability for death or personal injury caused by its negligence, for fraud or fraudulent misrepresentation or for any other liability which cannot be excluded by English law. In addition, your statutory rights as a consumer are not affected.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Availability",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "There may be occasions when access to the FYTme platform may be interrupted, including for scheduled maintenance or upgrades, for emergency repairs, or due to failure of telecommunications links and/or equipment. We reserve the right to remove any content or features from the FYTme platform for any reason without prior notice and/or to suspend or cease providing any services relating to the FYTme platform without notice, and shall have no liability or responsibility to you in such circumstances.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Termination",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "We may terminate or suspend your access to the FYTme platform for any reason, including if (a) you have breached any provision of these Terms; (b) we cannot verify or authenticate any information you provide to us; (c) we receive a complaint about you from another user; and/or (d) we have good reason to believe that an instructor has provided/intends to provide training sessions booked using the Platform, outside the FYTme platform paying system. FYTme is not responsible for the behaviour a user may exhibit on the platform and outside the platform. We take complaints seriously and will do our best to investigate each matter and try to resolve it.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "General",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "These Terms and the Privacy Policy (as amended from time to time) constitute the entire agreement between you and us concerning your use of the FYTme platform. If any provision of these Terms is held by a court of competent jurisdiction to be invalid or unenforceable, then such provision shall be construed, as nearly as possible, to reflect the intentions of the parties and all other provisions shall remain in full force. Our failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision. You consent to receive all communications including notices and other information from us electronically. We may provide all such communications by email, text or by posting them on the Website or App. These Terms shall be governed by and construed in accordance with English law and you agree to submit to the exclusive jurisdiction of the English Courts.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.check_circle_outline_rounded,
                  color: Color(0xFF2CC3D0),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    "Additional Terms for Users who download the App from the Apple iTunes App Store",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: config.App(context).appHeight(2),
            ),
            Text(
              "We both acknowledge that these Terms are concluded between you and us only, and not with Apple, and therefore, we, not Apple, are solely responsible for the App and the content thereof. The licence granted to you for the App is limited to a non- transferable licence to use the App on an iPhone, iPad or iPod touch that you own or control and as permitted by the Usage Rules set forth in the App Store Terms and Conditions. We, not Apple, are solely responsible for providing any maintenance and support services with respect to the App, as specified in the Terms. We both acknowledge that Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the App. We are solely responsible for any product warranties, whether express or implied by law, to the extent not effectively disclaimed in these Terms. In the event of any failure of the App to conform to any applicable warranty, you may notify Apple, and Apple will refund the purchase price (if any) for the App to you. To the maximum extent permitted by law, Apple will have no other warranty obligation whatsoever with respect to the App, and any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be our sole responsibility. We both acknowledge that we, not Apple, are responsible for addressing any claims by you or any third-party relating to the App or your use or possession of the App, including, but not limited to (a) product liability claims; (b) any claim that the App fails to conform to any applicable legal or regulatory requirement; and © claims arising under consumer protection or similar legislation. We both acknowledge that, in the event of any third- party claim that the App or your possession and use of the App infringes that third-party’s intellectual property rights, we, not Apple, will be solely responsible for the investigation, defence, settlement and discharge of any such intellectual property infringement claim.",
              style: Theme.of(context).textTheme.headline4.copyWith(height: 1.5),
              textAlign: TextAlign.justify,
            ),
            SizedBox(
              height: config.App(context).appHeight(15),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: Text(
                      "Copyrights FYTme© 2021.\nAll rights reserved.",
                      style: Theme.of(context).textTheme.headline4.copyWith(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          ),
                    ),
                  ),
                  Image.asset(
                    "assets/img/splash/logo.png",
                    height: config.App(context).appHeight(3),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
