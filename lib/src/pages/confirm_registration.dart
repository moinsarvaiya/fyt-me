import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_event.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:pinput/pinput.dart';

import '../../bloc/authentication/authentication_bloc.dart';
import '../../common/constants.dart';
import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class ConfirmRegistration extends StatefulWidget {
  final String email;

  const ConfirmRegistration({Key key, this.email}) : super(key: key);

  @override
  _ConfirmRegistrationState createState() => _ConfirmRegistrationState();
}

class _ConfirmRegistrationState extends StateMVC<ConfirmRegistration> with WidgetsBindingObserver {
  bool _clear = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print('total states ${state.toString()}');
    if (state == AppLifecycleState.detached) {
      context.read<AuthenticationBloc>().add(LoggedOut());
    }
  }

  GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: TextStyle(fontSize: 20, color: Color.fromRGBO(30, 60, 87, 1), fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: Color.fromRGBO(234, 239, 243, 1)),
        borderRadius: BorderRadius.circular(20),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: Color.fromRGBO(114, 178, 238, 1)),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration.copyWith(
        color: Color.fromRGBO(234, 239, 243, 1),
      ),
    );
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        //key: _con.scaffoldKey,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
            iconSize: 40,
            onPressed: () {
              context.read<AuthenticationBloc>().add(LoggedOut());
            },
          ),
          title: Image.asset(
            "assets/img/splash/logo.png",
            height: config.App(context).appHeight(3),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF2E94D0),
                Color(0xFF2B7DC9),
                Color(0xFF0B4B98),
                Color(0xFF09111D),
              ],
            ),
          ),
          child: Center(
            child: BlocListener<AuthBloc, AuthState>(
              listenWhen: (p, c) => p.status != c.status,
              listener: (BuildContext context, state) {
                switch (state.status.status) {
                  case StateStatuses.loading:
                    CustomLoader().show(context);
                    break;
                  case StateStatuses.failure:
                    CustomLoader().hide(context);
                    CustomWidgets.buildErrorSnackBar(context, state.status.message);
                    break;
                  case StateStatuses.success:
                    CustomLoader().hide(context);
                    CustomWidgets.buildSuccessSnackBar(context, state.status.message);
                    break;
                  default:
                    CustomLoader().hide(context);
                    break;
                }
              },
              child: Form(
                key: loginFormKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Please type the verification code sent to \n ${Storage().currentUser.email}",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, state) {
                        return Pinput(
                          defaultPinTheme: defaultPinTheme,
                          focusedPinTheme: focusedPinTheme,
                          length: 5,
                          submittedPinTheme: submittedPinTheme,
                          onCompleted: (pin) {
                            if (loginFormKey.currentState.validate()) {
                              loginFormKey.currentState.save();
                              context.read<AuthBloc>().add(ConfirmedEmail(email: Storage().currentUser.email, pinCode: pin));
                            }
                            // _con.confirmRegistration(widget.email, pin);
                          },
                          // actionButtonsEnabled: false,
                          // keyboardAction: TextInputAction.done,
                          // fieldsCount: 5,
                          // clearInput: _clear,
                          // inputDecoration: new InputDecoration(
                          //     focusedBorder: OutlineInputBorder(
                          //       borderSide: BorderSide(
                          //         color: Colors.white,
                          //         width: 2.0,
                          //       ),
                          //     ),
                          //     enabledBorder: OutlineInputBorder(
                          //       borderSide: BorderSide(
                          //           color: Theme.of(context).primaryColor,
                          //           width: 2.0),
                          //     ),
                          //     counterText: ""),
                          // onSubmit: (String pin) {
                          //   if (loginFormKey.currentState.validate()) {
                          //     loginFormKey.currentState.save();
                          //     context.read<AuthBloc>().add(ConfirmedEmail(
                          //         email: Storage().currentUser.email,
                          //         pinCode: pin));
                          //   }
                          //   // _con.confirmRegistration(widget.email, pin);
                          // },
                        );
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(2),
                    ),
                    ResendOtp(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ResendOtp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        return TextButton(
          child: Text(
            'Resend OTP',
            textAlign: TextAlign.center,
          ),
          onPressed: () {
            context.read<AuthBloc>().add(ResendOtpEvent());
          },
        );
      },
    );
  }
}
