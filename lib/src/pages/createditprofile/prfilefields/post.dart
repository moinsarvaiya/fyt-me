import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class Post extends StatefulWidget {
  @override
  _PostState createState() => _PostState();
}

class _PostState extends State<Post> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.postCode.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      PostCodeEvent(postCode: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return InpuString(
          width: MediaQuery.of(context).size.width,
          controller: _controller,
          error: state.postCode.isNotValid ? 'required*' : null,
        );
      },
    );
  }
}
