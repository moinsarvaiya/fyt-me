import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class FirstName extends StatefulWidget {
  @override
  _FirstNameState createState() => _FirstNameState();
}

class _FirstNameState extends State<FirstName> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    printLog('==== ${_authBloc.state.firstName.value}');
    _controller = TextEditingController(
      text: _authBloc.state.firstName.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      FirstNameEvent(firstName: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      buildWhen: (previous, current) => previous.firstName != current.firstName,
      builder: (context, state) {
        return InpuString(
          width: config.App(context).appWidth(40),
          onEditingComplete: FocusScope.of(context).nextFocus,
          hint: 'First name',
          textInputType: TextInputType.text,
          controller: _controller,
          error: state.firstName.isNotValid ? 'required*' : null,
        );
      },
    );
  }
}
