import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class City extends StatefulWidget {
  @override
  _CityState createState() => _CityState();
}

class _CityState extends State<City> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.cityName.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      CityEvent(city: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      buildWhen: (p, c) => p.cityName != c.cityName,
      builder: (context, state) {
        return InpuString(
          width: MediaQuery.of(context).size.width,
          controller: _controller,
          error: state.cityName.isNotValid ? 'required*' : null,
        );
      },
    );
  }
}
