import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class LastName extends StatefulWidget {
  @override
  _LastNameState createState() => _LastNameState();
}

class _LastNameState extends State<LastName> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.lastName.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      LastNameEvent(lName: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return InpuString(
          width: config.App(context).appWidth(40),
          hint: 'Last name',
          controller: _controller,
          error: state.lastName.isNotValid ? 'required*' : null,
        );
      },
    );
  }
}
