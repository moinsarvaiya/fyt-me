import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';

import 'input.dart';

class BioInput extends StatefulWidget {
  @override
  _BioInputState createState() => _BioInputState();
}

class _BioInputState extends State<BioInput> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.bio,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      BioEvent(bioEvent: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return InpuString(
          hint: 'enter your bio',
          width: MediaQuery.of(context).size.width,
          controller: _controller,
        );
      },
    );
  }
}
