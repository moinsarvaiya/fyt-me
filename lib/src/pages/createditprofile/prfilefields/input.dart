import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../helpers/app_config.dart' as config;

class InpuString extends StatelessWidget {
  final double width;
  final String hint;
  final FormFieldValidator<String> validators;
  final FormFieldSetter<String> onsaved;
  final VoidCallback onEditingComplete;
  final TextInputType textInputType;
  final TextEditingController controller;
  final String error;
  final List<TextInputFormatter> inputFormatters;

  InpuString(
      {this.inputFormatters,
      this.error,
      this.controller,
      this.hint,
      this.onsaved,
      this.validators,
      this.onEditingComplete,
      this.textInputType,
      this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? config.App(context).appWidth(35),
      child: TextFormField(
        inputFormatters: inputFormatters,
        controller: controller,
        onEditingComplete: onEditingComplete,
        textInputAction: TextInputAction.next,
        validator: validators,
        keyboardType: textInputType,
        onSaved: onsaved,
        cursorColor: Colors.white,
        style: Theme.of(context).textTheme.headline4,
        decoration: InputDecoration(
          errorText: error,
          hintText: hint,
          hintStyle: Theme.of(context).textTheme.headline4,
          filled: true,
          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
          contentPadding: EdgeInsets.symmetric(
            horizontal: 25,
            vertical: config.App(context).appHeight(1),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide.none,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide.none,
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }
}
