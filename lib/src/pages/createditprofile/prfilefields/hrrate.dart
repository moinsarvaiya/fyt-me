import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class HrRate extends StatefulWidget {
  @override
  _HrRateState createState() => _HrRateState();
}

class _HrRateState extends State<HrRate> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.hourlyRate.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      HrRateEvent(hrRate: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return InpuString(
          width: MediaQuery.of(context).size.width,
          inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
          textInputType: TextInputType.number,
          controller: _controller,
          error: state.hourlyRate.isNotValid ? 'required*' : null,
        );
      },
    );
  }
}
