import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class Phone extends StatefulWidget {
  @override
  _PhoneState createState() => _PhoneState();
}

class _PhoneState extends State<Phone> {
  TextEditingController _controller;

  ProfileBloc _authBloc;
  String countryCode = '';

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    String countryCode = '+44';
    if (Storage().userProfile != null) {
      countryCode = Storage().userProfile.countryCode ?? countryCode;
    }
    _authBloc?.add(
      CountryCodeEvent(countryCode: countryCode),
    );
    _controller = TextEditingController(
      text: _authBloc.state.phoneNumber.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      ContactNumberEvent(contactNumber: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                width: config.App(context).appWidth(16),
                decoration: BoxDecoration(
                  color: Color(0xFFF0F0F0).withOpacity(0.51),
                  borderRadius: BorderRadius.circular(15),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: config.App(context).appHeight(2.25),
                ),
                child: Text(
                  '${state.countryCode}',
                  style: Theme.of(context).textTheme.headline4,
                )),
            SizedBox(
              width: config.App(context).appWidth(3),
            ),
            InpuString(
              hint: '00000000',
              width: config.App(context).appWidth(61),
              controller: _controller,
              error: /*state.phoneNumber.value.isEmpty
                  ? ''
                  :*/
                  state.phoneNumber.isNotValid ? 'Enter a valid phone number' : null,
            )
          ],
        );
      },
    );
  }
}
