import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class Gender extends StatefulWidget {
  @override
  _GenderState createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                        // vertical: config.App(context).appHeight(1),
                        ),
                    decoration: BoxDecoration(
                      color: BlocProvider.of<ProfileBloc>(context).state.gender.value == 'female'
                          ? Theme.of(context).primaryColor
                          : Color(0xFFF0F0F0).withOpacity(0.51),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: TextButton(
                      onPressed: () {
                        BlocProvider.of<ProfileBloc>(context).add(GenderEvent(gender: 'female'));
                      },
                      child: Text(
                        "Female",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                        // vertical: config.App(context).appHeight(1),
                        ),
                    decoration: BoxDecoration(
                      color: BlocProvider.of<ProfileBloc>(context).state.gender.value == 'male'
                          ? Theme.of(context).primaryColor
                          : Color(0xFFF0F0F0).withOpacity(0.51),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: TextButton(
                      onPressed: () {
                        BlocProvider.of<ProfileBloc>(context).add(GenderEvent(gender: 'male'));
                      },
                      child: Text(
                        "Male",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            SizedBox(
              height: config.App(context).appHeight(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Checkbox(
                        value: BlocProvider.of<ProfileBloc>(context).state.gender.value == 'other' ? true : false,
                        onChanged: (value) {
                          if (value) {
                            BlocProvider.of<ProfileBloc>(context).add(GenderEvent(gender: 'other'));
                          } else {
                            BlocProvider.of<ProfileBloc>(context).add(GenderEvent(gender: ''));
                          }
                        },
                        activeColor: Theme.of(context).primaryColor,
                      ),
                      Text(
                        "Prefer not to say",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
