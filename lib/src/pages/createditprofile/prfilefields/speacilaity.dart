import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/expertise.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class Speciality extends StatefulWidget {
  @override
  _SpecialityState createState() => _SpecialityState();
}

class _SpecialityState extends State<Speciality> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /*List<Map<String, String>> _listNew = [
    {'key': 'athletics', 'val': 'Athletics'},
    {'key': 'bodybuilding', 'val': 'Bodybuilding'},
    {'key': 'boxing', 'val': 'Boxing'},
    {'key': 'brazilian_jiujitsu', 'val': 'Brazilian Jiujitsu'},
    {'key': 'cycling', 'val': 'Cycling'},
    {'key': 'dance', 'val': 'Dance'},
    {'key': 'hiit', 'val': 'HIIT'},
    {'key': 'judo', 'val': 'Judo'},
    {'key': 'martial_arts', 'val': 'Karate'},
    {'key': 'mma', 'val': 'MMA'},
    {'key': 'nutritionist', 'val': 'Nutritionist'},
    {'key': 'pilates', 'val': 'Pilates'},
    {'key': 'sports_massage', 'val': 'Sports massage'},
    {'key': 'tabata', 'val': 'Tabata'},
    {'key': 'taekwondo', 'val': 'Taekwondo'},
    {'key': 'weightlifting', 'val': 'Weightlifting'},
    {'key': 'yoga', 'val': 'Yoga'},
    {'key': 'other', 'val': 'Other'},
  ];*/

  List<String> _list = [
    'select_experties',
    'athletics',
    'bodybuilding',
    'boxing',
    'brazilian_jiujitsu',
    'cycling',
    'dance',
    'hiit',
    'judo',
    'karate',
    'martial_arts',
    'mma',
    'nutritionist',
    'pilates',
    'sports_massage',
    'tabata',
    'taekwondo',
    'weightlifting',
    'yoga',
    'other',
  ];

  // List<String> _list = [
  //   'Select experties',
  //   'Athletics',
  //   'Bodybuilding',
  //   'Boxing',
  //   'Brazilian Jiujitsu',
  //   'Cycling',
  //   'Dance',
  //   'HIIT',
  //   'Judo',
  //   'Karate',
  //   'Martial Arts',
  //   'MMA',
  //   'Nutritionist',
  //   'Pilates',
  //   'Sports massage',
  //   'Tabata',
  //   'Taekwondo',
  //   'Weightlifting',
  //   'Yoga',
  //   'Other'
  // ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Column(
          children: [
            Container(
              width: double.infinity,
              height: 45,
              decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  value: _list[0],
                  onChanged: (v) {
                    if (v != _list[0]) {
                      print('============');
                      print(v);
                      print('============');
                      context.read<ProfileBloc>().add(SpecialityEvent(speciality: v));
                    }
                  },
                  items: _list
                      .map((e) => DropdownMenuItem<String>(
                            value: e,
                            child: new Text(mapAbleList[e]),
                          ))
                      .toList(),
                ),
              ),
            ),
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.start,
              children: List.generate(
                  state.speciality?.length ?? 0,
                  (index) => Padding(
                        padding: const EdgeInsets.all(4),
                        child: Chip(
                          backgroundColor: Colors.white,
                          label: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              mapAbleList[state.speciality[index]],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          deleteIcon: Icon(Icons.clear),
                          onDeleted: () {
                            context.read<ProfileBloc>().add(SpecialityEventRe(speciality: state.speciality[index]));
                          },
                        ),
                      )),
            ),
          ],
        );
      },
    );
  }
}
