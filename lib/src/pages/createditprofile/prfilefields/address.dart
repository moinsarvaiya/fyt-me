import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/pages/map/new_map.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';
import '../../../../data/serveraddress.dart';

class MapAddress extends StatefulWidget {
  @override
  _MapAddressState createState() => _MapAddressState();
}

class _MapAddressState extends State<MapAddress> {
  Map address;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(builder: (context, snapshot) {
      return MaterialButton(
        padding: EdgeInsets.zero,
        onPressed: () async {
          Map dataMap = await Navigator.push(context, MaterialPageRoute(builder: (context) => PlacePicker(ServerAddress.googleApiKey)));
          if (dataMap != null) {
            setState(() {
              address = dataMap;
            });
            BlocProvider.of<ProfileBloc>(context).add(AddressEvent(address: address['address']));
            BlocProvider.of<ProfileBloc>(context).add(CoordinateEvent(coordinates: '${address['lat']}, ${address['lng']}'));
          }
          // user.address = address['address'];
        },
        child: Container(
          width: double.infinity,
          height: 45,
          decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                BlocProvider.of<ProfileBloc>(context).state.address.isNotValid != null
                    ? BlocProvider.of<ProfileBloc>(context).state.address.value
                    : 'Your address',
                maxLines: 2,
                overflow: TextOverflow.clip,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
      );
    });
  }
}
