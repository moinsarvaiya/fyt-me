import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:intl/intl.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class DateOfBirth extends StatefulWidget {
  final Function callBack;

  DateOfBirth({this.callBack});

  @override
  _DateOfBirthState createState() => _DateOfBirthState();
}

class _DateOfBirthState extends State<DateOfBirth> {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = (await showDatePicker(
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light().copyWith(primary: config.Colors().mainDarkColor(1)),
            ),
            child: child,
          );
        },
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1950, 8),
        lastDate: DateTime(2101)));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
      // user.dateOfBirth = selectedDate.toString().substring(0, 10);
      BlocProvider.of<ProfileBloc>(context).add(DobEvent(dob: selectedDate.toString().substring(0, 10)));
    }
  }

  final df = new DateFormat('dd/MM/yyyy');

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.zero,
      onPressed: () => _selectDate(context),
      child: Container(
        width: double.infinity,
        height: 45,
        decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            '  ' +
                (BlocProvider.of<ProfileBloc>(context).state.dob.value.isNotEmpty
                    ? df.format(DateTime.parse(BlocProvider.of<ProfileBloc>(context).state.dob.value))
                    : ''),
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
