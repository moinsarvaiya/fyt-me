import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';

import '../upload_photo.dart';

class CallProfileButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        if (Storage().currentUser.accountType == 'instructor')
          return CustomButton(
            string: 'Continue',
            callback: (state.speciality.isEmpty ||
                    state.exp == null ||
                    state.gender.value.isEmpty ||
                    state.address.value.isEmpty ||
                    state.firstName.value.isEmpty ||
                    state.lastName.value.isEmpty ||
                    state.phoneNumber.value.isEmpty ||
                    state.phoneNumber.isNotValid ||
                    state.countryName.value.isEmpty ||
                    state.cityName.value.isEmpty ||
                    state.dob.value.isEmpty ||
                    state.postCode.value.isEmpty ||
                    state.phoneNumber.isNotValid ||
                    state.phoneNumber.value.isEmpty ||
                    state.hourlyRate.value.isEmpty)
                ? null
                : () async {
                    var image = await Navigator.push(context, MaterialPageRoute(builder: (context) => UploadImage()));
                    if (image != null) {
                      if (image['step']) {
                        BlocProvider.of<ProfileBloc>(context).add(PhotoEvent(image: image['image']));
                      }
                      BlocProvider.of<ProfileBloc>(context).add(ProfileCreateEvent());
                    }
                  },
          );
        return CustomButton(
          string: 'Continue',
          callback: (state.gender.value.isEmpty ||
                  state.address.value.isEmpty ||
                  state.firstName.value.isEmpty ||
                  state.lastName.value.isEmpty ||
                  state.phoneNumber.value.isEmpty ||
                  state.countryName.value.isEmpty ||
                  state.cityName.value.isEmpty ||
                  state.dob.value.isEmpty ||
                  state.postCode.value.isEmpty ||
                  state.phoneNumber.value.isEmpty ||
                  state.phoneNumber.isNotValid)
              ? null
              : () async {
                  var image = await Navigator.push(context, MaterialPageRoute(builder: (context) => UploadImage()));
                  if (image != null) {
                    if (image['step']) {
                      BlocProvider.of<ProfileBloc>(context).add(PhotoEvent(image: image['image']));
                    }
                    BlocProvider.of<ProfileBloc>(context).add(ProfileCreateEvent());
                  }
                },
        );
      },
    );
  }
}

class CallEditProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      // buildWhen: (p, c) {
      //   if (
      //     p.userName.value != c.userName.value ||
      //     p.cityName.value != c.cityName.value ||
      //     p.address.value != c.address.value ||
      //       p.firstName.value != c.firstName.value ||
      //       p.lastName.value != c.lastName.value ||
      //       p.phoneNumber.value != c.phoneNumber.value ||
      //       p.countryName.value != p.countryName.value ||
      //       p.dob.value != c.dob.value ||
      //       p.postCode.value != c.postCode.value ||
      //       p.hourlyRate.value != c.hourlyRate.value ||
      //       p.gender.value != c.gender.value ||
      //       p.countryName.value != c.countryName.value ||
      //       p.speciality != c.speciality ||
      //       p.exp != c.exp) {
      //          print('previous');
      //         print(c.userName.value);
      //     context.read<ProfileBloc>().add(ProfileEditing(value: true));
      //     return true;
      //   } else {
      //     print('previous');
      //     print(p.userName.value);
      //     context.read<ProfileBloc>().add(ProfileEditing(value: false));
      //     return false;
      //   }
      // },
      builder: (context, state) {
        if (Storage().currentUser.accountType == 'instructor')
          return CustomButton(
            string: 'Save',
            callback: ((state.gender.isNotValid ||
                        state.userName.isNotValid ||
                        state.firstName.isNotValid ||
                        state.lastName.isNotValid ||
                        state.cityName.isNotValid ||
                        state.countryName.isNotValid ||
                        state.dob.isNotValid ||
                        state.address.isNotValid ||
                        state.phoneNumber.isNotValid ||
                        state.postCode.isNotValid ||
                        state.hourlyRate.isNotValid ||
                        state.speciality.isEmpty ||
                        state.exp == null) ||
                    state.tempUser.specialty == state.speciality &&
                        state.tempUser.bio == state.bio &&
                        state.tempUser.experience == state.exp &&
                        state.tempUser.gender == state.gender.value &&
                        state.tempUser.username == state.userName.value &&
                        state.tempUser.firstName == state.firstName.value &&
                        state.tempUser.lastName == state.lastName.value &&
                        state.tempUser.city == state.cityName.value &&
                        state.tempUser.country == state.countryName.value &&
                        state.tempUser.dateOfBirth == state.dob.value &&
                        state.tempUser.address == state.address.value &&
                        state.tempUser.isClientTravels == state.isClientTravels &&
                        state.tempUser.isVideoTraining == state.isVideoTraining &&
                        state.tempUser.contactNumber == state.phoneNumber.value &&
                        state.tempUser.hourlyRate == (state.hourlyRate.value.isEmpty ? null : int.parse(state.hourlyRate.value)) &&
                        state.tempUser.postCode == state.postCode.value)
                ? null
                : () {
                    BlocProvider.of<ProfileBloc>(context).add(UpdateProfile());
                  },
          );

        return CustomButton(
          string: 'Save',
          callback: ((state.gender.isNotValid ||
                      state.userName.isNotValid ||
                      state.firstName.isNotValid ||
                      state.lastName.isNotValid ||
                      state.cityName.isNotValid ||
                      state.countryName.isNotValid ||
                      state.dob.isNotValid ||
                      state.address.isNotValid ||
                      state.phoneNumber.isNotValid ||
                      state.postCode.isNotValid) ||
                  state.tempUser.gender == state.gender.value &&
                      state.tempUser.username == state.userName.value &&
                      state.tempUser.firstName == state.firstName.value &&
                      state.tempUser.lastName == state.lastName.value &&
                      state.tempUser.city == state.cityName.value &&
                      state.tempUser.country == state.countryName.value &&
                      state.tempUser.dateOfBirth == state.dob.value &&
                      state.tempUser.address == state.address.value &&
                      state.tempUser.contactNumber == state.phoneNumber.value &&
                      state.tempUser.postCode == state.postCode.value)
              ? null
              : () {
                  BlocProvider.of<ProfileBloc>(context).add(UpdateProfile());
                },
        );
      },
    );
  }
}
