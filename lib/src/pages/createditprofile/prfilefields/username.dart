import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/input.dart';

import '../../../../bloc/editprofilebloc/profile_bloc.dart';

class UserNameEdit extends StatefulWidget {
  @override
  _UserNameEditState createState() => _UserNameEditState();
}

class _UserNameEditState extends State<UserNameEdit> {
  TextEditingController _controller;

  ProfileBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProfileBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.userName.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      UserNameEvent(username: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      buildWhen: (previous, current) => previous.userName != current.userName,
      builder: (context, state) {
        return InpuString(
          width: double.infinity,
          onEditingComplete: FocusScope.of(context).nextFocus,
          hint: 'username',
          textInputType: TextInputType.text,
          controller: _controller,
          error: state.userName.isNotValid ? 'username required' : null,
        );
      },
    );
  }
}
