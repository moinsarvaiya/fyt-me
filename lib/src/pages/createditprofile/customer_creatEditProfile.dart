import 'package:flutter/material.dart';

import '../../helpers/app_config.dart' as config;
import '../../helpers/helper.dart';

class CustomerCreateAndEditProfile extends StatefulWidget {
  @override
  _CustomerCreateAndEditProfileState createState() => _CustomerCreateAndEditProfileState();
}

class _CustomerCreateAndEditProfileState extends State<CustomerCreateAndEditProfile> {
  GlobalKey<FormState> formKey;
  bool _isFemale = true;
  bool _isAge = true;
  bool _isGender = true;
  var _activityLevel;
  var _goal;

  @override
  void initState() {
    super.initState();
    formKey = new GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: config.App(context).appWidth(10),
            vertical: config.App(context).appHeight(2),
          ),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: config.App(context).appHeight(6),
                ),
                Container(
                  width: config.App(context).appWidth(80),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Customer",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                SizedBox(
                  height: config.App(context).appHeight(3),
                ),
                Text(
                  "Name",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: config.App(context).appWidth(35),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) {
                          print("$input");
                        },
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          hintText: "First Name",
                          hintStyle: Theme.of(context).textTheme.headline4,
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: config.App(context).appWidth(35),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) {
                          print("$input");
                        },
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          hintText: "Last Name",
                          hintStyle: Theme.of(context).textTheme.headline4,
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: config.App(context).appHeight(2),
                ),
                Text(
                  "Date Of Birth",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                TextFormField(
                  keyboardType: TextInputType.datetime,
                  onSaved: (input) {
                    print("$input");
                  },
                  cursorColor: Colors.white,
                  style: Theme.of(context).textTheme.headline4,
                  decoration: InputDecoration(
                    hintText: "MM/DD/YYYY",
                    hintStyle: Theme.of(context).textTheme.headline4,
                    filled: true,
                    fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 25,
                      vertical: config.App(context).appHeight(1),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: config.App(context).appHeight(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Checkbox(
                            value: _isAge,
                            onChanged: (value) {
                              if (value == !_isAge) {
                                setState(() {
                                  _isAge = value;
                                });
                              }
                            },
                            activeColor: Theme.of(context).primaryColor,
                          ),
                          Text(
                            "Don't share my age",
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Text(
                  "Gender",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      width: config.App(context).appWidth(35),
                      padding: EdgeInsets.symmetric(
                          // vertical: config.App(context).appHeight(1),
                          ),
                      decoration: BoxDecoration(
                        color: _isFemale == true ? Theme.of(context).primaryColor : Color(0xFFF0F0F0).withOpacity(0.51),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            _isFemale = true;
                          });
                        },
                        child: Text(
                          "Female",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: config.App(context).appWidth(35),
                      padding: EdgeInsets.symmetric(
                          // vertical: config.App(context).appHeight(1),
                          ),
                      decoration: BoxDecoration(
                        color: _isFemale == false ? Theme.of(context).primaryColor : Color(0xFFF0F0F0).withOpacity(0.51),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            _isFemale = false;
                          });
                        },
                        child: Text(
                          "Male",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: config.App(context).appHeight(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Checkbox(
                            value: _isGender,
                            onChanged: (value) {
                              if (value == !_isGender) {
                                setState(() {
                                  _isGender = value;
                                });
                              }
                            },
                            activeColor: Theme.of(context).primaryColor,
                          ),
                          Text(
                            "Prefer not to say",
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Text(
                  "Height/Weight",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: config.App(context).appWidth(35),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) {
                          print("$input");
                        },
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          hintText: "5'6\"",
                          hintStyle: Theme.of(context).textTheme.headline4,
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: config.App(context).appWidth(35),
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) {
                          print("$input");
                        },
                        cursorColor: Colors.white,
                        style: Theme.of(context).textTheme.headline4,
                        decoration: InputDecoration(
                          hintText: "145lbs",
                          hintStyle: Theme.of(context).textTheme.headline4,
                          filled: true,
                          fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 25,
                            vertical: config.App(context).appHeight(1),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: config.App(context).appHeight(2),
                ),
                Text(
                  "Activity Level",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                DropdownButtonFormField(
                  dropdownColor: Color(0xFF7F8389),
                  iconEnabledColor: Colors.white,
                  isExpanded: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 25,
                      vertical: config.App(context).appHeight(1),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                  ),
                  value: _activityLevel,
                  items: ["High", "Moderate", "Low"]
                      .map(
                        (label) => DropdownMenuItem(
                          child: Text(
                            label,
                            style: Theme.of(context).textTheme.headline4,
                          ),
                          value: label,
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      _activityLevel = value;
                    });
                  },
                ),
                SizedBox(
                  height: config.App(context).appHeight(2),
                ),
                Text(
                  "Fitness Goals",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                DropdownButtonFormField(
                  dropdownColor: Color(0xFF7F8389),
                  iconEnabledColor: Colors.white,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 25,
                      vertical: config.App(context).appHeight(1),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide.none,
                    ),
                  ),
                  value: _goal,
                  items: ["Lose Weight", "Build Muscles", "Improve Endurance", "Increase Flexibility", "Toning"]
                      .map(
                        (label) => DropdownMenuItem(
                          child: Text(
                            label,
                            style: Theme.of(context).textTheme.headline4,
                          ),
                          value: label,
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      _goal = value;
                    });
                  },
                ),
                SizedBox(
                  height: config.App(context).appHeight(4),
                ),
                Container(
                  width: config.App(context).appWidth(80),
                  alignment: Alignment.center,
                  child: TextButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          // return object of type Dialog
                          return AlertDialog(
                            title: Align(
                              alignment: Alignment.center,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: config.App(context).appHeight(2),
                                ),
                                child: Text(
                                  "SELF ASSESSMENT",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ),
                            ),
                            content: Container(
                              height: config.App(context).appHeight(18),
                              child: Column(
                                children: [
                                  Text(
                                    "You can change this at anytime",
                                    style: Theme.of(context).textTheme.headline4,
                                  ),
                                  SizedBox(
                                    height: config.App(context).appHeight(3),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                      width: config.App(context).appWidth(65),
                                      padding: EdgeInsets.symmetric(
                                        vertical: config.App(context).appHeight(3),
                                        horizontal: 20,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Theme.of(context).primaryColor,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Text(
                                        "Continue",
                                        style: Theme.of(context).textTheme.subtitle1,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            backgroundColor: Color(0xFF3D434D),
                          );
                        },
                      );
                    },
                    child: Container(
                      width: config.App(context).appWidth(65),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(3),
                        horizontal: 20,
                      ),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Text(
                        "Continue",
                        style: Theme.of(context).textTheme.subtitle1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: config.App(context).appWidth(80),
                  alignment: Alignment.center,
                  child: TextButton(
                    child: Text(
                      "CANCEL",
                      style: Theme.of(context).textTheme.headline4.copyWith(
                            fontWeight: FontWeight.w300,
                          ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
