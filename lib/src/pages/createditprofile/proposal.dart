import 'dart:core';

import 'package:date_util/date_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/proposalbloc/proposal_bloc.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:intl/intl.dart';

import 'proposal/select_month.dart';
import 'proposal/select_training_mode.dart';
import 'proposal/select_year.dart';

class ProposalArguments {
  final int instructorId;
  final int selectedHours;
  final DateTime selectedDate;

  ProposalArguments(this.instructorId, this.selectedDate, this.selectedHours);
}

class Proposal extends StatefulWidget {
  final int instructorId;
  final int selectedHours;
  final DateTime selectedDate;

  Proposal({Key key, @required this.instructorId, @required this.selectedDate, @required this.selectedHours}) : super(key: key);

  @override
  _ProposalState createState() => _ProposalState();
}

class _ProposalState extends State<Proposal> {
  List<String> inviteFriendsList = [];
  TextEditingController _controllerFriends = TextEditingController();
  bool _showError = false;
  int selectedStartHours = DateTime.now().hour;
  int selectedStartMinutes = DateTime.now().minute;
  int selectedEndHours = DateTime.now().hour;
  int selectedEndMinutes = DateTime.now().minute;
  int selectedDay = DateTime.now().day - 1;
  int selectedMonth = DateTime.now().month - 1;
  int selectedYear = DateTime.now().year;
  double itemExtent = 40.0;
  List<String> months = ['January', 'February', 'March', 'April', 'May', 'Jun', 'July', 'August', 'September', 'October', 'November', 'December'];

  FixedExtentScrollController _controllerDate;
  FixedExtentScrollController _controllerStartHours;
  FixedExtentScrollController _controllerStartMinutes;
  FixedExtentScrollController _controllerEndHours;
  FixedExtentScrollController _controllerEndMinutes;

  @override
  void initState() {
    selectedStartHours = widget.selectedHours;
    selectedStartMinutes = DateTime.now().minute;
    selectedEndHours = widget.selectedHours;
    selectedEndMinutes = DateTime.now().minute;
    selectedDay = widget.selectedDate.day - 1;
    selectedMonth = widget.selectedDate.month - 1;
    selectedYear = widget.selectedDate.year;
    _controllerDate = FixedExtentScrollController(initialItem: selectedDay);
    _controllerStartHours = FixedExtentScrollController(initialItem: selectedStartHours);
    _controllerStartMinutes = FixedExtentScrollController(initialItem: selectedStartMinutes);
    _controllerEndHours = FixedExtentScrollController(initialItem: selectedEndHours);
    _controllerEndMinutes = FixedExtentScrollController(initialItem: selectedEndMinutes);
    updateTimeDifferenceState();
    super.initState();
  }

  int timeDiff = 0;

  updateTimeDifferenceState() {
    var format = DateFormat("HH:mm");
    var one = format.parse("$selectedStartHours:$selectedStartMinutes");
    var two = format.parse("$selectedEndHours:$selectedEndMinutes");
    timeDiff = two.difference(one).inHours;
    //print("$timeDiff");
    //context.read<ProposalBloc>().add(TimeDifferenceEvent(vlaue: timeDiff));
  }

  scrollDatePicker(bool isUp) {
    /*print("selectedDay : DAYS${(selectedDay)} : ${DateTime.now().day}");
    print("selectedMonth : month$selectedMonth : ${DateTime.now().month}");
    print("selectedYear : year$selectedYear : ${DateTime.now().year}");
    print("selectedYear : year${(selectedDay <= DateTime.now().day - 1)}");
    print("selectedYear : year${(selectedMonth <= DateTime.now().month - 1)}");
    print("selectedYear : year${(selectedYear <= DateTime.now().year - 1)}");*/

    setState(() {
      if (isUp) {
        if ((selectedDay <= DateTime.now().day - 1) && (selectedMonth <= DateTime.now().month) && (selectedYear <= DateTime.now().year)) {
          return;
        }

        _controllerDate.animateTo((selectedDay - 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
      } else {
        _controllerDate.animateTo((selectedDay + 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
      }
    });
  }

  scrollExtentDownPicker(bool isStart) {
    if (isStart) {
      setState(() {
        if ((selectedStartHours + 1 >= selectedEndHours)) {
          _controllerEndHours.animateTo((selectedStartHours + 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
        }

        _controllerStartHours.animateTo((selectedStartHours + 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
      });
    } else {
      _controllerEndHours.animateTo((selectedEndHours + 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
    }
  }

  scrollExtentUpPicker(bool isStart) {
    /*print("selectedDay : DAYS${(selectedDay)} : ${DateTime.now().day - 1}");
    print("selectedMonth : month$selectedMonth : ${DateTime.now().month}");
    print(
        "selectedStartHours : current${selectedStartHours - 1} : ${DateTime.now().hour}");
    print(
        "selectedStartHours : current${(selectedStartHours - 1 < DateTime.now().hour)}");*/

    if (isStart) {
      setState(() {
        if ((selectedStartHours <= DateTime.now().hour) &&
            (selectedDay <= DateTime.now().day - 1) &&
            (selectedMonth <= DateTime.now().month) &&
            (selectedYear <= DateTime.now().year)) {
          return;
        }

        _controllerStartHours.animateTo((selectedStartHours - 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
      });
    } else {
      if ((selectedStartHours >= selectedEndHours)) {
        return;
      }
      _controllerEndHours.animateTo((selectedEndHours - 1) * itemExtent, curve: Curves.ease, duration: Duration(milliseconds: 200));
    }
  }

  List<Widget> hoursWidgetList(bool isStart) {
    List<Widget> text = [];
    for (int i = 0; i < 24; i++) {
      text.add(Align(
        alignment: Alignment.center,
        child: Container(
          color: Color(0xFF0a1726),
          child: Container(
            width: 50,
            height: 35,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: isStart
                  ? (selectedStartHours == i ? Color(0xFF1d8dc9) : Color(0xFF0a1726))
                  : (selectedEndHours == i ? Color(0xFF1d8dc9) : Color(0xFF0a1726)),
            ),
            alignment: Alignment.center,
            child: Text(
                (isStart
                        ? (selectedStartHours == i || selectedStartHours == i + 1 || selectedStartHours == i - 1)
                        : (selectedEndHours == i || selectedEndHours == i + 1 || selectedEndHours == i - 1))
                    ? (i < 10 ? '0$i' : '$i')
                    : '',
                style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.normal)),
          ),
        ),
      ));
    }
    return text;
  }

  List<Widget> minutesWidgetList(bool isStart) {
    List<Widget> text = [];
    for (int i = 0; i < 60; i++) {
      text.add(Align(
        alignment: Alignment.center,
        child: Container(
          color: Color(0xFF0a1726),
          child: Container(
            width: 50,
            height: 35,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: isStart
                  ? (selectedStartMinutes == i ? Color(0xFF1d8dc9) : Color(0xFF0a1726))
                  : (selectedEndMinutes == i ? Color(0xFF1d8dc9) : Color(0xFF0a1726)),
            ),
            alignment: Alignment.center,
            child: Text(
                (isStart
                        ? (selectedStartMinutes == i || selectedStartMinutes == i + 1 || selectedStartMinutes == i - 1)
                        : (selectedEndMinutes == i || selectedEndMinutes == i + 1 || selectedEndMinutes == i - 1))
                    ? (i < 10 ? '0$i' : '$i')
                    : '',
                style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.normal)),
          ),
        ),
      ));
    }
    return text;
  }

  int getMonthNo(String month) {
    // setState(() {
    //   selectedYear = year;
    // });

    if (month == 'January') {
      return 1;
    } else if (month == 'February') {
      return 2;
    } else if (month == 'March') {
      return 3;
    } else if (month == 'April') {
      return 4;
    } else if (month == 'May') {
      return 5;
    } else if (month == 'Jun') {
      return 6;
    } else if (month == 'July') {
      return 7;
    } else if (month == 'August') {
      return 8;
    } else if (month == 'September') {
      return 9;
    } else if (month == 'October') {
      return 10;
    } else if (month == 'November') {
      return 11;
    } else if (month == 'December') {
      return 12;
    } else {
      return DateTime.now().month;
    }
  }

  List<Widget> daysWidgetList(String month, int year) {
    selectedMonth = getMonthNo(month);
    if (year == DateTime.now().year && (selectedMonth < DateTime.now().month)) {
      selectedMonth = DateTime.now().month;
      context.read<ProposalBloc>().add(MonthEvent(value: months[selectedMonth - 1]));
    }

    selectedYear = year == null ? DateTime.now().year : year;
    List<String> list = getNoOfDaysFromMonth(selectedMonth, selectedYear);
    List<Widget> daysList = [];
    print('$selectedMonth $selectedYear');
    for (int i = 0; i < list.length; i++) {
      daysList.add(Container(
        color: Color(0xFF0a1726),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: selectedDay == i ? Color(0xFF1d8dc9) : Color(0xFF0a1726),
          ),
          alignment: Alignment.center,
          child: Text(selectedDay == i || selectedDay == i + 1 || selectedDay == i - 1 ? '${list[i]}' : '',
              style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.normal)),
        ),
      ));
    }
    return daysList;
  }

  List<String> getNoOfDaysFromMonth(int month, int year) {
    List<String> daysList = [];
    var daysCount = DateUtil().daysInMonth(month, year);
    for (int m = 0; m < daysCount; m++) {
      String parseDate = '${m + 1}/$month/$year';
      daysList.add(parseDateFormat(parseDate, 'dd/MM/yyyy', 'dd EEEE'));
    }
    return daysList;
  }

  int daysInMonth(DateTime date) {
    var firstDayThisMonth = DateTime(date.year, date.month, date.day);
    var firstDayNextMonth = DateTime(firstDayThisMonth.year, firstDayThisMonth.month + 1, firstDayThisMonth.day);
    return firstDayNextMonth.difference(firstDayThisMonth).inDays;
  }

  String parseDateFormat(String date, String currentFormat, String newFormat) {
    DateTime parseDate = DateFormat(currentFormat).parse(date);
    var inputDate = DateTime.parse(parseDate.toString());
    String actualDay = DateFormat(newFormat).format(inputDate);
    return actualDay;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<ProposalBloc, ProposalState>(
        listenWhen: (previous, current) => previous.status.status != current.status.status,
        listener: (contex, state) {
          switch (state.status.status) {
            case StateStatuses.loading:
              CustomLoader().show(context);
              break;
            case StateStatuses.failure:
              setState(() {
                inviteFriendsList = [];
              });
              CustomLoader().hide(context);
              CustomWidgets.buildErrorSnackBar(context, state.status.message);
              break;
            case StateStatuses.success:
              CustomLoader().hide(context);
              Navigator.of(context).pop();
              CustomWidgets.buildSuccessSnackBar(context, 'Your proposal has been sent');
              break;
            default:
              setState(() {
                inviteFriendsList = [];
              });
              CustomLoader().hide(context);
              break;
          }
        },
        child: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: config.App(context).appHeight(4),
              ),
              Container(
                child: Center(
                  child: Text(
                    'PROPOSAL',
                    style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(3),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Session Date',
                          style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        Row(
                          children: [
                            SelectMonth(year: selectedYear),
                            SizedBox(
                              width: 10,
                            ),
                            SelectYear()
                          ],
                        ),
                        Container(
                          height: 120,
                          child: Row(
                            //mainAxisSize: MainAxisSize.max,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      scrollDatePicker(true);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.keyboard_arrow_up,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                                    ),
                                  ),
                                  Icon(
                                    Icons.calendar_today,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      scrollDatePicker(false);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              BlocBuilder<ProposalBloc, ProposalState>(builder: (context, state) {
                                return Flexible(
                                  child: CupertinoPageScaffold(
                                      child: Container(
                                          height: 150,
                                          child: Center(
                                              child: CupertinoPicker(
                                                  scrollController: _controllerDate,
                                                  itemExtent: itemExtent,
                                                  diameterRatio: 2.35 / 2.1,
                                                  useMagnifier: true,
                                                  backgroundColor: Colors.transparent,
                                                  onSelectedItemChanged: (int index) {
                                                    setState(() {
                                                      if ((index < DateTime.now().day - 1) &&
                                                          (selectedMonth <= DateTime.now().month) &&
                                                          (selectedYear <= DateTime.now().year)) {
                                                        _controllerDate.animateTo((selectedDay) * itemExtent,
                                                            curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                        return;
                                                      }

                                                      selectedDay = index;
                                                      //print('$selectedDay');
                                                    });
                                                  },
                                                  children: daysWidgetList(state.month, state.year))))),
                                );
                              }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Session Start time',
                              style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Session End time',
                              style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 110,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              scrollExtentUpPicker(true);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.all(5.0),
                                              child: Icon(
                                                Icons.keyboard_arrow_up,
                                                color: Colors.white,
                                                size: 30,
                                              ),
                                            ),
                                          ),
                                          Icon(
                                            Icons.watch_later_outlined,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              scrollExtentDownPicker(true);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.all(5.0),
                                              child: Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.white,
                                                size: 30,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        height: 120,
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 60,
                                              child: CupertinoPageScaffold(
                                                  child: Container(
                                                      child: Center(
                                                          child: CupertinoPicker(
                                                scrollController: _controllerStartHours,
                                                looping: false,
                                                itemExtent: itemExtent,
                                                diameterRatio: 2.35 / 2.1,
                                                useMagnifier: true,
                                                backgroundColor: Colors.transparent,
                                                onSelectedItemChanged: (int index) {
                                                  /* print(
                                                      "selectedDay : DAYS$selectedDay : ${DateTime.now().day - 1}");
                                                  print(
                                                      "selectedMonth : month$selectedMonth : ${DateTime.now().month}");
                                                  print(
                                                      "selectedStartHours : current${selectedStartHours - 1} : ${DateTime.now().hour}");
                                                  print(
                                                      "selectedStartHours : current${(selectedStartHours - 1 < DateTime.now().hour)}");
*/
                                                  setState(() {
                                                    if ((index < DateTime.now().hour) &&
                                                        (selectedDay <= DateTime.now().day - 1) &&
                                                        (selectedMonth <= DateTime.now().month) &&
                                                        (selectedYear <= DateTime.now().year)) {
                                                      _controllerStartHours.animateTo((selectedStartHours) * itemExtent,
                                                          curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                      return;
                                                    }

                                                    if ((index >= selectedEndHours)) {
                                                      _controllerEndHours.animateTo((index) * itemExtent,
                                                          curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                      selectedEndHours = index;
                                                    }

                                                    selectedStartHours = index;
                                                    updateTimeDifferenceState();
                                                  });
                                                },
                                                children: hoursWidgetList(true),
                                              )))),
                                            ),
                                            Text(
                                              ':',
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            Container(
                                              width: 60,
                                              child: CupertinoPageScaffold(
                                                  child: Container(
                                                      child: Center(
                                                          child: CupertinoPicker(
                                                scrollController: _controllerStartMinutes,
                                                looping: false,
                                                itemExtent: itemExtent,
                                                diameterRatio: 2.35 / 2.1,
                                                useMagnifier: true,
                                                backgroundColor: Colors.transparent,
                                                onSelectedItemChanged: (int index) {
                                                  setState(() {
                                                    if ((index < DateTime.now().minute) &&
                                                        (selectedStartHours <= DateTime.now().hour) &&
                                                        (selectedDay <= DateTime.now().day - 1) &&
                                                        (selectedMonth <= DateTime.now().month) &&
                                                        (selectedYear <= DateTime.now().year)) {
                                                      _controllerStartMinutes.animateTo((selectedStartMinutes) * itemExtent,
                                                          curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                      return;
                                                    }

                                                    if ((selectedEndMinutes < index) && (selectedStartHours == selectedEndHours)) {
                                                      _controllerEndMinutes.animateTo((index) * itemExtent,
                                                          curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                      selectedEndMinutes = index;
                                                    }

                                                    selectedStartMinutes = index;
                                                    updateTimeDifferenceState();
                                                  });
                                                },
                                                children: minutesWidgetList(true),
                                              )))),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                            onTap: () {
                                              scrollExtentUpPicker(false);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.all(5.0),
                                              child: Icon(
                                                Icons.keyboard_arrow_up,
                                                color: Colors.white,
                                                size: 30,
                                              ),
                                            ),
                                          ),
                                          Icon(
                                            Icons.watch_later_outlined,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              scrollExtentDownPicker(false);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.all(5.0),
                                              child: Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.white,
                                                size: 30,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        height: 120,
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 60,
                                              child: CupertinoPageScaffold(
                                                  child: Container(
                                                      child: Center(
                                                          child: CupertinoPicker(
                                                scrollController: _controllerEndHours,
                                                looping: false,
                                                itemExtent: itemExtent,
                                                diameterRatio: 2.35 / 2.1,
                                                useMagnifier: true,
                                                backgroundColor: Colors.transparent,
                                                onSelectedItemChanged: (int index) {
                                                  setState(() {
                                                    if ((selectedStartHours >= index)) {
                                                      _controllerEndHours.animateTo((selectedEndHours) * itemExtent,
                                                          curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                      return;
                                                    }
                                                    selectedEndHours = index;
                                                    updateTimeDifferenceState();
                                                  });
                                                },
                                                children: hoursWidgetList(false),
                                              )))),
                                            ),
                                            Text(
                                              ':',
                                              style: TextStyle(color: Colors.white),
                                            ),
                                            Container(
                                              width: 60,
                                              child: CupertinoPageScaffold(
                                                  child: Container(
                                                      child: Center(
                                                          child: CupertinoPicker(
                                                scrollController: _controllerEndMinutes,
                                                looping: false,
                                                itemExtent: itemExtent,
                                                diameterRatio: 2.35 / 2.1,
                                                useMagnifier: true,
                                                backgroundColor: Colors.transparent,
                                                onSelectedItemChanged: (int index) {
                                                  setState(() {
                                                    if ((index < selectedStartMinutes) && (selectedStartHours == selectedEndHours)) {
                                                      _controllerEndMinutes.animateTo((selectedEndMinutes) * itemExtent,
                                                          curve: Curves.ease, duration: Duration(milliseconds: 200));
                                                      return;
                                                    }

                                                    selectedEndMinutes = index;
                                                    updateTimeDifferenceState();
                                                  });
                                                },
                                                children: minutesWidgetList(false),
                                              )))),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        TrainingMode(),
                        SizedBox(
                          height: 30,
                        ),
                        /*BlocBuilder<ProposalBloc, ProposalState>(
                            builder: (context, state) {
                          return ;
                        }),*/
                        BlocBuilder<ProposalBloc, ProposalState>(builder: (context, state) {
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Visibility(
                                  visible: true,
                                  //state.trainingMode != 'Online'
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Invite Friend',
                                        style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Container(
                                              width: config.App(context).appWidth(100),
                                              child: TextFormField(
                                                controller: _controllerFriends,
                                                textInputAction: TextInputAction.done,
                                                keyboardType: TextInputType.text,
                                                cursorColor: Colors.white,
                                                style: Theme.of(context).textTheme.headline4,
                                                decoration: InputDecoration(
                                                  hintText: '----------',
                                                  hintStyle: Theme.of(context).textTheme.headline4,
                                                  filled: true,
                                                  fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                                                  contentPadding: EdgeInsets.symmetric(
                                                    horizontal: 25,
                                                    vertical: config.App(context).appHeight(1),
                                                  ),
                                                  border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(15),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(15),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(15),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              if (_controllerFriends.value.text.toString().isEmpty) {
                                                setState(() {
                                                  _showError = true;
                                                });
                                              } else {
                                                setState(() {
                                                  inviteFriendsList.add(_controllerFriends.value.text.toString().trim());
                                                  _showError = false;
                                                  _controllerFriends = TextEditingController();
                                                });
                                              }
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              width: 90,
                                              height: 45,
                                              decoration:
                                                  BoxDecoration(color: Theme.of(context).primaryColor, borderRadius: BorderRadius.circular(12)),
                                              padding: EdgeInsets.symmetric(horizontal: 10),
                                              child: Text(
                                                'Invite',
                                                style: TextStyle(color: Colors.white),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Visibility(
                                        visible: _showError,
                                        child: Text(
                                          'Please enter friend name',
                                          style: TextStyle(color: Colors.red, fontSize: 10, fontWeight: FontWeight.normal),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Wrap(
                                        crossAxisAlignment: WrapCrossAlignment.start,
                                        children: List.generate(
                                            inviteFriendsList.length ?? 0,
                                            (index) => Padding(
                                                  padding: const EdgeInsets.only(right: 4.0, left: 4),
                                                  child: Chip(
                                                    backgroundColor: Theme.of(context).primaryColor,
                                                    label: Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: 10),
                                                      child: Text(
                                                        '${inviteFriendsList[index]}',
                                                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                                                      ),
                                                    ),
                                                    deleteIcon: Icon(
                                                      Icons.clear,
                                                      color: Colors.white,
                                                    ),
                                                    onDeleted: () {
                                                      setState(() {
                                                        inviteFriendsList.removeAt(index);
                                                      });
                                                    },
                                                  ),
                                                )),
                                      ),
                                    ],
                                  )),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(top: 30, left: 30, right: 30, bottom: 10),
                                child: CustomButton(
                                    callback: (timeDiff == 0 || state.trainingMode == null)
                                        ? null
                                        : () {
                                            String day = (selectedDay + 1) < 10 ? '0${(selectedDay + 1)}' : '${(selectedDay + 1)}';
                                            String month = selectedMonth < 10 ? '0$selectedMonth' : '$selectedMonth';
                                            String selectedDate = '$selectedYear-$month-$day';
                                            //print('$inviteFriendsList');
                                            String selStartHr = selectedStartHours < 10 ? '0$selectedStartHours' : '$selectedStartHours';
                                            String selStartMin = selectedStartMinutes < 10 ? '0$selectedStartMinutes' : '$selectedStartMinutes';
                                            String selEndHr = selectedEndHours < 10 ? '0$selectedEndHours' : '$selectedEndHours';
                                            String selEndMin = selectedEndMinutes < 10 ? '0$selectedEndMinutes' : '$selectedEndMinutes';
                                            context.read<ProposalBloc>().add(ProposalCreateEvent(
                                                instructorId: widget.instructorId,
                                                date: selectedDate,
                                                startTime: '$selectedDate $selStartHr:$selStartMin:00',
                                                endTime: '$selectedDate $selEndHr:$selEndMin:00',
                                                friends: inviteFriendsList));
                                          },
                                    string: 'Send Proposal'),
                              )
                            ],
                          );
                        }),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 40,
                            alignment: Alignment.center,
                            child: Text(
                              'Back',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
