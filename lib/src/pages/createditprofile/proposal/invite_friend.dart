import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/proposalbloc/proposal_bloc.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:fytme/src/pages/createditprofile/prfilefields/index.dart';

class InviteFriend extends StatefulWidget {
  @override
  _InviteFriendState createState() => _InviteFriendState();
}

class _InviteFriendState extends State<InviteFriend> {
  TextEditingController _controller;

  ProposalBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<ProposalBloc>(context);
    _controller = TextEditingController(
      text: _authBloc.state.inviteMember,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      InviteFriendEvent(value: _controller?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProposalBloc, ProposalState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InpuString(
              textInputType: TextInputType.text,
              hint: '----------',
              width: config.App(context).appWidth(100),
              controller: _controller,
              //error: state.inviteMember == null ? 'dfdfdfd' : null,
            ),
          ],
        );
      },
    );
  }
}
