import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/proposalbloc/proposal_bloc.dart';

class TrainingMode extends StatefulWidget {
  @override
  _TrainingModeState createState() => _TrainingModeState();
}

class _TrainingModeState extends State<TrainingMode> {
  List<String> trainingMode = [
    'Select Training Mode',
    'Online',
    'Gym',
    'Home',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProposalBloc, ProposalState>(
      buildWhen: (p, c) => p.trainingMode != c.trainingMode,
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Training Mode',
              style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 45,
              decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.white,
                ),
                value: state.trainingMode ?? trainingMode[0],
                onChanged: (v) {
                  //print(v);
                  if (v != trainingMode[0]) {
                    context.read<ProposalBloc>().add(TrainingModeEvent(value: v));
                  }
                },
                items: trainingMode
                    .map((e) => DropdownMenuItem<String>(
                          value: e,
                          child: new Text(e),
                        ))
                    .toList(),
              )),
            ),
          ],
        );
      },
    );
  }
}
