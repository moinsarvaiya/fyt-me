import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/proposalbloc/proposal_bloc.dart';

class SelectYear extends StatefulWidget {
  @override
  _SelectYearState createState() => _SelectYearState();
}

class _SelectYearState extends State<SelectYear> {
  List<int> years = [2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProposalBloc, ProposalState>(
      buildWhen: (p, c) => p.year != c.year,
      builder: (context, state) {
        return DropdownButtonHideUnderline(
            child: DropdownButton<int>(
          icon: Icon(
            Icons.keyboard_arrow_down,
            color: Colors.white,
          ),
          value: state.year ?? DateTime.now().year,
          onChanged: (v) {
            context.read<ProposalBloc>().add(YearEvent(value: v));
          },
          items: years
              .map((e) => DropdownMenuItem<int>(
                    value: e,
                    child: new Text('$e'),
                  ))
              .toList(),
        ));
      },
    );
  }
}
