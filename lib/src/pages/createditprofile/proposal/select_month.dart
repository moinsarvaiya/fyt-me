import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/proposalbloc/proposal_bloc.dart';

class SelectMonth extends StatefulWidget {
  final int year;

  SelectMonth({Key key, @required this.year}) : super(key: key);

  @override
  _SelectMonthState createState() => _SelectMonthState();
}

class _SelectMonthState extends State<SelectMonth> {
  List<String> months = ['January', 'February', 'March', 'April', 'May', 'Jun', 'July', 'August', 'September', 'October', 'November', 'December'];
  int selectedMonth = 1;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProposalBloc, ProposalState>(
      buildWhen: (p, c) => (p.month != c.month || p.year != c.year),
      builder: (context, state) {
        return DropdownButtonHideUnderline(
            child: DropdownButton<String>(
          icon: Icon(
            Icons.keyboard_arrow_down,
            color: Colors.white,
          ),
          value: state.month ?? months[DateTime.now().month - 1],
          onChanged: (v) {
            selectedMonth = months.indexOf(v);
            print("selectedYear : year${widget.year} : ${DateTime.now().year}");
            print("selectedYear : year${(state.year <= DateTime.now().year)}");
            print("selectedYear : year${((selectedMonth + 1) < DateTime.now().month)}");
            print("selectedYear : year${(((selectedMonth + 1) < DateTime.now().month) && (state.year <= DateTime.now().year))}");
            if (((selectedMonth + 1) < DateTime.now().month) && (state.year <= DateTime.now().year)) {
              return;
            }
            context.read<ProposalBloc>().add(MonthEvent(value: v));
          },
          items: months
              .map((e) => DropdownMenuItem<String>(
                    value: e,
                    child: Text(e),
                  ))
              .toList(),
        ));
      },
    );
  }
}
