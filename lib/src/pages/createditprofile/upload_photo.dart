import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';
import 'package:image_picker/image_picker.dart';

class UploadImage extends StatefulWidget {
  @override
  _UploadImageState createState() => _UploadImageState();
}

class _UploadImageState extends State<UploadImage> {
  File _image;
  final picker = ImagePicker();

  void selectImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  void selectimageFromlibrary() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    print(pickedFile.path);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose profile photo'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Spacer(),
            Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: Colors.white),
              child: InkWell(
                borderRadius: BorderRadius.circular(100),
                onTap: () {
                  showCupertinoModalPopup(
                      context: context,
                      builder: (BuildContext context) {
                        return CupertinoActionSheet(
                          title: Text('Choose'),
                          actions: <Widget>[
                            CupertinoActionSheetAction(
                              child: Text('Camera'),
                              onPressed: () {
                                Navigator.pop(context);
                                selectImageFromCamera();
                              },
                            ),
                            CupertinoActionSheetAction(
                              child: Text('Gallery'),
                              onPressed: () {
                                Navigator.pop(context);
                                selectimageFromlibrary();
                              },
                            ),
                          ],
                          cancelButton: CupertinoActionSheetAction(
                            child: Text(
                              "Cancel",
                              style: TextStyle(color: Colors.red),
                            ),
                            onPressed: () => Navigator.pop(context),
                          ),
                        );
                      });
                },
                child: _image == null
                    ? Center(
                        child: Icon(Icons.person_add),
                      )
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image.file(
                          _image,
                          fit: BoxFit.fill,
                          width: double.infinity,
                          height: double.infinity,
                        )),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              '250 x 250 min / 15 MB max',
              style: TextStyle(color: Colors.white),
            ),
            Spacer(),
            Center(
                child: CustomButton(
                    callback: _image == null
                        ? null
                        : () {
                            Navigator.pop(context, {"image": _image, "step": true});
                          },
                    string: 'Continue')),
            TextButton(
              child: Text(
                'Skip',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pop(context, {"image": null, "step": false});
              },
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
