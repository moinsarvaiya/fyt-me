import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';

import 'cert.dart';
import 'cpr.dart';
import 'insurance.dart';

class Form2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
          child: Column(
            children: [
              CertPicture(),
              SizedBox(
                height: 20,
              ),
              InsurancePicture(),
              SizedBox(
                height: 20,
              ),
              CprPicture(),
            ],
          ),
        );
      },
    );
  }
}
