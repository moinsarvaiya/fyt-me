import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';

class InsurancePicture extends StatefulWidget {
  @override
  _InsurancePictureState createState() => _InsurancePictureState();
}

class _InsurancePictureState extends State<InsurancePicture> with AutomaticKeepAliveClientMixin {
  String _fileName = 'Select Picture';

  _pickFile() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(allowedExtensions: ['jpg', 'pdf', 'png', 'jpeg'], type: FileType.custom);
    if (result != null) {
      File file = File(result.files.single.path);
      context.read<ProfileBloc>().add(InsuranceEvent(value: file.path));
      setState(() {
        _fileName = result.files.first.name;
      });
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Insurance',
              style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Text(state.insurance != null ? state.insurance.split('/').last : _fileName,
                        style: TextStyle(
                          color: Colors.white,
                        )),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.add_circle_outline,
                        color: Colors.white,
                      ),
                      onPressed: _pickFile)
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
