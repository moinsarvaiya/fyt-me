import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';

class DocType extends StatefulWidget {
  @override
  _DocTypeState createState() => _DocTypeState();
}

class _DocTypeState extends State<DocType> {
  List<String> docType = ['Select Document type', 'Citizen Card', 'Driving Licence', 'Passport'];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      buildWhen: (p, c) => p.docType != c.docType,
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Document Type',
              style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 45,
              decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.white,
                ),
                value: state.docType ?? docType[0],
                onChanged: (v) {
                  print(v);
                  if (v != docType[0]) {
                    context.read<ProfileBloc>().add(DocTypeEvent(value: v));
                  }
                },
                items: docType
                    .map((e) => DropdownMenuItem<String>(
                          value: e,
                          child: new Text(e),
                        ))
                    .toList(),
              )),
            ),
          ],
        );
      },
    );
  }
}
