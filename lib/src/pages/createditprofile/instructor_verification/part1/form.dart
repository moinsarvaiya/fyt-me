import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';
import 'package:fytme/src/pages/createditprofile/instructor_verification/part1/doc_back.dart';
import 'package:fytme/src/pages/createditprofile/instructor_verification/part1/doc_number.dart';
import 'package:fytme/src/pages/createditprofile/instructor_verification/part1/select_type_doc.dart';

import 'doc_front.dart';

class Form1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
          child: Column(
            children: [
              DocType(),
              SizedBox(
                height: 20,
              ),
              DocNumber(),
              SizedBox(
                height: 20,
              ),
              DocFrontPicture(),
              SizedBox(
                height: 20,
              ),
              DocBackPicture()
            ],
          ),
        );
      },
    );
  }
}
