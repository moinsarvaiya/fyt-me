import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';

import 'bank_account.dart';
import 'bank_sort.dart';

class Form3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
          child: Column(
            children: [
              BankAccountNumber(),
              SizedBox(
                height: 20,
              ),
              BankSortNumber(),
            ],
          ),
        );
      },
    );
  }
}
