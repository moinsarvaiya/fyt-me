import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';

import '../../../helpers/app_config.dart' as config;
import 'part1/form.dart';
import 'part2/form.dart';
import 'part3/form.dart';

class TabsInstructorVerification extends StatefulWidget {
  final bool fromSetting;

  TabsInstructorVerification({this.fromSetting = false});

  @override
  _TabsInstructorVerificationState createState() => _TabsInstructorVerificationState();
}

class _TabsInstructorVerificationState extends State<TabsInstructorVerification> {
  int _index = 0;

  Color getCircleColor(int index) {
    return _index == index ? Colors.white24 : Colors.grey.withOpacity(.1);
  }

  Color getTextColor(int index) {
    return _index == index ? Colors.white : Colors.white24;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<ProfileBloc, ProfileState>(
        listenWhen: (previous, current) => previous.status.status != current.status.status,
        listener: (con, state) {
          switch (state.status.status) {
            case StateStatuses.loading:
              CustomLoader().show(context);
              break;
            case StateStatuses.failure:
              CustomLoader().hide(context);
              CustomWidgets.buildErrorSnackBar(context, state.status.message);
              break;
            case StateStatuses.success:
              CustomLoader().hide(context);
              if (widget.fromSetting) {
                Navigator.pop(context);
              }
              break;
            default:
              CustomLoader().hide(context);
              break;
          }
        },
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: config.App(context).appHeight(10),
              ),
              Text(
                'INSTRUCTOR VERIFICATION',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
              SizedBox(
                height: config.App(context).appHeight(5),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: List.generate(
                    3,
                    (index) => Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: getCircleColor(index)),
                          child: Center(
                            child: Text(
                              (index + 1).toString(),
                              style: TextStyle(color: getTextColor(index), fontSize: 18),
                            ),
                          ),
                        )),
              ),
              _getPage(_index),
              if (_index == 0)
                BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
                  return CustomButton(
                      callback: (state.docType == null ||
                              state.docNumber == null ||
                              state.docNumber.isEmpty ||
                              state.dFrontPic == null ||
                              state.docBackPic == null)
                          ? null
                          : () {
                              setState(() {
                                _index = 1;
                              });
                            },
                      string: 'Continue');
                }),
              if (_index == 1)
                BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
                  return CustomButton(
                      callback: state.certificate == null || state.insurance == null || state.cpr == null
                          ? null
                          : () {
                              setState(() {
                                _index = 2;
                              });
                            },
                      string: 'Continue');
                }),
              if (_index == 2)
                BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
                  return CustomButton(
                      callback: state.bankSortNumber == null ||
                              state.bankAccountNumber == null ||
                              state.bankAccountNumber.isEmpty ||
                              state.bankSortNumber.isEmpty
                          ? null
                          : () {
                              if (widget.fromSetting) {
                                context.read<ProfileBloc>().add(SubmitVerificationSettingEvent());
                              } else {
                                context.read<ProfileBloc>().add(SubmitVerificationEvent());
                              }
                            },
                      string: 'Continue');
                }),
              if (_index == 0)
                BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
                  return TextButton(
                    child: Text(
                      'Cancel'.toUpperCase(),
                      style: TextStyle(color: Colors.white70),
                    ),
                    onPressed: () {
                      if (widget.fromSetting) {
                        Navigator.pop(context);
                      } else {
                        context.read<ProfileBloc>().add(SkipVerificationProcess());
                      }
                    },
                  );
                }),
              if (_index > 0)
                TextButton(
                  child: Text(
                    'Back'.toUpperCase(),
                    style: TextStyle(color: Colors.white70),
                  ),
                  onPressed: () {
                    setState(() {
                      _index = _index - 1;
                    });
                  },
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getPage(int index) {
    switch (index) {
      case 0:
        return Form1();
        break;
      case 1:
        return Form2();
        break;
      case 2:
        return Form3();
        break;
      default:
        return Form1();
    }
  }
}
