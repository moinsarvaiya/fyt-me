import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/bio.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/index.dart';
import 'package:fytme/src/pages/createditprofile/prfilefields/username.dart';

import '../../helpers/app_config.dart' as config;
import '../../helpers/helper.dart';

class InstructorCreatEditProfile extends StatefulWidget {
  @override
  _InstructorCreatEditProfileState createState() =>
      _InstructorCreatEditProfileState();
}

class _InstructorCreatEditProfileState
    extends State<InstructorCreatEditProfile> {
  @override
  void initState() {
    super.initState();
    if (Storage().currentUser.isProfileCompleted) {
      BlocProvider.of<ProfileBloc>(context).add(ProfileGetEvent());
      setState(() {
        bildNow = false;
      });
    }
  }

  bool bildNow = true;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: config.App(context).appWidth(10),
            vertical: config.App(context).appHeight(2),
          ),
          child: BlocListener<ProfileBloc, ProfileState>(
            listenWhen: (previous, current) =>
                previous.status.status != current.status.status,
            listener: (contex, state) {
              switch (state.status.status) {
                case StateStatuses.loading:
                  CustomLoader().show(context);
                  break;
                case StateStatuses.failure:
                  CustomLoader().hide(context);
                  if (Storage().currentUser.isProfileCompleted) {
                    setState(() {
                      bildNow = true;
                    });
                  }
                  CustomWidgets.buildErrorSnackBar(
                      context, state.status.message);
                  break;
                case StateStatuses.success:
                  if (Storage().currentUser.isProfileCompleted) {
                    setState(() {
                      bildNow = true;
                    });
                  }
                  CustomLoader().hide(context);
                  CustomWidgets.buildSuccessSnackBar(
                      context, state.status.message);
                  break;
                default:
                  CustomLoader().hide(context);
                  break;
              }
            },
            child: !bildNow
                ? Container()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: config.App(context).appHeight(6),
                      ),
                      Container(
                        width: config.App(context).appWidth(80),
                        alignment: Alignment.center,
                        child: Text(
                          Storage().currentUser.isProfileCompleted
                              ? 'Edit your profile'
                              : "Create your profile",
                          style: Theme.of(context).textTheme.headline1,
                        ),
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(3),
                      ),
                      if (Storage().currentUser.isProfileCompleted)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Username",
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            UserNameEdit(),
                          ],
                        ),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      Text(
                        "Name",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(child: FirstName()),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(child: LastName()),
                        ],
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      Text(
                        "Date Of Birth",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      DateOfBirth(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      Text(
                        "Bio",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      BioInput(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Postcode",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                SizedBox(
                                  height: config.App(context).appHeight(1),
                                ),
                                Post(),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "City",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                SizedBox(
                                  height: config.App(context).appHeight(1),
                                ),
                                City(),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Text(
                        "Country",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Country(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      Text(
                        "contact number",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      Phone(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      if (Storage().currentUser.accountType == 'instructor')
                        SizedBox(
                          height: config.App(context).appHeight(2),
                        ),
                      Text(
                        "Address",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      MapAddress(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      if (Storage().currentUser.accountType == 'instructor')
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Hourly Rate",
                                    style:
                                        Theme.of(context).textTheme.subtitle1,
                                  ),
                                  SizedBox(
                                    height: config.App(context).appHeight(1),
                                  ),
                                  HrRate(),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Experience",
                                    style:
                                        Theme.of(context).textTheme.subtitle1,
                                  ),
                                  SizedBox(
                                    height: config.App(context).appHeight(1),
                                  ),
                                  Exp(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      if (Storage().currentUser.accountType == 'instructor')
                        Text(
                          "Expertise",
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      if (Storage().currentUser.accountType == 'instructor')
                        SizedBox(
                          height: config.App(context).appHeight(1),
                        ),
                      if (Storage().currentUser.accountType == 'instructor')
                        Speciality(),
                      if (Storage().currentUser.accountType == 'instructor')
                        SizedBox(
                          height: config.App(context).appHeight(2),
                        ),
                      Text(
                        "Gender",
                        style: Theme.of(context).textTheme.subtitle1),
                      SizedBox(height: config.App(context).appHeight(1)),
                      Gender(),
                      SizedBox(height: config.App(context).appHeight(1)),
                      Visibility(
                        visible: Storage().currentUser.accountType == 'instructor' ? true: false,
                        child: BlocBuilder<ProfileBloc, ProfileState>(
                            builder: (context, state) {
                        return Row(children: [
                          Row(mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Checkbox(
                                  value: BlocProvider.of<ProfileBloc>(context).state.isVideoTraining ? true : false,
                                  onChanged: (value) {
                                      BlocProvider.of<ProfileBloc>(context)
                                          .add(VideoTrainingEvent(isVideoTraining: value));
                                  },
                                  activeColor: Theme.of(context).primaryColor),
                              Text("Video training",
                                  style: Theme.of(context).textTheme.headline4),
                            ],
                          ),
                          Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Checkbox(value:
                                            BlocProvider.of<ProfileBloc>(context).state.isClientTravels ? true : false,
                                        onChanged: (value) {
                                          BlocProvider.of<ProfileBloc>(context)
                                              .add(TravelsToClientEvent(isClientTravels: value));
                                        },
                                        activeColor: Theme.of(context).primaryColor),
                                    Text("Travels to client",
                                        style: Theme.of(context).textTheme.headline4),
                                  ],
                                ),
                              ],
                        );
                   },),
                      ),
                      SizedBox(height: config.App(context).appHeight(4)),
                      if (Storage().currentUser.isProfileCompleted)
                        CallEditProfile(),
                      if (!Storage().currentUser.isProfileCompleted)
                        CallProfileButton(),
                      if (Storage().currentUser.isProfileCompleted)
                        Container(
                          width: config.App(context).appWidth(80),
                          alignment: Alignment.center,
                          child: TextButton(
                            child: Text("CANCEL",
                              style: Theme.of(context).textTheme.headline4.copyWith(fontWeight: FontWeight.w300)),
                            onPressed: () {Navigator.pop(context);},
                          ),
                        )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
