import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:fytme/bloc/securityBloc/security_bloc.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends StateMVC<ChangePasswordScreen> {
  TextEditingController _currentPassword = TextEditingController();
  TextEditingController _newPassword = TextEditingController();
  bool isShowCurrentPassword = true;
  bool isShowNewPassword = true;

  @override
  void initState() {
    super.initState();
  }

  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'password is required'),
    MinLengthValidator(8, errorText: 'password must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])', errorText: 'passwords must have at least one special character')
  ]);

  GlobalKey<FormState> _keyForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
            iconSize: 40,
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Image.asset(
            "assets/img/splash/logo.png",
            height: config.App(context).appHeight(3),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        body: BlocListener<SecurityBloc, SecurityState>(
          listenWhen: (previous, current) => previous.status.status != current.status.status,
          listener: (con, state) {
            switch (state.status.status) {
              case StateStatuses.loading:
                CustomLoader().show(context);
                break;
              case StateStatuses.failure:
                CustomLoader().hide(context);
                CustomWidgets.buildErrorSnackBar(context, state.status.message);
                break;
              case StateStatuses.success:
                CustomLoader().hide(context);
                CustomWidgets.buildSuccessSnackBar(context, state.status.message);
                break;
              default:
                CustomLoader().hide(context);
                break;
            }
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF2E94D0),
                  Color(0xFF2B7DC9),
                  Color(0xFF0B4B98),
                  Color(0xFF09111D),
                ],
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      top: config.App(context).appHeight(15),
                      bottom: config.App(context).appHeight(10),
                    ),
                    child: Text(
                      "change password".toUpperCase(),
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  Center(
                    child: Form(
                      key: _keyForm,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Current Password",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(1),
                          ),
                          Stack(
                            alignment: Alignment.topRight,
                            children: [
                              TextFormField(
                                controller: _currentPassword,
                                obscureText: isShowCurrentPassword,
                                validator: RequiredValidator(errorText: 'password is required'),
                                cursorColor: Colors.white,
                                style: Theme.of(context).textTheme.headline4,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                                  /*suffixIcon: Icon(
                                    Icons.lock,
                                    color: Colors.white,
                                  ),*/
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25,
                                    vertical: config.App(context).appHeight(1),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide.none,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide.none,
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide.none,
                                  ),
                                  hintStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.transparent,
                                    minimumSize: Size.zero, // <-- Add this
                                    padding: EdgeInsets.zero, // <-- and this
                                    elevation: 0),
                                onPressed: () {
                                  setState(() {
                                    isShowCurrentPassword = !isShowCurrentPassword;
                                  });
                                },
                                child: Icon(isShowCurrentPassword ? Icons.visibility : Icons.visibility_off),
                              )
                            ],
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(5),
                          ),
                          Text(
                            "New Password",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(1),
                          ),
                          Stack(
                            alignment: Alignment.topRight,
                            children: [
                              TextFormField(
                                controller: _newPassword,
                                obscureText: isShowNewPassword,
                                validator: passwordValidator,
                                cursorColor: Colors.white,
                                style: Theme.of(context).textTheme.headline4,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                                  /*suffixIcon: Icon(
                                    Icons.lock,
                                    color: Colors.white,
                                  ),*/
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 25,
                                    vertical: config.App(context).appHeight(1),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide.none,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide.none,
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide.none,
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.transparent,
                                    minimumSize: Size.zero, // <-- Add this
                                    padding: EdgeInsets.zero, // <-- and this
                                    elevation: 0),
                                onPressed: () {
                                  setState(() {
                                    isShowNewPassword = !isShowNewPassword;
                                  });
                                },
                                child: Icon(isShowNewPassword ? Icons.visibility : Icons.visibility_off),
                              )
                            ],
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(15),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: BlocBuilder<SecurityBloc, SecurityState>(builder: (context, snapshot) {
                              return CustomButton(
                                string: 'Submit',
                                callback: () {
                                  if (_keyForm.currentState.validate()) {
                                    _keyForm.currentState.save();
                                    context
                                        .read<SecurityBloc>()
                                        .add(ChangePasswordEvent(oldPassword: _currentPassword.text, newPassword: _newPassword.text));
                                  }
                                },
                              );
                            }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
