import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_event.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:pinput/pinput.dart';

import '../../common/constants.dart';
import '../../data/model/blocs/blocs.dart';
import '../../data/services/custom_loader.dart';
import '../../data/services/custom_snackbar.dart';
import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class ConfirmForgotPasswordScreen extends StatefulWidget {
  final String email;
  final int userId;

  const ConfirmForgotPasswordScreen({Key key, this.email, this.userId}) : super(key: key);

  @override
  _ConfirmForgotPasswordScreenState createState() => _ConfirmForgotPasswordScreenState();
}

class _ConfirmForgotPasswordScreenState extends StateMVC<ConfirmForgotPasswordScreen> {
  bool _clear = false;
  bool isShowNewPassword = true;

  String otp = '';

  final TextEditingController _pass = TextEditingController();

  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'password is required'),
    MinLengthValidator(8, errorText: 'password must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])', errorText: 'passwords must have at least one special character')
  ]);

  @override
  void initState() {
    super.initState();
  }

  GlobalKey<FormState> _form = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 56,
      textStyle: TextStyle(fontSize: 20, color: Color.fromRGBO(30, 60, 87, 1), fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(color: Color.fromRGBO(234, 239, 243, 1)),
        borderRadius: BorderRadius.circular(20),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: Color.fromRGBO(114, 178, 238, 1)),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration.copyWith(
        color: Color.fromRGBO(234, 239, 243, 1),
      ),
    );

    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
              icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
              iconSize: 40,
              onPressed: () {
                context.read<AuthenticationBloc>().add(ForgetFormEventAuth());
              }),
          title: Image.asset(
            "assets/img/splash/logo.png",
            height: config.App(context).appHeight(3),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF2E94D0),
                Color(0xFF2B7DC9),
                Color(0xFF0B4B98),
                Color(0xFF09111D),
              ],
            ),
          ),
          child: BlocListener<AuthBloc, AuthState>(
            listenWhen: (p, c) => p.status != c.status,
            listener: (BuildContext context, state) {
              switch (state.status.status) {
                case StateStatuses.loading:
                  CustomLoader().show(context);
                  break;
                case StateStatuses.failure:
                  CustomLoader().hide(context);
                  CustomWidgets.buildErrorSnackBar(context, state.status.message);
                  break;

                case StateStatuses.success:
                  CustomLoader().hide(context);
                  break;
                default:
                  CustomLoader().hide(context);
                  break;
              }
            },
            child: SingleChildScrollView(
              child: Form(
                key: _form,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: config.App(context).appHeight(30),
                    ),
                    Text(
                      "Please type the verification code sent to \n ${Storage().currentUser.email}",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    Pinput(
                      defaultPinTheme: defaultPinTheme,
                      length: 5,
                      focusedPinTheme: focusedPinTheme,
                      submittedPinTheme: submittedPinTheme,
                      onCompleted: (pin) {
                        setState(() {
                          otp = pin;
                        });
                      },
                      // actionButtonsEnabled: false,
                      // keyboardAction: TextInputAction.done,
                      // fieldsCount: 5,
                      // clearInput: _clear,
                      // inputDecoration: new InputDecoration(
                      //     focusedBorder: OutlineInputBorder(
                      //       borderSide: BorderSide(
                      //         color: Colors.white,
                      //         width: 2.0,
                      //       ),
                      //     ),
                      //     enabledBorder: OutlineInputBorder(
                      //       borderSide: BorderSide(
                      //           color: Theme.of(context).primaryColor,
                      //           width: 2.0),
                      //     ),
                      //     counterText: ""),
                      // onSubmit: (String pin) {
                      //   setState(() {
                      //     otp = pin;
                      //   });
                      // },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    Stack(
                      alignment: Alignment.topRight,
                      children: [
                        TextFormField(
                          obscureText: isShowNewPassword,
                          controller: _pass,
                          validator: passwordValidator,
                          cursorColor: Colors.white,
                          style: Theme.of(context).textTheme.headline4,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                            /*suffixIcon: Icon(
                          Icons.lock,
                          color: Colors.white,
                        ),*/
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: 25,
                              vertical: config.App(context).appHeight(1),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15),
                              borderSide: BorderSide.none,
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15),
                              borderSide: BorderSide.none,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15),
                              borderSide: BorderSide.none,
                            ),
                            hintText: "New Password",
                            hintStyle: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.transparent,
                              minimumSize: Size.zero, // <-- Add this
                              padding: EdgeInsets.zero, // <-- and this
                              elevation: 0),
                          onPressed: () {
                            setState(() {
                              isShowNewPassword = !isShowNewPassword;
                            });
                          },
                          child: Icon(isShowNewPassword ? Icons.visibility : Icons.visibility_off),
                        )
                      ],
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(8),
                    ),
                    BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, state) {
                        return TextButton(
                          onPressed: () {
                            if (_form.currentState.validate()) {
                              _form.currentState.save();
                              context
                                  .read<AuthBloc>()
                                  .add(ResetPasswordEvent(otp: otp, newPassword: _pass.text, userId: Storage().currentUser.userId));
                            }
                          },
                          child: Container(
                            width: config.App(context).appWidth(50),
                            padding: EdgeInsets.symmetric(
                              vertical: config.App(context).appHeight(3),
                              horizontal: 20,
                            ),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Text(
                              "submit".toUpperCase(),
                              style: Theme.of(context).textTheme.subtitle1,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
