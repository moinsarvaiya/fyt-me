import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/state_status.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';

import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class QuizScreen extends StatefulWidget {
  @override
  _QuizScreenState createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  String q1, q2, q3, q4, q5, q6, q7, q8, q9, q10;

  List<String> arrQuestion = [
    "What are your major goals/targets ?",
    "Are there any body parts in particular that you wish to train ?",
    "What are your top three fitness/nutrition goals ?",
    "How long after beginning your training do you expect it to take to begin to see changes in your body ?",
    "Do you have a specific event /  date you want to achieve these by ?",
    "How would you describe your current knowledge of exercise and fitness training ?",
    "If you currently exercise, would you say your routine is :",
    "What will motivate you to achieve your goals ?",
    "How motivated are you to achieving your goals ?",
    "What, if any, are your expected barriers towards your exercise program ?",
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        // appBar: AppBar(
        //   leading: IconButton(
        //     icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
        //     iconSize: 40,
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //   ),
        //   title: Image.asset(
        //     "assets/img/splash/logo.png",
        //     height: config.App(context).appHeight(6),
        //   ),
        //   centerTitle: true,
        //   backgroundColor: Colors.transparent,
        //   elevation: 0,
        //   automaticallyImplyLeading: false,
        // ),
        body: BlocListener<ProfileBloc, ProfileState>(
          listenWhen: (previous, current) => previous.status.status != current.status.status,
          listener: (BuildContext context, state) {
            switch (state.status.status) {
              case StateStatuses.loading:
                CustomLoader().show(context);
                break;
              case StateStatuses.failure:
                CustomLoader().hide(context);
                CustomWidgets.buildSuccessSnackBar(context, state.status.message);
                break;
              case StateStatuses.success:
                CustomLoader().hide(context);
                /*Navigator.of(context)
                  .pushNamedAndRemoveUntil('/Home', (Route<dynamic> route) => false);*/
                break;
              default:
                CustomLoader().hide(context);
                break;
            }
          },
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
              horizontal: config.App(context).appWidth(5),
              vertical: config.App(context).appHeight(3),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "We need to ask you some questions !",
                  style: TextStyle(
                    color: Color(0xFF2CC3D0),
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                Text(
                  "Your goals or aims are what you intend to achieve. Research shows a positive link between people with clearly thought-out goals and their level of success.",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
                SizedBox(
                  height: config.App(context).appHeight(3),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[0],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "Weight loss / gain",
                        "General fitness",
                        "Aerobic conditioning",
                        "Muscular endurance",
                        "Muscular strength",
                        "Improved flexibility",
                        "Nutrition/Diet",
                        "Other:"
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q1 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[1],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "Legs",
                        "Arms",
                        "Bum",
                        "Stomach",
                        "Chest",
                        "Other:",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q2 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[2],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "1) Buik (>20 %)",
                        "2) Lean (<10 %)",
                        "3) Medium",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q3 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[3],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "1 week",
                        "2 weeks",
                        "4 weeks",
                        "6 weeks",
                        "2 months",
                        "3 months",
                        "6 months",
                        "9 months",
                        "year +",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q4 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[4],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "Wedding",
                        "Beach Holiday",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q5 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[5],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "I am not familiar",
                        "I have a little experience",
                        "I am quite experienced",
                        "I am an expert",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q6 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[6],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "Ineffective",
                        "Effective",
                        " Very Effective",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q7 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[7],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "beach holiday in 6 months time",
                        "wedding dress to fit into",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q8 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[8],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: ["Least", "1", "2", "3", "4", "5", "Most"]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q9 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      arrQuestion[9],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    DropdownButtonFormField(
                      dropdownColor: Colors.white,
                      iconEnabledColor: Theme.of(context).primaryColor,
                      isExpanded: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 0,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        hintText: "Answer goes here ...",
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor,
                        ),
                      ),
                      // value: _activityLevel,
                      items: [
                        "long work hours",
                        "lack of facilities or time",
                      ]
                          .map(
                            (label) => DropdownMenuItem(
                              child: Text(
                                label,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                              value: label,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          q10 = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                  ],
                ),
                SizedBox(
                  height: config.App(context).appHeight(3),
                ),
                Center(
                  child: CustomButton(
                      callback: q1 == null ||
                              q2 == null ||
                              q3 == null ||
                              q4 == null ||
                              q5 == null ||
                              q6 == null ||
                              q7 == null ||
                              q8 == null ||
                              q9 == null ||
                              q10 == null
                          ? null
                          : () {
                              context.read<ProfileBloc>().add(SubmitQuestionAnswer(listAnswer: {
                                    "user_id": Storage().currentUser.userId,
                                    "question_answers": {
                                      '${arrQuestion[0]}': q1,
                                      '${arrQuestion[1]}': q2,
                                      '${arrQuestion[2]}': q3,
                                      '${arrQuestion[3]}': q4,
                                      '${arrQuestion[4]}': q5,
                                      '${arrQuestion[5]}': q6,
                                      '${arrQuestion[6]}': q7,
                                      '${arrQuestion[7]}': q8,
                                      '${arrQuestion[8]}': q9,
                                      '${arrQuestion[9]}': q10
                                    }
                                  }));
                            },
                      string: 'Save'),
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    child: Text(
                      'Cancel',
                      style: TextStyle(color: Colors.white70),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
