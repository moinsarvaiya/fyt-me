import 'dart:async';

import 'package:fbroadcast/fbroadcast.dart';
import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';
import 'package:provider/provider.dart';

import '../../bloc/providers_bloc/notification_bloc.dart';
import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';
import 'home/search_trainers.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  InAppNotificationBloc _bloc;
  Timer timer;

  @override
  void dispose() {
    super.dispose();
    timer?.cancel();
  }

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 60), (Timer t) => _bloc.getNotificationCount());
    //init();
  }

  void registerReceiverToUpdateName() {
    FBroadcast.instance().register('updateNotCount', (value, callback) {
      _bloc.getNotificationCount();
      print(' _bloc.getNotificationCount()');
    });
  }

  @override
  Widget build(BuildContext context) {
    _bloc = Provider.of<InAppNotificationBloc>(context);
    final screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          // leading: IconButton(
          //   icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
          //   iconSize: 40,
          //   onPressed: () {
          //     Navigator.pop(context);
          //   },
          // ),
          //backwardsCompatibility: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        body: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: <Widget>[
            Container(
              child: Center(
                child: Image.asset(
                  'assets/img/splash/splash.png',
                  fit: BoxFit.cover,
                  width: screenSize.width,
                ),
              ),
            ),
            Positioned(
              top: screenSize.height / 3.2,
              right: 30,
              child: Image.asset(
                "assets/img/splash/logo.png",
                width: screenSize.width / 2.2,
              ),
            ),
            Positioned(
              bottom: config.App(context).appHeight(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    Storage().userProfile.accountType == 'instructor' ? '' : "WELLNESS AND FITNESS \n INSTRUCTORS",
                    style: Theme.of(context).textTheme.subtitle1,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  TextButton(
                    onPressed: () {
                      //_bloc.getNotification();
                      _bloc.getNotificationCount();
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SearchTrainer(isLoggedIn: true)));
                      // Navigator.of(context).pushNamed('/Pages', arguments: 4);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 17),
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2,
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          if (Storage().userProfile.accountType != 'instructor')
                            Icon(
                              Icons.location_on,
                              color: Colors.white,
                            ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            Storage().userProfile.accountType == 'instructor' ? 'My Wallet' : "Search Near Me",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
