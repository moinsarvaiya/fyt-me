import 'dart:async';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  String checkPref;

  @override
  void initState() {
    _controller = VideoPlayerController.asset("assets/img/splash/loading.mp4");

    _controller.setVolume(1.0);

    _initializeVideoPlayerFuture = _controller.initialize().then((_) {
      if (mounted)
        setState(() {
          _controller.play();
        });
    });

    super.initState();
    // loadData();
  }

  @override
  void dispose() {
    _controller?.dispose();
    print("Dispose");
    super.dispose();
  }

  /*void loadData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      checkPref = prefs.getString('current_user');

      if (checkPref == null) {
        Timer(Duration(seconds: 3), () {
          Navigator.of(context).pushReplacementNamed('/Welcome', arguments: 2);
        });
      } else {
        userRepo.currentUser.value.token =
            json.decode(prefs.getString('current_user'))['auth_token'];
        userRepo.getCurrentUser();
        Timer(Duration(seconds: 3), () {
          userRepo.currentUser.value.accountType.isEmpty
              ? Navigator.of(context).pushReplacementNamed('/AccountType')
              : Navigator.of(context)
                  .pushReplacementNamed('/Pages', arguments: 0);
        });
      }
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return VideoPlayer(_controller);
          } else {
            return Center(
              child: Container(),
            );
          }
        },
      ),
    );
  }
}
