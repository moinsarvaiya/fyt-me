import 'package:flutter/material.dart';

import '../elements/DrawerWidget.dart';
import '../elements/logoButtonWidget.dart';
import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';
import 'contact_us.dart';
import 'home/search_trainers.dart';

class AboutWidget extends StatefulWidget {
  @override
  _AboutWidgetState createState() => _AboutWidgetState();
}

class _AboutWidgetState extends State<AboutWidget> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        backgroundColor: Color(0xFF537687),
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/arrow_back.png"),
            iconSize: 40,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: LogoButtonWidget(),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            Builder(
              builder: (context) => IconButton(
                icon: Image.asset("assets/img/icons/menu2.png"),
                iconSize: 40,
                onPressed: () => Scaffold.of(context).openDrawer(),
              ),
            ),
          ],
        ),
        drawer: DrawerWidget(),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: config.App(context).appHeight(5),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: config.App(context).appWidth(5),
                    ),
                    child: Text(
                      "Providing a unique environment packed with experts to support you on your journey",
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(1),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ContactUsWidget()),
                      );
                    },
                    child: Container(
                      width: config.App(context).appWidth(30),
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white.withOpacity(0.4),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("Contact Us", style: Theme.of(context).textTheme.headline4),
                          SizedBox(
                            width: 5,
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(12),
                  ),
                  Container(
                    width: config.App(context).appWidth(100),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorDark,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.elliptical(300, 30),
                          topLeft: Radius.elliptical(300, 30),
                        ),
                        border: Border.all(width: 0)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: config.App(context).appHeight(30),
                        ),
                        RichText(
                          text: new TextSpan(
                            style: new TextStyle(
                              fontSize: 24.0,
                              color: Color.fromRGBO(61, 131, 225, 1),
                            ),
                            children: <TextSpan>[
                              new TextSpan(
                                text: 'About',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              new TextSpan(
                                text: ' FYTme',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF557889),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(1),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: config.App(context).appWidth(15),
                          ),
                          child: Text(
                            "FYTme concept has been developed based on input from personal coaches and popular trainers.",
                            style: Theme.of(context).textTheme.headline4,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(5),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                text: new TextSpan(
                                  style: new TextStyle(
                                    fontSize: 24.0,
                                    color: Color.fromRGBO(61, 131, 225, 1),
                                  ),
                                  children: <TextSpan>[
                                    new TextSpan(
                                      text: 'Our',
                                      style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: ' Goal',
                                      style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xFF557889),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: config.App(context).appHeight(2),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: config.App(context).appWidth(10)),
                                child: Text(
                                  "The aim is to provide a unique environment packed with competent"
                                  " experts to support you on your journey to reach a desired level of mobility,"
                                  " range of motion, stability, and fitness based on your location.",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                              SizedBox(
                                height: config.App(context).appHeight(8),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: config.App(context).appWidth(100),
                    padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xFF819BA7),
                          Color(0xFFC4D0D6),
                        ],
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: config.App(context).appHeight(5),
                        ),
                        RichText(
                          text: new TextSpan(
                            style: new TextStyle(
                              fontSize: 24.0,
                              color: Color.fromRGBO(61, 131, 225, 1),
                            ),
                            children: <TextSpan>[
                              new TextSpan(
                                text: 'Our',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              new TextSpan(
                                text: ' Values',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF557889),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(3),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/img/about_us/undraw_personal_training_0dqn.png",
                              width: config.App(context).appWidth(25),
                              fit: BoxFit.contain,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "Vision Statement",
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFF537687),
                                    ),
                                  ),
                                  SizedBox(
                                    height: config.App(context).appHeight(1),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      "Improve people's fitness in a fun and convenient way"
                                      " by making it easier to find fitness professionals.",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w300,
                                      ),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(5),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Mission Statement",
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFF537687),
                                    ),
                                  ),
                                  SizedBox(
                                    height: config.App(context).appHeight(1),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 40),
                                    child: Text(
                                      "We are out to provide a quality health and wellness"
                                      " program to those who require the means, structure,"
                                      " education, and motivation to reach their health and fitness goals.",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w300,
                                      ),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Image.asset(
                              "assets/img/about_us/undraw_yoga_248n.png",
                              width: config.App(context).appWidth(20),
                              fit: BoxFit.contain,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(5),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              "assets/img/about_us/undraw_working_out_6psf.png",
                              width: config.App(context).appWidth(30),
                              fit: BoxFit.contain,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "Find Your Program",
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xFF537687),
                                    ),
                                  ),
                                  SizedBox(
                                    height: config.App(context).appHeight(1),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      "FYTme has many different programs that are perfect for you."
                                      " Choose which ones are best for you and find a trainer.",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w300,
                                      ),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(5),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: config.App(context).appWidth(100),
                    padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(width: 0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: config.App(context).appHeight(3),
                        ),
                        Text(
                          "Get Started with\nFYTme",
                          style: TextStyle(
                            color: Color(0xFF537687),
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(5),
                        ),
                        Container(
                          width: config.App(context).appWidth(55),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFF537687),
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: TextButton(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.location_on,
                                  color: Color(0xFF537687),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Search Instructors Near Me",
                                  style: TextStyle(
                                    color: Color(0xFF537687),
                                    fontSize: 11,
                                  ),
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SearchTrainer(
                                            isLoggedIn: true,
                                          )));
                            },
                          ),
                        ),
                        SizedBox(
                          height: config.App(context).appHeight(22),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Positioned(
                bottom: 0,
                width: config.App(context).appWidth(100),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "FYTme",
                        style: Theme.of(context).textTheme.headline4.copyWith(
                              color: Color(0xFF537687),
                            ),
                      ),
                      Column(
                        children: <Widget>[
                          Image.asset(
                            "assets/img/about_us/undraw_fitness_tracker_3033.png",
                          ),
                          Text(
                            "© COPYRIGHT 2020",
                            style: TextStyle(
                              fontSize: 8,
                              color: Color(0xFF537687),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                top: config.App(context).appHeight(30),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(0.5)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        width: config.App(context).appWidth(35),
                        height: config.App(context).appWidth(55),
                        child: Image.asset(
                          "assets/img/about_us/about_us_1.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            width: config.App(context).appWidth(29),
                            height: config.App(context).appWidth(27),
                            child: Image.asset(
                              "assets/img/about_us/about_us_2.png",
                              fit: BoxFit.cover,
                            ),
                          ),
                          SizedBox(
                            height: config.App(context).appWidth(0.5),
                          ),
                          Container(
                            width: config.App(context).appWidth(29),
                            height: config.App(context).appWidth(27),
                            child: Image.asset(
                              "assets/img/about_us/about_us_3.png",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: config.App(context).appWidth(35),
                        height: config.App(context).appWidth(55),
                        child: Image.asset(
                          "assets/img/about_us/about_us_4.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
