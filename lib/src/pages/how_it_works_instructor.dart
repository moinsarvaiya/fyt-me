import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';

import '../elements/DrawerWidget.dart';
import '../elements/logoButtonWidget.dart';
import '../helpers/app_config.dart' as config;

class HowItWorksInstructorWidget extends StatefulWidget {
  @override
  _HowItWorksInstructorWidgetState createState() => _HowItWorksInstructorWidgetState();
}

class _HowItWorksInstructorWidgetState extends State<HowItWorksInstructorWidget> {
  List<ContentConfig> slides = [];
  double sizeIndicator = 20;

  @override
  void initState() {
    super.initState();

    slides.add(
      new ContentConfig(
        title: "Create a profile",
        backgroundColor: Color(0xFF32404D),
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontStyle: FontStyle.italic,
        ),
        description: "First impressions last."
            " Create a personalised page to showcase your business and brand."
            "Showcase your unique page where potential clients will contact"
            " and learn about the services you offer, and your preferred venue(s).",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        pathImage: "assets/img/how_it_work/instructor_1.png",
      ),
    );
    slides.add(
      new ContentConfig(
        title: "Build your plan",
        backgroundColor: Color(0xFF3A7576),
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontStyle: FontStyle.italic,
        ),
        description: "Start developing your unique fitness or wellness plan tailored for your clients,"
            " describing your products and services to attract and keep",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        pathImage: "assets/img/how_it_work/instructor_2.png",
      ),
    );
    slides.add(
      new ContentConfig(
        title: "Book your calendar",
        backgroundColor: Color(0xFF09111D),
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontStyle: FontStyle.italic,
        ),
        description: "Don't waste time! Let your clients know your availability. "
            "Use the calendar to schedule training sessions, pre & post-training feedback meetings, "
            "and periodical evaluation of the client's progress.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        pathImage: "assets/img/how_it_work/instructor_3.png",
      ),
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = [];
    for (int i = 0; i < slides.length; i++) {
      ContentConfig currentSlide = slides[i];
      tabs.add(Stack(
        children: [
          Container(
            color: currentSlide.backgroundColor,
            width: double.infinity,
            height: double.infinity,
            child: Container(
              margin: EdgeInsets.only(bottom: 40.0, top: config.App(context).appHeight(2)),
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: config.App(context).appWidth(10),
                      vertical: 20,
                    ),
                    child: Text(
                      "HOW IT WORKS",
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  GestureDetector(
                    child: Image.asset(
                      currentSlide.pathImage,
                      width: config.App(context).appWidth(80),
                      height: config.App(context).appWidth(80),
                      fit: BoxFit.contain,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: config.App(context).appWidth(5)),
                    alignment: Alignment.centerRight,
                    child: Text(
                      "INSTRUCTOR",
                      style: Theme.of(context).textTheme.headline4.copyWith(
                            fontWeight: FontWeight.w300,
                          ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: config.App(context).appWidth(5)),
                    child: Text(
                      currentSlide.title,
                      style: currentSlide.styleTitle,
                      textAlign: TextAlign.right,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                    child: Text(
                      currentSlide.description,
                      style: currentSlide.styleDescription,
                      textAlign: TextAlign.justify,
                    ),
                    margin: EdgeInsets.only(top: 20.0),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: -1 * config.App(context).appWidth(25),
            left: 0,
            child: Container(
              width: config.App(context).appWidth(120),
              height: config.App(context).appWidth(120),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.2),
                shape: BoxShape.circle,
              ),
            ),
          ),
        ],
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset("assets/img/icons/arrow_back.png"),
          iconSize: 40,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: LogoButtonWidget(),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
              icon: Image.asset("assets/img/icons/menu2.png"),
              iconSize: 40,
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
        ],
      ),
      drawer: DrawerWidget(),
      body: IntroSlider(
        isAutoScroll: true,
        isLoopAutoScroll: true,
        curveScroll: Curves.bounceIn,
        // Content config
        // listContentConfig: listContentConfig,
        // backgroundColorAllTabs: Colors.grey,

        // Skip button
        // renderSkipBtn: renderSkipBtn(),
        // skipButtonStyle: myButtonStyle(),

        // Next button
        // renderNextBtn: renderNextBtn(),
        // onNextPress: onNextPress,
        // nextButtonStyle: myButtonStyle(),

        // Done button
        // renderDoneBtn: renderDoneBtn(),
        // onDonePress: onDonePress,
        // doneButtonStyle: myButtonStyle(),

        // Indicator
        indicatorConfig: IndicatorConfig(
          sizeIndicator: sizeIndicator,
          indicatorWidget: Container(
            width: sizeIndicator,
            height: 10,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.orange),
          ),
          activeIndicatorWidget: Container(
            width: sizeIndicator,
            height: 10,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.orange),
          ),
          spaceBetweenIndicator: 10,
          typeIndicatorAnimation: TypeIndicatorAnimation.sliding,
        ),

        // showPrevBtn: false,
        // showSkipBtn: false,
        // showNextBtn: false,
        // showDoneBtn: false,
        // List slides
        // slides: this.slides,

        // Dot indicator
        // colorDot: Colors.white,
        // sizeDot: 7.0,
        // typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,

        // Tabs
        listCustomTabs: this.renderListCustomTabs(),
        // backgroundColorAllSlides: Colors.transparent,

        // Behavior
        scrollPhysics: BouncingScrollPhysics(),

        // Show or hide status bar
        // hideStatusBar: true,
      ),
    );
  }
}
