import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/search_instructor_model.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/home/common/scaffold.dart';
import 'package:http/http.dart' as http;

import '../helpers/app_config.dart' as config;
import 'instructor_profile.dart';

class TopInstructorsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  const TopInstructorsWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _TopInstructorsWidgetState createState() => _TopInstructorsWidgetState();
}

class _TopInstructorsWidgetState extends State<TopInstructorsWidget> {
  bool _isVideoTraining = false;
  bool _isTravelClient = false;
  bool isLoading = false;

  Widget rateStar(int rate) {
    return Row(
      children: [
        for (var i = 0; i < 5; i++)
          Icon(
            Icons.star_rounded,
            color: Color(i < rate ? 0xFFED8A19 : 0xFF7F8389),
            size: 18,
          ),
      ],
    );
  }

  List<String> tabs = [
    "Muscular \nStrength".toUpperCase(),
    "Flexibility".toUpperCase(),
    "Cardio-Vascular \nEndurance".toUpperCase(),
    "Aerobic \nEndurance".toUpperCase(),
    "Muscular \nEndurance".toUpperCase(),
    "Body \nComposition".toUpperCase(),
  ];

  List<Widget> getTabs() {
    List<Widget> tabWidget = [];
    for (int i = 0; i < tabs.length; i++) {
      tabWidget.add(Tab(
        child: Text(
          tabs[i],
          style: Theme.of(context).textTheme.subtitle1,
          textAlign: TextAlign.center,
        ),
      ));
    }
    return tabWidget;
  }

  filterData() {
    String isVideoTraining = "False";
    if (_isVideoTraining) {
      isVideoTraining = "True";
    }
    String isTravelClient = "False";
    if (_isTravelClient) {
      isTravelClient = "True";
    }
    searchInstructor(query: "travel_to_client=$isTravelClient&video_training=$isVideoTraining");
  }

  @override
  void initState() {
    searchInstructor(query: tabs[0]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return /*WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: */
        MyScaffold(
      bottomMenuIndex: 0,
      /*appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/menu1.png"),
            iconSize: 40,
            onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
          ),
          title: LogoButtonWidget(),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            new ProfileButtonWidget(
              size: 40,
              icon: Image.asset(
                  ""),
            ),
          ],
        ),*/
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: config.App(context).appHeight(5),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: config.App(context).appWidth(10),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: config.App(context).appWidth(3),
                      vertical: config.App(context).appWidth(2),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFF7F8389),
                    ),
                    child: PopupMenuButton(
                      offset: Offset(-10.0, 50.0),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                      color: Color(0xFF7F8389),
                      child: Icon(
                        Icons.tune_rounded,
                        color: Colors.white,
                      ),
                      itemBuilder: (BuildContext context) => [
                        PopupMenuItem(
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                _isVideoTraining = !_isVideoTraining;
                              });
                              Navigator.pop(context);
                              filterData();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  _isVideoTraining ? Icons.check_box_rounded : Icons.check_box_outline_blank_rounded,
                                  color: Colors.white,
                                  size: 20,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: Text(
                                    "Video Training",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        PopupMenuItem(
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                _isTravelClient = !_isTravelClient;
                              });
                              Navigator.pop(context);
                              filterData();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  _isTravelClient ? Icons.check_box_rounded : Icons.check_box_outline_blank_rounded,
                                  color: Colors.white,
                                  size: 20,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: Text(
                                    "Travels to Client",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                      onSelected: (type) {
                        print(type);
                      },
                    ),
                  ),
                  /*SizedBox(
                    width: config.App(context).appWidth(5),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: config.App(context).appWidth(3),
                      vertical: config.App(context).appWidth(2),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFF7F8389),
                    ),
                    child: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                  )*/
                ],
              ),
              SizedBox(
                height: config.App(context).appHeight(4),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(5),
                ),
                child: Center(
                  child: Text(
                    "TOP INSTRUCTORS",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(3),
              ),
              DefaultTabController(
                length: 6,
                child: Material(
                  color: Color(0xFF303743),
                  child: TabBar(
                    isScrollable: true,
                    tabs: getTabs(),
                    indicator: BoxDecoration(color: Theme.of(context).primaryColor),
                    onTap: (index) {
                      searchInstructor(query: tabs[index]);
                    },
                  ),
                ),
              ),
              Expanded(
                child: _listModel.isNotEmpty
                    ? Padding(
                        padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5), vertical: 10),
                        child: ListView.builder(
                            itemCount: _listModel.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                      builder: (BuildContext context) => InstructorProfileWidget(_listModel[index]),
                                    ),
                                  );
                                },
                                child: Container(
                                  margin: EdgeInsets.symmetric(vertical: 8),
                                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                                  decoration: BoxDecoration(
                                    color: Color(0xFF3D434D),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.all(Radius.circular(300)),
                                        child: CachedNetworkImage(
                                          imageUrl: _listModel[index].profilePicture != null
                                              ? _listModel[index].profilePicture.contains('https://dev.fytme.co.uk')
                                                  ? _listModel[index].profilePicture
                                                  : 'https://dev.fytme.co.uk${_listModel[index].profilePicture}'
                                              : '',
                                          fit: BoxFit.cover,
                                          errorWidget: (context, url, error) => Icon(
                                            Icons.person,
                                            color: Colors.white,
                                          ),
                                          width: config.App(context).appWidth(25),
                                          height: config.App(context).appWidth(25),
                                        ),
                                        /*Image.asset(
                                            "assets/img/instructors/instructor_1.png",
                                            width: config.App(context)
                                                .appWidth(25),
                                            height: config.App(context)
                                                .appWidth(25),
                                            fit: BoxFit.contain,
                                          ),*/
                                      ),
                                      SizedBox(
                                        width: config.App(context).appWidth(5),
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(left: 3.0),
                                            child: Text(
                                              '${_listModel[index].firstName} ${_listModel[index].lastName}',
                                              style: Theme.of(context).textTheme.subtitle1,
                                            ),
                                          ),
                                          SizedBox(
                                            height: config.App(context).appHeight(0.5),
                                          ),
                                          Row(
                                            children: [
                                              Icon(
                                                Icons.location_on,
                                                color: Colors.white,
                                                size: 16,
                                              ),
                                              Container(
                                                width: config.App(context).appWidth(50),
                                                child: RichText(
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  text: TextSpan(
                                                    style: Theme.of(context).textTheme.headline4,
                                                    text: _listModel[index].address,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: config.App(context).appHeight(1),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              rateStar(_listModel[index].ratings),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              InkWell(
                                                  onTap: () {
                                                    Navigator.of(context).pushNamed('/Reviews', arguments: _listModel[index]);
                                                  },
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        "(",
                                                        style: Theme.of(context).textTheme.headline4.copyWith(),
                                                      ),
                                                      Text(
                                                        "${_listModel[index].ratings} Reviews",
                                                        style: Theme.of(context).textTheme.headline4.copyWith(
                                                              decoration: TextDecoration.underline,
                                                            ),
                                                      ),
                                                      Text(
                                                        ")",
                                                        style: Theme.of(context).textTheme.headline4.copyWith(),
                                                      ),
                                                    ],
                                                  )),
                                            ],
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }))
                    : !isLoading
                        ? Center(
                            child: Text(
                            'No instructors found',
                            style: TextStyle(color: Colors.white),
                          ))
                        : Container(),
              )
            ],
          ),
          isLoading
              ? AlertDialog(
                  content: Container(
                    height: MediaQuery.of(context).size.height * .1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                        ),
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
      // ),
    );
  }

  List<InstructorModel> _listModel = [];

  searchInstructor({String query}) async {
    final result = await InternetAddress.lookup("google.com");
    if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
      CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
    } else {
      try {
        setState(() {
          isLoading = true;
        });
        final String url = '${ServerAddress.api_base_url}instructor-search/?$query';
        final client = new http.Client();
        final response = await client.get(Uri.parse(url));
        printLog('total instructor loaded');
        print('$url\n${response.body}');
        if (response.statusCode == 200) {
          List _list = jsonDecode(response.body);
          _list = _list.where((i) => (i["location_coordinates"] != null && i["id"] != Storage().currentUser.userId)).toList();

          setState(() {
            isLoading = false;
            _listModel = [];
            if (_list.isNotEmpty) {
              //_listModel = _list.where((i) => i["id"] != Storage().currentUser.userId).toList();
              _listModel = _list.map((e) => InstructorModel.fromJson(e as Map)).toList();
            }
          });
        } else {
          setState(() {
            isLoading = false;
            throw Exception();
          });
        }
      } catch (err) {
        setState(() {
          isLoading = false;
        });
        rethrow;
      }
    }
  }
}
