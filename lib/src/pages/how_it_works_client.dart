import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';

import '../elements/DrawerWidget.dart';
import '../elements/logoButtonWidget.dart';
import '../helpers/app_config.dart' as config;

class HowItWorksClientWidget extends StatefulWidget {
  @override
  _HowItWorksClientWidgetState createState() => _HowItWorksClientWidgetState();
}

class _HowItWorksClientWidgetState extends State<HowItWorksClientWidget> {
  List<ContentConfig> slides = [];
  double sizeIndicator = 20;

  @override
  void initState() {
    super.initState();
    slides.add(
      new ContentConfig(
        title: "Create a profile",
        backgroundColor: Color(0xFF204B62),
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontStyle: FontStyle.italic,
        ),
        description: "Every day the clock resets and so should you. "
            "Build a profile to gain access to fitness experts near you, "
            "contact them via in-app messaging services, "
            "book your session, and keep track of your health and fitness progress.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        pathImage: "assets/img/how_it_work/client_1.png",
      ),
    );
    slides.add(
      new ContentConfig(
        title: "Find an instructor",
        backgroundColor: Color(0xFF678B9D),
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontStyle: FontStyle.italic,
        ),
        description: "A dedicated professional, personally devoted to you. "
            "Search by post-code or specific criteria, "
            "find a personalized instructor, and get started on your journey.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        pathImage: "assets/img/how_it_work/client_2.png",
      ),
    );
    slides.add(
      new ContentConfig(
        title: "Select programs",
        backgroundColor: Color(0xFF555B6D),
        styleTitle: TextStyle(
          color: Colors.white,
          fontSize: 25,
          fontStyle: FontStyle.italic,
        ),
        description: "We offer a range of services tailored to all your physical, mental, and spiritual needs: "
            "anytime, anywhere. Professional instructors ready to assist you and replicate that gym "
            "experience and boost your confidence.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 16,
        ),
        pathImage: "assets/img/how_it_work/client_3.png",
      ),
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = [];
    for (int i = 0; i < slides.length; i++) {
      ContentConfig currentSlide = slides[i];
      tabs.add(Stack(
        children: [
          Positioned(
            bottom: -1 * config.App(context).appWidth(25),
            left: 0,
            child: Container(
              width: config.App(context).appWidth(120),
              height: config.App(context).appWidth(120),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.2),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Container(
            color: currentSlide.backgroundColor,
            width: double.infinity,
            height: double.infinity,
            child: Container(
              margin: EdgeInsets.only(bottom: 40.0, top: config.App(context).appHeight(2)),
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: config.App(context).appWidth(10),
                      vertical: 20,
                    ),
                    child: Text(
                      "HOW IT WORKS",
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  GestureDetector(
                    child: Image.asset(
                      currentSlide.pathImage,
                      width: config.App(context).appWidth(75),
                      height: config.App(context).appWidth(75),
                      fit: BoxFit.contain,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: config.App(context).appWidth(5)),
                    alignment: Alignment.centerRight,
                    child: Text(
                      "CLIENT",
                      style: Theme.of(context).textTheme.headline4.copyWith(
                            fontWeight: FontWeight.w300,
                          ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: config.App(context).appWidth(5)),
                    child: Text(
                      currentSlide.title,
                      style: currentSlide.styleTitle,
                      textAlign: TextAlign.right,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                    child: Text(
                      currentSlide.description,
                      style: currentSlide.styleDescription,
                      textAlign: TextAlign.justify,
                    ),
                    margin: EdgeInsets.only(top: 20.0),
                  ),
                  if (i == 2)
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed('/Programs');
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("VIEW OUR PROGRAMS",
                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300, color: Colors.white, decoration: TextDecoration.underline)),
                          SizedBox(width: config.App(context).appWidth(2)),
                          Icon(Icons.arrow_forward_ios, color: Colors.white, size: 18),
                        ],
                      ),
                    )
                  else
                    SizedBox(),
                ],
              ),
            ),
          ),
        ],
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset("assets/img/icons/arrow_back.png"),
          iconSize: 40,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: LogoButtonWidget(),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
              icon: Image.asset("assets/img/icons/menu2.png"),
              iconSize: 40,
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
        ],
      ),
      drawer: DrawerWidget(),
      body: IntroSlider(
        isAutoScroll: true,
        isLoopAutoScroll: true,
        curveScroll: Curves.bounceIn,
        // Content config
        // listContentConfig: listContentConfig,
        // backgroundColorAllTabs: Colors.grey,

        // Skip button
        // renderSkipBtn: renderSkipBtn(),
        // skipButtonStyle: myButtonStyle(),

        // Next button
        // renderNextBtn: renderNextBtn(),
        // onNextPress: onNextPress,
        // nextButtonStyle: myButtonStyle(),

        // Done button
        // renderDoneBtn: renderDoneBtn(),
        // onDonePress: onDonePress,
        // doneButtonStyle: myButtonStyle(),

        // Indicator
        indicatorConfig: IndicatorConfig(
          sizeIndicator: sizeIndicator,
          indicatorWidget: Container(
            width: sizeIndicator,
            height: 10,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.orange),
          ),
          activeIndicatorWidget: Container(
            width: sizeIndicator,
            height: 10,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4), color: Colors.orange),
          ),
          spaceBetweenIndicator: 10,
          typeIndicatorAnimation: TypeIndicatorAnimation.sliding,
        ),

        // showPrevBtn: false,
        // showSkipBtn: false,
        // showNextBtn: false,
        // showDoneBtn: false,
        // List slides
        // slides: this.slides,

        // Dot indicator
        // colorDot: Colors.white,
        // sizeDot: 7.0,
        // typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,

        // Tabs
        listCustomTabs: this.renderListCustomTabs(),
        // backgroundColorAllSlides: Colors.transparent,

        // Behavior
        scrollPhysics: BouncingScrollPhysics(),

        // Show or hide status bar
        // hideStatusBar: true,
      ),
    );
  }
}
