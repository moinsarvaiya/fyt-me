import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/basic_programs.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/home/common/scaffold.dart';
import 'package:http/http.dart' as http;

import '../helpers/app_config.dart' as config;

class ProgramsWidget extends StatefulWidget {
  @override
  _ProgramsWidgetState createState() => _ProgramsWidgetState();
}

class _ProgramsWidgetState extends State<ProgramsWidget> {
  bool isCommentHide = false;
  TextEditingController commentController = TextEditingController();
  List<BasicPrograms> programList = [];
  double currentRating = 3.0;

  @override
  void initState() {
    // Future.delayed(Duration(seconds: 3), () => {dialog()});
    super.initState();
    getProgramsData();
  }

  showLoader() {
    setState(() {
      CustomLoader().show(context);
    });
  }

  dismissLoader() {
    setState(() {
      CustomLoader().hide(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
        body: SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10), vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: config.App(context).appHeight(5)),
            Text("PROGRAMS", style: Theme.of(context).textTheme.headline1),
            SizedBox(height: config.App(context).appHeight(5)),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: programList.length,
              itemBuilder: (BuildContext context, int index) {
                return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 500),
                  child: SlideAnimation(
                    verticalOffset: 20.0,
                    child: FadeInAnimation(
                      child: uiOfProgramsList(index),
                    ),
                  ),
                );
              },
            ),
            // StaggeredGridView.countBuilder(
            //   crossAxisCount: 4,
            //   physics: NeverScrollableScrollPhysics(),
            //   itemCount: programList.length,
            //   itemBuilder: (BuildContext context, int index) => uiOfProgramsList(index),
            //   staggeredTileBuilder: (int index) => new StaggeredTile.fit(2),
            //   mainAxisSpacing: 20.0,
            //   crossAxisSpacing: 10.0,
            //   shrinkWrap: true,
            // ),
          ],
        ),
      ),
    )

        // body: SingleChildScrollView(
        //   padding: EdgeInsets.symmetric(
        //       horizontal: config.App(context).appWidth(10), vertical: 10),
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: <Widget>[
        //       SizedBox(
        //         height: config.App(context).appHeight(5),
        //       ),
        //       Text(
        //         "PROGRAMS",
        //         style: Theme.of(context).textTheme.headline1,
        //       ),
        //       SizedBox(
        //         height: config.App(context).appHeight(5),
        //       ),
        //       Row(
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           GestureDetector(
        //             onTap: () {
        //               print('1');
        //             },
        //             child: Container(
        //               height: config.App(context).appHeight(35),
        //               width: config.App(context).appWidth(35),
        //               decoration: BoxDecoration(
        //                 image: DecorationImage(
        //                   image: AssetImage(
        //                     "assets/img/programs/AdobeStock_247705981.png",
        //                   ),
        //                   fit: BoxFit.contain,
        //                 ),
        //               ),
        //               child: Center(
        //                 child: Container(
        //                   padding:
        //                       EdgeInsets.symmetric(horizontal: 5, vertical: 3),
        //                   decoration: BoxDecoration(
        //                     borderRadius: BorderRadius.circular(8),
        //                     color: Theme.of(context)
        //                         .primaryColorDark
        //                         .withOpacity(0.5),
        //                   ),
        //                   child: Text(
        //                     "MUSCULAR\nSTRENGTH",
        //                     textAlign: TextAlign.center,
        //                     style: Theme.of(context).textTheme.subtitle1,
        //                   ),
        //                 ),
        //               ),
        //             ),
        //           ),
        //           Column(
        //             mainAxisAlignment: MainAxisAlignment.start,
        //             children: [
        //               GestureDetector(
        //                 onTap: () {},
        //                 child: Container(
        //                   height: config.App(context).appHeight(20),
        //                   width: config.App(context).appWidth(35),
        //                   decoration: BoxDecoration(
        //                     image: DecorationImage(
        //                       image: AssetImage(
        //                         "assets/img/programs/AdobeStock_252411308.png",
        //                       ),
        //                       fit: BoxFit.contain,
        //                     ),
        //                   ),
        //                   child: Center(
        //                     child: Container(
        //                       padding: EdgeInsets.symmetric(
        //                           horizontal: 5, vertical: 3),
        //                       decoration: BoxDecoration(
        //                         borderRadius: BorderRadius.circular(8),
        //                         color: Theme.of(context)
        //                             .primaryColorDark
        //                             .withOpacity(0.5),
        //                       ),
        //                       child: Text(
        //                         "MUSCULAR \n ENDURANCE",
        //                         textAlign: TextAlign.center,
        //                         style: Theme.of(context).textTheme.subtitle1,
        //                       ),
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //               GestureDetector(
        //                 onTap: () {},
        //                 child: Container(
        //                   height: config.App(context).appHeight(15),
        //                   width: config.App(context).appWidth(35),
        //                   decoration: BoxDecoration(
        //                     image: DecorationImage(
        //                       image: AssetImage(
        //                         "assets/img/programs/AdobeStock_122954687.png",
        //                       ),
        //                       fit: BoxFit.contain,
        //                     ),
        //                   ),
        //                   child: Center(
        //                     child: Container(
        //                       padding: EdgeInsets.symmetric(
        //                           horizontal: 5, vertical: 3),
        //                       decoration: BoxDecoration(
        //                         borderRadius: BorderRadius.circular(8),
        //                         color: Theme.of(context)
        //                             .primaryColorDark
        //                             .withOpacity(0.5),
        //                       ),
        //                       child: Text(
        //                         "FLEXIBILITY",
        //                         textAlign: TextAlign.center,
        //                         style: Theme.of(context).textTheme.subtitle1,
        //                       ),
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //             ],
        //           )
        //         ],
        //       ),
        //       SizedBox(
        //         height: config.App(context).appHeight(2),
        //       ),
        //       Row(
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           Column(
        //             mainAxisAlignment: MainAxisAlignment.start,
        //             children: [
        //               GestureDetector(
        //                 onTap: () {},
        //                 child: Container(
        //                   height: config.App(context).appHeight(20),
        //                   width: config.App(context).appWidth(35),
        //                   decoration: BoxDecoration(
        //                     image: DecorationImage(
        //                       image: AssetImage(
        //                         "assets/img/programs/AdobeStock_210275653.png",
        //                       ),
        //                       fit: BoxFit.contain,
        //                     ),
        //                   ),
        //                   child: Center(
        //                     child: Container(
        //                       padding: EdgeInsets.symmetric(
        //                           horizontal: 5, vertical: 3),
        //                       decoration: BoxDecoration(
        //                         borderRadius: BorderRadius.circular(8),
        //                         color: Theme.of(context)
        //                             .primaryColorDark
        //                             .withOpacity(0.5),
        //                       ),
        //                       child: Text(
        //                         "BODY \n COMP",
        //                         textAlign: TextAlign.center,
        //                         style: Theme.of(context).textTheme.subtitle1,
        //                       ),
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //               GestureDetector(
        //                 onTap: () {},
        //                 child: Container(
        //                   height: config.App(context).appHeight(15),
        //                   width: config.App(context).appWidth(35),
        //                   decoration: BoxDecoration(
        //                     image: DecorationImage(
        //                       image: AssetImage("assets/img/programs/AdobeStock_201234793.png"),
        //                       fit: BoxFit.contain,
        //                     ),
        //                   ),
        //                   child: Center(
        //                     child: Container(
        //                       padding: EdgeInsets.symmetric(
        //                           horizontal: 5, vertical: 3),
        //                       decoration: BoxDecoration(
        //                         borderRadius: BorderRadius.circular(8),
        //                         color: Theme.of(context)
        //                             .primaryColorDark
        //                             .withOpacity(0.5),
        //                       ),
        //                       child: Text(
        //                         "AEROBIC \n ENDURANCE",
        //                         textAlign: TextAlign.center,
        //                         style: Theme.of(context).textTheme.subtitle1,
        //                       ),
        //                     ),
        //                   ),
        //                 ),
        //               ),
        //             ],
        //           ),
        //           GestureDetector(
        //             onTap: () {},
        //             child: Container(
        //               height: config.App(context).appHeight(35),
        //               width: config.App(context).appWidth(35),
        //               decoration: BoxDecoration(
        //                 image: DecorationImage(
        //                   image: AssetImage(
        //                     "assets/img/programs/AdobeStock_117730382.png",
        //                   ),
        //                   fit: BoxFit.contain,
        //                 ),
        //               ),
        //               child: Center(
        //                 child: Container(
        //                   padding:
        //                       EdgeInsets.symmetric(horizontal: 5, vertical: 3),
        //                   decoration: BoxDecoration(
        //                     borderRadius: BorderRadius.circular(8),
        //                     color: Theme.of(context)
        //                         .primaryColorDark
        //                         .withOpacity(0.5),
        //                   ),
        //                   child: Text(
        //                     "CARDIO",
        //                     textAlign: TextAlign.center,
        //                     style: Theme.of(context).textTheme.subtitle1,
        //                   ),
        //                 ),
        //               ),
        //             ),
        //           ),
        //         ],
        //       ),
        //     ],
        //   ),
        // ),
        );
  }

  // height: config.App(context).appHeight(35),
  // width: config.App(context).appWidth(35),

  uiOfProgramsList(int index) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          CachedNetworkImage(
            fit: BoxFit.scaleDown,
            imageUrl: programList[index].picture.toString(),
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Image.network(
              "https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 3, vertical: 3),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: Theme.of(context).primaryColorDark.withOpacity(0.5)),
            child: Text(
              programList[index].programName.toString(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
        ],
      ),
    );
  }

  getProgramsData() async {
    bool isInternet = await isInternetConnected();
    if (!isInternet) {
      CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
      CustomLoader().hide(context);
    } else {
      CustomLoader().show(context);
      final String url = '${ServerAddress.api_base_url}basic-program/';
      final client = new http.Client();
      var token = Storage().currentUser.token;
      print(token);
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('======== getUnreadNotifications');
      printLog(response.body);
      if (response.statusCode == 200) {
        setState(() {
          final responseBody = jsonDecode(response.body);
          if (responseBody != null)
            programList.addAll((responseBody['data'] as List).map((e) => BasicPrograms.fromJson(e)).toList());
          else
            print("Error");
          CustomLoader().hide(context);
        });
      } else {
        // getNotification();
      }
    }
  }

  Future<bool> isInternetConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}
