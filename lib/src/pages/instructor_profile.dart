import 'package:badges/badges.dart' as badges;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fytme/data/model/search_instructor_model.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/user_image.dart';
import 'package:fytme/src/pages/home/account.dart';

import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';
import 'createditprofile/proposal.dart';

class InstructorProfileWidget extends StatefulWidget {
  final InstructorModel instructorDetail;

  InstructorProfileWidget(this.instructorDetail);

  @override
  _InstructorProfileWidgetState createState() => _InstructorProfileWidgetState();
}

Widget rateStar(int rate) {
  return Row(
    children: [
      for (var i = 0; i < 5; i++)
        Icon(
          Icons.star_rounded,
          color: Color(i < rate ? 0xFFED8A19 : 0xFF7F8389),
          size: 16,
        ),
    ],
  );
}

class _InstructorProfileWidgetState extends State<InstructorProfileWidget> {
  DateTime bookingTime;
  DateTime selDate = DateTime.now();
  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController()
      ..addListener(() {
        print("offset = ${_controller.offset}");
      });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
            iconSize: 40,
            onPressed: () => Navigator.pop(context),
          ),
          title: Image.asset(
            "assets/img/splash/logo.png",
            height: config.App(context).appHeight(3),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => AccountWidget()));
              },
              child: UserImage(
                radius: 10,
              ),
            ),
            /* new ProfileButtonWidget(
              size: 40,
              icon: Image.asset(
                  "assets/img/profile/ayo-ogunseinde-0xWXyaa8bTQ-unsplash.png"),
            ), */
          ],
        ),
        body: SingleChildScrollView(
          // padding: EdgeInsets.only(top: 10),
          // padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(
                height: config.App(context).appHeight(5),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(8),
                ),
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        top: config.App(context).appWidth(23),
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: config.Colors().secondDarkColor(1),
                        ),
                        // width: config.App(context).appWidth(80),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconButton(
                                  icon: SvgPicture.asset("assets/img/icons/comm-17_chat.svg"),
                                  onPressed: () {
                                    print("Message");
                                  },
                                ),
                                IconButton(
                                  icon: SvgPicture.asset("assets/img/icons/video-call.svg"),
                                  onPressed: () {
                                    print("Video call");
                                  },
                                )
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(4),
                            ),
                            Text(
                              widget.instructorDetail.firstName + " " + widget.instructorDetail.lastName,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                            Text(
                              widget.instructorDetail.specialty.isNotEmpty ? "${widget.instructorDetail.specialty.join(', ')}" : '',
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.center,
                              // "Pilates Instructor",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                rateStar(widget.instructorDetail.ratings),
                                SizedBox(
                                  width: 5,
                                ),
                                InkWell(
                                    onTap: () {
                                      Navigator.of(context).pushNamed('/Reviews', arguments: widget.instructorDetail);
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                          "(",
                                          style: Theme.of(context).textTheme.headline4.copyWith(),
                                        ),
                                        Text(
                                          "${widget.instructorDetail.ratings} Reviews",
                                          style: Theme.of(context).textTheme.headline4.copyWith(
                                                decoration: TextDecoration.underline,
                                              ),
                                        ),
                                        Text(
                                          ")",
                                          style: Theme.of(context).textTheme.headline4.copyWith(),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Icon(
                                  Icons.check_box_rounded,
                                  color: Colors.white,
                                ),
                                Expanded(
                                  child: Text(
                                    "Video Training",
                                    style: Theme.of(context).textTheme.headline4,
                                  ),
                                ),
                                Icon(
                                  Icons.check_box_rounded,
                                  color: Colors.white,
                                ),
                                Expanded(
                                  child: Text(
                                    "Travels to Client",
                                    style: Theme.of(context).textTheme.headline4,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width - 150,
                                  child: Text(
                                    widget.instructorDetail.address,
                                    // overflow: TextOverflow.clip,
                                    // maxLines: 1,
                                    style: Theme.of(context).textTheme.headline4,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                          ],
                        ),
                      ),
                    ),
                    badges.Badge(
                      position: badges.BadgePosition.topEnd(
                        top: 0,
                        end: 0,
                      ),
                      badgeContent: Image.asset("assets/img/icons/premium-quality.png"),
                      child: Container(
                        decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.all(Radius.circular(300.0))),
                        child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(300)),
                            child: CachedNetworkImage(
                              width: config.App(context).appWidth(40),
                              height: config.App(context).appWidth(40),
                              imageUrl: widget.instructorDetail.profilePicture != null
                                  ? widget.instructorDetail.profilePicture.contains('https://dev.fytme.co.uk')
                                      ? widget.instructorDetail.profilePicture
                                      : 'https://dev.fytme.co.uk${widget.instructorDetail.profilePicture}'
                                  : '',
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                              errorWidget: (context, url, error) => Icon(
                                Icons.person,
                                color: Colors.white,
                              ),
                            )),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(5),
              ),
              Calendar(widget.instructorDetail),
            ],
          ),
        ),
      ),
    );
  }
}

class Calendar extends StatefulWidget {
  final InstructorModel instructorDetail;

  Calendar(this.instructorDetail);

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  DateTime selectedDate = DateTime.now(); // TO tracking date
  int selectedTime = 0; // TO tracking date

  int currentDateSelectedIndex = 0; //For Horizontal Date
  int currentTimeSelectedIndex = 0; //For Horizontal Date
  ScrollController scrollController = ScrollController(); //To Track Scroll of ListView

  List<String> listOfMonths = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "Jun",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  List<String> listOfDays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

  List<Map<String, String>> listOfTimes = [
    {'value': "00:00 AM", 'key': '0'},
    {'value': "01:00 AM", 'key': '1'},
    {'value': "02:00 AM", 'key': '2'},
    {'value': "03:00 AM", 'key': '3'},
    {'value': "04:00 AM", 'key': '4'},
    {'value': "05:00 AM", 'key': '5'},
    {'value': "06:00 AM", 'key': '6'},
    {'value': "07:00 AM", 'key': '7'},
    {'value': "08:00 AM", 'key': '8'},
    {'value': "09:00 AM", 'key': '9'},
    {'value': "10:00 AM", 'key': '10'},
    {'value': "11:00 AM", 'key': '11'},
    {'value': "12:00 AM", 'key': '12'},
    {'value': "01:00 PM", 'key': '13'},
    {'value': "02:00 PM", 'key': '14'},
    {'value': "03:00 PM", 'key': '15'},
    {'value': "04:00 PM", 'key': '16'},
    {'value': "05:00 PM", 'key': '17'},
    {'value': "06:00 PM", 'key': '18'},
    {'value': "07:00 PM", 'key': '19'},
    {'value': "08:00 PM", 'key': '20'},
    {'value': "09:00 PM", 'key': '21'},
    {'value': "10:00 PM", 'key': '22'},
    {'value': "11:00 PM", 'key': '23'},
  ];

  /*List<String> listOfTimes = [
    "00:00 AM",
    "01:00 AM",
    "02:00 AM",
    "03:00 AM",
    "04:00 AM",
    "05:00 AM",
    "06:00 AM",
    "07:00 AM",
    "08:00 AM",
    "09:00 AM",
    "10:00 AM",
    "11:00 AM",
    "12:00 PM",
    "01:00 PM",
    "02:00 PM",
    "03:00 PM",
    "04:00 PM",
    "05:00 PM",
    "06:00 PM",
    "07:00 PM",
    "08:00 PM",
    "09:00 PM",
    "10:00 PM",
    "11:00 PM",
  ];*/

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: config.App(context).appWidth(100),
      // height: config.App(context).appHeight(50),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: config.Colors().secondDarkColor(1),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: config.App(context).appWidth(8),
              vertical: config.App(context).appWidth(4),
            ),
            child: Text(
              listOfMonths[selectedDate.month - 1],
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          SizedBox(height: config.App(context).appHeight(1)),
          Container(
              height: config.App(context).appHeight(12),
              child: ListView.separated(
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(8),
                ),
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(width: config.App(context).appHeight(2));
                },
                itemCount: 10,
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        currentDateSelectedIndex = index;
                        selectedDate = DateTime.now().add(Duration(days: index));
                        if (isTodayDate(selectedDate)) {
                          currentTimeSelectedIndex = DateTime.now().hour;
                          selectedTime = currentTimeSelectedIndex;
                        }
                      });
                    },
                    child: Container(
                      width: config.App(context).appHeight(12),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: currentDateSelectedIndex == index ? Theme.of(context).primaryColor : Colors.grey),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            listOfDays[DateTime.now().add(Duration(days: index)).weekday - 1].toString(),
                            style: Theme.of(context).textTheme.headline4,
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(1),
                          ),
                          Text(
                            DateTime.now().add(Duration(days: index)).day.toString(),
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              )),
          SizedBox(
            height: config.App(context).appHeight(3),
          ),
          Container(
              //padding: EdgeInsets.all(16),
              height: config.App(context).appHeight(4),
              child: ListView.separated(
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(8),
                ),
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(width: config.App(context).appHeight(2));
                },
                itemCount: listOfTimes.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      if (isTodayDate(selectedDate)) {
                        if (int.parse(listOfTimes[index]['key']) >= DateTime.now().hour) {
                          setState(() {
                            currentTimeSelectedIndex = index;
                            print('selectedTime : ${listOfTimes[index]['key']}');
                            selectedTime = int.parse(listOfTimes[index]['key']);
                          });
                        } else {
                          CustomWidgets.buildErrorSnackBar(context, 'You cannot select past time');
                        }
                      } else {
                        setState(() {
                          currentTimeSelectedIndex = index;
                          print('selectedTime : ${listOfTimes[index]['key']}');
                          selectedTime = int.parse(listOfTimes[index]['key']);
                        });
                      }
                    },
                    child: Container(
                        width: config.App(context).appHeight(12),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: currentTimeSelectedIndex == index ? Theme.of(context).primaryColor : Colors.grey),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              Icons.watch_later_outlined,
                              size: config.App(context).appHeight(2),
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(listOfTimes[index]['value'], style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500, color: Colors.white)),
                          ],
                        )),
                  );
                },
              )),
          SizedBox(
            height: config.App(context).appHeight(3),
          ),
          InkWell(
            onTap: () {
              print('selected DateTime : ${selectedDate.day}-${selectedDate.month}-${selectedDate.year} $selectedTime');
              Navigator.of(context).pushNamed('/Proposal', arguments: ProposalArguments(widget.instructorDetail.id, selectedDate, selectedTime));
            },
            child: Center(
              child: Container(
                margin: EdgeInsets.only(bottom: 30, top: 10),
                width: config.App(context).appWidth(65),
                padding: EdgeInsets.symmetric(
                  vertical: config.App(context).appHeight(1.5),
                  horizontal: 20,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text(
                  "Book a Session".toUpperCase(),
                  style: Theme.of(context).textTheme.subtitle1,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool isTodayDate(DateTime selectedDate) {
    return (selectedDate.year == DateTime.now().year && selectedDate.month == DateTime.now().month && selectedDate.day == DateTime.now().day);
  }
}

/*CupertinoTheme(
                        data: CupertinoThemeData(
                          textTheme: CupertinoTextThemeData(
                            dateTimePickerTextStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ),
                        child: CupertinoDatePicker(
                            initialDateTime: DateTime.now(),
                            use24hFormat: true,
                            mode: CupertinoDatePickerMode.dateAndTime,
                            onDateTimeChanged: (dateTime) {
                              setState(() {
                                bookingTime = dateTime;
                              });
                            }),
                      ),*/
