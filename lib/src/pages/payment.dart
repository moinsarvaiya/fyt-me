import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:credit_card_type_detector/credit_card_type_detector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fytme/bloc/providers_bloc/notification_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/CardExpiration.dart';
import 'package:fytme/src/common_widgets/CustomInputFormatter.dart';
import 'package:http/http.dart' as http;
import 'package:stripe_payment/stripe_payment.dart';

class PaymentScreen extends StatefulWidget {
  final ProposalDetails proposalDetails;
  final int proposalId;

  PaymentScreen({Key key, @required this.proposalDetails, this.proposalId}) : super(key: key);

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  CreditCard testCard = CreditCard();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _txtExpiry = TextEditingController();
  TextEditingController _txtCardHolderNumber = TextEditingController();
  TextEditingController _txtCardHolderName = TextEditingController();
  TextEditingController _txtCVV = TextEditingController();
  CreditCardType creditCardType;

  IconData getCardType() {
    if (creditCardType == CreditCardType.visa) {
      return FontAwesomeIcons.ccVisa;
    } else if (creditCardType == CreditCardType.mastercard) {
      return FontAwesomeIcons.ccMastercard;
    } else if (creditCardType == CreditCardType.amex) {
      return FontAwesomeIcons.ccAmex;
    } else if (creditCardType == CreditCardType.discover) {
      return FontAwesomeIcons.ccDiscover;
    } else if (creditCardType == CreditCardType.dinersclub) {
      return FontAwesomeIcons.ccDinersClub;
    } else if (creditCardType == CreditCardType.jcb) {
      return FontAwesomeIcons.ccJcb;
    } else {
      return Icons.credit_card;
    }
  }

  bool isLoading = false;
  final String validationMsg = 'Please fill all valid card details to book proposal';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(11, 17, 28, 1),
        body: SafeArea(
          key: _scaffoldKey,
          child: Stack(children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Align(
                      alignment: Alignment.center,
                      child: Text(
                        "PROPOSAL PAYMENT",
                        style: Theme.of(context).textTheme.subtitle1,
                      )),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, right: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Proposal detail",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Date of Proposal",
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            ),
                            Text(
                              widget.proposalDetails.sessionDate,
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Session 's Hour",
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            ),
                            Text(
                              '${widget.proposalDetails.totalHours} Hrs',
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Instructor Hourly Rate",
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            ),
                            Text(
                              "\$${widget.proposalDetails.hourlyRate}",
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 1,
                          color: Colors.grey,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total Amount",
                              style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "\$${(widget.proposalDetails.hourlyRate * widget.proposalDetails.totalHours)}",
                              style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 60,
                        ),
                        Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Card Number",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                            )),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width - 40,
                          decoration: BoxDecoration(color: Color.fromRGBO(128, 131, 136, 1), borderRadius: BorderRadius.circular(8)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: TextField(
                                  controller: _txtCardHolderNumber,
                                  style: TextStyle(color: Colors.white, letterSpacing: 4),
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
                                      border: InputBorder.none,
                                      hintText: "0000 0000 0000 0000",
                                      hintStyle: TextStyle(color: Colors.white),
                                      counterText: ""),
                                  inputFormatters: [FilteringTextInputFormatter.digitsOnly, new CustomInputFormatter(' ', 4)],
                                  maxLength: creditCardType == CreditCardType.amex ? 18 : 19,
                                  onChanged: (text) {
                                    setState(() {
                                      creditCardType = detectCCType(text);
                                    });
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 16),
                                child: Icon(
                                  getCardType(),
                                  size: 32,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                    width: MediaQuery.of(context).size.width / 2 - 16,
                                    child: Text(
                                      "Valid Until",
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                                    )),
                                Text(
                                  "CVV",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                  textAlign: TextAlign.left,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 50,
                                  width: MediaQuery.of(context).size.width / 2 - 35,
                                  decoration: BoxDecoration(color: Color.fromRGBO(128, 131, 136, 1), borderRadius: BorderRadius.circular(8)),
                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                    Flexible(
                                      child: TextField(
                                        controller: _txtExpiry,
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        keyboardType: TextInputType.number,
                                        maxLength: 5,
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
                                            border: InputBorder.none,
                                            hintText: "MM/YY",
                                            hintStyle: TextStyle(color: Colors.white),
                                            counterText: ""),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.digitsOnly,
                                          new CardMonthInputFormatter(),
                                        ],
                                        onChanged: (value) {},
                                      ),
                                    ),
                                  ]),
                                ),
                                Container(
                                  height: 50,
                                  width: MediaQuery.of(context).size.width / 2 - 38,
                                  decoration: BoxDecoration(color: Color.fromRGBO(128, 131, 136, 1), borderRadius: BorderRadius.circular(8)),
                                  child: TextField(
                                    controller: _txtCVV,
                                    style: TextStyle(color: Colors.white),
                                    textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.number,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
                                      border: InputBorder.none,
                                      hintText: "***",
                                      hintStyle: TextStyle(color: Colors.white),
                                      counterText: "",
                                    ),
                                    maxLength: creditCardType == CreditCardType.amex ? 4 : 3,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Card Holder Name",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                            )),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width - 40,
                          decoration: BoxDecoration(color: Color.fromRGBO(128, 131, 136, 1), borderRadius: BorderRadius.circular(8)),
                          child: TextField(
                            controller: _txtCardHolderName,
                            style: TextStyle(color: Colors.white),
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
                              border: InputBorder.none,
                              hintText: "Enter card holder name",
                              hintStyle: TextStyle(color: Colors.white),
                              counterText: "",
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 90,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: 65,
                            width: MediaQuery.of(context).size.width / 1.5,
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.blue),
                            child: InkWell(
                              onTap: () {
                                if (isValidateEmptyField()) {
                                  if (creditCardType == CreditCardType.amex) {
                                    if (_txtCardHolderNumber.text.toString().trim().length != 18) {
                                      CustomWidgets.buildErrorSnackBar(context, 'Please enter valid card number');
                                    } else {
                                      isAllFieldsValid();
                                    }
                                  } else {
                                    if (_txtCardHolderNumber.text.toString().trim().length != 19) {
                                      CustomWidgets.buildErrorSnackBar(context, 'Please enter valid card number');
                                    } else {
                                      isAllFieldsValid();
                                    }
                                  }
                                  /*  // else {
                                    if (_txtExpiry.text
                                            .toString()
                                            .trim()
                                            .length !=
                                        5) {
                                      CustomWidgets.buildErrorSnackBar(context,
                                          'Please enter valid expiry date');
                                    } else {
                                      List<String> monthYear =
                                          _txtExpiry.text.toString().split('/');
                                      if (int.parse(monthYear[0]) > 12) {
                                        CustomWidgets.buildErrorSnackBar(
                                            context,
                                            'Please enter valid month');
                                      } else {
                                        String year =
                                            DateTime.now().year.toString();
                                        List<String> yearList = year.split('');
                                        int currentYY = int.parse(
                                            '${yearList[2]}${yearList[3]}');
                                        if (int.parse(monthYear[1]) <
                                            currentYY) {
                                          CustomWidgets.buildErrorSnackBar(
                                              context,
                                              'Please enter valid year');
                                        } else {
                                          if (int.parse(monthYear[1]) ==
                                              currentYY) {
                                            print(
                                                'same year${int.parse(monthYear[1])}');
                                            if (int.parse(monthYear[0]) <
                                                DateTime.now().month) {
                                              CustomWidgets.buildErrorSnackBar(
                                                  context,
                                                  'Please enter valid month');
                                            } else {
                                              validateCvvName(monthYear);
                                            }
                                          } else {
                                            validateCvvName(monthYear);
                                          }
                                        }
                                      }
                                    }
                                  }*/
                                } else {
                                  CustomWidgets.buildErrorSnackBar(context, validationMsg);
                                }
                              },
                              child: Center(
                                child: Text(
                                  "Start the payment",
                                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: 50,
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context, false);
                              },
                              child: Text(
                                "Back",
                                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16, color: Colors.white),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            isLoading
                ? AlertDialog(
                    content: Container(
                      height: MediaQuery.of(context).size.height * .1,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                          ),
                        ),
                      ),
                    ),
                  )
                : Container()
          ]),
        ));
  }

  bool isValidateEmptyField() {
    bool isValid = true;
    if (_txtCardHolderNumber.text.toString().trim().isEmpty) {
      isValid = false;
    }
    if (_txtExpiry.text.toString().trim().isEmpty) {
      isValid = false;
    }
    if (_txtCVV.text.toString().trim().isEmpty) {
      isValid = false;
    }
    if (_txtCardHolderName.text.toString().trim().isEmpty) {
      isValid = false;
    }
    return isValid;
  }

  bool isAllFieldsValid() {
    bool isValid = true;
    if (_txtExpiry.text.toString().trim().length != 5) {
      CustomWidgets.buildErrorSnackBar(context, 'Please enter valid expiry date');
      isValid = false;
    } else {
      List<String> monthYear = _txtExpiry.text.toString().split('/');
      if (int.parse(monthYear[0]) > 12 || int.parse(monthYear[0]) < 1) {
        CustomWidgets.buildErrorSnackBar(context, 'Please enter valid month');
        isValid = false;
      } else {
        String year = DateTime.now().year.toString();
        List<String> yearList = year.split('');
        int currentYY = int.parse('${yearList[2]}${yearList[3]}');
        if (int.parse(monthYear[1]) < currentYY) {
          CustomWidgets.buildErrorSnackBar(context, 'Please enter valid year');
          isValid = false;
        } else {
          if (int.parse(monthYear[1]) == currentYY) {
            print('same year${int.parse(monthYear[1])}');
            if (int.parse(monthYear[0]) < DateTime.now().month) {
              CustomWidgets.buildErrorSnackBar(context, 'Please enter valid month');
              isValid = false;
            } else {
              validateCvvName(monthYear);
            }
          } else {
            validateCvvName(monthYear);
          }
        }
      }
    }
    return isValid;
  }

  validateCvvName(List<String> monthYear) {
    if (creditCardType == CreditCardType.amex) {
      if (_txtCVV.text.toString().trim().length != 4) {
        CustomWidgets.buildErrorSnackBar(context, 'Please enter valid cvv');
      } else {
        fillCardDetails(monthYear);
      }
    } else {
      if (_txtCVV.text.toString().trim().length != 3) {
        CustomWidgets.buildErrorSnackBar(context, 'Please enter valid cvv');
      } else {
        fillCardDetails(monthYear);
      }
    }
  }

  fillCardDetails(List<String> monthYear) {
    testCard = CreditCard(
        number: _txtCardHolderNumber.text.toString().trim(),
        name: _txtCardHolderName.text.toString().trim(),
        expMonth: int.parse(monthYear[0]),
        expYear: int.parse(monthYear[1]),
        cvc: _txtCVV.text.toString().trim());
    setState(() {
      isLoading = true;
    });
    callPaymentApi();
  }

  callPaymentApi() async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }

      final String url = '${ServerAddress.api_base_url}make-payments/';

      Map data = <String, dynamic>{
        "proposal": widget.proposalId,
        "amount": widget.proposalDetails.hourlyRate * widget.proposalDetails.totalHours,
        "currency": 'usd',
      };
      print(data.toString());
      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Storage().currentUser.token}',
        },
        body: json.encode(data),
      );

      printLog('payment =========== payment ===========');
      print(response.body);
      print(response.statusCode);
      final responseBody = json.decode(response.body);
      if (response.statusCode == 200) {
        print(response.body);
        stripePayment(responseBody['publishableKey'], responseBody['clientSecret']);
      } else {
        dismissDialog();
        Navigator.of(context).pop(false);
        CustomWidgets.buildErrorSnackBar(context, responseBody['proposal'][0].toString());
      }
    } catch (err) {
      dismissDialog();
      CustomWidgets.buildErrorSnackBar(context, 'Something went wrong, please try again');
    }
  }

  stripePayment(String pubKey, String secKey) {
    StripePayment.setOptions(StripeOptions(publishableKey: pubKey, androidPayMode: 'test'));
    StripePayment.createPaymentMethod(
      PaymentMethodRequest(
        card: testCard,
      ),
    ).then((paymentMethod) {
      print(JsonEncoder.withIndent('  ').convert(paymentMethod?.toJson() ?? {}));
      confirmPayment(paymentMethod.id, secKey);
    }).catchError((exc) {
      dismissDialog();
      CustomWidgets.buildErrorSnackBar(context, exc.toString());
    });
  }

  confirmPayment(String paymentId, String secKey) {
    StripePayment.confirmPaymentIntent(
      PaymentIntent(
        clientSecret: secKey,
        paymentMethodId: paymentId,
      ),
    ).then((paymentIntent) {
      Navigator.pop(context, true);
      dismissDialog();
      CustomWidgets.buildSuccessSnackBar(context, 'Your payment has been done successfully');
      print(paymentIntent.paymentIntentId);
    }).catchError((exc) {
      dismissDialog();
      CustomWidgets.buildErrorSnackBar(context, exc.toString());
    });
  }

  dismissDialog() {
    setState(() {
      isLoading = false;
    });
  }
}

class PaymentArguments {
  final ProposalDetails proposalDetails;
  final int proposalId;

  PaymentArguments(this.proposalDetails, this.proposalId);
}
