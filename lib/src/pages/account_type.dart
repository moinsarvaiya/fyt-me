import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../helpers/app_config.dart' as config;
import '../helpers/helper.dart';

class AccountTypeScreen extends StatefulWidget {
  @override
  _AccountTypeScreenState createState() => _AccountTypeScreenState();
}

class _AccountTypeScreenState extends StateMVC<AccountTypeScreen> {
  //UserController _con;
  String _isInstructor = "instructor";

  /*_AccountTypeScreenState() : super(UserController()) {
    _con = controller;
  }*/
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF2E94D0),
                Color(0xFF2B7DC9),
                Color(0xFF0B4B98),
                Color(0xFF09111D),
              ],
            ),
          ),
          height: config.App(context).appHeight(100),
          child: BlocListener<AuthBloc, AuthState>(
            listenWhen: (previous, current) => previous.status.status != current.status.status,
            listener: (BuildContext context, state) {
              switch (state.status.status) {
                case StateStatuses.loading:
                  CustomLoader().show(context);
                  break;
                case StateStatuses.failure:
                  CustomLoader().hide(context);
                  CustomWidgets.buildSuccessSnackBar(context, state.status.message);
                  break;

                case StateStatuses.success:
                default:
                  CustomLoader().hide(context);
                  break;
              }
            },
            child: Column(
              children: [
                SizedBox(
                  height: config.App(context).appHeight(30),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          _isInstructor = "instructor";
                        });
                      },
                      child: Container(
                        width: config.App(context).appWidth(35),
                        height: config.App(context).appWidth(35),
                        decoration: BoxDecoration(
                          color: _isInstructor == "instructor" ? Theme.of(context).primaryColor : Theme.of(context).cardColor.withOpacity(0.51),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: _isInstructor == "instructor" ? Colors.white.withOpacity(0.5) : Theme.of(context).primaryColor,
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/appicons/provider2.png',
                                color: _isInstructor == "instructor" ? null : Theme.of(context).primaryColor,
                                height: 50,
                              ),
                              // Icon(
                              //   Icons.fitness_center_sharp,
                              //   color: _isInstructor == "instructor"
                              //       ? Colors.white
                              //       : Theme.of(context).primaryColor,
                              //   size: 50,
                              // ),
                              SizedBox(
                                height: config.App(context).appHeight(1),
                              ),
                              Text(
                                "instructor",
                                style: TextStyle(
                                  color: _isInstructor == "instructor" ? Colors.white : Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: config.App(context).appWidth(5),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          _isInstructor = "customer";
                        });
                      },
                      child: Container(
                        width: config.App(context).appWidth(35),
                        height: config.App(context).appWidth(35),
                        decoration: BoxDecoration(
                          color: _isInstructor == "instructor" ? Theme.of(context).cardColor.withOpacity(0.51) : Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: _isInstructor == "instructor" ? Theme.of(context).primaryColor : Colors.white.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/appicons/client2.png',
                                color: _isInstructor == "instructor" ? Theme.of(context).primaryColor : null,
                                height: 50,
                              ),
                              // Icon(
                              //   Icons.account_circle,
                              //   color: _isInstructor == "instructor"
                              //       ? Theme.of(context).primaryColor
                              //       : Colors.white,
                              //   size: 50,
                              // ),
                              SizedBox(
                                height: config.App(context).appHeight(1),
                              ),
                              Text(
                                "customer",
                                style: TextStyle(
                                  color: _isInstructor == "instructor" ? Theme.of(context).primaryColor : Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: config.App(context).appHeight(15),
                ),
                Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    onPressed: () {
                      context.read<AuthBloc>().add(AuthAccountType(accountType: _isInstructor));
                    },
                    child: Container(
                      width: config.App(context).appWidth(40),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(2),
                        horizontal: 20,
                      ),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Text(
                        "Continue",
                        style: Theme.of(context).textTheme.subtitle1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
