import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fytme/bloc/providers_bloc/notification_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;
import 'package:fytme/src/pages/participant_widget.dart';
import 'package:http/http.dart' as http;
import 'package:twilio_programmable_video/twilio_programmable_video.dart';
import 'package:uuid/uuid.dart';

class LiveCall extends StatefulWidget {
  final String roomName;
  final String accessToken;
  final int proposalId;
  final ProposalDetails proposalDetails;

  LiveCall({Key key, @required this.roomName, @required this.accessToken, @required this.proposalId, @required this.proposalDetails})
      : super(key: key);

  @override
  _LiveCallState createState() => _LiveCallState();
}

class _LiveCallState extends State<LiveCall> {
  Room _room;
  final Completer<Room> _completer = Completer<Room>();
  List<ParticipantWidget> remoteWidgetList = [];
  ParticipantWidget localParticipantWidget;
  bool isInstructorAvailable = false;
  CameraCapturer _cameraCapture;
  bool isSwapCamera = false;
  bool isShowTools = false;
  bool isVideoEnable = true;
  bool isAudioEnable = true;

  @override
  void initState() {
    connectToRoom();
    super.initState();
  }

  Future<Room> connectToRoom() async {
    var trackId = Uuid().v4();
    var cameraSources = await CameraSource.getSources();
    _cameraCapture = CameraCapturer(
      cameraSources.firstWhere((source) => source.isFrontFacing),
    );
    var connectOptions = ConnectOptions(
      widget.accessToken,
      roomName: widget.roomName,
      region: Region.in1,
      preferredAudioCodecs: [OpusCodec()],
      preferredVideoCodecs: [Vp8Codec()],
      audioTracks: [LocalAudioTrack(true, 'audio_track-$trackId')],
      dataTracks: [
        LocalDataTrack(
          DataTrackOptions(ordered: true, name: 'data_track-$trackId' // Optional
              ), // Optional
        ),
      ],
      videoTracks: ([LocalVideoTrack(true, _cameraCapture)]), // Optional list of video tracks.
    );

    _room = await TwilioProgrammableVideo.connect(connectOptions);
    _room.onConnected.listen(_onConnected);
    _room.onDisconnected.listen(_onDisconnected);
    _room.onReconnecting.listen(_onReconnecting);
    _room.onConnectFailure.listen(_onConnectFailure);
    _cameraCapture.onCameraSwitched.listen(_onCameraSwitched);
    return _completer.future;
  }

  void _onConnected(Room room) {
    print('Connected to ${room.name}');

    _room.onParticipantConnected.listen(_onParticipantConnected);
    _room.onParticipantDisconnected.listen(_onParticipantDisconnected);

    final localParticipant = room.localParticipant;
    if (localParticipant == null) {
      print('ConferenceRoom._onConnected => localParticipant is null');
      return;
    }

    setState(() {
      ParticipantWidget connectedParticipant = ParticipantWidget(
          child: localParticipant.localVideoTracks[0].localVideoTrack.widget(),
          audioEnabled: true,
          videoEnabled: true,
          id: localParticipant.identity,
          isRemote: true);
      if (Storage().currentUser.accountType == 'instructor') {
        localParticipantWidget = connectedParticipant;
        isInstructorAvailable = true;
      } else {
        remoteWidgetList.add(connectedParticipant);
      }
    });

    print('remoteParticipants ${room.remoteParticipants.length}');
    for (final remoteParticipant in room.remoteParticipants) {
      _addRemoteParticipantListeners(remoteParticipant);
    }

    _room.localParticipant.onDataTrackPublished.listen(_onLocalDataTrackPublished);
    _completer.complete(_room);
  }

  void _addOrUpdateParticipant(RemoteParticipantEvent event) {
    setState(() {
      if (event is RemoteVideoTrackSubscriptionEvent) {
        ParticipantWidget joinParticipant = ParticipantWidget(
            child: event.remoteVideoTrack.widget(mirror: false),
            audioEnabled: true,
            videoEnabled: true,
            id: event.remoteParticipant.identity,
            isRemote: true);
        print("onParticipantAddOrUpdate : ${joinParticipant.id}");
        if (widget.proposalDetails.instructorInfo.username == joinParticipant.id) {
          localParticipantWidget = joinParticipant;
          isInstructorAvailable = true;
        } else {
          remoteWidgetList.add(joinParticipant);
        }
        if (remoteWidgetList.length == 1 && Storage().currentUser.accountType == 'instructor') {
          ParticipantWidget tempParticipant = localParticipantWidget;
          localParticipantWidget = remoteWidgetList[0];
          remoteWidgetList[0] = tempParticipant;
        }
      }
    });
  }

  void _onParticipantDisconnected(RoomParticipantDisconnectedEvent event) {
    setState(() {
      if (remoteWidgetList.length == 1 && Storage().currentUser.accountType == 'instructor' && !isSwapCamera) {
        ParticipantWidget tempParticipant = localParticipantWidget;
        localParticipantWidget = remoteWidgetList[0];
        remoteWidgetList[0] = tempParticipant;
      }
    });
    Future.delayed(Duration(milliseconds: 1000), () {
      setState(() {
        remoteWidgetList.removeWhere((ParticipantWidget p) {
          if (widget.proposalDetails.instructorInfo.username == event.remoteParticipant.identity) {
            if (!isSwapCamera) {
              localParticipantWidget = p;
            }
            isInstructorAvailable = false;
          }
          /*else {
            if (remoteWidgetList.length == 1 &&
                Storage().currentUser.accountType == 'instructor') {
              if (!isSwapCamera) {
                localParticipantWidget = remoteWidgetList[0];
              }
            }
          }*/
          print('onParticipantDisconnected : ${p.id} : ${event.remoteParticipant.identity}');
          return p.id == event.remoteParticipant.identity;
        });
        print('_onParticipantDisconnected ${remoteWidgetList.length}');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await _room.disconnect().then((value) {
          Navigator.of(context).pop();
        });
        return true;
      },
      child: Scaffold(
          body: SafeArea(
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            isInstructorAvailable
                ? localParticipantWidget.child
                : (remoteWidgetList.length > 0
                    ? remoteWidgetList[0].child
                    : localParticipantWidget != null
                        ? localParticipantWidget
                        : Container()),
            remoteWidgetList.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    itemCount: isInstructorAvailable ? remoteWidgetList.length : (remoteWidgetList.length - 1),
                    physics: ClampingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 48.0),
                            width: config.App(context).appWidth(30),
                            height: config.App(context).appHeight(20),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: remoteWidgetList[isInstructorAvailable ? index : (index + 1)].child,
                            ),
                            decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(10)),
                          ),
                          !remoteWidgetList[isInstructorAvailable ? index : (index + 1)].videoEnabled
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 48.0),
                                    width: config.App(context).appWidth(30),
                                    height: config.App(context).appHeight(20),
                                    child: Image.asset("assets/img/icons/ic_video_disable.png", width: 24),
                                    decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(10)),
                                  ),
                                )
                              : Container(),
                          !remoteWidgetList[isInstructorAvailable ? index : (index + 1)].audioEnabled
                              ? Padding(
                                  padding: const EdgeInsets.only(right: 24.0, bottom: 56.0),
                                  child: Image.asset("assets/img/icons/ic_mute.png", width: 16),
                                )
                              : Container(),
                        ],
                      );
                    })
                : Container(),
            InkWell(
              onTap: () {
                setState(() {
                  isShowTools = !isShowTools;
                });
              },
              child: Container(
                width: config.App(context).appWidth(100),
                height: config.App(context).appHeight(100),
                color: Colors.transparent,
              ),
            ),
            disableAudioVideoWidget(context),
            if (remoteWidgetList.length == 1) //swap camera
              InkWell(
                onTap: () {
                  isSwapCamera = !isSwapCamera;
                  ParticipantWidget tempParticipant = localParticipantWidget;
                  setState(() {
                    localParticipantWidget = remoteWidgetList[0];
                    remoteWidgetList[0] = tempParticipant;
                  });
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 48.0),
                  width: config.App(context).appWidth(30),
                  height: config.App(context).appHeight(20),
                  decoration: BoxDecoration(color: Colors.transparent, borderRadius: BorderRadius.circular(10)),
                ),
              ),
            InkWell(
              onTap: () {
                if (remoteWidgetList.length > 3) {
                  dialogGroupCall();
                }
              },
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Text(
                  remoteWidgetList.length > 3 ? '+${(remoteWidgetList.length - 3)} more' : '',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }

  void _onDisconnected(RoomDisconnectedEvent event) {
    print('Disconnected from ${event.room.name}');
  }

  void _onReconnecting(RoomReconnectingEvent event) {
    print('Reconnecting to room ${event.room.name}, exception = ${event.exception.message}');
  }

  void _onConnectFailure(RoomConnectFailureEvent event) {
    print('Failed to connect to room ${event.room.name} with exception: ${event.exception}');
    _completer.completeError(event.exception);
  }

  void _onCameraSwitched(CameraSwitchedEvent event) async {}

  void _addRemoteParticipantListeners(RemoteParticipant remoteParticipant) {
    remoteParticipant.onAudioTrackDisabled.listen(_onAudioTrackDisabled);
    remoteParticipant.onAudioTrackEnabled.listen(_onAudioTrackEnabled);
    remoteParticipant.onAudioTrackPublished.listen(_onAudioTrackPublished);
    remoteParticipant.onAudioTrackSubscribed.listen(_onAudioTrackSubscribed);
    remoteParticipant.onAudioTrackSubscriptionFailed.listen(_onAudioTrackSubscriptionFailed);
    remoteParticipant.onAudioTrackUnpublished.listen(_onAudioTrackUnpublished);
    remoteParticipant.onAudioTrackUnsubscribed.listen(_onAudioTrackUnsubscribed);
    remoteParticipant.onDataTrackPublished.listen(_onDataTrackPublished);
    remoteParticipant.onDataTrackSubscribed.listen(_onDataTrackSubscribed);
    remoteParticipant.onDataTrackSubscriptionFailed.listen(_onDataTrackSubscriptionFailed);
    remoteParticipant.onDataTrackUnpublished.listen(_onDataTrackUnpublished);
    remoteParticipant.onDataTrackUnsubscribed.listen(_onDataTrackUnsubscribed);
    remoteParticipant.onNetworkQualityLevelChanged.listen(_onNetworkQualityChanged);
    remoteParticipant.onVideoTrackDisabled.listen(_onVideoTrackDisabled);
    remoteParticipant.onVideoTrackEnabled.listen(_onVideoTrackEnabled);
    remoteParticipant.onVideoTrackPublished.listen(_onVideoTrackPublished);
    remoteParticipant.onVideoTrackSubscribed.listen(_onVideoTrackSubscribed);
    remoteParticipant.onVideoTrackSubscriptionFailed.listen(_onVideoTrackSubscriptionFailed);
    remoteParticipant.onVideoTrackUnpublished.listen(_onVideoTrackUnpublished);
    remoteParticipant.onVideoTrackUnsubscribed.listen(_onVideoTrackUnsubscribed);
  }

  void _onAudioTrackDisabled(RemoteAudioTrackEvent event) {
    _setRemoteAudioEnabled(event);
  }

  void _onAudioTrackEnabled(RemoteAudioTrackEvent event) {
    _setRemoteAudioEnabled(event);
  }

  void _onAudioTrackPublished(RemoteAudioTrackEvent event) {
    print('ConferenceRoom._onAudioTrackPublished(), ${event.remoteParticipant.sid}}');
  }

  void _onAudioTrackSubscribed(RemoteAudioTrackSubscriptionEvent event) {
    _addOrUpdateParticipant(event);
  }

  void _onAudioTrackSubscriptionFailed(RemoteAudioTrackSubscriptionFailedEvent event) {
    print('ConferenceRoom._onAudioTrackSubscriptionFailed(), ${event.remoteParticipant.sid}, ${event.remoteAudioTrackPublication.trackSid}');
  }

  void _onAudioTrackUnpublished(RemoteAudioTrackEvent event) {
    print('ConferenceRoom._onAudioTrackUnpublished(), ${event.remoteParticipant.sid}, ${event.remoteAudioTrackPublication.trackSid}');
  }

  void _onAudioTrackUnsubscribed(RemoteAudioTrackSubscriptionEvent event) {
    print('ConferenceRoom._onAudioTrackUnsubscribed(), ${event.remoteParticipant.sid}, ${event.remoteAudioTrack.sid}');
  }

  void _onDataTrackPublished(RemoteDataTrackEvent event) {
    print('ConferenceRoom._onDataTrackPublished(), ${event.remoteParticipant.sid}}');
  }

  void _onDataTrackSubscribed(RemoteDataTrackSubscriptionEvent event) {
    final dataTrack = event.remoteDataTrackPublication.remoteDataTrack;
    if (dataTrack == null) {
      return;
    }

    dataTrack.onMessage.listen(_onMessage);
    dataTrack.onBufferMessage.listen(_onBufferMessage);
  }

  void _onDataTrackSubscriptionFailed(RemoteDataTrackSubscriptionFailedEvent event) {
    print('ConferenceRoom._onDataTrackSubscriptionFailed(), ${event.remoteParticipant.sid}, ${event.remoteDataTrackPublication.trackSid}');
  }

  void _onDataTrackUnpublished(RemoteDataTrackEvent event) {
    print('ConferenceRoom._onDataTrackUnpublished(), ${event.remoteParticipant.sid}, ${event.remoteDataTrackPublication.trackSid}');
  }

  void _onDataTrackUnsubscribed(RemoteDataTrackSubscriptionEvent event) {
    print('ConferenceRoom._onDataTrackUnsubscribed(), ${event.remoteParticipant.sid}, ${event.remoteDataTrack.sid}');
  }

  void _onNetworkQualityChanged(RemoteNetworkQualityLevelChangedEvent event) {
    print('ConferenceRoom._onNetworkQualityChanged(), ${event.remoteParticipant.sid}, ${event.networkQualityLevel}');
  }

  void _onVideoTrackDisabled(RemoteVideoTrackEvent event) {
    _setRemoteVideoEnabled(event);
  }

  void _onVideoTrackEnabled(RemoteVideoTrackEvent event) {
    _setRemoteVideoEnabled(event);
  }

  void _onVideoTrackPublished(RemoteVideoTrackEvent event) {
    print('ConferenceRoom._onVideoTrackPublished(), ${event.remoteParticipant.sid}, ${event.remoteVideoTrackPublication.trackSid}');
  }

  void _onVideoTrackSubscribed(RemoteVideoTrackSubscriptionEvent event) {
    print("_onVideoTrackSubscribed");
    _addOrUpdateParticipant(event);
  }

  void _onVideoTrackSubscriptionFailed(RemoteVideoTrackSubscriptionFailedEvent event) {
    print(
        'ConferenceRoom._onVideoTrackSubscriptionFailed(),${event.exception.toString()}, ${event.remoteParticipant.sid}, ${event.remoteVideoTrackPublication.trackSid}');
  }

  void _onVideoTrackUnpublished(RemoteVideoTrackEvent event) {
    print('ConferenceRoom._onVideoTrackUnpublished(), ${event.remoteParticipant.sid}, ${event.remoteVideoTrackPublication.trackSid}');
  }

  void _onVideoTrackUnsubscribed(RemoteVideoTrackSubscriptionEvent event) {
    print('ConferenceRoom._onVideoTrackUnsubscribed(), ${event.remoteParticipant.sid}, ${event.remoteVideoTrack.sid}');
  }

  void _onMessage(RemoteDataTrackStringMessageEvent event) {
    print('onMessage => ${event.remoteDataTrack.sid}, ${event.message}');
  }

  void _onBufferMessage(RemoteDataTrackBufferMessageEvent event) {
    print('onBufferMessage => ${event.remoteDataTrack.sid}, ${String.fromCharCodes(event.message.asUint8List())}');
  }

  void _setRemoteAudioEnabled(RemoteAudioTrackEvent event) {
    var index = remoteWidgetList.indexWhere((ParticipantWidget participant) => participant.id == event.remoteParticipant.identity);
    if (index < 0) {
      return;
    }

    setState(() {
      remoteWidgetList[index].audioEnabled = event.remoteAudioTrackPublication.isTrackEnabled;
    });
  }

  void _setRemoteVideoEnabled(RemoteVideoTrackEvent event) {
    var index = remoteWidgetList.indexWhere((ParticipantWidget participant) => participant.id == event.remoteParticipant.identity);
    if (index < 0) {
      return;
    }

    setState(() {
      remoteWidgetList[index].videoEnabled = event.remoteVideoTrackPublication.isTrackEnabled;
    });
  }

  void _onParticipantConnected(RoomParticipantConnectedEvent event) {
    _addRemoteParticipantListeners(event.remoteParticipant);
  }

  void _onLocalDataTrackPublished(LocalDataTrackPublishedEvent event) {
    event.localDataTrackPublication.localDataTrack.send('Hello world');
  }

  Future<void> toggleVideoEnabled() async {
    final tracks = _room.localParticipant?.localVideoTracks ?? [];
    final localVideoTrack = tracks.isEmpty ? null : tracks[0].localVideoTrack;
    if (localVideoTrack == null) {
      print('ConferenceRoom.toggleVideoEnabled() => Track is not available yet!');
      return;
    }
    await localVideoTrack.enable(!localVideoTrack.isEnabled).then((value) {
      setState(() {
        isVideoEnable = localVideoTrack.isEnabled;
      });
    });

    var index = remoteWidgetList.indexWhere((ParticipantWidget participant) => !participant.isRemote);
    if (index < 0) {
      return;
    }
    remoteWidgetList[index] = remoteWidgetList[index].copyWith(videoEnabled: localVideoTrack.isEnabled);
    print('ConferenceRoom.toggleVideoEnabled() => ${localVideoTrack.isEnabled}');
  }

  Future<void> toggleAudioEnabled() async {
    final tracks = _room.localParticipant?.localAudioTracks ?? [];
    final localAudioTrack = tracks.isEmpty ? null : tracks[0].localAudioTrack;
    if (localAudioTrack == null) {
      print('ConferenceRoom.toggleAudioEnabled() => Track is not available yet!');
      return;
    }
    await localAudioTrack.enable(!localAudioTrack.isEnabled).then((value) {
      setState(() {
        isAudioEnable = localAudioTrack.isEnabled;
      });
    });

    var index = remoteWidgetList.indexWhere((ParticipantWidget participant) => !participant.isRemote);
    if (index < 0) {
      return;
    }

    remoteWidgetList[index] = remoteWidgetList[index].copyWith(audioEnabled: localAudioTrack.isEnabled);
    print('ConferenceRoom.toggleAudioEnabled() => ${localAudioTrack.isEnabled}');
  }

  Future<void> switchCamera() async {
    print('ConferenceRoom.switchCamera()');
    try {
      var cameraSources = await CameraSource.getSources();
      var cameraSource = cameraSources.firstWhere((source) => source.isBackFacing);
      await _cameraCapture.switchCamera(cameraSource);
      //await _cameraCapture.switchCamera();
    } on FormatException catch (e) {
      print(
        'ConferenceRoom.switchCamera() failed because of FormatException with message: ${e.message}',
      );
    }
  }

  /*void _onReconnected(Room room) {
    print('Reconnected to room ${room.name}');
  }

  void _onRecordingStarted(Room room) {
    print('Recording started in ${room.name}');
  }

  void _onRecordingStopped(Room room) {
    print('Recording stopped in ${room.name}');
  }*/

  disableAudioVideoWidget(BuildContext context) {
    return Visibility(
      visible: isShowTools,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: config.App(context).appHeight(12),
            padding: EdgeInsets.only(left: 16, right: 16),
            decoration: new BoxDecoration(
              gradient: new LinearGradient(colors: [
                const Color(0xFF3A414C),
                const Color(0x00000000),
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    toggleVideoEnabled();
                  },
                  child: Container(
                    width: config.App(context).appHeight(6),
                    height: config.App(context).appHeight(6),
                    decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(25)),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Image.asset(
                        isVideoEnable ? "assets/img/icons/ic_video_enable.png" : "assets/img/icons/ic_video_disable.png",
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    toggleAudioEnabled();
                  },
                  child: Container(
                    width: config.App(context).appHeight(6),
                    height: config.App(context).appHeight(6),
                    decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(25)),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Image.asset(
                        isAudioEnable ? "assets/img/icons/ic_unmute.png" : "assets/img/icons/ic_mute.png",
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    switchCamera();
                  },
                  child: Container(
                    width: config.App(context).appHeight(6),
                    height: config.App(context).appHeight(6),
                    decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(25)),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Image.asset(
                        "assets/img/icons/ic_rotate_camera.png",
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    dialogEndSession(context);
                  },
                  child: Container(
                    width: config.App(context).appHeight(6),
                    height: config.App(context).appHeight(6),
                    decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(25)),
                    child: Padding(
                      padding: const EdgeInsets.all(14.0),
                      child: Image.asset(
                        "assets/img/icons/ic_close.png",
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.bottomRight,
            width: config.App(context).appWidth(100),
            height: config.App(context).appHeight(12),
            decoration: new BoxDecoration(
              gradient: new LinearGradient(colors: [
                const Color(0x00000000),
                const Color(0xFF3A414C),
              ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
            ),
          )
        ],
      ),
    );
  }

  dialogGroupCall() {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height) / 2.1;
    final double itemWidth = size.width / 2;
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Color.fromRGBO(11, 17, 28, 1),
        isDismissible: false,
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.keyboard_arrow_left,
                            size: 30,
                            color: Colors.white,
                          )),
                      SizedBox(width: 5),
                      Text(
                        "In the Session",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15, color: Colors.white),
                      )
                    ],
                  ),
                ),
                Visibility(
                  visible: remoteWidgetList.isNotEmpty,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 16, 20, 20),
                    child: GridView.count(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 3,
                      mainAxisSpacing: 16,
                      crossAxisSpacing: 4,
                      childAspectRatio: itemWidth / itemHeight,
                      children: List.generate(remoteWidgetList.length, (index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Stack(
                              alignment: Alignment.bottomRight,
                              children: [
                                Container(
                                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 16.0, 32.0),
                                  width: config.App(context).appWidth(30),
                                  height: config.App(context).appHeight(20),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: remoteWidgetList[index].child,
                                  ),
                                  decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(10)),
                                ),
                                !remoteWidgetList[index].videoEnabled
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                          width: config.App(context).appWidth(30),
                                          height: config.App(context).appHeight(20),
                                          child: Image.asset("assets/img/icons/ic_video_disable.png", width: 24),
                                          decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(10)),
                                        ),
                                      )
                                    : Container(),
                                !remoteWidgetList[index].audioEnabled
                                    ? Padding(
                                        padding: const EdgeInsets.only(right: 24.0, bottom: 40.0),
                                        child: Image.asset("assets/img/icons/ic_mute.png", width: 16),
                                      )
                                    : Container(),
                              ],
                            ),
                            Text(
                              remoteWidgetList[index].id,
                              textAlign: TextAlign.start,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: Colors.white),
                            ),
                          ],
                        );
                      }),
                    ),
                  ),
                ),
              ],
            );
          });
        });
  }

  dialogEndSession(BuildContext context) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Color(0xFF3D434D),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 0.0,
                        offset: Offset(0.0, 0.0),
                      ),
                    ]),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "END SESSION",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    Text(
                      "Are you sure you want to\nleave the session?",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    InkWell(
                      onTap: () async {
                        await _room.disconnect().then((value) {
                          Navigator.of(context).pop();
                          String oppName = Storage().currentUser.accountType == 'instructor'
                              ? widget.proposalDetails.proposalBy
                              : widget.proposalDetails.instructorInfo.instructorName;
                          int reviewTo = Storage().currentUser.accountType == 'instructor'
                              ? widget.proposalDetails.clientId
                              : widget.proposalDetails.instructorId;
                          dialogRatingAndReview(oppName, reviewTo);
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                        width: config.App(context).appWidth(65),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(2),
                          horizontal: 20,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "End video session",
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                        width: config.App(context).appWidth(65),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(2),
                          horizontal: 20,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  dialogRatingAndReview(String opponentName, int reviewTo) {
    TextEditingController commentController = TextEditingController();
    double currentRating = 3.0;
    bool isCommentPrivate = false;

    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Color.fromRGBO(49, 55, 66, 1),
        isDismissible: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context, StateSetter setState /*You can rename this!*/) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 50,
                ),
                Text(
                  Storage().currentUser.accountType == 'instructor' ? 'Let us know how the session went.' : "Enjoyed the session with $opponentName?",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  "Rate your experience",
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14, color: Colors.grey),
                ),
                SizedBox(
                  height: 25,
                ),
                RatingBar.builder(
                  initialRating: currentRating,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: false,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    this.setState(() {
                      currentRating = rating;
                    });
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16),
                  child: Container(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    height: 150,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Color.fromRGBO(42, 47, 57, 1)),
                    child: TextField(
                      minLines: 1,
                      controller: commentController,
                      decoration: InputDecoration(
                          hintText: "Leave a comment", hintStyle: TextStyle(color: Colors.white, fontSize: 15), border: InputBorder.none),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Storage().currentUser.accountType == 'instructor'
                    ? Container(
                        child: Column(
                          children: [
                            Text(
                              "P.s: your comments won't be made \n public until reviewed by our team. \n  ",
                              style: TextStyle(fontSize: 15, color: Colors.grey),
                            ),
                            Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      height: 25,
                                      width: 25,
                                      margin: EdgeInsets.all(5),
                                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), color: Colors.white),
                                      child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            isCommentPrivate = !isCommentPrivate;
                                          });
                                        },
                                        child: Icon(
                                          Icons.check,
                                          color: isCommentPrivate ? Colors.black : Colors.white,
                                          size: 20,
                                        ),
                                      )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    "Make Private",
                                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.grey),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    : SizedBox(),
                SizedBox(
                  height: 20,
                ),
                Center(
                    child: CustomButton(
                        callback: () {
                          this.setState(() {
                            if (commentController.text.trim().isNotEmpty) {
                              CustomLoader().show(context);
                              submitReview(currentRating.toInt(), reviewTo, commentController.text.trim());
                            } else {
                              CustomWidgets.buildErrorSnackBar(context, 'Please Enter Comment');
                            }
                          });
                        },
                        string: 'Submit Review')),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'CANCEL',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            );
          });
        });
  }

  submitReview(int rating, int reviewTo, String comments) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-reviews/';
      print(url);
      Map<String, dynamic> data = {
        "review_by": Storage().currentUser.userId,
        "review_to": reviewTo,
        "proposal": widget.proposalId,
        "comments": comments,
        "rating": rating,
      };

      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Storage().currentUser.token}',
        },
        body: json.encode(data),
      );

      printLog('submitReview ====== ${response.statusCode}');
      printLog('response ====== ${response.body}');

      if (response.statusCode == 201) {
        CustomWidgets.buildSuccessSnackBar(context, "Your review has been sent");
      } else {
        CustomWidgets.buildErrorSnackBar(context, "Something went wrong, please try again");
        throw Exception(response.body);
      }
      CustomLoader().hide(context);
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }
}
