// import 'package:flutter/material.dart';
// import 'package:form_field_validator/form_field_validator.dart';

// import 'package:mvc_pattern/mvc_pattern.dart';

// import '../helpers/helper.dart';
// import '../helpers/app_config.dart' as config;
// import '../controllers/user_controller.dart';

// class ForgotPasswordScreen extends StatefulWidget {
//   @override
//   _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
// }

// class _ForgotPasswordScreenState extends StateMVC<ForgotPasswordScreen> {
//   UserController _con;
//   TextEditingController _emailController = TextEditingController();
//   _ForgotPasswordScreenState() : super(UserController()) {
//     _con = controller;
//   }
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: Helper.of(context).onWillPop,
//       child: Scaffold(
//         key: _con.scaffoldKey,
//         extendBodyBehindAppBar: true,
//         appBar: AppBar(
//           leading: IconButton(
//             icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
//             iconSize: 40,
//             onPressed: () => Navigator.of(context).pop(),
//           ),
//           title: Image.asset(
//             "assets/img/splash/logo.png",
//             height: config.App(context).appHeight(6),
//           ),
//           centerTitle: true,
//           backgroundColor: Colors.transparent,
//           elevation: 0,
//           automaticallyImplyLeading: false,
//         ),
//         body: Container(
//           padding:
//               EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
//           decoration: BoxDecoration(
//             gradient: LinearGradient(
//               begin: Alignment.topCenter,
//               end: Alignment.bottomCenter,
//               colors: [
//                 Color(0xFF2E94D0),
//                 Color(0xFF2B7DC9),
//                 Color(0xFF0B4B98),
//                 Color(0xFF09111D),
//               ],
//             ),
//           ),
//           child: Stack(
//             children: [
//               Positioned(
//                 child: Padding(
//                   padding: EdgeInsets.symmetric(
//                     vertical: config.App(context).appHeight(15),
//                   ),
//                   child: Text(
//                     "forgot password".toUpperCase(),
//                     style: Theme.of(context).textTheme.subtitle1,
//                   ),
//                 ),
//               ),
//               Center(
//                 child: Form(
//                   key: _con.loginFormKey,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       SizedBox(
//                         height: config.App(context).appHeight(5),
//                       ),
//                       Text(
//                         "please enter your email",
//                         style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 15,
//                         ),
//                       ),
//                       SizedBox(
//                         height: config.App(context).appHeight(2),
//                       ),
//                       TextFormField(
//                         keyboardType: TextInputType.text,
//                         controller: _emailController,
//                         validator: MultiValidator([
//                           RequiredValidator(errorText: 'email is required'),
//                           EmailValidator(
//                               errorText: 'enter a valid email address')
//                         ]),
//                         cursorColor: Colors.white,
//                         style: Theme.of(context).textTheme.headline4,
//                         decoration: InputDecoration(
//                           filled: true,
//                           fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
//                           suffixIcon: Icon(
//                             Icons.email,
//                             color: Colors.white,
//                           ),
//                           contentPadding: EdgeInsets.symmetric(
//                             horizontal: 25,
//                             vertical: config.App(context).appHeight(1),
//                           ),
//                           border: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(15),
//                             borderSide: BorderSide.none,
//                           ),
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(15),
//                             borderSide: BorderSide.none,
//                           ),
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(15),
//                             borderSide: BorderSide.none,
//                           ),
//                         ),
//                       ),
//                       SizedBox(
//                         height: config.App(context).appHeight(5),
//                       ),
//                       Align(
//                         alignment: Alignment.center,
//                         child: TextButton(
//                           onPressed: () {
//                             _con.forgotPassword(_emailController.text);
//                           },
//                           child: Container(
//                             padding: EdgeInsets.symmetric(
//                               vertical: config.App(context).appHeight(3),
//                               horizontal: config.App(context).appWidth(10),
//                             ),
//                             decoration: BoxDecoration(
//                               color: Theme.of(context).primaryColor,
//                               borderRadius: BorderRadius.circular(15),
//                             ),
//                             child: Text(
//                               "Submit".toUpperCase(),
//                               style: Theme.of(context).textTheme.subtitle1,
//                               textAlign: TextAlign.center,
//                             ),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
