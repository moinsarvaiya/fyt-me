import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/calorie_bloc/calorie_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/calories.dart';
import 'package:fytme/data/model/food_calories.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/home/common/scaffold.dart';
import 'package:horizontal_calendar_widget/date_helper.dart';
import 'package:horizontal_calendar_widget/horizontal_calendar.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../helpers/app_config.dart' as config;

class CalorieTrackerWidget extends StatefulWidget {
  @override
  _CalorieTrackerWidgetState createState() => _CalorieTrackerWidgetState();
}

class _CalorieTrackerWidgetState extends State<CalorieTrackerWidget> {
  DateTime firstDate;
  DateTime lastDate;
  DateTime selectedDate = new DateTime.now();
  bool isSwitched = false;

  bool isBindCalorie = false;
  bool isBindFoodCalorie = false;

  TextEditingController newGoalController = new TextEditingController();
  TextEditingController foodNameController = new TextEditingController();
  TextEditingController foodQuantityController = new TextEditingController();

  int editCalorieId = 0;

  String _typeOfMeal = "Breakfast";
  List<String> mealType = ["Breakfast", "Lunch", "Dinner", "Snacks"];
  String _unitOfQuantity = "Grams";
  List<String> quantityUnit = ["Grams", 'Kilogram', 'Milliliter', 'Liter'];

  @override
  void initState() {
    super.initState();
    const int days = 7;
    firstDate = toDateMonthYear(DateTime(selectedDate.year, selectedDate.month, selectedDate.day - 4));
    lastDate = toDateMonthYear(firstDate.add(Duration(days: days - 1)));
    BlocProvider.of<CalorieBloc>(context).add(GetCalorieEvent());
  }

  ScrollController scrollController = ScrollController();

  List<Calories> calorieList = [];
  Calories calorieDetails;

  List<FoodCalories> foodCalorieList = [];
  FoodCalories foodCalorieDetails;

  showCalorieDetails() {
    bool isCalorieCreated = false;
    for (int c = 0; c < calorieList.length; c++) {
      if (calorieList[c].calorieIntakeDate == DateFormat('yyyy-MM-dd').format(selectedDate)) {
        isCalorieCreated = true;
        calorieDetails = calorieList[c];
        break;
      }
    }
    setState(() {
      if (isCalorieCreated) {
        editCalorieId = calorieDetails.id;
      } else {
        editCalorieId = 0;
        calorieDetails = Calories(
            id: 0,
            calorieIntakeDate: DateFormat('yyyy-MM-dd').format(selectedDate),
            calorieIntakeGoal: 0,
            totalConsumedCalories: 0.0,
            totalConsumedProteins: 0.0,
            totalConsumedFats: 0.0,
            totalConsumedCarbohydrates: 0.0,
            userId: Storage().currentUser.userId);
      }
      BlocProvider.of<CalorieBloc>(context).add(GetFoodCalorieEvent(calorieDate: '${DateFormat('yyyy-MM-dd').format(selectedDate)}'));
      isBindCalorie = true;
    });
  }

  showFoodDetails() {
    bool isFoodCreated = false;
    for (int c = 0; c < foodCalorieList.length; c++) {
      if (foodCalorieList[c].calorieIntakeDate == DateFormat('yyyy-MM-dd').format(selectedDate)) {
        isFoodCreated = true;
        foodCalorieDetails = foodCalorieList[c];
        break;
      }
    }

    if (!isFoodCreated) {
      foodCalorieDetails = FoodCalories(
          id: 0,
          userId: Storage().currentUser.userId,
          calorieIntakeDate: DateFormat('yyyy-MM-dd').format(selectedDate),
          breakfast: [],
          lunch: [],
          dinner: [],
          snacks: [],
          caloriesBreakdownData: CaloriesBreakdownData(
              totalConsumedCalories: '0.0',
              totalBreakfastCalories: '0.0',
              totalLunchCalories: '0.0',
              totalDinnerCalories: '0.0',
              totalSnacksCalories: '0.0'));
    }
    setState(() {
      isBindFoodCalorie = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      bottomMenuIndex: null,
      body: BlocListener<CalorieBloc, CalorieState>(
        listenWhen: (previous, current) => previous.status.status != current.status.status,
        listener: (con, state) {
          switch (state.status.status) {
            case StateStatuses.loading:
              CustomLoader().show(context);
              break;
            case StateStatuses.failure:
              CustomLoader().hide(context);
              CustomWidgets.buildErrorSnackBar(context, state.status.message);
              break;
            case StateStatuses.success:
              CustomLoader().hide(context);
              if (state.isGoalSetEdit) {
                isBindCalorie = false;
                BlocProvider.of<CalorieBloc>(context).add(GetCalorieEvent());
                if (state.isFoodCreated) {
                  BlocProvider.of<CalorieBloc>(context).add(GetFoodCalorieEvent(calorieDate: '${DateFormat('yyyy-MM-dd').format(selectedDate)}'));
                }
              } else {
                if (!isBindCalorie) {
                  if (state.calorieData != null) {
                    calorieList = [];
                    calorieList = state.calorieData;
                    showCalorieDetails();
                  }
                }
              }
              if (state.foodCalorieData != null) {
                foodCalorieList = state.foodCalorieData;
                //if (foodCalorieList.isNotEmpty) {
                showFoodDetails();
                //}
              }
              break;
            default:
              CustomLoader().hide(context);
              break;
          }
        },
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: config.App(context).appWidth(10),
            vertical: 10,
          ),
          child: Column(
            children: [
              SizedBox(
                height: config.App(context).appHeight(2),
              ),
              Container(
                //constraints: BoxConstraints( maxWidth:config.App(context).appWidth(50)),
                child: HorizontalCalendar(
                  initialSelectedDates: [selectedDate],
                  onDateSelected: (date) async {
                    final result = await InternetAddress.lookup("google.com");
                    if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
                      CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
                    } else {
                      selectedDate = date;
                      showCalorieDetails();
                      showFoodDetails();
                    }
                  },
                  selectedDateTextStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),
                  selectedWeekDayTextStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),
                  key: UniqueKey(),
                  firstDate: firstDate,
                  lastDate: lastDate,
                  labelOrder: [
                    LabelType.weekday,
                    LabelType.date,
                  ],
                  spacingBetweenDates: config.App(context).appWidth(0.5),
                  dateTextStyle: Theme.of(context).textTheme.subtitle1,
                  weekDayTextStyle: Theme.of(context).textTheme.headline4,
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(1),
              ),
              Text(
                "${DateFormat('EEEE').format(selectedDate).toString()}, ${DateFormat('MMMM d y').format(selectedDate).toString()}",
                style: Theme.of(context).textTheme.headline4,
              ),
              SizedBox(
                height: config.App(context).appHeight(2),
              ),
              isBindCalorie
                  ? BlocBuilder<CalorieBloc, CalorieState>(builder: (context, state) {
                      return Container(
                        width: config.App(context).appWidth(80),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Color(0xFF3D434D),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: config.App(context).appWidth(8),
                                vertical: config.App(context).appHeight(3),
                              ),
                              child: Row(
                                children: [
                                  Text(
                                    "Calories for today",
                                    style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Image.asset("assets/img/icons/Calories.png"),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            CircularPercentIndicator(
                              radius: config.App(context).appWidth(38),
                              animation: true,
                              lineWidth: 10.0,
                              backgroundWidth: 2,
                              backgroundColor: Colors.white,
                              percent: editCalorieId != 0 ? calculateCalories() : 0.0,
                              center: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    calorieDetails.totalConsumedCalories != null ? '${calorieDetails.totalConsumedCalories.toInt()}' : '0.0',
                                    style: TextStyle(
                                      fontSize: 36,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    calorieDetails.calorieIntakeGoal != 0 ? 'Goal ${calorieDetails.calorieIntakeGoal}' : 'No Goal Set',
                                    style: Theme.of(context).textTheme.headline4,
                                  ),
                                ],
                              ),
                              progressColor: Color(0xFF45FDFE),
                              circularStrokeCap: CircularStrokeCap.round,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            InkWell(
                              onTap: () {
                                openEditGoalDialog(
                                    context: context,
                                    onClickEvent: (int goal) {
                                      BlocProvider.of<CalorieBloc>(context).add(CreateCalorieEvent(
                                          calorieIntakeDate: '${DateFormat('yyyy-MM-dd').format(selectedDate)}',
                                          calorieIntakeGoal: goal,
                                          editCalorieId: editCalorieId));
                                    });
                              },
                              child: Visibility(
                                visible: isFutureDate(selectedDate),
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 30, top: 10),
                                  width: config.App(context).appWidth(35),
                                  padding: EdgeInsets.symmetric(
                                    vertical: config.App(context).appHeight(2),
                                    horizontal: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Text(
                                    editCalorieId != 0 ? "Edit Goal" : "Set Goal",
                                    style: Theme.of(context).textTheme.subtitle1,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  commonText(
                                    "Carbohydrates",
                                  ),
                                  commonText(
                                    "${calorieDetails.totalConsumedCarbohydrates.toStringAsFixed(2)} g",
                                  )
                                ],
                              ),
                            ),
                            /*SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: config.App(context).appWidth(5)),
                              child: LinearPercentIndicator(
                                width: config.App(context).appWidth(65),
                                animation: true,
                                lineHeight: 10.0,
                                percent: calculatePercentage(
                                    calorieDetails.totalConsumedCarbohydrates),
                                backgroundColor: Color(0xFF7F8389),
                                progressColor: Colors.white,
                              ),
                            ),*/
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  commonText(
                                    "Protein",
                                  ),
                                  commonText(
                                    "${calorieDetails.totalConsumedProteins.toStringAsFixed(2)} g",
                                  )
                                ],
                              ),
                            ),
                            /*SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: config.App(context).appWidth(5)),
                              child: LinearPercentIndicator(
                                width: config.App(context).appWidth(65),
                                animation: true,
                                lineHeight: 10.0,
                                percent: calculatePercentage(
                                    calorieDetails.totalConsumedProteins),
                                backgroundColor: Color(0xFF7F8389),
                                progressColor: Colors.white,
                              ),
                            ),*/
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  commonText(
                                    "Fats",
                                  ),
                                  commonText(
                                    "${calorieDetails.totalConsumedFats.toStringAsFixed(2)} g",
                                  )
                                ],
                              ),
                            ),
                            /*SizedBox(
                              height: config.App(context).appHeight(1.5),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: config.App(context).appWidth(5)),
                              child: LinearPercentIndicator(
                                width: config.App(context).appWidth(65),
                                animation: true,
                                lineHeight: 10.0,
                                percent: calculatePercentage(
                                    calorieDetails.totalConsumedFats),
                                backgroundColor: Color(0xFF7F8389),
                                progressColor: Colors.white,
                              ),
                            ),*/
                            SizedBox(height: config.App(context).appHeight(3)),
                          ],
                        ),
                      );
                    })
                  : Container(),
              SizedBox(
                height: config.App(context).appHeight(4),
              ),
              Container(
                width: config.App(context).appWidth(80),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF3D434D),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          top: config.App(context).appHeight(1.5),
                          bottom: config.App(context).appHeight(1.5),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              height: 24,
                              child: Text(
                                "Breakfast",
                                style: Theme.of(context).textTheme.headline4,
                              ),
                            ),
                            Visibility(
                              visible: (isFutureDate(selectedDate) && editCalorieId != 0),
                              child: IconButton(
                                padding: EdgeInsets.zero,
                                constraints: BoxConstraints(),
                                icon: Image.asset(
                                  "assets/img/icons/Add_Button.png",
                                  width: 24,
                                ),
                                onPressed: () {
                                  _typeOfMeal = 'Breakfast';
                                  addMealDialog(
                                      context: context,
                                      setState: setState,
                                      onClickEvent: () {
                                        createFoodCalories();
                                      });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      (isBindFoodCalorie && foodCalorieDetails.breakfast.length > 0)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    width: config.App(context).appWidth(24),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 3,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.white,
                                    ),
                                    child: commonText("${foodCalorieDetails.caloriesBreakdownData.totalBreakfastCalories} Calories",
                                        textColor: Theme.of(context).primaryColor)),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalBreakfastCarbohydrates}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalBreakfastProteins}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalBreakfastFats}g",
                                    )),
                              ],
                            )
                          : Container(),
                      isBindFoodCalorie
                          ? ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: foodCalorieDetails.breakfast.length > 0 ? config.App(context).appHeight(1.5) : 0),
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemCount: foodCalorieDetails.breakfast.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: EdgeInsets.only(
                                      bottom: (index == foodCalorieDetails.breakfast.length - 1) ? config.App(context).appHeight(2) : 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: config.App(context).appWidth(24),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            commonText(
                                              foodCalorieDetails.breakfast[index].consumedFood,
                                            ),
                                            commonText(
                                              "${foodCalorieDetails.breakfast[index].consumedCalories} cals",
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.breakfast[index].consumedCarbohydrates}g",
                                          )),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.breakfast[index].consumedProteins}g",
                                          )),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.breakfast[index].consumedFats}g",
                                          )),
                                    ],
                                  ),
                                );
                              })
                          : Container(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(4),
              ),
              Container(
                width: config.App(context).appWidth(80),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF3D434D),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: config.App(context).appHeight(2), bottom: config.App(context).appHeight(2)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Lunch",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Visibility(
                              visible: (isFutureDate(selectedDate) && editCalorieId != 0),
                              child: IconButton(
                                padding: EdgeInsets.zero,
                                constraints: BoxConstraints(),
                                icon: Image.asset("assets/img/icons/Add_Button.png", width: 24),
                                onPressed: () {
                                  _typeOfMeal = 'Lunch';
                                  addMealDialog(
                                      context: context,
                                      setState: setState,
                                      onClickEvent: () {
                                        createFoodCalories();
                                      });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      (isBindFoodCalorie && foodCalorieDetails.lunch.length > 0)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    width: config.App(context).appWidth(24),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 3,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.white,
                                    ),
                                    child: commonText("${foodCalorieDetails.caloriesBreakdownData.totalLunchCalories} Calories",
                                        textColor: Theme.of(context).primaryColor)),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalLunchCarbohydrates}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalLunchProteins}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalLunchFats}g",
                                    )),
                              ],
                            )
                          : Container(),
                      isBindFoodCalorie
                          ? ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: foodCalorieDetails.lunch.length > 0 ? config.App(context).appHeight(1.5) : 0),
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemCount: foodCalorieDetails.lunch.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding:
                                      EdgeInsets.only(bottom: (index == foodCalorieDetails.lunch.length - 1) ? config.App(context).appHeight(2) : 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: config.App(context).appWidth(24),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            commonText(
                                              foodCalorieDetails.lunch[index].consumedFood,
                                            ),
                                            commonText(
                                              "${foodCalorieDetails.lunch[index].consumedCalories} cals",
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        width: config.App(context).appWidth(12),
                                        alignment: Alignment.center,
                                        padding: EdgeInsets.symmetric(vertical: 5),
                                        child: commonText(
                                          "${foodCalorieDetails.lunch[index].consumedCarbohydrates}g",
                                        ),
                                      ),
                                      Container(
                                        width: config.App(context).appWidth(12),
                                        alignment: Alignment.center,
                                        padding: EdgeInsets.symmetric(vertical: 5),
                                        child: commonText(
                                          "${foodCalorieDetails.lunch[index].consumedProteins}g",
                                        ),
                                      ),
                                      Container(
                                        width: config.App(context).appWidth(12),
                                        alignment: Alignment.center,
                                        padding: EdgeInsets.symmetric(vertical: 5),
                                        child: commonText(
                                          "${foodCalorieDetails.lunch[index].consumedFats}g",
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              })
                          : Container(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(4),
              ),
              Container(
                width: config.App(context).appWidth(80),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF3D434D),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: config.App(context).appHeight(2), bottom: config.App(context).appHeight(2)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Dinner",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Visibility(
                              visible: (isFutureDate(selectedDate) && editCalorieId != 0),
                              child: IconButton(
                                padding: EdgeInsets.zero,
                                constraints: BoxConstraints(),
                                icon: Image.asset("assets/img/icons/Add_Button.png", width: 24),
                                onPressed: () {
                                  _typeOfMeal = 'Dinner';
                                  addMealDialog(
                                      context: context,
                                      setState: setState,
                                      onClickEvent: () {
                                        createFoodCalories();
                                      });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      (isBindFoodCalorie && foodCalorieDetails.dinner.length > 0)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    width: config.App(context).appWidth(24),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 3,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.white,
                                    ),
                                    child: commonText("${foodCalorieDetails.caloriesBreakdownData.totalDinnerCalories} Calories",
                                        textColor: Theme.of(context).primaryColor)),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalDinnerCarbohydrates}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalDinnerProteins}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalDinnerFats}g",
                                    )),
                              ],
                            )
                          : Container(),
                      isBindFoodCalorie
                          ? ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: foodCalorieDetails.dinner.length > 0 ? config.App(context).appHeight(1.5) : 0),
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemCount: foodCalorieDetails.dinner.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding:
                                      EdgeInsets.only(bottom: (index == foodCalorieDetails.dinner.length - 1) ? config.App(context).appHeight(2) : 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: config.App(context).appWidth(24),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            commonText(
                                              foodCalorieDetails.dinner[index].consumedFood,
                                            ),
                                            commonText(
                                              "${foodCalorieDetails.dinner[index].consumedCalories} cals",
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.dinner[index].consumedCarbohydrates}g",
                                          )),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.dinner[index].consumedProteins}g",
                                          )),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.dinner[index].consumedFats}g",
                                          )),
                                    ],
                                  ),
                                );
                              })
                          : Container(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(4),
              ),
              Container(
                width: config.App(context).appWidth(80),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Color(0xFF3D434D),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(5)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: config.App(context).appHeight(2), bottom: config.App(context).appHeight(2)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Snacks",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Visibility(
                              visible: (isFutureDate(selectedDate) && editCalorieId != 0),
                              child: IconButton(
                                padding: EdgeInsets.zero,
                                constraints: BoxConstraints(),
                                icon: Image.asset("assets/img/icons/Add_Button.png", width: 24),
                                onPressed: () {
                                  _typeOfMeal = 'Snacks';
                                  addMealDialog(
                                      context: context,
                                      setState: setState,
                                      onClickEvent: () {
                                        createFoodCalories();
                                      });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      (isBindFoodCalorie && foodCalorieDetails.snacks.length > 0)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    width: config.App(context).appWidth(24),
                                    padding: EdgeInsets.symmetric(
                                      vertical: 5,
                                      horizontal: 3,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.white,
                                    ),
                                    child: commonText("${foodCalorieDetails.caloriesBreakdownData.totalSnacksCalories} Calories",
                                        textColor: Theme.of(context).primaryColor)),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalSnacksCarbohydrates}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalSnacksProteins}g",
                                    )),
                                Container(
                                    width: config.App(context).appWidth(12),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    child: commonText(
                                      "${foodCalorieDetails.caloriesBreakdownData.totalSnacksFats}g",
                                    )),
                              ],
                            )
                          : Container(),
                      isBindFoodCalorie
                          ? ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: foodCalorieDetails.snacks.length > 0 ? config.App(context).appHeight(1.5) : 0),
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemCount: foodCalorieDetails.snacks.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding:
                                      EdgeInsets.only(bottom: (index == foodCalorieDetails.snacks.length - 1) ? config.App(context).appHeight(2) : 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        width: config.App(context).appWidth(24),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            commonText(
                                              foodCalorieDetails.snacks[index].consumedFood,
                                            ),
                                            commonText(
                                              "${foodCalorieDetails.snacks[index].consumedCalories} cals",
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.snacks[index].consumedCarbohydrates}g",
                                          )),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.snacks[index].consumedProteins}g",
                                          )),
                                      Container(
                                          width: config.App(context).appWidth(12),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.symmetric(vertical: 5),
                                          child: commonText(
                                            "${foodCalorieDetails.snacks[index].consumedFats}g",
                                          )),
                                    ],
                                  ),
                                );
                              })
                          : Container(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(2),
              ),
            ],
          ),
        ),
      ),
    );
  }

  openEditGoalDialog({BuildContext context, Function onClickEvent}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.all(16),
              child: Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                  maxWidth: MediaQuery.of(context).size.width,
                ),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Container(
                        margin: const EdgeInsets.only(top: 50),
                        decoration: BoxDecoration(
                          color: Color(0xFF303743),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(2),
                          horizontal: config.App(context).appHeight(2),
                        ),
                        child: Column(mainAxisSize: MainAxisSize.min, children: [
                          SizedBox(
                            height: config.App(context).appHeight(2),
                          ),
                          Text(
                            editCalorieId != 0 ? "Edit Goal".toUpperCase() : "Set Goal".toUpperCase(),
                            style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(3),
                          ),
                          Visibility(
                            visible: editCalorieId != 0,
                            child: Padding(
                              padding: EdgeInsets.only(bottom: config.App(context).appHeight(3)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    " Your current goal",
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context).textTheme.subtitle1,
                                  ),
                                  Text(
                                    "${calorieDetails.calorieIntakeGoal.toString()} Calories",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: 14, color: Colors.grey),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              editCalorieId != 0 ? " Enter new goal" : " Enter goal",
                              textAlign: TextAlign.start,
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(3),
                          ),
                          TextField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Color(0xFF091727),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                // borderSide: BorderSide(color: Color(0xFF091727))
                              ),
                              contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 12.0, bottom: 12.0),
                              counterText: '',
                              hintText: "000",
                              hintStyle: TextStyle(color: Colors.grey),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(color: Color(0xFF091727)),
                              ),
                              suffix: Text("Calories", style: TextStyle(color: Colors.grey)),
                            ),
                            controller: newGoalController,
                            maxLines: 1,
                            minLines: 1,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                            maxLength: 10,
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(3),
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: InkWell(
                                onTap: () {
                                  newGoalController.clear();
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  width: config.App(context).appWidth(35),
                                  padding: EdgeInsets.symmetric(
                                    vertical: config.App(context).appHeight(2),
                                    horizontal: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    // color: Color(0xFF3D434D),
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Text(
                                    "Cancel",
                                    style: Theme.of(context).textTheme.subtitle1,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )),
                              SizedBox(width: 16),
                              Expanded(
                                  child: InkWell(
                                onTap: () {
                                  if (newGoalController.text.trim().isNotEmpty) {
                                    if (int.parse(newGoalController.text.trim()) > 0) {
                                      Navigator.of(context).pop();
                                      onClickEvent(int.parse(newGoalController.text.trim()));
                                      newGoalController.clear();
                                    } else {
                                      CustomWidgets.buildErrorSnackBar(context, "New goal calories should be greater then 0");
                                    }
                                  } else {
                                    CustomWidgets.buildErrorSnackBar(context, "Enter your new goal calories");
                                  }
                                },
                                child: Container(
                                  width: config.App(context).appWidth(35),
                                  padding: EdgeInsets.symmetric(
                                    vertical: config.App(context).appHeight(2),
                                    horizontal: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: Text(
                                    editCalorieId != 0 ? "Edit Goal" : "Set Goal",
                                    style: Theme.of(context).textTheme.subtitle1,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ))
                            ],
                          )
                        ]),
                      ),
                    ),
                    Positioned(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Icon(Icons.close, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  addMealDialog({BuildContext context, Function onClickEvent, StateSetter setState}) {
    foodQuantityController.clear();
    foodNameController.clear();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
            return Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.all(16),
              child: Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                  maxWidth: MediaQuery.of(context).size.width,
                ),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Container(
                        margin: const EdgeInsets.only(top: 50),
                        decoration: BoxDecoration(
                          color: Color(0xFF303743),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(2),
                          horizontal: config.App(context).appHeight(2),
                        ),
                        child: SingleChildScrollView(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            Text(
                              "Add Meal".toUpperCase(),
                              style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                " Type of Meal",
                                textAlign: TextAlign.start,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            /*Container(
                              padding: const EdgeInsets.only(
                                  left: 24.0, right: 20.0),
                              alignment: Alignment.centerLeft,
                              decoration: BoxDecoration(
                                color: Color(0xFF091727),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                icon: Icon(
                                  Icons.keyboard_arrow_down_outlined,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                isExpanded: true,
                                value: _typeOfMeal,
                                onChanged: (v) {
                                  setState(() {
                                    _typeOfMeal = v;
                                  });
                                },
                                items: mealType
                                    .map((e) => DropdownMenuItem<String>(
                                          value: e,
                                          child: new Text(e),
                                        ))
                                    .toList(),
                              )),
                            ),*/
                            Container(
                              decoration: BoxDecoration(
                                color: Color(0xFF091727),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              padding: EdgeInsets.only(left: 24.0, right: 10.0, top: 16, bottom: 16),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                _typeOfMeal,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                " Food",
                                textAlign: TextAlign.start,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            TextField(
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xFF091727),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 12.0, bottom: 12.0),
                                counterText: '',
                                hintText: "Enter Food Name",
                                hintStyle: TextStyle(color: Colors.grey),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(color: Color(0xFF091727)),
                                ),
                              ),
                              controller: foodNameController,
                              maxLines: 1,
                              minLines: 1,
                              keyboardType: TextInputType.name,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                " Quantity",
                                textAlign: TextAlign.start,
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: TextField(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Color(0xFF091727),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                        // borderSide: BorderSide(color: Color(0xFF091727))
                                      ),
                                      contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 12.0, bottom: 12.0),
                                      counterText: '',
                                      hintText: "80",
                                      hintStyle: TextStyle(color: Colors.grey),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                        borderSide: BorderSide(color: Color(0xFF091727)),
                                      ),
                                    ),
                                    controller: foodQuantityController,
                                    maxLines: 1,
                                    minLines: 1,
                                    maxLength: 2,
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width / 3,
                                  margin: EdgeInsets.only(left: 10),
                                  padding: const EdgeInsets.only(left: 24.0, right: 10.0),
                                  alignment: Alignment.centerLeft,
                                  decoration: BoxDecoration(
                                    color: Color(0xFF091727),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: DropdownButtonHideUnderline(
                                      child: DropdownButton<String>(
                                    isExpanded: true,
                                    icon: Icon(
                                      Icons.keyboard_arrow_down_outlined,
                                      color: Colors.white,
                                      size: 30,
                                    ),
                                    value: _unitOfQuantity,
                                    onChanged: (v) {
                                      setState(() {
                                        _unitOfQuantity = v;
                                        print("_unitOfQuantity : $v");
                                      });
                                    },
                                    items: quantityUnit
                                        .map((e) => DropdownMenuItem<String>(
                                              value: e,
                                              child: new Text(e),
                                            ))
                                        .toList(),
                                  )),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: InkWell(
                                  onTap: () {
                                    foodNameController.clear();
                                    Navigator.of(context).pop();
                                  },
                                  child: Container(
                                    width: config.App(context).appWidth(35),
                                    padding: EdgeInsets.symmetric(
                                      vertical: config.App(context).appHeight(2),
                                      horizontal: 20,
                                    ),
                                    decoration: BoxDecoration(
                                      // color: Color(0xFF3D434D),
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    child: Text(
                                      "Cancel",
                                      style: Theme.of(context).textTheme.subtitle1,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                )),
                                SizedBox(width: 16),
                                Expanded(
                                    child: InkWell(
                                  onTap: () {
                                    if (foodNameController.text.trim().isNotEmpty) {
                                      if (foodQuantityController.text.trim().isNotEmpty) {
                                        if (int.parse(foodQuantityController.text.trim()) > 0) {
                                          Navigator.of(context).pop();
                                          onClickEvent();
                                        } else {
                                          CustomWidgets.buildErrorSnackBar(context, "Please enter valid quantity");
                                        }
                                      } else {
                                        CustomWidgets.buildErrorSnackBar(context, "Please enter quantity");
                                      }
                                    } else {
                                      CustomWidgets.buildErrorSnackBar(context, "Please enter food name");
                                    }
                                  },
                                  child: Container(
                                    width: config.App(context).appWidth(35),
                                    padding: EdgeInsets.symmetric(
                                      vertical: config.App(context).appHeight(2),
                                      horizontal: 20,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(12),
                                    ),
                                    child: Text(
                                      "Add".toUpperCase(),
                                      style: Theme.of(context).textTheme.subtitle1,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ))
                              ],
                            )
                          ]),
                        ),
                      ),
                    ),
                    Positioned(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Icon(Icons.close, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
        });
  }

  bool isFutureDate(DateTime selectedDate) {
    return (selectedDate.isAfter(DateTime.now()) ||
        DateTime(selectedDate.year, selectedDate.month, selectedDate.day) == DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day));
  }

  createFoodCalories() {
    BlocProvider.of<CalorieBloc>(context).add(CreateFoodCalorieEvent(
        calorieObjId: editCalorieId,
        typeOfMeal: _typeOfMeal,
        consumedFood: foodNameController.text,
        quantityOfFood: '${foodQuantityController.text} ${getQuantityOfFood()}'));
  }

  String getQuantityOfFood() {
    if (_unitOfQuantity == 'Grams') {
      return 'g';
    } else if (_unitOfQuantity == 'Kilogram') {
      return 'kg';
    } else if (_unitOfQuantity == 'Milliliter') {
      return 'ml';
    } else {
      return 'l';
    }
  }

  double calculateCalories() {
    double percent = 0.0;
    percent = ((calorieDetails.totalConsumedCalories * 100) / calorieDetails.calorieIntakeGoal) / 100;
    if (percent > 1.0) {
      percent = 1.0;
    }
    return percent;
  }

  Widget commonText(String text, {Color textColor = Colors.white}) {
    return Text(
      text,
      style: Theme.of(context).textTheme.headline4.copyWith(
            color: textColor,
            fontSize: 12,
          ),
      overflow: TextOverflow.ellipsis,
      maxLines: 1,
    );
  }
}
