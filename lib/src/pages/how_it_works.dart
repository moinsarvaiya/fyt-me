import 'package:flutter/material.dart';

import '../elements/DrawerWidget.dart';
import '../elements/logoButtonWidget.dart';
import '../helpers/app_config.dart' as config;
import 'how_it_works_client.dart';
import 'how_it_works_instructor.dart';

class HowItWorksWidget extends StatefulWidget {
  @override
  _HowItWorksWidgetState createState() => _HowItWorksWidgetState();
}

class _HowItWorksWidgetState extends State<HowItWorksWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF105971),
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset("assets/img/icons/arrow_back.png"),
          iconSize: 40,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: LogoButtonWidget(),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
              icon: Image.asset("assets/img/icons/menu2.png"),
              iconSize: 40,
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
        ],
      ),
      drawer: DrawerWidget(),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: config.App(context).appWidth(10),
              vertical: 40,
            ),
            child: Text(
              "HOW IT WORKS",
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          Center(
            child: Image.asset(
              "assets/img/how_it_work/how_it_work.png",
              width: config.App(context).appWidth(80),
            ),
          ),
          Positioned(
            bottom: -1 * config.App(context).appWidth(25),
            left: 0,
            child: Container(
              width: config.App(context).appWidth(120),
              height: config.App(context).appWidth(120),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.3),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Positioned(
            bottom: config.App(context).appHeight(10),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerRight,
                    width: config.App(context).appWidth(80),
                    child: Text(
                      "choose one",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(5),
                  ),
                  Container(
                    width: config.App(context).appWidth(80),
                    decoration: BoxDecoration(
                      color: config.Colors().accentDarkColor(1),
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Colors.white,
                        width: 1,
                      ),
                    ),
                    child: TextButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          "INSTRUCTOR",
                          style: Theme.of(context).textTheme.headline4.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => HowItWorksInstructorWidget()),
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(1),
                  ),
                  Text(
                    "or",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(1),
                  ),
                  Container(
                    width: config.App(context).appWidth(80),
                    decoration: BoxDecoration(
                      color: config.Colors().accentDarkColor(1),
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Colors.white,
                        width: 1,
                      ),
                    ),
                    child: TextButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          "CLIENT",
                          style: Theme.of(context).textTheme.headline4.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => HowItWorksClientWidget()),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
