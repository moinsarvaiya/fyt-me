import 'package:flutter/material.dart';

import '../helpers/helper.dart';
import 'home/search_trainers.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: <Widget>[
            Container(
              child: Center(
                child: Image.asset(
                  'assets/img/splash/splash.png',
                  fit: BoxFit.cover,
                  width: screenSize.width,
                ),
              ),
            ),
            Positioned(
              top: screenSize.height / 3.2,
              right: 30,
              child: Image.asset(
                "assets/img/splash/logo.png",
                width: screenSize.width / 2.2,
              ),
            ),
            Positioned(
              bottom: 20,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "WELLNESS AND FITNESS \n INSTRUCTORS",
                    style: Theme.of(context).textTheme.subtitle1,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SearchTrainer(
                                    isLoggedIn: false,
                                  )));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 17),
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2,
                          color: Colors.white,
                        ),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Search Near Me",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/SignUp');
                    },
                    child: Container(
                      width: screenSize.width / 1.9,
                      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Text(
                        "Sign Up",
                        style: Theme.of(context).textTheme.subtitle1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      print("Already have an account?");
                      Navigator.of(context).pushNamed('/Login');
                    },
                    child: Container(
                      height: kToolbarHeight / 1.20,
                      width: MediaQuery.of(context).size.width / 1.3,
                      child: new Align(
                        alignment: Alignment.center,
                        child: new RichText(
                          text: new TextSpan(
                              style: new TextStyle(
                                fontSize: 14.0,
                                color: Color.fromRGBO(61, 131, 225, 1),
                              ),
                              children: <TextSpan>[
                                new TextSpan(
                                  text: 'Already have an account?',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                                new TextSpan(
                                  text: ' Sign in',
                                  style: Theme.of(context).textTheme.subtitle1.copyWith(
                                        color: Theme.of(context).primaryColor,
                                        decoration: TextDecoration.underline,
                                      ),
                                ),
                              ]),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
