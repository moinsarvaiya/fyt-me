import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';

import '../../helpers/app_config.dart' as config;

class UsernameInput extends StatefulWidget {
  @override
  __UsernameInputState createState() => __UsernameInputState();
}

class __UsernameInputState extends State<UsernameInput> {
  TextEditingController _nameController;

  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _nameController = TextEditingController(
      text: _authBloc.state.userName.value,
    )..addListener(_onUserNameChanged);
  }

  void _onUserNameChanged() {
    _authBloc?.add(
      AuthUserNameChanged(userName: _nameController?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _nameController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (BuildContext context, state) {
      if (state.formType == EmailPasswordSignInFormType.register)
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: config.App(context).appHeight(3),
            ),
            Text(
              "Username",
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(
              height: config.App(context).appHeight(1),
            ),
            TextFormField(
              textInputAction: TextInputAction.next,
              onEditingComplete: FocusScope.of(context).nextFocus,
              controller: _nameController,
              keyboardType: TextInputType.text,
              cursorColor: Colors.white,
              style: Theme.of(context).textTheme.headline4,
              decoration: InputDecoration(
                errorText: state.userName.value.isEmpty
                    ? null
                    : state.userName.isNotValid
                        ? 'username must be at least 3 digits long'
                        : null,
                filled: true,
                fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                suffixIcon: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: config.App(context).appHeight(2),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none,
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none,
                ),
              ),
            ),
          ],
        );
      return Container();
    });
  }
}
