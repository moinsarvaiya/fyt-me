import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';

class AuthButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      if (state.formType == EmailPasswordSignInFormType.signIn)
        return CustomButton(
          callback: (state.email.isValid && state.password.isValid)
              ? () {
                  printLog('sing in');
                  FocusScope.of(context).unfocus();
                  context.read<AuthBloc>().add(AuthSubmitted());
                }
              : null,
          string: 'Sign In',
        );
      if (state.formType == EmailPasswordSignInFormType.register)
        return CustomButton(
          callback: (state.email.isValid && state.password.isValid && state.userName.isValid && (state.password.value == state.conFirmPassword))
              ? () {
                  FocusScope.of(context).unfocus();
                  context.read<AuthBloc>().add(AuthSubmitted());
                }
              : null,
          string: 'Create Account',
        );
      return CustomButton(
        callback: state.isEmailValid ? () => context.read<AuthBloc>().add(AuthSubmitted()) : null,
        string: 'Submit',
      );
    });
  }
}
