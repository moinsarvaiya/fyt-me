import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';

class SwitcherForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        return TextButton(
            onPressed: () {
              if (state.formType == EmailPasswordSignInFormType.register) {
                context.read<AuthBloc>().add(LoginForm());
              } else {
                context.read<AuthBloc>().add(RegisterForm());
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width / 1.3,
                child: new Align(
                    alignment: Alignment.center,
                    child: new RichText(
                      text: new TextSpan(
                          style: new TextStyle(
                            fontSize: 14.0,
                            color: Color.fromRGBO(61, 131, 225, 1),
                          ),
                          children: <TextSpan>[
                            new TextSpan(
                              text: state.formType == EmailPasswordSignInFormType.register ? 'Back to' : 'Don`t have an account?',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            new TextSpan(
                              text: state.formType == EmailPasswordSignInFormType.register ? ' Sign in' : ' Sign up',
                              style: Theme.of(context).textTheme.subtitle1.copyWith(
                                    color: Theme.of(context).primaryColor,
                                    decoration: TextDecoration.underline,
                                  ),
                            ),
                          ]),
                    ))));
      },
    );
  }
}
