import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_event.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';

import '../../helpers/app_config.dart' as config;
import '../../helpers/helper.dart';
import 'index.dart';

class AuthWidgets extends StatefulWidget {
  AuthWidgets({@required this.formType});

  final EmailPasswordSignInFormType formType;

  @override
  _AuthWidgetsState createState() => _AuthWidgetsState();
}

class _AuthWidgetsState extends State<AuthWidgets> {
  @override
  void initState() {
    super.initState();
    if (widget.formType == EmailPasswordSignInFormType.signIn) {
      printLog('true');
      context.read<AuthBloc>().add(LoginForm());
    } else if (widget.formType == EmailPasswordSignInFormType.forgetPassword) {
      printLog('EmailPasswordSignInFormType.forgetPassword');
      context.read<AuthBloc>().add(ForgetPasswordForm());
    } else {
      context.read<AuthBloc>().add(RegisterForm());
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: IconButton(
            icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
            iconSize: 40,
            onPressed: () {
              context.read<AuthenticationBloc>().add(LoggedOut());
              Navigator.of(context).pushNamed('/Welcome');
            },
          ),
          title: Image.asset(
            "assets/img/splash/logo.png",
            height: config.App(context).appHeight(3),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        body: Container(
          padding: EdgeInsets.only(
            top: config.App(context).appHeight(13),
            bottom: config.App(context).appHeight(2),
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF2E94D0),
                Color(0xFF2B7DC9),
                Color(0xFF0B4B98),
                Color(0xFF09111D),
              ],
            ),
          ),
          child: BlocListener<AuthBloc, AuthState>(
            listenWhen: (previous, current) => previous.status.status != current.status.status,
            listener: (BuildContext context, state) {
              switch (state.status.status) {
                case StateStatuses.loading:
                  CustomLoader().show(context);
                  break;
                case StateStatuses.failure:
                  CustomLoader().hide(context);
                  CustomWidgets.buildErrorSnackBar(context, state.status.message);
                  break;

                case StateStatuses.success:
                default:
                  CustomLoader().hide(context);
                  break;
              }
            },
            child: SingleChildScrollView(
              child: Container(
                width: config.App(context).appWidth(100),
                padding: const EdgeInsets.only(
                  right: 20,
                  left: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    HeadText(),
                    UsernameInput(),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    EmailInput(),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    PasswordInput(),
                    SizedBox(
                      height: config.App(context).appHeight(1),
                    ),
                    ConfirmPasswordInput(),
                    ForgetPassword(),
                    AgreeToTerms(),
                    SizedBox(
                      height: config.App(context).appHeight(2),
                    ),
                    AuthButton(),
                    Platform.isIOS ? Container() : SocialAuth(),
                    SwitcherForm(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AgreeToTerms extends StatefulWidget {
  @override
  _AgreeToTermsState createState() => _AgreeToTermsState();
}

class _AgreeToTermsState extends State<AgreeToTerms> {
  bool _agree = true;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state.formType == EmailPasswordSignInFormType.register)
          return SizedBox(
            height: config.App(context).appHeight(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Checkbox(
                  value: _agree,
                  onChanged: (value) {
                    if (value == !_agree) {
                      setState(() {
                        _agree = value;
                      });
                    }
                  },
                  activeColor: Theme.of(context).primaryColor,
                ),
                Align(
                  alignment: Alignment.center,
                  child: new RichText(
                    text: new TextSpan(
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Color.fromRGBO(61, 131, 225, 1),
                        height: 1.3,
                      ),
                      children: <TextSpan>[
                        new TextSpan(
                          text: 'I agree to the ',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        new TextSpan(
                            text: ' Terms & Conditions',
                            style: Theme.of(context).textTheme.subtitle1.copyWith(
                                  color: Theme.of(context).primaryColor,
                                  decoration: TextDecoration.underline,
                                ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pushNamed(context, '/TermsAndConditions');
                              }),
                        new TextSpan(
                          text: '\n and ',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        new TextSpan(
                            text: ' Privacy Policy',
                            style: Theme.of(context).textTheme.subtitle1.copyWith(
                                  color: Theme.of(context).primaryColor,
                                  decoration: TextDecoration.underline,
                                ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pushNamed(context, '/PrivacyAndPolicy');
                              }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );

        return Container();
      },
    );
  }
}
