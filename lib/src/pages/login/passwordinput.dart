import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';

import '../../helpers/app_config.dart' as config;

class PasswordInput extends StatefulWidget {
  @override
  __PasswordInputState createState() => __PasswordInputState();
}

class __PasswordInputState extends State<PasswordInput> {
  TextEditingController _passwordController;

  AuthBloc _authBloc;
  bool isShowPassword = true;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _passwordController = TextEditingController(
      text: _authBloc.state.password.value,
    )..addListener(_onPasswordValueChanged);
  }

  void _onPasswordValueChanged() {
    _authBloc?.add(
      AuthPasswordChanged(password: _passwordController?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _passwordController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (BuildContext context, state) {
        if (state.formType == EmailPasswordSignInFormType.register || state.formType == EmailPasswordSignInFormType.signIn)
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Password",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              SizedBox(
                height: config.App(context).appHeight(1),
              ),
              Stack(
                alignment: Alignment.topRight,
                children: [
                  TextFormField(
                    obscureText: isShowPassword,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: FocusScope.of(context).nextFocus,
                    controller: _passwordController,
                    keyboardType: TextInputType.text,
                    cursorColor: Colors.white,
                    style: Theme.of(context).textTheme.headline4,
                    decoration: InputDecoration(
                      errorText: state.formType == EmailPasswordSignInFormType.register
                          ? (state.password.value.isEmpty
                              ? null
                              : state.password.isNotValid
                                  ? 'Password must be 8 character'
                                  : null)
                          : (state.password.value.isEmpty
                              ? null
                              : state.password.isNotValid
                                  ? 'Password was 8 character'
                                  : null),
                      filled: true,
                      fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                      /*suffixIcon: Icon(
                    Icons.lock,
                    color: Colors.white,
                  ),*/
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 25,
                        vertical: config.App(context).appHeight(1),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide.none,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide.none,
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide.none,
                      ),
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.transparent,
                        minimumSize: Size.zero, // <-- Add this
                        padding: EdgeInsets.zero, // <-- and this
                        elevation: 0),
                    onPressed: () {
                      setState(() {
                        isShowPassword = !isShowPassword;
                      });
                    },
                    child: Icon(isShowPassword ? Icons.visibility : Icons.visibility_off),
                  )
                ],
              ),
            ],
          );
        return Container();
      },
    );
  }
}
