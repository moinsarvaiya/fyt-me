import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';

import '../../helpers/app_config.dart' as config;

class EmailInput extends StatefulWidget {
  @override
  __EmailInputState createState() => __EmailInputState();
}

class __EmailInputState extends State<EmailInput> {
  TextEditingController _emailController;

  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _emailController = TextEditingController(
      text: _authBloc.state.email.value,
    )..addListener(_onEmailValueChanged);
  }

  void _onEmailValueChanged() {
    _authBloc?.add(
      AuthEmailChanged(email: _emailController?.text ?? ''),
    );
  }

  @override
  void dispose() {
    _emailController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (BuildContext context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Email",
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(
              height: config.App(context).appHeight(1),
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onEditingComplete: FocusScope.of(context).nextFocus,
              controller: _emailController,
              cursorColor: Colors.white,
              style: Theme.of(context).textTheme.headline4,
              decoration: InputDecoration(
                errorText: state.email.value.isEmpty
                    ? null
                    : state.email.isNotValid
                        ? 'enter a valid email address'
                        : null,
                alignLabelWithHint: true,
                filled: true,
                fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                suffixIcon: Icon(
                  Icons.email,
                  color: Colors.white,
                ),
                contentPadding: EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: config.App(context).appHeight(1),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none,
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide.none,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
