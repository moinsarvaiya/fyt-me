import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';

class ForgetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      if (state.formType == EmailPasswordSignInFormType.signIn)
        return Align(
          alignment: Alignment.bottomRight,
          child: InkWell(
            onTap: () {
              context.read<AuthBloc>().add(ForgetPasswordForm());
            },
            child: Text(
              "Forgot Password?",
              style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Theme.of(context).primaryColor,
                    height: 3,
                    decoration: TextDecoration.underline,
                  ),
            ),
          ),
        );
      return Container();
    });
  }
}
