import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:the_apple_sign_in/the_apple_sign_in.dart';

import '../../helpers/app_config.dart' as config;

class SocialAuth extends StatelessWidget {
  final GoogleSignIn googleSignIn = new GoogleSignIn();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      if (state.formType == EmailPasswordSignInFormType.signIn)
        return Align(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Platform.isIOS
                  ? SizedBox(
                      height: config.App(context).appHeight(2),
                    )
                  : Container(),
              Platform.isIOS
                  ? TextButton(
                      onPressed: () {
                        signInWithApple(context);
                      },
                      child: Container(
                        width: config.App(context).appWidth(70),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(3),
                          horizontal: 20,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(
                              Icons.apple,
                              color: Colors.black,
                              size: 20,
                            ),
                            Text(
                              "Sign in with Apple",
                              style: Theme.of(context).textTheme.subtitle1.copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(),
              SizedBox(
                height: config.App(context).appHeight(2),
              ),
              TextButton(
                onPressed: () {
                  signInWithGoogle(context);
                },
                child: Container(
                  width: config.App(context).appWidth(70),
                  padding: EdgeInsets.symmetric(
                    vertical: config.App(context).appHeight(3),
                    horizontal: 20,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Image.asset("assets/img/icons/Google.png"),
                      Text(
                        "Sign In With Google",
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: config.App(context).appHeight(2),
              ),
              Visibility(
                visible: false,
                child: TextButton(
                  onPressed: () {
                    signInWithFacebook(context);
                  },
                  child: Container(
                    width: config.App(context).appWidth(70),
                    padding: EdgeInsets.symmetric(
                      vertical: config.App(context).appHeight(3),
                      horizontal: 20,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Image.asset("assets/img/icons/Facebook.png"),
                        Text(
                          "Sign In With Facebook",
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.w400,
                              ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      return Container();
    });
  }

  Future<void> signInWithGoogle(BuildContext context) async {
    // _isLoading = true;
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication gSA = await googleSignInAccount.authentication;
    AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: gSA.accessToken,
      idToken: gSA.idToken,
    );
    UserCredential authResult = await FirebaseAuth.instance.signInWithCredential(credential);
    User user = authResult.user;
    user.getIdToken().then((token) {
      googleSignIn.signOut();
      print('F-Name : ${user.displayName.split(" ")[0]}');
      print('L-Name : ${user.displayName.split(" ")[1]}');
      print('Email : ${user.email}');
      print('Token : ${gSA.accessToken}');
      context.read<AuthBloc>().add(SocialSignUp(provider: 'google-oauth2', accessToken: gSA.accessToken));
    });
  }

  Future<void> signInWithFacebook(BuildContext context) async {
    try {
      final LoginResult result = await FacebookAuth.instance.login();
      if (result.status == LoginStatus.success) {
        final AuthCredential facebookCredential = FacebookAuthProvider.credential(result.accessToken.token);
        final userCredential = await FirebaseAuth.instance.signInWithCredential(facebookCredential);
        print('userCredential : $userCredential');
        print('facebookCredential : $facebookCredential');
        print('Token : ${result.accessToken.token}');
        context.read<AuthBloc>().add(SocialSignUp(provider: 'facebook', accessToken: result.accessToken.token));
      } else {
        print('status : ${result.status}');
      }
    } on FirebaseAuthException catch (e) {
      CustomWidgets.buildErrorSnackBar(context, e.message);
    }
  }

  Future<void> signInWithApple(BuildContext context) async {
    if (await TheAppleSignIn.isAvailable()) {
      final AuthorizationResult result = await TheAppleSignIn.performRequests([
        AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
      ]);

      switch (result.status) {
        case AuthorizationStatus.authorized:
          print(result.credential.user);
          print(result.credential.fullName.givenName);
          print(result.credential.fullName.familyName);
          print(result.credential.email);
          context.read<AuthBloc>().add(SocialSignUp(provider: 'apple', accessToken: result.credential.user));
          break;
        case AuthorizationStatus.error:
          print("Sign in failed: ${result.error.localizedDescription}");
          break;
        case AuthorizationStatus.cancelled:
          print('User cancelled');
          break;
      }
    } else {
      CustomWidgets.buildErrorSnackBar(context, 'Your device not support apple sign in');
    }
  }
}

class Resource {
  final Status status;

  Resource({@required this.status});
}

enum Status { Success, Error, Cancelled }
