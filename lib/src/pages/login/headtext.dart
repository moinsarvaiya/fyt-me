import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/authbloc/auth_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';

import '../../helpers/app_config.dart' as config;

class HeadText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state.formType == EmailPasswordSignInFormType.forgetPassword)
          return SizedBox(
            height: config.App(context).appHeight(20),
          );
        if (state.formType == EmailPasswordSignInFormType.signIn)
          return Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Login",
              style: TextStyle(
                color: Colors.white,
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
            ),
          );

        return Align(
          alignment: Alignment.topLeft,
          child: Text(
            "CREATE ACCOUNT",
            style: Theme.of(context).textTheme.subtitle1,
          ),
        );
      },
    );
  }
}
