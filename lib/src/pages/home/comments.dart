import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/user_comments.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:http/http.dart' as http;

import '../../helpers/app_config.dart' as config;

class Comment extends StatefulWidget {
  final int currentTab;
  final String imageId;
  final int userImagesObj;
  final int userId;
  final UserComments userComments;
  final String imagePath;

  Comment(
      {Key key,
      @required this.currentTab,
      @required this.imageId,
      @required this.userImagesObj,
      @required this.userId,
      @required this.userComments,
      @required this.imagePath})
      : super(key: key);

  @override
  FytmeDetailScreenPage createState() => FytmeDetailScreenPage();
}

class FytmeDetailScreenPage extends State<Comment> {
  TextEditingController commentController = TextEditingController();

  int currentTab = 0;
  bool isLoading = false;
  List<UserList> likesList = [];
  List<Data> commentsList = [];
  int totalLikes = 0;
  int totalComments = 0;

  @override
  void initState() {
    currentTab = widget.currentTab;
    print(widget.imageId.toString());
    if (widget.userComments.data.isNotEmpty) {
      setState(() {
        totalLikes = widget.userComments.totalLikes != null ? widget.userComments.totalLikes : 0;
        totalComments = widget.userComments.totalComments != null ? widget.userComments.totalComments : 0;
        likesList = widget.userComments.userList;
        commentsList = widget.userComments.data.where((i) => i.commentedBy != null).toList();
      });
    }
    super.initState();
  }

  showLoader() {
    setState(() {
      isLoading = true;
    });
  }

  dismissLoader() {
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
        return true;
      },
      child: Scaffold(
        body: Container(
          child: Stack(children: [
            Column(children: [
              _headerImageWidget(),
              _tabbingWidget(),
              currentTab == 0 ? _memberList() : _commentList(),
            ]),
            closeWidgetBtn(),
            isLoading
                ? AlertDialog(
                    content: Container(
                      height: MediaQuery.of(context).size.height * .1,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                          ),
                        ),
                      ),
                    ),
                  )
                : Container()
          ]),
        ),
        bottomSheet: currentTab == 0 ? _memberBottomWidget() : _commentBottomWidget(),
      ),
    );
  }

  closeWidgetBtn() {
    return Container(
        alignment: Alignment.topRight,
        margin: EdgeInsets.only(top: 32.0),
        child: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.close, color: Colors.white),
        ));
  }

  Future<bool> onWillPop() {
    goBack();
    return Future.value(true);
  }

  void goBack() {
    Navigator.pop(context, true);
  }

  Widget _headerImageWidget() {
    return Container(
      height: config.App(context).appHeight(35),
      width: config.App(context).appWidth(100),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: CachedNetworkImage(
          imageUrl: widget.imagePath,
          fit: BoxFit.cover,
          errorWidget: (context, url, error) => Icon(
            Icons.person,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _tabbingWidget() {
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(16),
        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          InkWell(
              onTap: () {
                this.setState(() {
                  currentTab = 0;
                });
              },
              child: Container(
                  child: Text('$totalLikes FYT',
                      style: TextStyle(color: currentTab == 0 ? Colors.white : Colors.grey, fontSize: 12, fontWeight: FontWeight.normal)))),
          SizedBox(width: 30),
          Container(
              alignment: Alignment.centerLeft,
              // color: lightGrayColor,
              child: InkWell(
                onTap: () {
                  this.setState(() {
                    currentTab = 1;
                  });
                },
                child: Container(
                    alignment: Alignment.center,
                    child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Icon(Icons.mode_comment, size: 18, color: currentTab == 1 ? Colors.white : Colors.grey),
                      SizedBox(width: 10),
                      Text('$totalComments Comments',
                          style: TextStyle(color: currentTab == 1 ? Colors.white : Colors.grey, fontSize: 12, fontWeight: FontWeight.normal)),
                    ])),
              ))
        ]));
  }

  Widget _memberList() {
    return Expanded(
      child: Container(
        child: ListView.builder(
          padding: EdgeInsets.only(bottom: 100),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          itemCount: totalLikes,
          itemBuilder: (context, index) {
            return ListTile(
              dense: true,
              leading: Container(
                height: 30,
                width: 30,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(50.0),
                    child: CachedNetworkImage(
                      imageUrl: likesList[index].profilePicture != null
                          ? likesList[index].profilePicture.contains('https://dev.fytme.co.uk')
                              ? likesList[index].profilePicture
                              : 'https://dev.fytme.co.uk/mediafiles/${likesList[index].profilePicture}'
                          : '',
                      fit: BoxFit.cover,
                      errorWidget: (context, url, error) => Icon(
                        Icons.person,
                        color: Colors.white,
                      ),
                    )),
              ),
              title: Text(likesList[index].fullName, style: Theme.of(context).textTheme.subtitle1),
            );
          },
        ),
      ),
    );
  }

  Widget _commentList() {
    return Expanded(
      child: Container(
          //padding: EdgeInsets.only(left: 10),
          child: ListView.builder(
              padding: EdgeInsets.only(bottom: 100),
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              physics: AlwaysScrollableScrollPhysics(),
              itemCount: totalComments,
              itemBuilder: (context, index) {
                return ListTile(
                    // dense: true,
                    leading: Container(
                      height: 40,
                      width: 40,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(50.0),
                          child: CachedNetworkImage(
                            imageUrl: commentsList[index].commentedBy.profilePicture != null
                                ? commentsList[index].commentedBy.profilePicture.contains('https://dev.fytme.co.uk')
                                    ? commentsList[index].commentedBy.profilePicture
                                    : 'https://dev.fytme.co.uk${commentsList[index].commentedBy.profilePicture}'
                                : '',
                            fit: BoxFit.cover,
                            errorWidget: (context, url, error) => Icon(
                              Icons.person,
                              color: Colors.white,
                            ),
                          )),
                    ),
                    title: Card(
                        margin: EdgeInsets.only(right: 5),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(12.0))),
                        elevation: 0,
                        color: Color(0xFF0a1726),
                        // child:
                        child: Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.all(16),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(commentsList[index].commentedBy.fullName,
                                    textAlign: TextAlign.start, style: Theme.of(context).textTheme.subtitle1),
                                SizedBox(height: 8),
                                Text(
                                  commentsList[index].comment,
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 12),
                                  textAlign: TextAlign.start,
                                ),
                              ],
                            ))));
              })),
    );
  }

  Widget _memberBottomWidget() {
    return Container(
      color: config.Colors().secondDarkColor(1),
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: InkWell(
              onTap: () {
                showLoader();
                sendUserLikes();
              },
              child: Container(
                margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                //width: config.App(context).appWidth(35),
                padding: EdgeInsets.symmetric(
                  vertical: config.App(context).appHeight(2),
                  horizontal: config.App(context).appHeight(2),
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text(
                  "FYT",
                  style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  currentTab = 1;
                });
              },
              child: Container(
                  margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                  //width: config.App(context).appWidth(35),
                  padding: EdgeInsets.symmetric(
                    vertical: config.App(context).appHeight(2),
                    horizontal: 20,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.messenger,
                        color: Theme.of(context).primaryColor,
                        size: 16,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "Comment",
                        style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  )),
            ),
          )
        ],
      ),
    );
  }

  Widget _commentBottomWidget() {
    return Container(
      color: config.Colors().secondDarkColor(1),
      child: Container(
        margin: EdgeInsets.all(16.0),
        padding: EdgeInsets.symmetric(
          // vertical: config.App(context).appHeight(1),
          horizontal: config.App(context).appHeight(1),
        ),
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(10.0))),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
          Expanded(
              child: TextField(
                  controller: commentController,
                  decoration: InputDecoration(
                      hintText: 'Write a Comment..',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                      hintStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.grey)),
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black))),
          InkWell(
              onTap: () {
                if (commentController.text.toString().trim().isNotEmpty) {
                  showLoader();
                  sendUserComments();
                } else {}
              },
              child: Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(left: 16.0),
                  child: Text('Post', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Theme.of(context).primaryColor))))
        ]),
      ),
    );
  }

  sendUserComments() async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-comments/';
      print('postUserComments url : $url');
      Map<String, dynamic> data = {
        "comment": commentController.text.toString().trim(),
        "user_images_obj": widget.userImagesObj,
        "image_id": widget.imageId,
      };
      print('postUserComments Parameters : $data');

      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Storage().currentUser.token}',
        },
        body: json.encode(data),
      );

      printLog('postUserComments-Status ====== ${response.statusCode}');
      printLog('postUserComments-body ====== ${response.body}');

      if (response.statusCode == 201) {
        dismissLoader();
        commentController.clear();
        getUserLikesComments();
      } else {
        dismissLoader();
        throw Exception(response.body);
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }

  sendUserLikes() async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-comments/';
      print('sendUserLikes URL : $url);// ${Storage().currentUser.userId} ${widget.imageId}');
      Map<String, dynamic> data = {
        "likes": Storage().currentUser.userId,
        "user_images_obj": widget.userImagesObj,
        "image_id": widget.imageId,
      };
      print('sendUserLikes Parameters : $data');

      final client = new http.Client();
      final response = await client.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${Storage().currentUser.token}',
        },
        body: json.encode(data),
      );

      printLog('sendUserLikes ====== ${response.body}');

      if (response.statusCode == 201) {
        getUserLikesComments();
      } else {
        dismissLoader();
        throw Exception(response.body);
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }

  getUserLikesComments() async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      /*int userId = Storage().currentUser.accountType == 'instructor'
          ? proposalDetails.clientId
          : typesOfAction == 'invitation'
          ? proposalDetails.clientId
          : proposalDetails.instructorId;*/

      final String url = '${ServerAddress.api_base_url}user-comments/?user_id=${widget.userId}&image_id=${widget.imageId}';

      final client = new http.Client();
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${Storage().currentUser.token}',
      });
      printLog('getUserComments ====== ${response.body}');

      if (response.statusCode == 200) {
        UserComments userComments = UserComments.fromJson(jsonDecode(response.body));
        dismissLoader();
        if (userComments.data.isNotEmpty) {
          setState(() {
            likesList = [];
            commentsList = [];
            totalLikes = userComments.totalLikes != null ? userComments.totalLikes : 0;
            totalComments = userComments.totalComments != null ? userComments.totalComments : 0;
            likesList = userComments.userList;
            commentsList = userComments.data.where((i) => i.commentedBy != null).toList();
          });
        }
      } else {
        dismissLoader();
        throw Exception(response.body);
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }
}

class CommentsArguments {
  final UserComments userComments;
  final int currentTab;
  final String imagePath;
  final int userImagesObj;
  final int userId;
  final String imageId;

  CommentsArguments(this.userComments, this.currentTab, this.imagePath, this.userImagesObj, this.userId, this.imageId);
}
