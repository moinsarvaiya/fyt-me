import 'package:flutter/material.dart';
import 'package:fytme/src/pages/home/common/scaffold.dart';
import 'package:fytme/src/pages/home/search_trainers.dart';

class GloBalHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      bottomMenuIndex: 2,
    );
  }

  void onWillPop(BuildContext context) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => SearchTrainer(
                  isLoggedIn: true,
                )));
  }
}
