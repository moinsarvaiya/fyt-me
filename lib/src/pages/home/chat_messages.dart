import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';
import 'package:intl/intl.dart';

import '../../helpers/app_config.dart' as config;

class ChatMessageArguments {
  final int otherUserId;
  final String otherUserProfile;
  final String otherUserName;

  ChatMessageArguments({this.otherUserId, this.otherUserProfile, this.otherUserName});
}

final fireStore = FirebaseFirestore.instance;

class ChatMessagesWidget extends StatefulWidget {
  final int otherUserId;
  final String otherUserProfile;
  final String otherUserName;

  const ChatMessagesWidget({Key key, this.otherUserId, this.otherUserProfile, this.otherUserName}) : super(key: key);

  @override
  _ChatMessagesWidgetState createState() => _ChatMessagesWidgetState();
}

class _ChatMessagesWidgetState extends State<ChatMessagesWidget> {
  TextEditingController messageController = TextEditingController();
  String identifier;

  @override
  void initState() {
    super.initState();
    identifier = Storage().userProfile.userId < widget.otherUserId
        ? "${Storage().userProfile.userId}_${widget.otherUserId}"
        : "${widget.otherUserId}_${Storage().userProfile.userId}";
    setZeroUnreadCount();
  }

  setZeroUnreadCount() async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection(Storage().userProfile.userId.toString());
    QuerySnapshot result = await collection.get();
    List<DocumentSnapshot> documents = result.docs;
    if (documents.isNotEmpty) {
      List<Users> users = [];
      for (var i = 0; i < documents.length; i++) {
        DocumentSnapshot<Map<String, dynamic>> snapshot = await collection.doc(documents[i].id).get();
        users.add(Users(snapshot.data()['id'].toString(), documents[i].id, snapshot.data()['unreadCount']));
      }
      for (var u = 0; u < users.length; u++) {
        if (users[u].id == widget.otherUserId.toString()) {
          collection.doc(users[u].documentId).update({
            'unreadCount': 0,
          });
          break;
        }
      }
    }
  }

  //add chat user details into logged in user
  addCurrentUser(String message) async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection(Storage().userProfile.userId.toString());

    QuerySnapshot result = await collection.get();
    List<DocumentSnapshot> documents = result.docs;

    if (documents.isNotEmpty) {
      List<Users> users = [];
      for (var i = 0; i < documents.length; i++) {
        DocumentSnapshot<Map<String, dynamic>> snapshot = await collection.doc(documents[i].id).get();
        users.add(Users(snapshot.data()['id'].toString(), documents[i].id, snapshot.data()['unreadCount']));
      }

      bool isUserExists = false;
      String documentId = '';
      for (var u = 0; u < users.length; u++) {
        documentId = users[u].documentId;
        if (users[u].id == widget.otherUserId.toString()) {
          isUserExists = true;
          break;
        }
      }

      if (!isUserExists) {
        collection.add(currentUserDetails(message));
      } else {
        collection.doc(documentId).update(updateLastMessage(message, 0, true));
      }
    } else {
      collection.add(currentUserDetails(message));
    }
  }

  Map<String, dynamic> currentUserDetails(String message) {
    return {
      'id': widget.otherUserId,
      'name': widget.otherUserName,
      'profilePic': widget.otherUserProfile,
      'timestamp': DateTime.now(),
      'identifier': identifier,
      'lastMessage': message,
      'unreadCount': 0,
    };
  }

  //add logged in user details into chat user
  addOtherUser(String message) async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection(widget.otherUserId.toString());

    QuerySnapshot result = await collection.get();
    List<DocumentSnapshot> documents = result.docs;

    if (documents.isNotEmpty) {
      List<Users> users = [];
      for (var i = 0; i < documents.length; i++) {
        DocumentSnapshot<Map<String, dynamic>> snapshot = await collection.doc(documents[i].id).get();
        users.add(Users(snapshot.data()['id'].toString(), documents[i].id, snapshot.data()['unreadCount']));
      }

      bool isUserExists = false;
      String documentId = '';
      int unreadCount = 0;
      for (var u = 0; u < users.length; u++) {
        documentId = users[u].documentId;
        unreadCount = users[u].unreadCount;
        if (users[u].id == Storage().userProfile.userId.toString()) {
          isUserExists = true;
          break;
        }
      }

      unreadCount += 1;
      if (!isUserExists) {
        collection.add(otherUserDetails(message, unreadCount));
      } else {
        collection.doc(documentId).update(updateLastMessage(message, unreadCount, false));
      }
    } else {
      collection.add(otherUserDetails(message, 1));
    }
  }

  Map<String, dynamic> otherUserDetails(String message, int unreadCount) {
    return {
      'id': Storage().userProfile.userId,
      'name': '${Storage().userProfile.firstName} ${Storage().userProfile.lastName}',
      'profilePic': Storage().userProfile.profilePicture,
      'timestamp': DateTime.now(),
      'identifier': identifier,
      'lastMessage': message,
      'unreadCount': unreadCount,
    };
  }

  Map<String, dynamic> updateLastMessage(String message, int unreadCount, bool isLoggedInUser) {
    return {
      'name': isLoggedInUser ? widget.otherUserName : '${Storage().userProfile.firstName} ${Storage().userProfile.lastName}',
      'profilePic': isLoggedInUser ? widget.otherUserProfile : Storage().userProfile.profilePicture,
      'timestamp': DateTime.now(),
      'lastMessage': message,
      'unreadCount': unreadCount,
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(config.App(context).appHeight(15)),
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 40, 0, 15),
          decoration: BoxDecoration(
            color: Color(0xFF3D434D),
            borderRadius: BorderRadius.circular(20),
          ),
          child: ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                /*Badge(
                  badgeColor: Color(0xFF17D52D),
                  position: BadgePosition.topStart(top: 1, start: 5),
                  shape: BadgeShape.circle,
                  showBadge: true,
                  child:
                ),*/
                ClipRRect(
                  borderRadius: BorderRadius.circular(config.App(context).appWidth(13)),
                  child: Container(
                    width: config.App(context).appWidth(25),
                    height: config.App(context).appWidth(25),
                    child: CachedNetworkImage(
                      imageUrl: widget.otherUserProfile != null
                          ? widget.otherUserProfile.contains('https://dev.fytme.co.uk')
                              ? widget.otherUserProfile
                              : 'https://dev.fytme.co.uk${widget.otherUserProfile}'
                          : "",
                      fit: BoxFit.cover,
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorDark,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  widget.otherUserName,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                  ),
                ),
              ],
            ),
            trailing: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          StreamBuilder(
            stream: fireStore.collection(identifier).orderBy("timestamp", descending: true).limit(10).snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              List<String> dates = [];
              List<Messages> chatMessages = snapshot.data.docs.map<Messages>((m) {
                final data = m.data();
                final messageText = data['text'];
                final otherUserId = data['otherUserId'];
                final timeStamp = data['timestamp'];
                DateTime date = (timeStamp as Timestamp).toDate();
                dates.add(DateFormat('MMMM dd, yyyy').format(date));

                return Messages(
                  otherUserId: otherUserId,
                  text: messageText,
                  date: date,
                  timestamp: timeStamp,
                );
              }).toList();

              List<Widget> widgetList = [];
              List<String> uniqueDates = dates.toSet().toList();
              for (int p = 0; p < uniqueDates.length; p++) {
                List<Messages> msgList = chatMessages.where((element) => DateFormat('MMMM dd, yyyy').format(element.date) == uniqueDates[p]).toList();

                msgList.forEach((element) {
                  widgetList.add(MessageBubble(
                    otherUserId: element.otherUserId,
                    text: element.text,
                    timestamp: element.timestamp,
                    isMe: Storage().userProfile.userId != element.otherUserId,
                  ));
                });

                String displayDate = (DateFormat("MMMM dd, yyyy").format(DateTime.now()) == uniqueDates[p]
                    ? 'Today'
                    : DateFormat("MMMM dd, yyyy").format(DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day - 1)) ==
                            uniqueDates[p]
                        ? 'Yesterday'
                        : uniqueDates[p]);

                widgetList.add(Center(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(12)), color: Color(0xFF3D434D)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 4.0, bottom: 4.0),
                        child: Text(displayDate,
                            style: TextStyle(
                              fontSize: 10.0,
                              color: Colors.white.withOpacity(0.5),
                            )),
                      ),
                    ),
                  ),
                ));
              }

              return ListView(
                reverse: true,
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(5),
                  vertical: config.App(context).appHeight(8),
                ),
                children: widgetList,
              );
            },
          ),
          Positioned(
            left: 15.0,
            bottom: config.App(context).appHeight(2),
            right: 15.0,
            child: Container(
              width: config.App(context).appWidth(75),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                color: config.Colors().accentDarkColor(1),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 15),
                      child: TextField(
                        autofocus: false,
                        maxLines: null,
                        textCapitalization: TextCapitalization.sentences,
                        onChanged: (value) {},
                        controller: messageController,
                        decoration: InputDecoration.collapsed(
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                          ),
                          hintText: 'Write a reply...',
                        ),
                        cursorColor: Colors.white,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    icon: Icon(Icons.send),
                    iconSize: 29.0,
                    color: Colors.white,
                    onPressed: () async {
                      if (messageController.text.toString().trim().isNotEmpty) {
                        String message = messageController.text.toString().trim();
                        fireStore.collection(identifier).add({
                          'otherUserId': widget.otherUserId,
                          'text': message,
                          'timestamp': Timestamp.now(),
                        }).then((value) => messageController.clear());

                        addCurrentUser(message);
                        Future.delayed(Duration(milliseconds: 500), () {
                          addOtherUser(message);
                        });
                      }
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  /*Expanded(
                  child: IconButton(
                    icon: Icon(
                      Icons.videoCamera,
                      color: config.Colors().accentDarkColor(1),
                      size: 40,
                    ),
                    onPressed: () {},
                  ),
                ),*/

  String parseDateFormat(String date, String currentFormat, String newFormat) {
    DateTime parseDate = DateFormat(currentFormat).parse(date);
    var inputDate = DateTime.parse(parseDate.toString());
    String actualDay = DateFormat(newFormat).format(inputDate);
    return actualDay;
  }
}

class MessageBubble extends StatelessWidget {
  MessageBubble({this.otherUserId, this.text, this.timestamp, this.isMe});

  final int otherUserId;
  final String text;
  final Timestamp timestamp;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    final dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000);
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Material(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(15.0),
              topLeft: isMe ? Radius.circular(15.0) : Radius.circular(0),
              topRight: isMe ? Radius.circular(0) : Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
            ),
            elevation: 5.0,
            color: isMe ? Color.fromRGBO(10, 103, 150, 1) : Color.fromRGBO(20, 38, 65, 1),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Column(
                crossAxisAlignment: isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                children: [
                  Text(
                    text,
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 6.0),
            child: Text(
              "${DateFormat('h:mm a').format(dateTime)}",
              style: TextStyle(
                fontSize: 9.0,
                color: Colors.white.withOpacity(0.5),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Users {
  String id;
  String documentId;
  int unreadCount;

  Users(this.id, this.documentId, this.unreadCount);
}

class Messages {
  int otherUserId;
  String text;
  DateTime date;
  Timestamp timestamp;

  Messages({this.otherUserId, this.text, this.date, this.timestamp});
}

/* fetchAll() async {
    DocumentSnapshot documentSnapshot =
        await fireStore.collection('messages').doc(identifier).get();
    // contactList = documentSnapshot.data();
  }

  firebaseSignIn() async {
    QuerySnapshot result = await FirebaseFirestore.instance
        .collection('conversations')
        .where('id', isEqualTo: Storage().userProfile.userId)
        .get();

    List<DocumentSnapshot> documents = result.docs;
    DocumentReference<Map<String, Object>> documentReference = FirebaseFirestore
        .instance
        .collection('conversations')
        .doc(Storage().userProfile.userId.toString());
  }*/

/*var userDocument = snapshot.data;
              List<Widget> chatMessagesWidget = snapshot.data.map((doc) {
                final messageText = doc['text'];
                final fromId = doc['fromId'];
                final toId = doc['toId'];
                final timeStamp = doc['timestamp'];
                return MessageBubble(
                  fromId: fromId,
                  toId: toId,
                  text: messageText,
                  timestamp: timeStamp,
                  isMe: Storage().userProfile.userId == fromId,
                );
              }).toList();*/

/*fireStore
                                .collection('movies')
                                //.doc(identifier)
                                .withConverter<ChatMessages>(
                                  fromFirestore: (snapshot, _) =>
                                      ChatMessages.fromJson(snapshot.data()),
                                  toFirestore: (movie, _) => movie.toJson(),
                                )
                                .add(ChatMessages(
                                    fromId: Storage().userProfile.userId,
                                    toId: widget.otherUserId,
                                    text: messageController.text
                                        .toString()
                                        .trim(),
                                    timestamp: Timestamp.now()))
                                .then((value) => messageController.clear());*/

/*fireStore
                                .runTransaction((transaction) async {
                              transaction.set(
                                FirebaseFirestore.instance
                                    .collection('messages')
                                    .doc(identifier),
                                {
                                  'fromId': Storage().userProfile.userId,
                                  'toId': widget.otherUserId,
                                  'text':
                                      messageController.text.toString().trim(),
                                  'timestamp': Timestamp.now(),
                                },
                              );
                            }).then((value) => messageController.clear());*/

/*fireStore.collection('conversations').add({
                                'fromId': Storage().userProfile.userId,
                                'toId': widget.otherUserId,
                                'identifier': identifier,
                                'fullName':
                                    '${Storage().userProfile.firstName} ${Storage().userProfile.lastName}',
                                'profilePic':
                                    Storage().userProfile.profilePicture,
                                'timestamp': DateTime.now(),
                              }).then((value) => messageController.clear());*/

/*DocumentReference<Map<String, Object>> documentReference = FirebaseFirestore
                                .instance
                                .collection('conversations')
                                .doc(Storage().userProfile.userId.toString());*/
/*value.data()['Users'].forEach((i) =>
          {
            if (i['id'].toString() !=
                Storage().userProfile.userId.toString())
              {documentReference.update(otherUserConversations())}
            else
              {
                documentReference.update({
                  'lastMessage':
                  messageController.text.toString().trim()
                })
              }
          });*/

/*addUpdateCurrentUserList() {
    DocumentReference<Map<String, Object>> documentReference = fireStore
        .collection('users')
        .doc(Storage().userProfile.userId.toString());
    documentReference.get().then((docSnapshot) {
      if (docSnapshot.exists) {
        bool isUserExists = true;
        fireStore
            .collection("users")
            .doc(Storage().userProfile.userId.toString())
            .get()
            .then((value) {
          List users = value.data()['Users'];
          for (var i = 0; i < users.length; i++) {
            if (users[i]['id'].toString() != widget.otherUserId.toString()) {
              isUserExists = false;
              break;
            }
          }
          if (!isUserExists) {
            documentReference.update(currentUserConversations());
          } else {
            documentReference.update(
                {'lastMessage': messageController.text.toString().trim()});
          }
        });
      } else {
        documentReference.set(currentUserConversations());
      }
    });
  }

  Map<String, Object> currentUserConversations() {
    return {
      'id': Storage().userProfile.userId,
      'Users': FieldValue.arrayUnion([
        {
          'id': widget.otherUserId,
          'name': widget.otherUserName,
          'profilePic': widget.otherUserProfile,
          'timestamp': DateTime.now(),
          'identifier': identifier,
          'lastMessage': messageController.text.toString().trim(),
        }
      ]),
    };
  }

  addUpdateOtherUserList() {
    DocumentReference<Map<String, Object>> documentReference =
        fireStore.collection('users').doc(widget.otherUserId.toString());

    documentReference.get().then((docSnapshot) {
      if (docSnapshot.exists) {
        bool isUserExists = false;
        fireStore
            .collection("users")
            .doc(widget.otherUserId.toString())
            .get()
            .then((value) {
          for (var i = 0; i < value.data()['Users'].length; i++) {
            if (value.data()['Users'][i]['id'].toString() ==
                Storage().userProfile.userId.toString()) {
              isUserExists = true;
              break;
            }
          }

          if (!isUserExists) {
            documentReference.update(otherUserConversations());
          } else {
            documentReference.update(
                {'lastMessage': messageController.text.toString().trim()});
          }
        });
      } else {
        documentReference.set(otherUserConversations());
      }
    });
  }

  Map<String, Object> otherUserConversations() {
    return {
      'id': widget.otherUserId,
      'Users': FieldValue.arrayUnion([
        {
          'id': Storage().userProfile.userId,
          'name':
              '${Storage().userProfile.firstName} ${Storage().userProfile.lastName}',
          'profilePic': Storage().userProfile.profilePicture,
          'timestamp': DateTime.now(),
          'identifier': identifier,
          'lastMessage': messageController.text.toString().trim(),
        }
      ]),
    };
  }*/

/*QuerySnapshot result =
                                await fireStore.collection('users').get();

                            List<DocumentSnapshot> documents = result.docs;
                            DocumentReference<Map<String, Object>>
                                documentReference =
                                fireStore.collection('users').doc(
                                    Storage().userProfile.userId.toString());
                            if (documents.isEmpty) {
                              documentReference.set(addConversations());
                            } else {
                              documentReference.update(addConversations());
                            }*/

/*DocumentReference<Map<String, Object>>
                                documentReference =
                                fireStore.collection('users').doc(
                                    Storage().userProfile.userId.toString());

                            documentReference.get().then((docSnapshot) => {
                                  if (docSnapshot.exists)
                                    {
                                      documentReference
                                          .update(addConversations())
                                    }
                                  else
                                    {
                                      documentReference.set(addConversations())
                                      // create the document
                                    }
                                });*/

//old
/*QuerySnapshot result = await fireStore
                                .collection('conversations')
                                .get();
                            List<DocumentSnapshot> documents = result.docs;
                            if (documents.isEmpty) {
                              fireStore
                                  .collection('conversations')
                                  .doc()
                                  .set(addConversations())
                                  .then((value) => messageController.clear());
                            } else {
                              fireStore
                                  .collection('conversations')
                                  .doc(documents[0].id)
                                  .set(addConversations())
                                  .then((value) => messageController.clear());
                            }*/
