import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/state_status.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/createditprofile/proposal.dart';

import '../../../bloc/homeBloc/home_bloc.dart';
import '../../helpers/app_config.dart' as config;
import '../instructor_profile.dart';

class ListBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
      if (state.status.status == StateStatuses.loading)
        Center(
          child: CircularProgressIndicator(),
        );
      if (state.result.isNotEmpty) {
        return Expanded(
          child: Stack(
            children: [
              if (state.status.status == StateStatuses.loading)
                Center(
                  child: CircularProgressIndicator(),
                ),
              SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(10),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: state.result
                      .map(
                        (e) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: InkWell(
                            onTap: () {
                              if (Storage().currentUser != null) {
                                Navigator.of(context).pushNamed('/Proposal', arguments: ProposalArguments(e.id, DateTime.now(), DateTime.now().hour));
                              } else {
                                Navigator.of(context).pushNamed('/Home');
                                CustomWidgets.buildErrorSnackBar(context, 'Please signup/login');
                              }
                            },
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: CachedNetworkImage(
                                    imageUrl: e.profilePicture ?? '',
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) => CircularProgressIndicator(),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.person,
                                      color: Colors.white,
                                    ),
                                    width: config.App(context).appWidth(20),
                                    height: config.App(context).appWidth(20),
                                  ),
                                ),
                                SizedBox(
                                  width: config.App(context).appWidth(5),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("${e.firstName} ${e.lastName}", style: Theme.of(context).textTheme.subtitle1),
                                      Text(
                                        e.specialty.isNotEmpty ? "${e.specialty.join(', ')}" : '',
                                        overflow: TextOverflow.clip,
                                        style: Theme.of(context).textTheme.headline4.copyWith(
                                              fontSize: 12,
                                            ),
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.location_on,
                                            color: Colors.white,
                                            size: 15,
                                          ),
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "${e.address}",
                                              maxLines: 1,
                                              overflow: TextOverflow.clip,
                                              style: Theme.of(context).textTheme.headline4,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          //if (e.ratings != null) rateStar(5),
                                          //if (e.ratings == null) rateStar(0),
                                          rateStar(e.ratings),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              if (Storage().currentUser != null) {
                                                Navigator.of(context).pushNamed('/Reviews', arguments: e);
                                              } else {
                                                CustomWidgets.buildErrorSnackBar(context, 'Please signup/login');
                                              }
                                            },
                                            child: Row(
                                              children: [
                                                Text(
                                                  "(",
                                                  style: Theme.of(context).textTheme.headline4.copyWith(),
                                                ),
                                                Text(
                                                  "${e.ratings} Reviews",
                                                  style: Theme.of(context).textTheme.headline4.copyWith(
                                                        decoration: TextDecoration.underline,
                                                      ),
                                                ),
                                                Text(
                                                  ")",
                                                  style: Theme.of(context).textTheme.headline4.copyWith(),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              )
            ],
          ),
        );
      } else {
        return Expanded(
          child: Center(
            child: Text("Instructor Not Found", style: Theme.of(context).textTheme.subtitle1),
          ),
        );
      }
    });
  }
}
