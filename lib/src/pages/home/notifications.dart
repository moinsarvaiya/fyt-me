import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:fbroadcast/fbroadcast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fytme/bloc/providers_bloc/notification_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/home/chat_messages.dart';
import 'package:fytme/src/pages/home/other_user.dart';
import 'package:fytme/src/pages/home/search_trainers.dart';
import 'package:fytme/src/pages/live_video.dart';
import 'package:fytme/src/pages/payment.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../helpers/app_config.dart' as config;
import 'common/scaffold.dart';

final fireStore = FirebaseFirestore.instance;
final lastMessage = 'Conversation started';

class Notifications extends StatefulWidget {
  final List<int> latestIds;

  Notifications({Key key, @required this.latestIds}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  //final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = false;
  List<InAppNotificationModel> listInAppNotification = [];
  List<int> latestIds = [];

  int opponentId = 0;
  int reviewTo = 0;
  int proposalId = 0;
  String opponentName = '';
  String identifier = '';

  String capitalize(String s) => (s != null && s.length > 1)
      ? s[0].toUpperCase() + s.substring(1)
      : s != null
          ? s.toUpperCase()
          : '';

  @override
  void initState() {
    getUnreadNotifications();
    super.initState();
  }

  int timeDifference(String time) {
    var format = DateFormat('dd/MM/yyyy - HH:mm a');
    var one = format.parse(time);
    var two = format.parse(format.format(DateTime.now()));
    return two.difference(one).inHours;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        onWillPop();
        return true;
      },
      child: MyScaffold(
        bottomMenuIndex: 3,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Stack(children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: config.App(context).appHeight(3),
                  ),
                  Text(
                    'Notifications',
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(3),
                  ),
                  Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Today',
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
                      )),
                  Column(
                      children: List.generate(
                    listInAppNotification.length,
                    (index) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Text(
                                    listInAppNotification[index].notificationSubject,
                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () async {
                                  bool isInternet = await isInternetConnected();
                                  if (!isInternet) {
                                    CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
                                  } else {
                                    showLoader();
                                    getProposalDetails(listInAppNotification[index].proposalId, listInAppNotification[index].typeOfAction,
                                        listInAppNotification[index].isPaid, context);
                                  }
                                },
                                child: Visibility(
                                  visible: getActionableVisibility(listInAppNotification[index].actionable, listInAppNotification[index].typeOfAction,
                                      listInAppNotification[index].isPaid, listInAppNotification[index].proposalType),
                                  child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      padding: EdgeInsets.all(10),
                                      child: Text(
                                        getLabelText(listInAppNotification[index].typeOfAction, listInAppNotification[index].isPaid,
                                            listInAppNotification[index].proposalType),
                                        style: TextStyle(color: Colors.white),
                                      )),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(1),
                          ),
                          Text(
                            listInAppNotification[index].notificationMessage,
                            style: TextStyle(color: Colors.white70, height: 1.6),
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(2),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                /* timeDifference(listInAppNotification[index]
                                    .created_on) >
                                    24
                                    ? listInAppNotification[index].created_on
                                    : '${timeDifference(listInAppNotification[index].created_on)} Hours ago',*/
                                listInAppNotification[index].createdOn,
                                style: TextStyle(color: Colors.white70),
                              ),
                              Visibility(
                                visible: listInAppNotification[index].isHighLight,
                                child: Container(
                                    margin: EdgeInsets.only(left: config.App(context).appHeight(1)),
                                    width: config.App(context).appHeight(1.5),
                                    height: config.App(context).appHeight(1.5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(config.App(context).appHeight(.75)),
                                      color: Theme.of(context).primaryColor,
                                    )),
                              )
                            ],
                          ),
                          if (listInAppNotification.length > index)
                            Divider(
                              color: Colors.white70,
                              height: 50,
                            ),
                        ],
                      ),
                    ),
                  )),
                ],
              ),
            ),
            isLoading
                ? AlertDialog(
                    content: Container(
                      height: MediaQuery.of(context).size.height * .1,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                          ),
                        ),
                      ),
                    ),
                  )
                : Container()
          ]),
        ),
      ),
    );
  }

  bool getActionableVisibility(bool actionable, String typeOfAction, bool isPaid, String proposalType) {
    if (actionable) {
      return isPaid ? (proposalType == 'online' ? true : false) : true;
    } else {
      return false;
    }
  }

  String getLabelText(String action, bool isPaid, String proposalType) {
    if (action == 'payment' || isPaid) {
      return isPaid ? (proposalType == 'online' ? 'Join' : '') : 'Pay';
    } else {
      return 'View';
    }
  }

  getNotification() async {
    bool isInternet = await isInternetConnected();
    if (!isInternet) {
      CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
    } else {
      setState(() {
        isLoading = true;
      });
      final String url = '${ServerAddress.api_base_url}notification/';
      final client = new http.Client();
      var token = Storage().currentUser.token;
      print(token);
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('======== getNotification');
      printLog(response.body);
      if (response.statusCode == 200) {
        listInAppNotification = [];
        List<dynamic> _list = jsonDecode(response.body);
        if (_list.isNotEmpty) {
          _list.forEach((element) {
            listInAppNotification.add(InAppNotificationModel.fromJson(element));
          });
          for (int n = 0; n < listInAppNotification.length; n++) {
            if (latestIds != null) {
              for (int l = 0; l < latestIds.length; l++) {
                if (latestIds[l] == listInAppNotification[n].id) {
                  listInAppNotification[n].isHighLight = true;
                  break;
                }
              }
            }
          }
          dismissLoader();
        } else {
          dismissLoader();
        }
      } else {
        dismissLoader();
      }
    }
  }

  getUnreadNotifications() async {
    bool isInternet = await isInternetConnected();
    if (!isInternet) {
      CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
    } else {
      final String url = '${ServerAddress.api_base_url}notification/?unread_count=true';
      final client = new http.Client();
      var token = Storage().currentUser.token;
      print(token);
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('======== getUnreadNotifications');
      printLog(response.body);
      if (response.statusCode == 200) {
        final responseBody = jsonDecode(response.body);
        List<dynamic> _list = responseBody['latest_ids'];
        latestIds = [];
        if (_list.isNotEmpty) {
          _list.forEach((element) {
            latestIds.add(element);
          });
        }
        getNotification();
        FBroadcast.instance().broadcast(
          'updateNotCount',
          value: responseBody['count'],
        );
      } else {
        getNotification();
      }
    }
  }

  showLoader() {
    setState(() {
      isLoading = true;
    });
  }

  dismissLoader() {
    setState(() {
      isLoading = false;
    });
  }

  getProposalDetails(int proposalId, String typesOfAction, bool isPaid, BuildContext context) async {
    final String url = '${ServerAddress.api_base_url}proposal-details/?proposal_id=$proposalId';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });
    printLog('======== getProposalDetails$proposalId');
    printLog(response.body);
    final responseBody = jsonDecode(response.body);
    if (response.statusCode == 200) {
      ProposalDetails proposalDetails = ProposalDetails.fromJson(responseBody);
      dismissLoader();
      dialog(context, typesOfAction, isPaid, proposalDetails, onAccept: () {
        showLoader();
        acceptRejectProposal(proposalId, 'accept', typesOfAction);
        Navigator.of(context).pop();
      }, onPay: () async {
        Navigator.of(context).pop();
        if (typesOfAction == 'payment' || isPaid) {
          if (isPaid) {
            //Map<Permission, PermissionStatus> statuses = await [
            await [
              Permission.camera,
              Permission.microphone,
            ].request();
            if (await Permission.camera.request().isGranted && await Permission.microphone.request().isGranted) {
              reviewTo = proposalDetails.instructorId;
              this.proposalId = proposalId;
              showLoader();
              createRoom(proposalId, proposalDetails);
            }
          } else {
            Navigator.of(context).pushNamed('/Payment', arguments: PaymentArguments(proposalDetails, proposalId)).then((value) {
              if (value) {
                identifier = Storage().userProfile.userId < proposalDetails.instructorId
                    ? "${Storage().userProfile.userId}_${proposalDetails.instructorId}"
                    : "${proposalDetails.instructorId}_${Storage().userProfile.userId}";
                addCurrentUser(proposalDetails.instructorId, proposalDetails.instructorInfo);
                addOtherUser(
                  proposalDetails.instructorId,
                );
                getUnreadNotifications();
              }
            });
          }
        }
      }, onCancel: () {
        showLoader();
        acceptRejectProposal(proposalId, 'reject', typesOfAction);
        Navigator.of(context).pop();
      }, onClose: () {
        Navigator.of(context).pop();
      });
    } else {
      dismissLoader();
    }
  }

  createRoom(int proposalId, ProposalDetails proposalDetails) async {
    final String url = '${ServerAddress.api_base_url}create-video-room/';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "proposal_id": proposalId,
        "room_name": 'room_$proposalId',
      }),
    );
    printLog('======== createRoom');
    final responseBody = jsonDecode(response.body);
    if (response.statusCode == 200) {
      dismissLoader();
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              LiveCall(roomName: 'room_$proposalId', accessToken: responseBody['token'], proposalId: proposalId, proposalDetails: proposalDetails),
        ),
      );
    } else {
      printLog(responseBody.toString());
      dismissLoader();
    }
  }

  acceptRejectProposal(int proposalId, String action, String typeOfAction) async {
    final String url = '${ServerAddress.api_base_url}${typeOfAction == 'invitation' ? 'invitation-accept-reject' : 'proposal-accept-reject'}';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "proposal_id": proposalId,
        "action": action,
        "comments": action,
      }),
    );
    printLog('======== acceptRejectProposal $url ${response.statusCode}');
    if (response.statusCode == 204) {
      dismissLoader();
      CustomWidgets.buildSuccessSnackBar(context, 'You have ${action}ed $typeOfAction.');
    } else {
      printLog(response.body);
      dismissLoader();
      final responseBody = jsonDecode(response.body);

      CustomWidgets.buildErrorSnackBar(context, responseBody['proposal_id'][0]);
    }
  }

  dialog(BuildContext context, String typesOfAction, bool isPaid, ProposalDetails proposalDetails,
      {Function onAccept, Function onCancel, Function onClose, Function onPay}) {
    print('user profile url :-> ${Storage().currentUser.userId}');
    return showDialog(
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.all(16),
              child: Container(
                margin: const EdgeInsets.all(16),
                child: Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      physics: new ClampingScrollPhysics(),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        margin: const EdgeInsets.only(top: 30),
                        decoration: BoxDecoration(
                            color: Color(0xFF3D434D),
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(20.0),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 0.0,
                                offset: Offset(0.0, 0.0),
                              ),
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              typesOfAction == 'invitation' ? "NEW INVITATION" : "NEW OFFER",
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Text(
                              typesOfAction == 'invitation'
                                  ? "You have a new invitation for live\nworkout session."
                                  : proposalDetails.instructorId != Storage().currentUser.userId
                                      ? "You have a live workout session."
                                      : "You have a new Offer for live\nworkout session.",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(3),
                            ),
                            Text(
                              typesOfAction == 'invitation'
                                  ? "From"
                                  : proposalDetails.instructorId != Storage().currentUser.userId
                                      ? "With"
                                      : "From",
                              style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            InkWell(
                              onTap: () {
                                viewUserDetails(proposalDetails, typesOfAction, isPaid);
                              },
                              child: Container(
                                height: 100,
                                width: 100,
                                child: ClipRRect(
                                    borderRadius: BorderRadius.all(Radius.circular(60)),
                                    child: CachedNetworkImage(
                                      imageUrl: getPicturePath(
                                          proposalDetails.profilePicture, typesOfAction, proposalDetails.instructorInfo.instructorPicture),
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) => CircularProgressIndicator(),
                                      errorWidget: (context, url, error) => Icon(
                                        Icons.person,
                                        color: Colors.white,
                                      ),
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            InkWell(
                              onTap: () {
                                viewUserDetails(proposalDetails, typesOfAction, isPaid);
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Text(
                                  Storage().currentUser.accountType == 'instructor'
                                      ? proposalDetails.proposalBy
                                      : typesOfAction == 'invitation'
                                          ? proposalDetails.proposalBy
                                          : proposalDetails.instructorInfo.instructorName,
                                  // proposalDetails.proposalBy,
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            Visibility(
                              visible: typesOfAction == 'invitation' ? true : false,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Instructor",
                                    style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                  ),
                                  Text(
                                    proposalDetails.instructorInfo.instructorName,
                                    style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Training Mode",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  capitalize(proposalDetails.trainingMode),
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Session Date",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  proposalDetails.sessionDate,
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Session Start Timing",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  proposalDetails.sessionStartTiming,
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Session End Timing",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  proposalDetails.sessionEndTiming,
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Session's Hour",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  '${proposalDetails.totalHours}',
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Hourly Rate",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  String.fromCharCodes(Runes('\u0024')) + "${proposalDetails.hourlyRate}",
                                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.white70,
                              height: 20,
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Total Amount",
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  String.fromCharCodes(Runes('\u0024')) + "${(proposalDetails.totalHours * proposalDetails.hourlyRate)}",
                                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(4),
                            ),
                            (typesOfAction == 'payment' || isPaid)
                                ? InkWell(
                                    onTap: () async {
                                      bool isInternet = await isInternetConnected();
                                      if (!isInternet) {
                                        CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
                                      } else {
                                        onPay();
                                      }
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.symmetric(
                                        vertical: config.App(context).appHeight(2),
                                        horizontal: 20,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: Text(
                                        isPaid ? 'Join' : "Pay",
                                        style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  )
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          onTap: () async {
                                            bool isInternet = await isInternetConnected();
                                            if (!isInternet) {
                                              CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
                                            } else {
                                              onCancel();
                                            }
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                                            padding: EdgeInsets.symmetric(
                                              vertical: config.App(context).appHeight(2),
                                              horizontal: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(15),
                                            ),
                                            child: Text(
                                              "Decline",
                                              style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () async {
                                            bool isInternet = await isInternetConnected();
                                            if (!isInternet) {
                                              CustomWidgets.buildErrorSnackBar(context, 'Check your network connection');
                                            } else {
                                              onAccept();
                                            }
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(left: config.App(context).appHeight(1)),
                                            //width: config.App(context).appWidth(65),
                                            padding: EdgeInsets.symmetric(
                                              vertical: config.App(context).appHeight(2),
                                              // horizontal: 20,
                                            ),
                                            decoration: BoxDecoration(
                                              color: Theme.of(context).primaryColor,
                                              borderRadius: BorderRadius.circular(15),
                                            ),
                                            child: Text(
                                              "Accept",
                                              style: Theme.of(context).textTheme.subtitle1,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      //right: 0.0,
                      child: GestureDetector(
                        onTap: () {
                          onClose();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Icon(Icons.close, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  String getPicturePath(String profilePicture, String typesOfAction, String instructorPicture) {
    if (Storage().currentUser.accountType == 'instructor') {
      if (profilePicture != null) {
        if (profilePicture.contains('https://dev.fytme.co.uk')) {
          return profilePicture;
        } else {
          return 'https://dev.fytme.co.uk$profilePicture';
        }
      } else {
        return '';
      }
    } else {
      if (typesOfAction == 'invitation') {
        if (profilePicture != null) {
          if (profilePicture.contains('https://dev.fytme.co.uk')) {
            return profilePicture;
          } else {
            return 'https://dev.fytme.co.uk$profilePicture';
          }
        } else {
          return '';
        }
      } else {
        if (instructorPicture != null) {
          if (instructorPicture.contains('https://dev.fytme.co.uk')) {
            return instructorPicture;
          } else {
            return 'https://dev.fytme.co.uk$instructorPicture';
          }
        } else {
          return '';
        }
      }
    }
  }

  void onWillPop() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => SearchTrainer(
                  isLoggedIn: true,
                )));
  }

  void viewUserDetails(ProposalDetails proposalDetails, String typesOfAction, bool isPaid) {
    Navigator.of(context).pushNamed('/OtherUser',
        arguments: OtherUserArguments(
            userId: Storage().currentUser.accountType == 'instructor'
                ? proposalDetails.clientId
                : typesOfAction == 'invitation'
                    ? proposalDetails.clientId
                    : proposalDetails.instructorId,
            userProfile: Storage().currentUser.accountType == 'instructor'
                ? proposalDetails.profilePicture
                : typesOfAction == 'invitation'
                    ? proposalDetails.profilePicture
                    : proposalDetails.instructorInfo.instructorPicture,
            userType: Storage().currentUser.accountType == 'instructor'
                ? 'user'
                : typesOfAction == 'invitation'
                    ? 'user'
                    : 'instructor',
            userName: Storage().currentUser.accountType == 'instructor'
                ? proposalDetails.proposalBy
                : typesOfAction == 'invitation'
                    ? proposalDetails.proposalBy
                    : proposalDetails.instructorInfo.instructorName,
            isPaid: isPaid));
  }

  Future<bool> isInternetConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  addCurrentUser(int instructorId, InstructorInfo instructorInfo) async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection(Storage().userProfile.userId.toString());

    QuerySnapshot result = await collection.get();
    List<DocumentSnapshot> documents = result.docs;

    if (documents.isNotEmpty) {
      List<Users> users = [];
      for (var i = 0; i < documents.length; i++) {
        DocumentSnapshot<Map<String, dynamic>> snapshot = await collection.doc(documents[i].id).get();
        users.add(Users(snapshot.data()['id'].toString(), documents[i].id, snapshot.data()['unreadCount']));
      }

      bool isUserExists = false;
      for (var u = 0; u < users.length; u++) {
        if (users[u].id == instructorId.toString()) {
          isUserExists = true;
          break;
        }
      }
      if (!isUserExists) {
        collection.add(currentUserDetails(instructorId, instructorInfo));
      }
    } else {
      collection.add(currentUserDetails(instructorId, instructorInfo));
    }
  }

  Map<String, dynamic> currentUserDetails(int instructorId, InstructorInfo instructorInfo) {
    return {
      'id': instructorId,
      'name': instructorInfo.instructorName,
      'profilePic': instructorInfo.instructorPicture,
      'timestamp': DateTime.now(),
      'identifier': identifier,
      'lastMessage': lastMessage,
      'unreadCount': 0,
    };
  }

  addOtherUser(
    int instructorId,
  ) async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection(instructorId.toString());

    QuerySnapshot result = await collection.get();
    List<DocumentSnapshot> documents = result.docs;

    if (documents.isNotEmpty) {
      List<Users> users = [];
      for (var i = 0; i < documents.length; i++) {
        DocumentSnapshot<Map<String, dynamic>> snapshot = await collection.doc(documents[i].id).get();
        users.add(Users(snapshot.data()['id'].toString(), documents[i].id, snapshot.data()['unreadCount']));
      }

      bool isUserExists = false;
      int unreadCount = 0;
      for (var u = 0; u < users.length; u++) {
        unreadCount = users[u].unreadCount;
        if (users[u].id == Storage().userProfile.userId.toString()) {
          isUserExists = true;
          break;
        }
      }

      unreadCount += 1;
      if (!isUserExists) {
        collection.add(otherUserDetails(instructorId, unreadCount));
      }
    } else {
      collection.add(otherUserDetails(instructorId, 1));
    }
    //Provider.of<InAppChatBloc>(context).updateCount(1);
  }

  Map<String, dynamic> otherUserDetails(int instructorId, int unreadCount) {
    fireStore.collection(identifier).add({
      'otherUserId': instructorId,
      'text': 'Conversation start',
      'timestamp': Timestamp.now(),
    });
    return {
      'id': Storage().userProfile.userId,
      'name': '${Storage().userProfile.firstName} ${Storage().userProfile.lastName}',
      'profilePic': Storage().userProfile.profilePicture,
      'timestamp': DateTime.now(),
      'identifier': identifier,
      'lastMessage': lastMessage,
      'unreadCount': unreadCount,
    };
  }
}
