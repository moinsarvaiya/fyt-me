import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:fytme/bloc/homeBloc/home_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/state_status.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart' as loc;
import 'package:path_provider/path_provider.dart' as path_provider;

import '../../helpers/app_config.dart' as config;
import 'instructrcard.dart';

class GoogleMapBuilder extends StatefulWidget {
  @override
  _GoogleMapBuilderState createState() => _GoogleMapBuilderState();
}

class _GoogleMapBuilderState extends State<GoogleMapBuilder> {
  //LatLng initialTarget = LatLng(5.5911921, -0.3198162);
  LatLng initialTarget = LatLng(0.0, 0.0);

  Set<Marker> _markers = Set<Marker>();

  loc.LocationData myLocation;

  PolylinePoints polylinePoints;
  List<LatLng> polylineCoordinates = [];

  Map<PolylineId, Polyline> polyLines = {};

  double zoomPosition = 0.0;

  String previousLat = '', previousLng = '';

  // LatLng latLng;
  String _mapStyle;

  @override
  void initState() {
    webServiceCall();
    rootBundle.loadString('assets/map_style.txt').then((string) {
      _mapStyle = string;
    });
    super.initState();
  }

  Future<void> webServiceCall() async {
    String searchText = await _getSearchText();
    bool getVideoTraining = await _getVideoTraining();
    bool getTravelClient = await _getTravelClient();
    String isVideoTraining = "False";
    if (getVideoTraining) {
      isVideoTraining = "True";
    }
    String isTravelClient = "False";
    if (getTravelClient) {
      isTravelClient = "True";
    }
    await _saveRouteDrawLat('');
    await _saveRouteDrawLng('');
    previousLat = await _getRouteLat();
    previousLng = await _getRouteLng();
    context.read<HomeBloc>().add(HomeSearchEvent(query: "search=$searchText&travel_to_client=$isTravelClient&video_training=$isVideoTraining"));
    checkLocationPermissions();
  }

  Future<String> _getSearchText() async {
    return await Storage().getSearchText();
  }

  Future<bool> _getVideoTraining() async {
    return await Storage().getVideoTraining();
  }

  Future<bool> _getTravelClient() async {
    return await Storage().getTravelClient();
  }

  Future<String> _getRouteLat() async {
    return await Storage().getRouteDrawLat();
  }

  Future<String> _getRouteLng() async {
    return await Storage().getRouteDrawLng();
  }

  Future<bool> _saveRouteDrawLat(String lat) async {
    return Storage().setRouteDrawLat(lat);
  }

  Future<bool> _saveRouteDrawLng(String lng) async {
    return Storage().setRouteDrawLng(lng);
  }

  drawPolyLines(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    print('device width : ${MediaQuery.of(context).size.width}');
    print('current latLng : $startLatitude $startLongitude');
    polylineCoordinates = [];
    polylinePoints = PolylinePoints();
    polyLines = {};
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      ServerAddress.googleApiKey, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.driving,
    );
    // await _saveRouteDrawLat(destinationLatitude.toString());
    // await _saveRouteDrawLng(destinationLongitude.toString());
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    } else {
      print(result.errorMessage);
      CustomWidgets.buildErrorSnackBar(context, 'Route Not Found'); //result.errorMessage);
    }
    PolylineId id = PolylineId('poly');
    Polyline polyline = Polyline(
      polylineId: id,
      color: Color(0xFF2B979C),
      points: polylineCoordinates,
      width: 8,
    );
    setState(() {
      polyLines[id] = polyline;
    });
  }

  Future<void> checkLocationPermissions() async {
    bool serviceStatusResult = await loc.Location().requestService();
    print("Service status activated after request: $serviceStatusResult");
    if (serviceStatusResult) {
      await getUserLocation().then((value) {
        print("Value : $value");
        getCurrentLocation();
      });
    } else {
      checkLocationPermissions();
    }
  }

  Future<bool> getUserLocation() async {
    try {
      loc.Location location = loc.Location();
      //loc.LocationData userLocation = await location.getLocation();
      await location.getLocation();
    } on PlatformException catch (e) {
      print("Location fetch failed : ${e.toString()}");
    }
    return Future.value(true);
  }

  // Future<String>
  Future<void> getCurrentLocation() async {
    try {
      print("called");
      //call this async method from wherever you need
      //loc.LocationData currentLocation;
      // var myLocation;
      loc.Location location = new loc.Location();
      loc.PermissionStatus check = await location.requestPermission().catchError((e) {
        printLog('allowed to get location catchError');
        print('catchError -> $e');
      });

      print("check : $check");
      if (check == loc.PermissionStatus.denied) {
        await location.requestPermission();
        printLog('allowed to get location denied');
        if (mounted) {
          setState(() {
            //currentLocation = null;
            print("You just click on - deny");
            getCurrentLocation();
          });
        }
      } else if (check == loc.PermissionStatus.deniedForever) {
        printLog('allowed to get location deniedForever');
        await location.requestService();
        if (mounted) {
          setState(() {
            //currentLocation = null;
            print("You just click on - deny");
          });
        }
      } else {
        printLog('allowed to get location');
        myLocation = await location.getLocation();
        print("myLocation :  $myLocation");

        if (mounted) {
          setState(() {
            if (myLocation != null) {
              //currentLocation = myLocation;
              print("latitude : ${myLocation.latitude}");
              print("longitude : ${myLocation.longitude}");
              if (mounted) {
                setState(() async {
                  initialTarget = LatLng(myLocation.latitude, myLocation.longitude);
                  _markers.add(Marker(
                    markerId: MarkerId(Storage().currentUser.userId.toString()),
                    position: LatLng(myLocation.latitude, myLocation.longitude),
                    // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
                    icon: await getMarkerIcon(
                        currentLocation: true,
                        imagePath: Storage().userProfile.profilePicture == null
                            ? 'assets/img/icons/current_location_dot.png'
                            : Storage().userProfile.profilePicture.contains('https://dev.fytme.co.uk')
                                ? Storage().userProfile.profilePicture
                                : 'https://dev.fytme.co.uk${Storage().userProfile.profilePicture}',
                        size: Size(70.0, 70.0)),
                  ));
                });
                moveToLocation(initialTarget);
                // }
              }
            }
          });
        }
      }
      // return currentLocation;
    } catch (e) {
      print("Exception : $e");
      throw Exception;
    }
  }

  final Completer<GoogleMapController> mapController = Completer();

  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(_mapStyle);
    mapController.complete(controller);
    moveToCurrentUserLocation();
  }

  void moveToCurrentUserLocation() {
    var location = loc.Location();
    location.getLocation().then((locationData) {
      if (locationData.latitude != null) {
        var target = LatLng(locationData.latitude, locationData.longitude);
        print("locationData.latitude : ${locationData.latitude.toString()}");
        print("locationData.longitude : ${locationData.longitude.toString()}");
        moveToLocation(target);
      } else {
        print("locationData.latitude : -> ${locationData.latitude.toString()}");
        print("locationData.longitude : -> ${locationData.longitude.toString()}");
      }
    });
  }

  void moveToLocation(LatLng latLng) {
    initialTarget = latLng;
    mapController.future.then((controller) {
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: latLng,
            zoom: zoomPosition,
          ),
        ),
      );
    });
    setMarker(latLng);
  }

  void setMarker(LatLng latLng) {
    print("latLng 1 -> $latLng");
    setState(() async {
      //_markers.clear();
      zoomPosition = 15.0;
      initialTarget = latLng;
      _markers.add(Marker(
        markerId: MarkerId(Storage().currentUser.userId.toString()),
        position: LatLng(latLng.latitude, latLng.longitude),
        // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        icon: await getMarkerIcon(
            currentLocation: true,
            imagePath: Storage().userProfile.profilePicture == null
                ? 'assets/img/icons/current_location_dot.png'
                : Storage().userProfile.profilePicture.contains('https://dev.fytme.co.uk')
                    ? Storage().userProfile.profilePicture
                    : 'https://dev.fytme.co.uk${Storage().userProfile.profilePicture}',
            size: Size(70.0, 70.0)),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listenWhen: (previous, current) => previous.status.status != current.status.status,
      listener: (context, state) {
        if (state.result.isNotEmpty) {
          moveToCurrentUserLocation();
        } else {
          print("latLng 2 -> ${state.result}, " + "${state.result}");
          _markers.clear();
          moveToCurrentUserLocation();
        }
        if (state.result.isNotEmpty)
          mapController.future.then((controller) {
            print(state.result.length);
            /*  controller.moveCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                  target: LatLng(state.result[0].location_coordinates[0],
                      state.result[0].location_coordinates[1]),
                  zoom: zoomPosition,
                ),
              ),
            );*/
          });

        if (state.result.isNotEmpty) {
          _markers = Set<Marker>();
          state.result.forEach((f) async {
            _markers.add(Marker(
                onTap: () {
                  mapController.future.then((controller) {
                    controller.animateCamera(
                      CameraUpdate.newCameraPosition(
                        CameraPosition(
                          target: LatLng(f.locationCoordinates.coordinates[0], f.locationCoordinates.coordinates[1]),
                          zoom: 15,
                        ),
                      ),
                    );
                  });
                  print(f.firstName);
                  context.read<HomeBloc>().add(HomeSelectedEvent(instructorModel: f));
                },
                icon: await getMarkerIcon(
                    currentLocation: false,
                    imagePath: f.profilePicture != null
                        ? f.profilePicture.contains('https://dev.fytme.co.uk')
                            ? f.profilePicture
                            : 'https://dev.fytme.co.uk${f.profilePicture}'
                        : '',
                    size: Size(80.0, 80.0)),
                // icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
                markerId: MarkerId('${f.id}'),
                position: LatLng(f.locationCoordinates.coordinates[0], f.locationCoordinates.coordinates[1])));
          });
        }
      },
      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          // setMarker(latLng);
          return Expanded(
            child: Stack(
              children: [
                Container(
                  width: config.App(context).appWidth(100),
                  height: config.App(context).appHeight(100),
                  child: GoogleMap(
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                    onTap: (latLng) {
                      print('latLong $latLng');
                      context.read<HomeBloc>().add(HomeSelectedEvent(instructorModel: null));
                    },
                    polylines: Set<Polyline>.of(polyLines.values),
                    initialCameraPosition: CameraPosition(
                      target: state.result.isNotEmpty
                          ? LatLng(state.result[0].locationCoordinates.coordinates[0], state.result[0].locationCoordinates.coordinates[1])
                          : initialTarget,
                      zoom: zoomPosition,
                    ),
                    
                    onMapCreated: onMapCreated,
                    markers: state.markers.isNotEmpty ? state.markers : _markers,
                  ),
                ),
                if (state.selected != null)
                  Positioned(
                    bottom: 10,
                    right: 20,
                    left: 20,
                    child: Center(child: InstructorCard(
                      onTap: (value) {
                        drawPolyLines(initialTarget.latitude, initialTarget.longitude, value[0], value[1]);
                      },
                    )),
                  ),
                if (state.status.status == StateStatuses.loading)
                  Center(
                    child: CircularProgressIndicator(),
                  ),
              ],
            ),
          );
        },
      ),
    );
  }

  Future<BitmapDescriptor> getMarkerIcon({bool currentLocation, String imagePath, Size size}) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final Radius radius = Radius.circular(size.width / 2);

/*
    final Paint tagPaint = Paint()..color = Colors.blue;
    final double tagWidth = 40.0;
*/

    final Paint shadowPaint = Paint()..color = Color(0xFF2B979C).withAlpha(100);
    final double shadowWidth = currentLocation ? 6.0 : 3.0;

    final Paint borderPaint = Paint()..color = currentLocation ? Colors.transparent : Colors.black45;
    final double borderWidth = 5.0;

    final double imageOffset = shadowWidth + borderWidth;

    // Add shadow circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(0.0, 0.0, size.width, size.height),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        shadowPaint);

    // Add border circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(shadowWidth, shadowWidth, size.width - (shadowWidth * 2), size.height - (shadowWidth * 2)),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        borderPaint);

/*
    // Add tag circle
    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(size.width - tagWidth, 0.0, tagWidth, tagWidth),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        tagPaint);

  // Add tag text
    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    textPainter.text = const TextSpan(
      text: '1',
      style: TextStyle(fontSize: 20.0, color: Colors.white),
    );

    textPainter.layout();
    textPainter.paint(
        canvas,
        Offset(size.width - tagWidth / 2 - textPainter.width / 2,
            tagWidth / 2 - textPainter.height / 2));
*/
    // Oval for the image
    Rect oval = Rect.fromLTWH(imageOffset, imageOffset, size.width - (imageOffset * 2), size.height - (imageOffset * 2));

    // Add path for oval image
    canvas.clipPath(Path()..addOval(oval));

    // Add image
    try {
      final response = await http.get(Uri.parse(imagePath));
      ui.Codec codec = await ui.instantiateImageCodec(response.bodyBytes);
      ui.FrameInfo frame = await codec.getNextFrame();
      paintImage(canvas: canvas, image: frame.image, rect: oval, fit: BoxFit.cover);
    } catch (e) {
      String image = await getUiImage(currentLocation ? 'assets/img/icons/current_location_dot.png' : 'assets/img/splash/fytme_logo_1.png', 80, 80);
      Uint8List _bytesImage;
      _bytesImage = Base64Decoder().convert(image);
      Image image12 = Image.memory(_bytesImage);
      //  paintImage(canvas: canvas, image: image12, rect: oval, fit: BoxFit.fitWidth);
      print("Exception : ${e.toString()}");
      // throw "Couldn't resolve network Image.";
    }

    // Convert canvas to image
    final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(size.width.toInt(), size.height.toInt());

    // Convert image to bytes
    final ByteData byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List unit8List = byteData.buffer.asUint8List();
    setState(() {});

    return BitmapDescriptor.fromBytes(unit8List);
  }

  Future<String> getUiImage(final imageAssetPath, int height, int width) async {
    final config = ImageConfiguration();
    final AssetBundleImageKey key = await imageAssetPath.obtainKey(config);
    final ByteData data = await key.bundle.load(key.name);
    final dir = await path_provider.getTemporaryDirectory();
    final File file = createFile('${dir.absolute.path}/test.png');
    file.createSync(recursive: true);
    file.writeAsBytesSync(data.buffer.asUint8List());
    return file.absolute.path;

    // final ByteData assetImageByteData = await rootBundle.load(imageAssetPath);
    // image.Image baseSizeImage = image.decodeImage(assetImageByteData.buffer.asUint8List());
    // image.Image resizeImage = image.copyResize(baseSizeImage, height: height, width: width);
    // ui.Codec codec = await ui.instantiateImageCodec(image.encodePng(resizeImage));
    // ui.FrameInfo frameInfo = await codec.getNextFrame();
    // return frameInfo.image;
  }

  File createFile(String path) {
    final file = File(path);
    if (!file.existsSync()) {
      file.createSync(recursive: true);
    }
    return file;
  }
}
