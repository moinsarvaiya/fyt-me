import 'package:flutter/material.dart';
import 'package:fytme/src/common_widgets/user_image.dart';
import 'package:fytme/src/elements/DrawerWidget.dart';
import 'package:fytme/src/elements/logoButtonWidget.dart';

import '../../../../common/constants.dart';
import '../account.dart';
import 'bottom_menu.dart';

class MyScaffold extends StatefulWidget {
  final Widget body;
  final int bottomMenuIndex;

  MyScaffold({this.body, this.bottomMenuIndex});

  @override
  _MyScaffoldState createState() => _MyScaffoldState();
}

class _MyScaffoldState extends State<MyScaffold> {
  @override
  Widget build(BuildContext context) {
    print(widget.bottomMenuIndex);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: Storage().currentUser == null ? null : DrawerWidget(),
      appBar: Storage().currentUser == null
          ? AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
            )
          : AppBar(
              leading: Builder(
                builder: (ctx) {
                  return IconButton(
                    icon: Image.asset("assets/img/icons/menu1.png"),
                    iconSize: 40,
                    onPressed: () => Scaffold.of(ctx).openDrawer(),
                  );
                },
              ),
              title: LogoButtonWidget(),
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0,
              automaticallyImplyLeading: false,
              actions: <Widget>[
                if (widget.bottomMenuIndex != null && widget.bottomMenuIndex < 4)
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => AccountWidget()));
                    },
                    child: UserImage(
                      radius: 10,
                    ),
                  ),
              ],
            ),
      body: Container(height: MediaQuery.of(context).size.height, width: MediaQuery.of(context).size.width, child: widget.body),
      bottomNavigationBar: widget.bottomMenuIndex == null ? null : MyAppBottomMenu(widget.bottomMenuIndex),
    );
  }
}
