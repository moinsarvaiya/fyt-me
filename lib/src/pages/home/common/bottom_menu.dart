import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fytme/bloc/providers_bloc/chat_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/pages/home/notifications.dart';
import 'package:provider/provider.dart';

import '../../../../bloc/providers_bloc/notification_bloc.dart';
import '../account.dart';
import '../search_trainers.dart';

class MyAppBottomMenu extends StatelessWidget {
  final int menuIndex;

  MyAppBottomMenu(this.menuIndex);

  Color colorByIndex(int index) {
    return index == menuIndex ? Colors.white : Colors.white70;
  }

  BottomNavigationBarItem getItem(Widget icon, String title, int index) {
    return BottomNavigationBarItem(
      icon: icon,
      label: '',
    );
  }

  @override
  Widget build(BuildContext context) {
    final _blocNotification = Provider.of<InAppNotificationBloc>(context);
    final _blocChat = Provider.of<InAppChatBloc>(context);
    List<BottomNavigationBarItem> menuItems = [
      getItem(
          Icon(
            Storage().userProfile.accountType == 'instructor' ? Icons.home : Icons.search,
            color: colorByIndex(menuIndex),
          ),
          'Home',
          0),
      getItem(
          _blocChat.unreadCount > 0
              ? badges.Badge(
                  badgeContent: Text(
                    _blocChat.unreadCount.toString(),
                    style: TextStyle(color: Colors.white),
                  ),
                  child: SvgPicture.asset(
                    "assets/img/icons/Nav Bar Message.svg",
                    height: 18,
                    color: colorByIndex(menuIndex),
                  ),
                )
              : SvgPicture.asset(
                  "assets/img/icons/Nav Bar Message.svg",
                  height: 18,
                  color: colorByIndex(menuIndex),
                ),
          '',
          1),
      /*getItem(
          SvgPicture.asset(
            "assets/img/icons/Nav Bar Message.svg",
            height: 18,
            color: colorByIndex(menuIndex),
          ),
          '',
          1),*/
      getItem(
          Icon(
            Icons.public,
            color: colorByIndex(menuIndex),
          ),
          '',
          2),
      getItem(
          _blocNotification.unreadCount != null
              ? _blocNotification.unreadCount.count > 0
                  ? badges.Badge(
                      badgeContent: Text(
                        _blocNotification.unreadCount.count.toString(),
                        style: TextStyle(color: Colors.white),
                      ),
                      child: Icon(
                        Icons.notifications,
                        color: colorByIndex(menuIndex),
                      ),
                    )
                  : Icon(
                      Icons.notifications,
                      color: colorByIndex(menuIndex),
                    )
              : Icon(
                  Icons.notifications,
                  color: colorByIndex(menuIndex),
                ),
          '',
          3),
      getItem(
          Icon(
            Icons.person,
            color: colorByIndex(menuIndex),
          ),
          '',
          4),
    ];

    return ClipRRect(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(10),
        topLeft: Radius.circular(10),
      ),
      child: Container(
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Theme.of(context).colorScheme.secondary,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white,
          currentIndex: menuIndex,
          items: menuItems,
          onTap: (value) {
            print(value);
            switch (value) {
              case 0:
                _blocNotification.setLength();
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchTrainer(
                              isLoggedIn: true,
                            )));
                break;
              case 1:
                _blocNotification.setLength();
                _blocChat.updateCount(0);
                Navigator.of(context).pushNamed('/ChatUsers');
                break;
              case 2:
                _blocNotification.setLength();
                Navigator.of(context).pushNamed('/Home');
                break;
              case 3:
                _blocNotification.setCountLength();
                //_bloc.getNotification();
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Notifications(
                              latestIds: _blocNotification.unreadCount.latestIds,
                            )));
                break;
              case 4:
                _blocNotification.setLength();
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => AccountWidget()));
                break;
            }
          },
        ),
      ),
    );
  }
}
