import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/bloc/editprofilebloc/profile_bloc.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/blocs/state_status.dart';
import 'package:fytme/data/model/calender_data.dart';
import 'package:fytme/data/model/search_instructor_model.dart';
import 'package:fytme/data/model/user_comments.dart';
import 'package:fytme/data/repositories/abstracts/profile_respository.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/main.dart';
import 'package:fytme/src/common_widgets/custom_button.dart';
import 'package:fytme/src/common_widgets/user_image.dart';
import 'package:fytme/src/pages/home/comments.dart';
import 'package:fytme/src/pages/home/common/scaffold.dart';
import 'package:fytme/src/pages/home/search_trainers.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../helpers/app_config.dart' as config;

class AccountWidget extends StatelessWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  const AccountWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          ProfileBloc(authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
      child: AccountWidget2(
        parentScaffoldKey: parentScaffoldKey,
      ),
    );
  }
}

class AccountWidget2 extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  const AccountWidget2({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _AccountWidgetsState createState() => _AccountWidgetsState();
}

class _AccountWidgetsState extends StateMVC<AccountWidget2> {
  bool bindImageNow = true;
  bool bindProgramNow = true;
  bool bindCommentsNow = true;
  bool bindCalenderNow = true;

  @override
  void initState() {
    super.initState();
    if (Storage().currentUser.accountType == 'instructor') {
      BlocProvider.of<ProfileBloc>(context).add(GetUserProgramsEvent(userId: Storage().currentUser.userId));
    }
    BlocProvider.of<ProfileBloc>(context).add(GetUserImagesEvent(userId: Storage().currentUser.userId));

    DateTime firstDateOfMonth = DateTime.utc(DateTime.now().year, DateTime.now().month, 1);
    DateTime lastDateOfMonth = DateTime.utc(
      DateTime.now().year,
      DateTime.now().month + 1,
    ).subtract(Duration(days: 1));
    String firstDay = DateFormat('yyyy-MM-dd').format(firstDateOfMonth);
    String lastDay = DateFormat('yyyy-MM-dd').format(lastDateOfMonth);
    BlocProvider.of<ProfileBloc>(context).add(GetCalenderDataEvent(startTime: firstDay, endTime: lastDay, userId: Storage().currentUser.userId));

    setState(() {
      bindImageNow = false;
      bindProgramNow = false;
      bindCommentsNow = false;
      bindCalenderNow = false;
    });
  }

  UserComments userComments;
  UserImages userImages;

  Widget rateStar(int rate) {
    return Row(
      children: [
        for (var i = 0; i < 5; i++)
          Icon(
            Icons.star_rounded,
            color: Color(i < rate ? 0xFFED8A19 : 0xFF7F8389),
            size: 16,
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        onWillPop();
        return true;
      },
      child: MyScaffold(
        bottomMenuIndex: 4,
        body: BlocListener<ProfileBloc, ProfileState>(
          listenWhen: (previous, current) => previous.status.status != current.status.status,
          listener: (con, state) {
            switch (state.status.status) {
              case StateStatuses.loading:
                CustomLoader().show(context);
                break;
              case StateStatuses.failure:
                CustomLoader().hide(context);
                CustomWidgets.buildErrorSnackBar(context, state.status.message);
                break;
              case StateStatuses.success:
                CustomLoader().hide(context);
                if (state.userImages != null) {
                  setState(() {
                    bindImageNow = true;
                  });
                }
                if (state.isImageEdited) {
                  BlocProvider.of<ProfileBloc>(context).add(GetUserImagesEvent(userId: Storage().currentUser.userId));
                }
                if (state.programs != null) {
                  setState(() {
                    bindProgramNow = true;
                  });
                }
                if (state.calenderData != null) {
                  setState(() {
                    bindCalenderNow = true;
                  });
                }
                if (state.userComments != null) {
                  print('Test UserComment ::::: ${state.userComments.totalComments}');

                  setState(() {
                    print('Test UserComment ::::: ${state.userComments.totalComments}');

                    userComments = state.userComments;
                    bindCommentsNow = true;
                  });
                }
                if (state.status.message.isNotEmpty) {
                  CustomWidgets.buildSuccessSnackBar(context, state.status.message);
                }
                break;
              default:
                CustomLoader().hide(context);
                break;
            }
          },
          child: Stack(children: [
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10), vertical: 10),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: config.App(context).appHeight(5),
                  ),
                  Stack(
                    alignment: Alignment.topCenter,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          top: config.App(context).appWidth(23),
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 12),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: config.Colors().secondDarkColor(1),
                          ),
                          width: config.App(context).appWidth(80),
                          child: Column(
                            children: <Widget>[
                              /*Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  IconButton(
                                    icon: SvgPicture.asset(
                                        "assets/img/icons/comm-17_chat.svg"),
                                    onPressed: () {
                                      Navigator.of(context)
                                          .pushNamed('/ChatUsers');
                                    },
                                  ),
                                  IconButton(
                                    icon: SvgPicture.asset(
                                        "assets/img/icons/video-call.svg"),
                                    onPressed: () {
                                      print("Video call");
                                    },
                                  )
                                ],
                              ),*/
                              SizedBox(
                                height: config.App(context).appHeight(8),
                              ),
                              UserNameBuilder(),
                              SizedBox(
                                height: config.App(context).appHeight(1),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).pushNamed('/Reviews',
                                      arguments: InstructorModel(
                                          id: Storage().userProfile.userId,
                                          firstName: Storage().userProfile.firstName,
                                          lastName: Storage().userProfile.lastName,
                                          specialty: [],
                                          address: Storage().userProfile.address,
                                          locationCoordinates: LocationCoordinates(coordinates: [0.0, 0.0]),
                                          profilePicture: Storage().userProfile.profilePicture,
                                          ratings: 0));
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    //if (e.ratings != null) rateStar(5),
                                    //if (e.ratings == null) rateStar(0),
                                    rateStar(averageRatings),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "(",
                                          style: Theme.of(context).textTheme.headline4.copyWith(),
                                        ),
                                        Text(
                                          "$noOfReviews Reviews",
                                          style: Theme.of(context).textTheme.headline4.copyWith(
                                                decoration: TextDecoration.underline,
                                              ),
                                        ),
                                        Text(
                                          ")",
                                          style: Theme.of(context).textTheme.headline4.copyWith(),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: config.App(context).appHeight(1),
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  AddressBuilder(),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      DottedBorder(
                        borderType: BorderType.Circle,
                        color: Color(0xFF0750D2),
                        dashPattern: [8, 8],
                        strokeWidth: 3,
                        child: /*Stack(
                          children: [*/
                            // ProfileImageBuilder(),
                            ProfileImageUpload(),
                        // ]),
                      ),
                    ],
                  ),
                  Visibility(
                    visible: Storage().currentUser.accountType == 'instructor',
                    child: Column(
                      children: [
                        SizedBox(
                          height: config.App(context).appHeight(4),
                        ),
                        !bindProgramNow ? Container() : UserProgramsBuilder(),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(2),
                  ),
                  !bindImageNow ? Container() : imageView(),
                  SizedBox(
                    height: config.App(context).appHeight(4),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "CALENDAR",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      UserCalenderBuilder(),
                    ],
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  final picker = ImagePicker();

  void selectImageFromCamera(int emptyPos, int id) async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      File file = File(pickedFile.path);
      print(file.length());
      context.read<ProfileBloc>().add(SelectPictureEvent(value: pickedFile.path));
      Navigator.of(context).pop();
      BlocProvider.of<ProfileBloc>(context).add(UploadUserImageEvent(pathImage: pickedFile.path, selIndex: '$emptyPos', id: id));
    }
  }

  String filePath = '';

  void selectImageFromLibrary(int emptyPos, int id) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    print(pickedFile.path);
    if (pickedFile != null) {
      Navigator.of(context).pop();
      /*setState(() {
        filePath = pickedFile.path;
      });*/
      context.read<ProfileBloc>().add(SelectPictureEvent(value: pickedFile.path));
      BlocProvider.of<ProfileBloc>(context).add(UploadUserImageEvent(pathImage: pickedFile.path, selIndex: '$emptyPos', id: id));
    }
  }

  Widget imageView() {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        List<Map<String, String>> images = [];
        List<String> uploadImages = [];
        if (state.userImages != null) {
          uploadImages.add(state.userImages.image1);
          uploadImages.add(state.userImages.image2);
          uploadImages.add(state.userImages.image3);
          uploadImages.add(state.userImages.image4);
          uploadImages.add(state.userImages.image5);
          uploadImages.add(state.userImages.image6);
          if (state.userImages.image1 != null) {
            images.add({'image': state.userImages.image1, 'key': '1'});
          }
          if (state.userImages.image2 != null) {
            images.add({'image': state.userImages.image2, 'key': '2'});
          }
          if (state.userImages.image3 != null) {
            images.add({'image': state.userImages.image3, 'key': '3'});
          }
          if (state.userImages.image4 != null) {
            images.add({'image': state.userImages.image4, 'key': '4'});
          }
          if (state.userImages.image5 != null) {
            images.add({'image': state.userImages.image5, 'key': '5'});
          }
          if (state.userImages.image6 != null) {
            images.add({'image': state.userImages.image6, 'key': '6'});
          }
        }
        return Container(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "PHOTOS",
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      /*onTapUploadDialog(
                          context,
                          images,
                          state.selectPic);*/
                      showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        backgroundColor: Color.fromRGBO(49, 55, 66, 1),
                        isDismissible: false,
                        enableDrag: false,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
                        ),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        builder: (BuildContext context) {
                          return BottomSheet(
                            onClosing: () {},
                            builder: (BuildContext context) {
                              return StatefulBuilder(
                                  builder: (BuildContext context, setState) => Container(
                                        padding: EdgeInsets.all(config.App(context).appHeight(2)),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            SizedBox(
                                              height: config.App(context).appHeight(4),
                                            ),
                                            Text(
                                              "Upload New image",
                                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                                            ),
                                            SizedBox(
                                              height: config.App(context).appHeight(1),
                                            ),
                                            Text(
                                              "Click on the + icon And select the Image.",
                                              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
                                            ),
                                            SizedBox(
                                              height: config.App(context).appHeight(3),
                                            ),
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
                                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                                  child: InkWell(
                                                    onTap: () {
                                                      showCupertinoModalPopup(
                                                          context: context,
                                                          builder: (BuildContext context) {
                                                            return CupertinoActionSheet(
                                                              title: Text('Choose'),
                                                              actions: <Widget>[
                                                                CupertinoActionSheetAction(
                                                                  child: Text('Camera'),
                                                                  onPressed: () {
                                                                    Navigator.pop(context);
                                                                    int emptyPos = 0;
                                                                    for (int i = 0; i < uploadImages.length; i++) {
                                                                      if (uploadImages[i] == null || uploadImages[i].isEmpty) {
                                                                        emptyPos = (i + 1);
                                                                        break;
                                                                      }
                                                                    }
                                                                    print('$emptyPos');
                                                                    selectImageFromCamera(emptyPos, state.userImages.id);
                                                                  },
                                                                ),
                                                                CupertinoActionSheetAction(
                                                                  child: Text('Gallery'),
                                                                  onPressed: () {
                                                                    Navigator.pop(context);
                                                                    int emptyPos = 0;
                                                                    for (int i = 0; i < uploadImages.length; i++) {
                                                                      if (uploadImages[i] == null || uploadImages[i].isEmpty) {
                                                                        emptyPos = (i + 1);
                                                                        break;
                                                                      }
                                                                    }
                                                                    print('$emptyPos');
                                                                    selectImageFromLibrary(emptyPos, state.userImages.id);
                                                                  },
                                                                ),
                                                              ],
                                                              cancelButton: CupertinoActionSheetAction(
                                                                child: Text(
                                                                  "Cancel",
                                                                  style: TextStyle(color: Colors.red),
                                                                ),
                                                                onPressed: () => Navigator.pop(context),
                                                              ),
                                                            );
                                                          });
                                                    },
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                              //          state.selectPic != null
                                                              filePath.isNotEmpty ? filePath.split('/').last : 'Select Picture',
                                                              style: TextStyle(
                                                                color: Colors.white,
                                                              )),
                                                        ),
                                                        SizedBox(
                                                          width: 10,
                                                        ),
                                                        IconButton(
                                                            icon: Icon(
                                                              Icons.add_circle_outline,
                                                              color: Colors.white,
                                                            ),
                                                            onPressed: () {
                                                              showCupertinoModalPopup(
                                                                  context: context,
                                                                  builder: (BuildContext context) {
                                                                    return CupertinoActionSheet(
                                                                      title: Text('Choose'),
                                                                      actions: <Widget>[
                                                                        CupertinoActionSheetAction(
                                                                          child: Text('Camera'),
                                                                          onPressed: () {
                                                                            Navigator.pop(context);
                                                                            int emptyPos = 0;
                                                                            for (int i = 0; i < uploadImages.length; i++) {
                                                                              if (uploadImages[i] == null || uploadImages[i].isEmpty) {
                                                                                emptyPos = (i + 1);
                                                                                break;
                                                                              }
                                                                            }
                                                                            print('$emptyPos');
                                                                            selectImageFromCamera(emptyPos, state.userImages.id);
                                                                          },
                                                                        ),
                                                                        CupertinoActionSheetAction(
                                                                          child: Text('Gallery'),
                                                                          onPressed: () {
                                                                            Navigator.pop(context);
                                                                            int emptyPos = 0;
                                                                            for (int i = 0; i < uploadImages.length; i++) {
                                                                              if (uploadImages[i] == null || uploadImages[i].isEmpty) {
                                                                                emptyPos = (i + 1);
                                                                                break;
                                                                              }
                                                                            }
                                                                            print('$emptyPos');
                                                                            selectImageFromLibrary(emptyPos, state.userImages.id);
                                                                          },
                                                                        ),
                                                                      ],
                                                                      cancelButton: CupertinoActionSheetAction(
                                                                        child: Text(
                                                                          "Cancel",
                                                                          style: TextStyle(color: Colors.red),
                                                                        ),
                                                                        onPressed: () => Navigator.pop(context),
                                                                      ),
                                                                    );
                                                                  });
                                                            })
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: config.App(context).appHeight(3),
                                            ),
                                            Center(
                                              child: CustomButton(
                                                  callback: null,
                                                  /* (state.selectPic == null) ? null: () {
                                                            int emptyPos = 0;
                                                            for (int i = 0;
                                                                i <
                                                                    images
                                                                        .length;
                                                                i++) {
                                                              if (images[i]
                                                                  .isEmpty) {
                                                                emptyPos = i;
                                                                break;
                                                              }
                                                            }
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },*/
                                                  string: 'Upload Image'),
                                            ),
                                            Center(
                                              child: TextButton(
                                                child: Text(
                                                  'Back',
                                                  style: TextStyle(color: Colors.white70),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ));
                            },
                          );
                        },
                      );
                    },
                    child: Visibility(
                      visible: images.length < 6,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "UPLOAD",
                          style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: config.App(context).appHeight(2),
              ),
              Visibility(
                visible: images.length > 0,
                child: GridView.count(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 3,
                  crossAxisSpacing: config.App(context).appWidth(4),
                  mainAxisSpacing: config.App(context).appWidth(4),
                  children: List.generate(images.length ?? 0, (index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          BlocProvider.of<ProfileBloc>(context).add(
                              GetUserCommentsEvent(selIndex: images[index]['key'], id: state.userImages.id, userId: Storage().currentUser.userId));
                          userComments = null;
                          onTapFullScreenDialog(
                              context,
                              state.userImages.id,
                              images[index]['image'].contains('https://dev.fytme.co.uk')
                                  ? images[index]['image']
                                  : 'https://dev.fytme.co.uk${images[index]['image']}',
                              (index + 1),
                              index,
                              images);
                        });
                      },
                      child: Container(
                        height: config.App(context).appWidth(24),
                        width: config.App(context).appWidth(24),
                        decoration: BoxDecoration(
                          color: Theme.of(context).cardColor.withOpacity(0.51),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: CachedNetworkImage(
                            imageUrl: images[index]['image'].contains('https://dev.fytme.co.uk')
                                ? images[index]['image']
                                : 'https://dev.fytme.co.uk${images[index]['image']}',
                            fit: BoxFit.cover,
                            errorWidget: (context, url, error) => Icon(
                              Icons.person,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  onTapFullScreenDialog(BuildContext context, int id, String imagePath, int selPos, int index, List<Map<String, String>> images) {
    Future.delayed(const Duration(milliseconds: 500), () {
      print('State UserComment ::::: ${userComments != null ? userComments.totalComments : 0}');

      if (userComments == null) {
        onTapFullScreenDialog(context, id, imagePath, selPos, index, images);
      } else {
        dialogFullImage(context, id, imagePath, (index + 1), userComments, onFyt: () {
          Navigator.of(context).pushNamed('/Comments',
              arguments: CommentsArguments(
                userComments,
                0,
                imagePath,
                id,
                Storage().currentUser.userId,
                images[index]['key'],
              ));
        }, onComment: () {
          Navigator.of(context).pushNamed(
            '/Comments',
            arguments: CommentsArguments(
              userComments,
              1,
              imagePath,
              id,
              Storage().currentUser.userId,
              images[index]['key'],
            ),
          );
        }, onClose: () {
          Navigator.of(context).pop();
        }, onDelete: () {
          Navigator.of(context).pop();
          print('delete ${(index + 1)} $id');
          BlocProvider.of<ProfileBloc>(context).add(DeleteUserImageEvent(selIndex: images[index]['key'], id: id));
        });
      }
      // Here you can write your code for open new view
    });
  }

  /*Future<Image> _getImage(String imagePath) async {
    final Completer completer = Completer();
    final String url =
        'http://images-jp.amazon.com/images/P/4101098018.09.MZZZZZZZ';
    final image = NetworkImage(imagePath);
    image
        .resolve(ImageConfiguration())
        .addListener(ImageStreamListener((ImageInfo info, bool isSync) {
      print(info.image.width);
      completer.complete(info.image);
    }));

    return completer.future;
  }

  Future<Size> _calculateImageDimension(String imageUrl) {
    Completer<Size> completer = Completer();
    Image image = Image.network(imageUrl);
    image.image.resolve(ImageConfiguration()).addListener(
      ImageStreamListener(
        (ImageInfo image, bool synchronousCall) {
          var myImage = image.image;
          Size size = Size(myImage.width.toDouble(), myImage.height.toDouble());
          completer.complete(size);
        },
      ),
    );
    return completer.future;
  }*/

  dialogFullImage(BuildContext context, int id, String imagePath, int selPos, UserComments userComments,
      {Function onFyt, Function onComment, Function onClose, Function onDelete}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.all(16),
              child: Container(
                margin: const EdgeInsets.all(16),
                child: Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      physics: ClampingScrollPhysics(),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        margin: const EdgeInsets.only(top: 50),
                        decoration: BoxDecoration(
                            color: Color(0xFF3D434D),
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(20.0),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 0.0,
                                offset: Offset(0.0, 0.0),
                              ),
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              height: config.App(context).appHeight(60),
                              width: config.App(context).appWidth(80),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child:
                                    /*FutureBuilder(
                                  future: _getImage(imagePath),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<Image> snapshot) {
                                    print(snapshot.hasData);
                                    if (snapshot.hasData) {
                                      return snapshot.data;
                                    } else {
                                      return Text('Loading...');
                                    }
                                  },
                                ),*/
                                    CachedNetworkImage(
                                  imageUrl: imagePath,
                                  fit: BoxFit.fitWidth,
                                  errorWidget: (context, url, error) => Icon(
                                    Icons.person,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(2),
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${userComments != null ? (userComments.totalLikes != null ? userComments.totalLikes : 0) : 0} FYT',
                                      style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                    ),
                                    SizedBox(
                                      height: config.App(context).appHeight(1),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        onFyt();
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                                        width: config.App(context).appWidth(35),
                                        padding: EdgeInsets.symmetric(
                                          vertical: config.App(context).appHeight(2),
                                          horizontal: 20,
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Text(
                                          "FYT",
                                          style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${userComments != null ? (userComments.totalComments != null ? userComments.totalComments : 0) : 0} Comments',
                                      style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                                    ),
                                    SizedBox(
                                      height: config.App(context).appHeight(1),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        onComment();
                                      },
                                      child: Container(
                                        width: config.App(context).appWidth(35),
                                        padding: EdgeInsets.symmetric(
                                          vertical: config.App(context).appHeight(2),
                                          horizontal: 20,
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Icon(
                                              Icons.messenger,
                                              color: Theme.of(context).primaryColor,
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "Comment",
                                              style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                                              textAlign: TextAlign.center,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      child: GestureDetector(
                        onTap: () {},
                        child: PopupMenuButton(
                          offset: Offset(30.0, -10.0),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                          color: Color(0xFF7F8389),
                          child: Icon(
                            Icons.more_vert,
                            color: Colors.white,
                          ),
                          itemBuilder: (BuildContext context) => [
                            PopupMenuItem(
                              value: 0,
                              height: 25,
                              child: Text(
                                "Delete Image",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                          onSelected: (type) {
                            dialogDelete(context, onDelete: () {
                              Navigator.of(context).pop();
                              onDelete();
                            }, onClose: () {
                              Navigator.of(context).pop();
                            });
                          },
                        ),
                      ),
                    ),
                    Positioned(
                      child: GestureDetector(
                        onTap: () {
                          onClose();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Icon(Icons.close, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  dialogDelete(BuildContext context, {Function onDelete, Function onClose}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              //insetPadding: const EdgeInsets.all(1),
              child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Color(0xFF3D434D),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 0.0,
                        offset: Offset(0.0, 0.0),
                      ),
                    ]),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "DELETE IMAGE?",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    Text(
                      "Are you sure you want to\ndelete this image?",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: config.App(context).appHeight(3),
                    ),
                    InkWell(
                      onTap: () {
                        onDelete();
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                        width: config.App(context).appWidth(65),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(2),
                          horizontal: 20,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "Delete",
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        onClose();
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                        width: config.App(context).appWidth(65),
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(2),
                          horizontal: 20,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  onTapUploadDialog(BuildContext context, List<Map<String, String>> images, String selectPic) {
    Future.delayed(const Duration(milliseconds: 500), () {
      if (userImages == null) {
        onTapUploadDialog(context, images, selectPic);
      } else {
        dialogUploadImage(context, images, selectPic, onUpload: () {
          Navigator.of(context).pop();
        });
      }
      // Here you can write your code for open new view
    });
  }

  dialogUploadImage(BuildContext context, List<Map<String, String>> images, String selectPic, {Function onUpload}) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Color.fromRGBO(49, 55, 66, 1),
        isDismissible: false,
        enableDrag: false,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(config.App(context).appHeight(2)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: config.App(context).appHeight(4),
                ),
                Text(
                  "Upload New image",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                ),
                SizedBox(
                  height: config.App(context).appHeight(1),
                ),
                Text(
                  "Click on the + icon And select the Image.",
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
                ),
                SizedBox(
                  height: config.App(context).appHeight(3),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(color: Color(0xFFF0F0F0).withOpacity(0.51), borderRadius: BorderRadius.circular(12)),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(selectPic != null ? selectPic.split('/').last : 'Select Picture',
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          IconButton(
                              icon: Icon(
                                Icons.add_circle_outline,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                showCupertinoModalPopup(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return CupertinoActionSheet(
                                        title: Text('Choose'),
                                        actions: <Widget>[
                                          CupertinoActionSheetAction(
                                            child: Text('Camera'),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              /*int emptyPos = 0;
                                               for (int i = 0;
                                                  i < uploadImages.length;
                                                  i++) {
                                                if (uploadImages[i] == null ||
                                                    uploadImages[i].isEmpty) {
                                                  emptyPos = (i + 1);
                                                  break;
                                                }
                                              }
                                              print('$emptyPos');
                                              selectImageFromCamera(emptyPos,
                                                  state.userImages.id);*/
                                            },
                                          ),
                                          CupertinoActionSheetAction(
                                            child: Text('Gallery'),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              /*int emptyPos = 0;
                                              for (int i = 0;
                                                  i < uploadImages.length;
                                                  i++) {
                                                if (uploadImages[i] == null ||
                                                    uploadImages[i].isEmpty) {
                                                  emptyPos = (i + 1);
                                                  break;
                                                }
                                              }
                                              print('$emptyPos');
                                              selectImageFromLibrary(emptyPos,
                                                  state.userImages.id);*/
                                            },
                                          ),
                                        ],
                                        cancelButton: CupertinoActionSheetAction(
                                          child: Text(
                                            "Cancel",
                                            style: TextStyle(color: Colors.red),
                                          ),
                                          onPressed: () => Navigator.pop(context),
                                        ),
                                      );
                                    });
                              })
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: config.App(context).appHeight(3),
                ),
                Center(
                  child: CustomButton(
                      callback: () {
                        /*int emptyPos = 0;
                        for (int i = 0; i < images.length; i++) {
                          if (images[i].isEmpty) {
                            emptyPos = i;
                            break;
                          }
                        }*/
                        Navigator.of(context).pop();
                      },
                      string: 'Upload Image'),
                ),
                Center(
                  child: TextButton(
                    child: Text(
                      'Back',
                      style: TextStyle(color: Colors.white70),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          );
        });
  }

  void onWillPop() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => SearchTrainer(
                  isLoggedIn: true,
                )));
  }
}

class ProfileImageUpload extends StatefulWidget {
  @override
  _ProfileImageUploadState createState() => _ProfileImageUploadState();
}

class _ProfileImageUploadState extends State<ProfileImageUpload> {
  final picker = ImagePicker();

  void selectImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      BlocProvider.of<ProfileBloc>(context).add(UploadImageEvent(pathImage: pickedFile.path));
    }
  }

  void selectImageFromLibrary() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    print(pickedFile.path);
    if (pickedFile != null) {
      BlocProvider.of<ProfileBloc>(context).add(UploadImageEvent(pathImage: pickedFile.path));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Stack(children: [
          InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () => showImageUploadPopup(context),
            child: UserImage(
              radius: null,
              image: state.tempUser?.profilePicture,
            ),
          ),
          Positioned(
              bottom: 0,
              right: 0,
              child: IconButton(
                onPressed: () {
                  showImageUploadPopup(context);
                },
                icon: Icon(
                  Icons.camera_alt,
                  color: Colors.white,
                  size: 28,
                ),
              )),
        ]);
      },
    );
  }

  void showImageUploadPopup(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return CupertinoActionSheet(
            title: Text('Choose'),
            actions: <Widget>[
              CupertinoActionSheetAction(
                child: Text('Camera'),
                onPressed: () {
                  Navigator.pop(context);
                  selectImageFromCamera();
                },
              ),
              CupertinoActionSheetAction(
                child: Text('Gallery'),
                onPressed: () {
                  Navigator.pop(context);
                  selectImageFromLibrary();
                },
              ),
            ],
            cancelButton: CupertinoActionSheetAction(
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () => Navigator.pop(context),
            ),
          );
        });
  }
}

/*
class ProfileImageBuilder extends StatelessWidget {
  final double radius;

  ProfileImageBuilder({this.radius});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return InkWell(
          onTap: () => null,//showImageUploadPopup(context),
          child: UserImage(
            radius: radius,
            image: state.tempUser?.profilePicture,
          ),
        );
      },
    );
  }
}
*/

class UserNameBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      buildWhen: (p, c) => p.tempUser != c.tempUser,
      builder: (context, state) {
        return Text(
          state.tempUser != null
              ? "${state.tempUser.firstName} ${state.tempUser.lastName}"
              : "${Storage().userProfile.firstName} ${Storage().userProfile.lastName}",
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );
  }
}

class AddressBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      buildWhen: (p, c) => p.tempUser != c.tempUser,
      builder: (context, state) {
        return Flexible(
          child: Text(
            state.tempUser != null ? "${state.tempUser.city}" : "${Storage().userProfile.city}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.headline4,
          ),
        );
      },
    );
  }
}

class UserProgramsBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Visibility(
          visible: state.programs.programName.isNotEmpty ? true : false,
          child: Container(
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "PROGRAMS",
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                SizedBox(
                  height: config.App(context).appHeight(2),
                ),
                GridView.count(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 3,
                  crossAxisSpacing: config.App(context).appWidth(4),
                  mainAxisSpacing: config.App(context).appWidth(4),
                  children: List.generate(state.programs.programName.length ?? 0, (index) {
                    return Container(
                      height: config.App(context).appWidth(24),
                      width: config.App(context).appWidth(24),
                      decoration: BoxDecoration(
                        color: Theme.of(context).cardColor.withOpacity(0.51),
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                          color: Colors.white,
                          width: 1,
                        ),
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/img/icons/${state.programs.programName[index]}.svg",
                              width: config.App(context).appWidth(8),
                              height: config.App(context).appWidth(8),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Text(
                              state.programs.programName[index],
                              style: Theme.of(context).textTheme.headline4.copyWith(
                                    fontSize: 8,
                                  ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class UserCalenderBuilder extends StatelessWidget {
  _presentIcon(String day) => Container(
        alignment: Alignment.center,
        child: Text(
          day,
          style: TextStyle(
            color: Colors.blue,
            fontSize: 12,
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        EventList<Event> _markedDateMap = EventList<Event>(
          events: {},
        );

        if (state.calenderData != null) {
          for (int i = 0; i < state.calenderData.dates.length; i++) {
            //print(' format ${DateTime.parse(state.calenderData.dates[i])}');
            _markedDateMap.add(
              DateTime.parse(state.calenderData.dates[i]),
              Event(
                date: DateTime.parse(state.calenderData.dates[i]),
                title: 'Event ${i + 1}',
                icon: _presentIcon('${DateFormat('d').format(DateTime.parse(state.calenderData.dates[i]))}'),
              ),
            );
          }
        }

        return Container(
          decoration: BoxDecoration(
            color: config.Colors().secondDarkColor(1),
            borderRadius: BorderRadius.circular(20),
          ),
          child: CalendarCarousel(
            showOnlyCurrentMonthDate: true,
            height: 250,
            dayPadding: 0.0,
            minSelectedDate: DateTime(DateTime.now().year),
            maxSelectedDate: DateTime(DateTime.now().year + 10),
            headerTextStyle: TextStyle(
              fontSize: 14,
              color: Colors.white,
              fontWeight: FontWeight.w400,
            ),
            headerMargin: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
            leftButtonIcon: Transform.rotate(
              angle: 3.14,
              child: Icon(
                Icons.play_arrow,
                color: Colors.white,
                size: 12,
              ),
            ),
            rightButtonIcon: Icon(
              Icons.play_arrow,
              color: Colors.white,
              size: 12,
            ),
            weekdayTextStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
            ),
            selectedDayTextStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
            ),
            weekendTextStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
            ),
            daysTextStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
            ),
            todayButtonColor: config.Colors().mainColor(1),
            childAspectRatio: 2 / 1.3,
            markedDatesMap: _markedDateMap,
            markedDateShowIcon: true,
            markedDateIconMaxShown: 1,
            markedDateMoreShowTotal: null,
            onDayPressed: (date, list) {
              if (_markedDateMap.getEvents(date).isNotEmpty) {
                String eventDate = DateFormat('yyyy-MM-dd').format(date);
                print('selected date : $eventDate');
                List<CalenderEvents> eventList = [];
                for (int c = 0; c < state.calenderData.data.length; c++) {
                  if (eventDate == state.calenderData.data[c].startDate) {
                    eventList.add(state.calenderData.data[c]);
                  }
                }
                openCalenderEventPopup(context, date, eventList);
              }
            },
            markedDateIconBuilder: (event) {
              return event.getIcon();
            },
            onCalendarChanged: (DateTime datetime) {
              String firstDateOfMonth = DateFormat('yyyy-MM-dd').format(datetime);
              List<String> list = firstDateOfMonth.split('-');
              DateTime lastDate = DateTime.utc(
                int.parse(list[0]),
                int.parse(list[1]) + 1,
              ).subtract(Duration(days: 1));
              String lastDateOfMonth = DateFormat('yyyy-MM-dd').format(lastDate);
              print('first : $firstDateOfMonth last : $lastDateOfMonth');
              BlocProvider.of<ProfileBloc>(context)
                  .add(GetCalenderDataEvent(startTime: firstDateOfMonth, endTime: lastDateOfMonth, userId: Storage().currentUser.userId));
            },
          ),
        );
      },
    );
  }

  getImageType(String mode) {
    if (mode == 'home') {
      return "assets/img/icons/gym_icon.png";
    } else if (mode == 'online') {
      return "assets/img/icons/video_icon.png";
    } else {
      return "assets/img/icons/gym_icon.png";
    }
  }

  getImageTypes(String mode, BuildContext context) {
    if (mode == 'home') {
      return Icon(
        Icons.home,
        color: Theme.of(context).primaryColor,
        size: 16,
      );
    } else if (mode == 'online') {
      return Image.asset(
        "assets/img/icons/video_icon.png",
        width: 16,
      );
    } else {
      return Image.asset(
        "assets/img/icons/gym_icon.png",
        width: 16,
      );
    }
  }

  getTextType(String mode) {
    if (mode == 'home') {
      return 'At Home';
    } else if (mode == 'online') {
      return "Live";
    } else {
      return "At Gym";
    }
  }

  openCalenderEventPopup(BuildContext context, DateTime date, List<CalenderEvents> events) {
    return showDialog(
        context: context,
        builder: (BuildContext context) => Dialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.all(16),
              child: Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                  maxWidth: MediaQuery.of(context).size.width,
                ),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Container(
                        margin: const EdgeInsets.only(top: 50),
                        decoration: BoxDecoration(
                          color: Color(0xFF303743),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        padding: EdgeInsets.only(top: config.App(context).appHeight(3)),
                        child: Column(mainAxisSize: MainAxisSize.min, children: [
                          Text(
                            "${DateFormat('dd MMM yyyy').format(DateTime.parse(date.toString()))}".toUpperCase(),
                            style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(.5),
                          ),
                          Text(
                            "All The Sessions You have Joined",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: config.App(context).appHeight(.5),
                          ),
                          ConstrainedBox(
                            constraints: BoxConstraints(
                              maxHeight: config.App(context).appHeight(60),
                            ),
                            child: Container(
                                child: ListView.builder(
                                    itemCount: events.length,
                                    shrinkWrap: true,
                                    padding: EdgeInsets.all(16),
                                    physics: ClampingScrollPhysics(),
                                    scrollDirection: Axis.vertical,
                                    itemBuilder: (BuildContext context, int index) {
                                      return Container(
                                          padding: EdgeInsets.all(16),
                                          margin: EdgeInsets.only(top: 16),
                                          decoration: BoxDecoration(color: Color(0xFF091727), borderRadius: BorderRadius.circular(10.0)),
                                          child: Stack(alignment: AlignmentDirectional.topEnd, children: [
                                            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin: EdgeInsets.only(right: 10),
                                                  width: 16,
                                                  height: 16,
                                                  child: getImageTypes(events[index].modeOfTraining, context),
                                                ),
                                                Text(
                                                  "${getTextType(events[index].modeOfTraining)}",
                                                  style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 14, fontWeight: FontWeight.bold),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ]),
                                              SizedBox(height: config.App(context).appHeight(1)),
                                              Container(
                                                child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                                  Text(
                                                    events[index].startTime,
                                                    style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                                                  ),
                                                  SizedBox(width: config.App(context).appWidth(3)),
                                                  Text(
                                                    "To",
                                                    style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal),
                                                  ),
                                                  SizedBox(width: config.App(context).appWidth(3)),
                                                  Text(
                                                    events[index].endTime,
                                                    style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                                                  ),
                                                ]),
                                              ),
                                              SizedBox(height: config.App(context).appHeight(3)),
                                              Container(
                                                child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                                  Text(
                                                    '${events[index].totalPrice}' + String.fromCharCodes(Runes('\u0024')),
                                                    style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                                                  ),
                                                  SizedBox(width: config.App(context).appWidth(3)),
                                                  Container(
                                                    width: 6,
                                                    height: 6,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(3)),
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                  SizedBox(width: config.App(context).appWidth(3)),
                                                  Text(
                                                    '${events[index].totalHours} Hrs',
                                                    style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                                                  ),
                                                  SizedBox(width: config.App(context).appWidth(3)),
                                                  Container(
                                                    width: 6,
                                                    height: 6,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(3)),
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                  SizedBox(width: config.App(context).appWidth(3)),
                                                  Text(
                                                    events[index].status == 'paid' ? 'Paid' : 'Not Paid',
                                                    style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
                                                  ),
                                                ]),
                                              ),
                                              SizedBox(height: config.App(context).appHeight(1)),
                                              Text(
                                                Storage().currentUser.accountType == 'instructor'
                                                    ? 'by ${events[index].clientName}'
                                                    : 'by ${events[index].instructorName}',
                                                style: TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.normal),
                                              ),
                                            ]),
/*                                                    Container(
                                                      alignment:
                                                          Alignment.topRight,
                                                      child: IconButton(
                                                          padding:
                                                              EdgeInsets.all(0),
                                                          alignment: Alignment
                                                              .topRight,
                                                          icon: Icon(
                                                              Icons.more_vert,
                                                              color:
                                                                  Colors.white),
                                                          onPressed: null),
                                                    ),*/
                                          ]));
                                    })),
                          )
                        ]),
                      ),
                    ),
                    Positioned(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Icon(Icons.close, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}

dialog1Delete(BuildContext context, {Function onDelete, Function onClose}) {
  return showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            //insetPadding: const EdgeInsets.all(1),
            child: Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: Color(0xFF3D434D),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(20.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ]),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "DELETE IMAGE?",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(3),
                  ),
                  Text(
                    "Are you sure you want to\ndelete this image?",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(3),
                  ),
                  InkWell(
                    onTap: () {
                      onDelete();
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                      width: config.App(context).appWidth(65),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(2),
                        horizontal: 20,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "Delete",
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      onClose();
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                      width: config.App(context).appWidth(65),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(2),
                        horizontal: 20,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ));
}

dialog2Delete(BuildContext context, {Function onDelete, Function onClose}) {
  return showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            //insetPadding: const EdgeInsets.all(1),
            child: Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: Color(0xFF3D434D),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(20.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ]),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "DELETE IMAGE?",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(3),
                  ),
                  Text(
                    "Are you sure you want to\ndelete this image?",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: config.App(context).appHeight(3),
                  ),
                  InkWell(
                    onTap: () {
                      onDelete();
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                      width: config.App(context).appWidth(65),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(2),
                        horizontal: 20,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "Delete",
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      onClose();
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: config.App(context).appHeight(1)),
                      width: config.App(context).appWidth(65),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(2),
                        horizontal: 20,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ));
}

/*showModalBottomSheet(
                          isScrollControlled: true,
                          backgroundColor: Color.fromRGBO(49, 55, 66, 1),
                          isDismissible: false,
                          enableDrag: false,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.vertical(top: Radius.circular(30)),
                          ),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          context: context,
                          builder: (BuildContext context) {
                            return BottomSheet(
                              onClosing: () {},
                              builder: (BuildContext context) {
                                return StatefulBuilder(
                                    builder: (BuildContext context, setState) =>
                                        Container());
                              },
                            );
                          });*/
