import 'package:badges/badges.dart' as badges;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fytme/bloc/homeBloc/home_bloc.dart';
import 'package:fytme/data/model/blocs/state_status.dart';
import 'package:fytme/data/model/dashboard_model.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:intl/intl.dart';
import 'package:sticky_headers/sticky_headers.dart';

import '../../../common/constants.dart';
import '../../../main.dart';
import '../../helpers/app_config.dart' as config;
import 'common/scaffold.dart';

enum TransactionInterval { currentMonth, currentWeek, today, lastMonth }

class StickyHeaderList {
  String date;
  List<Data> transaction;

  StickyHeaderList(this.date, this.transaction);
}

class DashboardWidget extends StatefulWidget {
  final bool isLoggedIn;

  DashboardWidget({this.isLoggedIn});

  @override
  _DashboardWidgetState createState() => _DashboardWidgetState();
}

class _DashboardWidgetState extends State<DashboardWidget> {
  bool isMap = true;
  DateTime currentBackPressTime;
  TransactionInterval interval = TransactionInterval.currentMonth;
  String label = 'This Month';

  bool isBindResult = false;
  DashboardModel dashboardResult;
  List<StickyHeaderList> stickyList = [];
  String transactionType = 'all';
  String totalEarnings = '0.0';

  String startDate = '';
  String endDate = '';

  @override
  void initState() {
    filterBasedOnDate();
    super.initState();
  }

  filterBasedOnDate() {
    DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    DateTime now = DateTime.now();
    if (interval == TransactionInterval.today) {
      startDate = dateFormat.format(now);
      endDate = dateFormat.format(now);
    } else if (interval == TransactionInterval.lastMonth) {
      startDate = dateFormat.format(DateTime.utc(now.year, now.month - 1, 1));
      endDate = dateFormat.format(DateTime.utc(
        now.year,
        now.month - 1,
      ).add(Duration(days: DateTime(now.year, now.month, 0).day - 1)));
    } else if (interval == TransactionInterval.currentWeek) {
      startDate = dateFormat.format(DateTime(now.year, now.month, now.day).subtract(Duration(days: now.weekday - 1)));
      endDate = dateFormat.format(DateTime(now.year, now.month, now.day).add(Duration(days: DateTime.daysPerWeek - now.weekday)));
    } else {
      startDate = dateFormat.format(DateTime.utc(DateTime.now().year, now.month, 1));
      endDate = dateFormat.format(DateTime.utc(
        now.year,
        now.month + 1,
      ).subtract(Duration(days: 1)));
    }
    isBindResult = false;
    String queryParams = 'start_date=$startDate&end_date=$endDate';
    if (transactionType != 'all') {
      queryParams = '$queryParams&transaction_type=$transactionType';
    }
    context.read<HomeBloc>().add(DashboardEvent(query: queryParams));
  }

  Widget rateStar(int rate) {
    return Row(
      children: [
        for (var i = 0; i < 5; i++)
          Icon(
            Icons.star_rounded,
            color: Color(i < rate ? 0xFFED8A19 : 0xFF7F8389),
            size: 18,
          ),
      ],
    );
  }

  updateIntervalStatus(TransactionInterval status, String intLabel) {
    Navigator.pop(context);
    if (label != intLabel) {
      setState(() {
        interval = status;
        label = intLabel;
      });
      filterBasedOnDate();
    }
  }

  getDescriptions() {
    if (transactionType == 'all') {
      return 'Your Transactions will start\nshowing here once you\nreceive or withdraw payment.';
    } else if (transactionType == 'withdrawal') {
      return 'Your Transactions will start\nshowing here once you withdraw\npayment from your wallet.';
    } else {
      return 'Your Transactions will start\nshowing here once you receive\npayment from your clients.';
    }
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null || now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      if (widget.isLoggedIn) {
        Fluttertoast.showToast(
          msg: 'Click BACK again for exit',
          gravity: ToastGravity.BOTTOM,
        );
      } else {
        Navigator.of(context).pushNamed('/Home');
      }
      return Future.value(false);
    } else {
      SystemNavigator.pop();
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    printLog('===== user login');
    print(widget.isLoggedIn);
    return WillPopScope(
      onWillPop: () => onWillPop(),
      child: MyScaffold(
        bottomMenuIndex: widget.isLoggedIn ? 0 : null,
        body: BlocListener<HomeBloc, HomeState>(
            listenWhen: (previous, current) => previous.status.status != current.status.status,
            listener: (con, state) {
              switch (state.status.status) {
                case StateStatuses.loading:
                  CustomLoader().show(context);
                  break;
                case StateStatuses.failure:
                  CustomLoader().hide(context);
                  CustomWidgets.buildErrorSnackBar(context, state.status.message);
                  break;
                case StateStatuses.success:
                  CustomLoader().hide(context);
                  if (!isBindResult) {
                    if (state.dashboardResult != null) {
                      dashboardResult = state.dashboardResult;
                      averageRatings = double.parse(dashboardResult.ratings).toInt();
                      noOfReviews = dashboardResult.noOfReviews;
                      totalEarnings = dashboardResult.totalEarnings;
                      stickyList = [];
                      var dateList = [];
                      for (int p = 0; p < dashboardResult.data.length; p++) {
                        dateList.add(dashboardResult.data[p].paymentDate);
                      }
                      List<dynamic> dates = dateList.toSet().toList();
                      for (int p = 0; p < dates.length; p++) {
                        List<Data> transactionList = dashboardResult.data.where((element) => element.paymentDate == dates[p]).toList();
                        stickyList.add(StickyHeaderList(
                          dates[p],
                          transactionList,
                        ));
                      }
                      setState(() {
                        isBindResult = true;
                      });
                    }
                    if (state.status.message.isNotEmpty) {
                      setState(() {
                        totalEarnings = '0.0';
                      });
                      CustomWidgets.buildSuccessSnackBar(context, state.status.message);
                    }
                  }
                  break;
                default:
                  CustomLoader().hide(context);
                  break;
              }
            },
            child: isBindResult
                ? Column(
                    children: [
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      createCardView(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      balanceView(),
                      SizedBox(
                        height: config.App(context).appHeight(2),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 16),
                            child: Text(
                              'All Transaction',
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white),
                            ),
                          ),
                          PopupMenuButton(
                            offset: Offset(-15.0, 25.0),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            color: Color(0xFF7F8389),
                            child: Padding(
                              padding: const EdgeInsets.only(right: 16, left: 16),
                              child: Row(
                                children: [
                                  Text(
                                    label,
                                    style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.grey,
                                  )
                                ],
                              ),
                            ),
                            itemBuilder: (BuildContext context) => [
                              PopupMenuItem(
                                  child: MyRadioOption(
                                value: TransactionInterval.currentMonth,
                                groupValue: interval,
                                label: label,
                                text: 'This Month',
                                onChanged: (value) {
                                  updateIntervalStatus(value, 'This Month');
                                },
                              )),
                              PopupMenuItem(
                                  child: MyRadioOption(
                                value: TransactionInterval.currentWeek,
                                groupValue: interval,
                                label: label,
                                text: 'This Week',
                                onChanged: (value) {
                                  updateIntervalStatus(value, 'This Week');
                                },
                              )),
                              PopupMenuItem(
                                  child: MyRadioOption(
                                value: TransactionInterval.today,
                                groupValue: interval,
                                label: label,
                                text: 'Today',
                                onChanged: (value) {
                                  updateIntervalStatus(value, 'Today');
                                },
                              )),
                              PopupMenuItem(
                                  child: MyRadioOption(
                                value: TransactionInterval.lastMonth,
                                groupValue: interval,
                                label: label,
                                text: 'Last Month',
                                onChanged: (value) {
                                  updateIntervalStatus(value, 'Last Month');
                                },
                              )),
                            ],
                            onSelected: (type) {
                              print(type);
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: config.App(context).appHeight(1),
                      ),
                      transactionList(context),
                      Visibility(
                        visible: stickyList.isEmpty,
                        child: Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'No Transactions',
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.grey),
                            ),
                            SizedBox(
                              height: config.App(context).appHeight(1),
                            ),
                            Text(
                              getDescriptions(),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16, color: Colors.grey),
                            ),
                          ],
                        )),
                      ),
                      Visibility(
                        visible: stickyList.isNotEmpty,
                        child: Expanded(
                          child: ListView.builder(
                              padding: EdgeInsets.only(bottom: 50),
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemCount: stickyList.length,
                              itemBuilder: (context, sIndex) {
                                return StickyHeader(
                                    header: Container(
                                      height: 40.0,
                                      color: Theme.of(context).primaryColorDark,
                                      padding: EdgeInsets.only(left: 16.0),
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        parseDateFormat(stickyList[sIndex].date, 'yyyy-MM-dd', 'dd MMMM yyyy'),
                                        style: const TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    content: ListView.builder(
                                        padding: const EdgeInsets.only(left: 16, right: 16),
                                        shrinkWrap: true,
                                        scrollDirection: Axis.vertical,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: stickyList[sIndex].transaction.length,
                                        itemBuilder: (context, cIndex) {
                                          return Container(
                                            margin: EdgeInsets.only(top: 12),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(16),
                                              color: stickyList[sIndex].transaction[cIndex].transactionType == "Received"
                                                  ? Color.fromRGBO(8, 41, 40, 1.0)
                                                  : Color.fromRGBO(44, 27, 37, 1.0),
                                            ),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 16),
                                                      child: ClipRRect(
                                                        borderRadius: BorderRadius.all(Radius.circular(30)),
                                                        child: CachedNetworkImage(
                                                            width: 60,
                                                            height: 60,
                                                            imageUrl: stickyList[sIndex].transaction[cIndex].profilePicture ?? '',
                                                            fit: BoxFit.cover,
                                                            errorWidget: (context, url, error) => Icon(
                                                                  Icons.person,
                                                                  color: Colors.white,
                                                                )),
                                                      ),
                                                    ),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          stickyList[sIndex].transaction[cIndex].clientName,
                                                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Colors.white),
                                                        ),
                                                        SizedBox(
                                                          height: 5,
                                                        ),
                                                        Container(
                                                          width: config.App(context).appWidth(45),
                                                          child: Text(
                                                            'Yoga training on\n${parseDateFormat(stickyList[sIndex].transaction[cIndex].paymentDate, 'yyyy-MM-dd', 'dd MMMM yyyy')}',
                                                            style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
                                                          ),
                                                        )
                                                      ],
                                                    )
                                                  ],
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(right: 16.0),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Text(
                                                        '+ ${String.fromCharCodes(Runes('\u0024'))}${stickyList[sIndex].transaction[cIndex].amount}',
                                                        style: TextStyle(
                                                            fontWeight: FontWeight.bold,
                                                            fontSize: 16,
                                                            color: stickyList[sIndex].transaction[cIndex].transactionType == "Received"
                                                                ? Color.fromRGBO(30, 183, 110, 1)
                                                                : Color.fromRGBO(255, 88, 88, 1)),
                                                      ),
                                                      SizedBox(
                                                        height: 16,
                                                      ),
                                                      Text(
                                                        parseDateFormat(stickyList[sIndex].transaction[cIndex].paymentTime, 'HH:mm:ss', 'HH:mm a'),
                                                        style: TextStyle(
                                                            fontWeight: FontWeight.normal, fontSize: 12, color: Color.fromRGBO(127, 131, 137, 1)),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        }));
                              }),
                        ),
                      )
                    ],
                  )
                : Container()),
      ),
    );
  }

  transactionList(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8, left: 8),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              TextButton(
                onPressed: () {
                  filterBasedOnType('all');
                },
                child: Container(
                  alignment: Alignment.center,
                  width: config.App(context).appWidth(27),
                  height: config.App(context).appWidth(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: transactionType == 'all' ? Theme.of(context).primaryColor : Color(0xFF7F8389),
                  ),
                  child: Text(
                    "All",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  filterBasedOnType('received');
                },
                child: Container(
                  alignment: Alignment.center,
                  width: config.App(context).appWidth(27),
                  height: config.App(context).appWidth(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: transactionType == 'received' ? Theme.of(context).primaryColor : Color(0xFF7F8389),
                  ),
                  child: Text(
                    "Received",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  filterBasedOnType('withdrawal');
                },
                child: Container(
                  alignment: Alignment.center,
                  width: config.App(context).appWidth(27),
                  height: config.App(context).appWidth(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: transactionType == 'withdrawal' ? Theme.of(context).primaryColor : Color(0xFF7F8389),
                  ),
                  child: Text(
                    "Withdrawal",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  filterBasedOnType(String type) {
    if (transactionType != type) {
      setState(() {
        transactionType = type;
      });
      isBindResult = false;
      String queryParams = 'start_date=$startDate&end_date=$endDate';
      if (transactionType != 'all') {
        queryParams = '$queryParams&transaction_type=$transactionType';
      }
      context.read<HomeBloc>().add(DashboardEvent(query: queryParams));
    }
  }

  Widget createCardView() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Color.fromRGBO(62, 67, 76, 1),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 16),
                  child: badges.Badge(
                    position: badges.BadgePosition.topEnd(
                      top: 0,
                      end: -10,
                    ),
                    badgeContent: Container(width: 15, height: 15, child: Image.asset("assets/img/icons/premium-quality.png")),
                    badgeStyle: badges.BadgeStyle(badgeColor: Colors.transparent),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      child: CachedNetworkImage(
                          width: 60,
                          height: 60,
                          imageUrl: dashboardResult.profilePicture ?? '',
                          fit: BoxFit.cover,
                          errorWidget: (context, url, error) => Icon(
                                Icons.person,
                                color: Colors.white,
                              )),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${dashboardResult.name}',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Container(
                      width: config.App(context).appWidth(40),
                      child: RichText(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        text: TextSpan(
                          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16, color: Colors.white),
                          text: '${dashboardResult.specialities}',
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${dashboardResult.ratings}",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17, color: Colors.white),
                        // style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.orange,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "${dashboardResult.noOfReviews} Reviews",
                    style: TextStyle(fontWeight: FontWeight.normal, fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget balanceView() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Color.fromRGBO(62, 67, 76, 1),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Your Balance',
                    style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.grey),
                    // style: Theme.of(context).textTheme.subtitle1,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    '${String.fromCharCodes(Runes('\u0024'))}  $totalEarnings',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, right: 16),
              child: Row(
                children: [
                  Visibility(
                    visible: totalEarnings != '0.0',
                    child: TextButton(
                      onPressed: () {
                        context.read<HomeBloc>().add(WithdrawalEvent(amount: totalEarnings));
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: config.App(context).appWidth(30),
                        height: config.App(context).appWidth(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).primaryColor,
                        ),
                        child: Text(
                          "Withdrawal",
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ),
                    ),
                  ),
                  /*Container(
                    alignment: Alignment.topRight,
                    child: IconButton(
                        padding: EdgeInsets.only(top: 10),
                        alignment: Alignment.topRight,
                        icon: Icon(Icons.more_vert, color: Colors.white),
                        onPressed: null),
                  ),*/
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String parseDateFormat(String date, String currentFormat, String newFormat) {
    DateTime parseDate = DateFormat(currentFormat).parse(date);
    var inputDate = DateTime.parse(parseDate.toString());
    String actualDay = DateFormat(newFormat).format(inputDate);
    return actualDay;
  }
}

class MyRadioOption<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final String label;
  final String text;
  final ValueChanged<T> onChanged;

  const MyRadioOption({
    @required this.value,
    @required this.groupValue,
    @required this.label,
    @required this.text,
    @required this.onChanged,
  });

  Widget _buildLabel(BuildContext context) {
    final bool isSelected = value == groupValue;
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 24,
          height: 24,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Colors.white,
          ),
        ),
        Container(
          width: 12,
          height: 12,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: isSelected ? Theme.of(context).primaryColor : Colors.white,
          ),
        )
      ],
    );
  }

  Widget _buildText() {
    return Text(
      text,
      style: const TextStyle(color: Colors.white, fontSize: 12),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      child: InkWell(
        onTap: () => onChanged(value),
        splashColor: Colors.cyan.withOpacity(0.5),
        child: Row(
          children: [
            _buildLabel(context),
            SizedBox(width: 10),
            _buildText(),
          ],
        ),
      ),
    );
  }
}
/*startDate = dateFormat.format((now.month < 12)
          ? DateTime(now.year, now.month, 1)
          : DateTime(now.year + 1, 1, 1));
      endDate = dateFormat.format((now.month < 12)
          ? DateTime(now.year, now.month + 1, 0)
          : DateTime(now.year + 1, 1, 0));*/
