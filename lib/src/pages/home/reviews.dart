import 'package:badges/badges.dart' as badges;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fytme/bloc/reviewbloc/review_bloc.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/review.dart';
import 'package:fytme/data/model/search_instructor_model.dart';
import 'package:fytme/data/services/custom_loader.dart';
import 'package:fytme/data/services/custom_snackbar.dart';

class Reviews extends StatefulWidget {
  final InstructorModel instructorDetails;

  Reviews({Key key, @required this.instructorDetails}) : super(key: key);

  @override
  _InstructorReviewState createState() => _InstructorReviewState();
}

class _InstructorReviewState extends State<Reviews> {
  bool isLoading = false;

  List<Review> reviewList = [];
  int totalReviews = 0;
  int averageReviews = 0;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ReviewBloc>(context).add(ReviewInitialEvent(id: widget.instructorDetails.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Image.asset("assets/img/icons/arrow_back_blue.png"),
          iconSize: 40,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          'REVIEWS',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
          // style: Theme.of(context).textTheme.subtitle1,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
      ),
      body: BlocListener<ReviewBloc, ReviewState>(
        listenWhen: (previous, current) => previous.status.status != current.status.status,
        listener: (con, state) {
          switch (state.status.status) {
            case StateStatuses.loading:
              CustomLoader().show(context);
              break;
            case StateStatuses.failure:
              reviewList = [];
              CustomLoader().hide(context);
              CustomWidgets.buildErrorSnackBar(context, state.status.message);
              break;
            case StateStatuses.success:
              CustomLoader().hide(context);
              if (state.review != null) {
                this.setState(() {
                  reviewList = state.review;
                  totalReviews = reviewList.length;
                  averageReviews = (reviewList.map((m) => m.rating).reduce((a, b) => a + b) / reviewList.length).round();
                });
              }
              break;
            default:
              reviewList = [];
              CustomLoader().hide(context);
              break;
          }
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Column(
              children: [
                SizedBox(
                  height: 32,
                ),
                createCardView(),
                SizedBox(
                  height: 32,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Detailed Reviews",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                createListView(),
                /* Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      isLoading = true;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: 24, bottom: 24),
                    child: Center(
                        child: Text(
                      "SHOW MORE REVIEWS",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Theme.of(context).primaryColor),
                    )),
                  ),
                ),
              ),*/
                Container(
                  height: isLoading ? 50.0 : 0,
                  color: Colors.transparent,
                  child: Center(
                    child: new CircularProgressIndicator(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget createCardView() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Color.fromRGBO(62, 67, 76, 1),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 16),
                  child: badges.Badge(
                    position: badges.BadgePosition.topEnd(
                      top: 0,
                      end: -10,
                    ),
                    badgeContent: Container(width: 15, height: 15, child: Image.asset("assets/img/icons/premium-quality.png")),
                    badgeStyle: badges.BadgeStyle(badgeColor: Colors.transparent),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      child: CachedNetworkImage(
                          width: 60,
                          height: 60,
                          imageUrl: widget.instructorDetails.profilePicture ?? '',
                          fit: BoxFit.cover,
                          errorWidget: (context, url, error) => Icon(
                                Icons.person,
                                color: Colors.white,
                              )),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${widget.instructorDetails.firstName} ${widget.instructorDetails.lastName}',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                      // style: Theme.of(context).textTheme.subtitle1,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.instructorDetails.lastName,
                      style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14, color: Colors.white),
                    )
                  ],
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "$averageReviews",
                        //"${widget.instructorDetails.ratings}",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17, color: Colors.white),
                        // style: Theme.of(context).textTheme.subtitle1,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.orange,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "$totalReviews Reviews",
                    //"${widget.instructorDetails.ratings} Reviews",
                    style: TextStyle(fontWeight: FontWeight.normal, fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget createListView() {
    return Expanded(
      child: SingleChildScrollView(
          child: ListView.builder(
              itemCount: reviewList.length,
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 16),
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                                  child: Icon(
                                    Icons.person,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    reviewList[index].reviewByUserInfo.username,
                                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: Colors.white),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  AbsorbPointer(
                                    child: RatingBar.builder(
                                      initialRating: reviewList[index].rating.toDouble(),
                                      direction: Axis.horizontal,
                                      itemCount: 5,
                                      minRating: 1,
                                      itemSize: 15,
                                      unratedColor: Color.fromRGBO(128, 131, 136, 1),
                                      itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                                      itemBuilder: (context, _) => Icon(
                                        Icons.star,
                                        color: Colors.amber,
                                      ),
                                      onRatingUpdate: (rating) {
                                        print(rating);
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          /*Padding(
                            padding: EdgeInsets.only(right: 8),
                            child: InkWell(
                              onTap: () {},
                              child: Icon(
                                Icons.more_vert,
                                color: Colors.white,
                              ),
                            ),
                          )*/
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        child: Text(
                          reviewList[index].comments,
                          style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 12),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                );
              })),
    );
  }
}

/*
int page = 1;
  Future _loadData() async {
    // perform fetching data delay
    await new Future.delayed(new Duration(seconds: 2));
    print("load more");
    // update data and loading status
    setState(() {
      // arrReviews.addAll(arrReviews);
      var arrThird = arrReviews;
      arrReviews = arrReviews + arrThird;
      print("Arr reviews count ${arrReviews.length}");
      isLoading = false;
    });
  }
*/
