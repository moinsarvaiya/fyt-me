import 'package:badges/badges.dart' as badges;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/pages/home/search_trainers.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../helpers/app_config.dart' as config;
import 'chat_messages.dart';
import 'common/scaffold.dart';

final fireStore = FirebaseFirestore.instance;

class ChatUsersWidget extends StatefulWidget {
  @override
  _ChatUsersWidgetState createState() => _ChatUsersWidgetState();
}

class _ChatUsersWidgetState extends State<ChatUsersWidget> {
  getDoc() async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection('scanQR');
    final snapshot = await collection.doc(Storage().userProfile.userId.toString()).get();
    DocumentReference<Map<String, dynamic>> documentReference = collection.doc(Storage().userProfile.userId.toString());
    if (snapshot == null || !snapshot.exists) {
      documentReference.set({
        'QR': [
          {'qrCode': '1234567890'},
          {'test2': 'xyz'}
        ]
      });
    } else {
      documentReference.update({
        'QR': [
          {'qrCode': '1234567890', 'qrCode1': '1234567'},
          {'test2': 'xyzpq'}
        ]
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        onWillPop();
        return true;
      },
      child: MyScaffold(
        bottomMenuIndex: 1,
        body: Stack(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: config.App(context).appWidth(10),
                vertical: config.App(context).appHeight(5),
              ),
              child: Text(
                "Messages",
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: config.App(context).appHeight(6),
              ),
              child:
                  /*ListView(
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.symmetric(
                  horizontal: config.App(context).appWidth(5),
                  vertical: config.App(context).appHeight(8),
                ),
                children: chatUsersWidgets,
              ),

              old query
              fireStore
                    .collection("users")
                    .doc(Storage().userProfile.userId.toString())
                    //.get()
                    .snapshots()
                    snapshot.data.data()['Users'].map<Widget>((m) {
              */
                  StreamBuilder(
                stream: fireStore.collection(Storage().userProfile.userId.toString()).orderBy("timestamp", descending: true).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  List<Widget> chatUsersWidgets = snapshot.data.docs.map<Widget>((m) {
                    final id = m['id'];
                    final name = m['name'];
                    final profilePic = m['profilePic'];
                    //final identifier = m['identifier'];
                    final timeStamp = m['timestamp'];
                    final lastMessage = m['lastMessage'];
                    final unreadCount = m['unreadCount'];

                    return ChatUsers(
                      displayId: id,
                      displayName: name,
                      displayPic: profilePic,
                      timestamp: timeStamp,
                      lastMessage: lastMessage,
                      unreadCount: unreadCount,
                    );
                  }).toList();

                  return ListView(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.symmetric(
                      horizontal: config.App(context).appWidth(2),
                      vertical: config.App(context).appHeight(5),
                    ),
                    children: chatUsersWidgets,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void onWillPop() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => SearchTrainer(
                  isLoggedIn: true,
                )));
  }
}

/*DocumentReference<Map<String, Object>>
documentReference =
fireStore.collection('conversations').doc(
    Storage().userProfile.userId.toString());
documentReference.set({
'userId': widget.otherUserId,
'fullName': widget.otherUserName,
'profilePic': widget.otherUserProfile,
'identifier': identifier,
'timestamp': DateTime.now(),
'lastMessage':
messageController.text.toString().trim(),
});*/
class ChatUsers extends StatelessWidget {
  ChatUsers({this.displayId, this.displayName, this.displayPic, this.timestamp, this.lastMessage, this.unreadCount});

  final int displayId;
  final String displayName;
  final String displayPic;
  final Timestamp timestamp;
  final String lastMessage;
  final int unreadCount;

  @override
  Widget build(BuildContext context) {
    final dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000);
    return ListTile(
      contentPadding: EdgeInsets.only(left: 5, right: 5),
      horizontalTitleGap: 0,
      onTap: () {
        Navigator.of(context).pushNamed('/ChatMessages',
            arguments: ChatMessageArguments(otherUserId: displayId, otherUserProfile: displayPic, otherUserName: displayName));
      },
      leading: /*Badge(
        badgeColor: status == 'online' ? Color(0xFF17D52D) : Colors.grey,
        position: BadgePosition.topStart(top: 1, start: 5),
        shape: BadgeShape.circle,
        showBadge: true,
        child:
      ),*/
          Padding(
        padding: EdgeInsets.only(right: 10.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(config.App(context).appWidth(8)),
          child: Container(
            width: config.App(context).appWidth(14),
            height: config.App(context).appWidth(14),
            child: CachedNetworkImage(
              imageUrl: displayPic != null
                  ? displayPic.contains('https://dev.fytme.co.uk')
                      ? displayPic
                      : 'https://dev.fytme.co.uk$displayPic'
                  : "",
              fit: BoxFit.cover,
              placeholder: (context, url) => CircularProgressIndicator(),
              errorWidget: (context, url, error) => Container(
                decoration: BoxDecoration(
                  color: Color(0xFF3D434D),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ),
      title: Text(
        displayName,
        style: Theme.of(context).textTheme.subtitle1,
      ),
      subtitle: Text(
        lastMessage,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
        style: Theme.of(context).textTheme.headline4,
      ),
      trailing: Column(
        children: [
          badges.Badge(
            position: badges.BadgePosition.center(),
            badgeContent: Text(
              unreadCount > 0 ? '$unreadCount' : '',
              style: Theme.of(context).textTheme.headline4,
            ),
            badgeStyle: badges.BadgeStyle(
              badgeColor: unreadCount > 0 ? Theme.of(context).primaryColor : Theme.of(context).primaryColorDark,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            timeago.format(dateTime),
            //"${DateFormat('h:mm a').format(dateTime)}",
            style: Theme.of(context).textTheme.headline4.copyWith(fontSize: 10),
          ),
        ],
      ),
    );
  }
}
/*getChatUsersList() async {
  QuerySnapshot result = await FirebaseFirestore.instance
      .collection('conversations')
      .where('id', isEqualTo: Storage().userProfile.userId)
      .get();

   List<DocumentSnapshot> documents = result.docs;
    DocumentReference<Map<String, Object>> documentReference = FirebaseFirestore
        .instance
        .collection('conversations')
        .doc(Storage().userProfile.userId.toString());
    if (documents.isEmpty) {
      documentReference.set(addConversations());
    } else {}
}*/

/*
Map<String, Object> addConversations() {
  String identifier = Storage().userProfile.userId < widget.otherUserId
      ? "${Storage().userProfile.userId}_${widget.otherUserId}"
      : "${widget.otherUserId}_${Storage().userProfile.userId}";
  return {
    'fromId': Storage().userProfile.userId,
    'toId': widget.otherUserId,
    'identifier': identifier,
    'fullName':
    '${Storage().userProfile.firstName} ${Storage().userProfile.lastName}',
    'profilePic': Storage().userProfile.profilePicture,
    'createdAt': DateTime.now(),
  };
}*/
/*Future<void> fetchAndSetList() {
    fireStore
        .collection("users")
        .doc(Storage().userProfile.userId.toString())
        .get()
        .then((value) => {
              value.data()['Users'].forEach((i) => {
                    chatUsersWidgets.add(ChatUsers(
                      displayId: i['id'],
                      displayName: i['name'],
                      displayPic: i['profilePic'],
                      timestamp: i['timestamp'],
                      lastMessage: i['lastMessage'],
                    ))
                  })
            });
  }*/
