import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:fytme/bloc/homeBloc/home_bloc.dart';
import 'package:fytme/data/model/blocs/state_status.dart';
import 'package:fytme/data/repositories/abstracts/home_repository.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../helpers/app_config.dart' as config;
import 'chat_messages.dart';
import 'common/scaffold.dart';
import 'dashboard.dart';
import 'listresult.dart';
import 'map.dart';

final fireStore = FirebaseFirestore.instance;

class SearchTrainer extends StatelessWidget {
  final bool isLoggedIn;

  SearchTrainer({this.isLoggedIn});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(homeRepo: RepositoryProvider.of<HomeRepository>(context)),
      child: !isLoggedIn
          ? SearchTrainersWidget(
              isLoggedIn: isLoggedIn,
            )
          : Storage().userProfile.accountType == 'instructor'
              ? DashboardWidget(
                  isLoggedIn: isLoggedIn,
                )
              : SearchTrainersWidget(
                  isLoggedIn: isLoggedIn,
                ),
    );
  }
}

class SearchTrainersWidget extends StatefulWidget {
  final bool isLoggedIn;

  SearchTrainersWidget({this.isLoggedIn});

  @override
  _SearchTrainersWidgetState createState() => _SearchTrainersWidgetState();
}

class _SearchTrainersWidgetState extends State<SearchTrainersWidget> {
  bool isMap = true;
  DateTime currentBackPressTime;

  //InAppNotificationBloc _bloc;
  //Timer timer;

  @override
  void dispose() {
    super.dispose();
    //timer?.cancel();
  }

  @override
  void initState() {
    super.initState();
    //timer = Timer.periodic(
    // Duration(seconds: 60), (Timer t) => _bloc.getNotificationCount());
  }

  updateStatus(AppLifecycleState state) async {
    CollectionReference<Map<String, dynamic>> collection = fireStore.collection(Storage().userProfile.userId.toString());
    QuerySnapshot result = await collection.get();
    List<DocumentSnapshot> documents = result.docs;
    if (documents.isNotEmpty) {
      List<Users> users = [];
      for (var i = 0; i < documents.length; i++) {
        DocumentSnapshot<Map<String, dynamic>> snapshot = await collection.doc(documents[i].id).get();
        users.add(Users(snapshot.data()['id'].toString(), documents[i].id, snapshot.data()['unreadCount']));
      }
      for (var u = 0; u < users.length; u++) {
        String status = 'offline';
        if (state == AppLifecycleState.resumed) {
          status = 'online';
        }
        collection.doc(users[u].documentId).update({
          'status': status,
        });
        /*if (users[u].id == Storage().userProfile.userId.toString()) {
          String status = 'offline';
          if (state == AppLifecycleState.resumed) {
            status = 'online';
          }
          collection.doc(users[u].documentId).update({
            'status': status,
          });
          break;
        }*/
      }
    }
  }

  Widget rateStar(int rate) {
    return Row(
      children: [
        for (var i = 0; i < 5; i++)
          Icon(
            Icons.star_rounded,
            color: Color(i < rate ? 0xFFED8A19 : 0xFF7F8389),
            size: 18,
          ),
      ],
    );
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null || now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      if (widget.isLoggedIn) {
        Fluttertoast.showToast(
          msg: 'Click BACK again for exit',
          gravity: ToastGravity.BOTTOM,
        );
      } else {
        Navigator.of(context).pushNamed('/Home');
      }
      // CommonMethods.showError(_scaffoldKey, 'Click BACK again for exit');
      return Future.value(false);
    } else {
      SystemNavigator.pop();
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    printLog('===== user login');
    print(widget.isLoggedIn);
    //_bloc = Provider.of<InAppNotificationBloc>(context);
    return WillPopScope(
      onWillPop: () async {
        onWillPop();
        return true;
      },
      child: MyScaffold(
        bottomMenuIndex: widget.isLoggedIn ? 0 : null,
        body: BlocListener<HomeBloc, HomeState>(
          listenWhen: (previous, current) => previous.status.status != current.status.status,
          listener: (context, snapshot) {},
          child: Column(
            children: [
              SizedBox(
                height: config.App(context).appHeight(3),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {
                      setState(() {
                        isMap = false;
                        FocusScope.of(context).requestFocus(new FocusNode());
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: config.App(context).appWidth(20),
                      height: config.App(context).appWidth(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: isMap == false ? Theme.of(context).primaryColor : Color(0xFF7F8389),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          isMap == false ? SizedBox(width: 7) : SizedBox(),
                          Text(
                            "LIST ",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          isMap == false
                              ? Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.white,
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        isMap = true;
                      });
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: config.App(context).appWidth(20),
                      height: config.App(context).appWidth(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: isMap == true ? Theme.of(context).primaryColor : Color(0xFF7F8389),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          isMap == true ? SizedBox(width: 8) : SizedBox(),
                          Text(
                            "MAP",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          isMap == true
                              ? Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.white,
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: config.App(context).appHeight(3),
              ),
              SearchBuilder(),
              SizedBox(
                height: config.App(context).appHeight(3),
              ),
              isMap == true ? GoogleMapBuilder() : ListBuilder()
            ],
          ),
        ),
      ),
    );
  }
}

class SearchBuilder extends StatefulWidget {
  @override
  _SearchBuilderState createState() => _SearchBuilderState();
}

class _SearchBuilderState extends State<SearchBuilder> {
  TextEditingController _controller = TextEditingController();

  bool _isVideoTraining = false;
  bool _isTravelClient = false;

  Future<bool> _saveSearchText(String searchText) async {
    return Storage().setSearchText(searchText);
  }

  Future<String> _getSearchText() async {
    return await Storage().getSearchText();
  }

  Future<bool> _saveVideoTraining(bool isVideoTraining) async {
    return Storage().setVideoTraining(isVideoTraining);
  }

  Future<bool> _getVideoTraining() async {
    return await Storage().getVideoTraining();
  }

  Future<bool> _saveTravelClient(bool isTravelClient) async {
    return Storage().setTravelClient(isTravelClient);
  }

  Future<bool> _getTravelClient() async {
    return await Storage().getTravelClient();
  }

  filterData() async {
    String searchText = await _getSearchText();
    bool getVideoTraining = await _getVideoTraining();
    bool getTravelClient = await _getTravelClient();
    String isVideoTraining = "False";
    if (getVideoTraining) {
      isVideoTraining = "True";
    }
    String isTravelClient = "False";
    if (getTravelClient) {
      isTravelClient = "True";
    }
    context.read<HomeBloc>().add(HomeSearchEvent(query: "search=$searchText&travel_to_client=$isTravelClient&video_training=$isVideoTraining"));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: config.App(context).appWidth(10)),
          child: Stack(
            alignment: Alignment.centerRight,
            children: [
              TextFormField(
                enabled: state.status.status != StateStatuses.loading,
                controller: _controller,
                keyboardType: TextInputType.text,
                onSaved: (input) {
                  print("$input");
                },
                onChanged: (text) async {
                  print('search ->-> $text');
                  await _saveSearchText(_controller.text);
                  if (text.length == 0) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    filterData();
                    /*context.read<HomeBloc>().add(
                        HomeSearchEvent(query: "search=${_controller.text}"));*/
                  }
                },
                textInputAction: TextInputAction.search,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  filterData();
                  /*context.read<HomeBloc>().add(
                      HomeSearchEvent(query: "search=${_controller.text}"));*/
                },
                cursorColor: Colors.white,
                style: Theme.of(context).textTheme.headline4,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  // suffixIcon:
                  hintText: "",
                  hintStyle: Theme.of(context).textTheme.headline4,
                  filled: true,
                  fillColor: Color(0xFFF0F0F0).withOpacity(0.51),
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: 25,
                    vertical: config.App(context).appHeight(1),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none,
                  ),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.transparent,
                    minimumSize: Size.zero, // <-- Add this
                    padding: EdgeInsets.zero, // <-- and this
                    elevation: 0),
                onPressed: () {},
                child: PopupMenuButton(
                  offset: Offset(0.0, 50.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  color: Color(0xFF7F8389),
                  child: Icon(
                    Icons.tune_rounded,
                    color: Colors.white,
                  ),
                  itemBuilder: (BuildContext context) => [
                    PopupMenuItem(
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            _isVideoTraining = !_isVideoTraining;
                          });
                          _saveVideoTraining(_isVideoTraining);
                          Navigator.pop(context);
                          filterData();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Icon(_isVideoTraining ? Icons.check_box_rounded : Icons.check_box_outline_blank_rounded, color: Colors.white, size: 20),
                            SizedBox(width: 5),
                            Expanded(
                              child: Text(
                                "Video Training",
                                style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.w400),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    PopupMenuItem(
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            _isTravelClient = !_isTravelClient;
                          });
                          _saveTravelClient(_isTravelClient);
                          Navigator.pop(context);
                          filterData();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Icon(
                              _isTravelClient ? Icons.check_box_rounded : Icons.check_box_outline_blank_rounded,
                              color: Colors.white,
                              size: 20,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Text(
                                "Travels to Client",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                  onSelected: (type) {
                    print(type);
                  },
                ),
              )
              /* FlatButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                padding: EdgeInsets.all(0),
                onPressed: () {},
                child:,
              ),*/
            ],
          ),
        );
      },
    );
  }
}
