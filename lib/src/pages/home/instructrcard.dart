import 'dart:math' as math;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/homeBloc/home_bloc.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:fytme/src/pages/createditprofile/proposal.dart';

import '../../../common/constants.dart';
import '../../helpers/app_config.dart' as config;
import '../instructor_profile.dart';

typedef OnRouteCallBack = void Function(List);

class InstructorCard extends StatefulWidget {
  final OnRouteCallBack onTap;

  InstructorCard({Key key, this.onTap}) : super(key: key);

  @override
  _InstructorCardState createState() => _InstructorCardState();
}

class _InstructorCardState extends State<InstructorCard> {
  String previousLat = '';
  String previousLng = '';

  @override
  void initState() {
    super.initState();
    checkPreviousLatLng();
  }

  Future<void> checkPreviousLatLng() async {
    previousLat = await _getRouteLat();
    print('previousLat :: $previousLat');
    previousLng = await _getRouteLng();
    print('previousLng :: $previousLng');
  }

  Future<String> _getRouteLat() async {
    return await Storage().getRouteDrawLat();
  }

  Future<String> _getRouteLng() async {
    return await Storage().getRouteDrawLng();
  }

  Future<bool> _saveRouteDrawLat(String lat) async {
    return Storage().setRouteDrawLat(lat);
  }

  Future<bool> _saveRouteDrawLng(String lng) async {
    return Storage().setRouteDrawLng(lng);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
      if (state.selected != null)
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                      imageUrl: state.selected.profilePicture ?? '',
                      fit: BoxFit.cover,
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(
                        Icons.person,
                        color: Colors.black,
                      ),
                      width: config.App(context).appWidth(25),
                      height: config.App(context).appWidth(25),
                    ),
                  ),
                  SizedBox(
                    width: config.App(context).appWidth(5),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${state.selected.firstName} ${state.selected.lastName}',
                          style: Theme.of(context).textTheme.subtitle1.copyWith(
                                color: Colors.black,
                              ),
                        ),
                        Text(
                          state.selected.specialty.isNotEmpty ? "${state.selected.specialty.join(', ') ?? ''}" : '',
                          style: Theme.of(context).textTheme.headline4.copyWith(
                                color: Colors.black,
                                fontSize: 12,
                              ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.location_on,
                              color: Colors.black,
                              size: 15,
                            ),
                            Expanded(
                              child: Text(
                                "${state.selected.address}",
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                                style: Theme.of(context).textTheme.headline4.copyWith(
                                      color: Colors.black,
                                    ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            rateStar(state.selected.ratings),
                            SizedBox(
                              width: 5,
                            ),
                            InkWell(
                              onTap: () {
                                if (Storage().currentUser != null) {
                                  Navigator.of(context).pushNamed('/Reviews', arguments: state.selected);
                                } else {
                                  Navigator.of(context).pushNamed('/Home');
                                  CustomWidgets.buildErrorSnackBar(context, 'Please signup/login');
                                }
                              },
                              child: Row(
                                children: [
                                  Text(
                                    "(",
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                          color: Colors.black,
                                        ),
                                  ),
                                  Text(
                                    "${state.selected.ratings} Reviews",
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                          color: Colors.black,
                                          decoration: TextDecoration.underline,
                                        ),
                                  ),
                                  Text(
                                    ")",
                                    style: Theme.of(context).textTheme.headline4.copyWith(
                                          color: Colors.black,
                                        ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: config.App(context).appHeight(1),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextButton(
                    onPressed: () async {
                      if (state.selected.locationCoordinates.coordinates[0].toString() != previousLat &&
                          state.selected.locationCoordinates.coordinates[1].toString() != previousLng) {
                        widget.onTap(state.selected.locationCoordinates.coordinates);
                        await _saveRouteDrawLat(state.selected.locationCoordinates.coordinates[0].toString());
                        await _saveRouteDrawLng(state.selected.locationCoordinates.coordinates[1].toString());
                        checkPreviousLatLng();
                      } else {
                        print('route already draw before');
                      }
                    },
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        minimumSize: Size(
                          config.App(context).appHeight(5),
                          config.App(context).appHeight(5),
                        ),
                        alignment: Alignment.center),
                    child: Container(
                        padding: EdgeInsets.symmetric(
                          vertical: config.App(context).appHeight(1),
                          horizontal: config.App(context).appHeight(1),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Transform.rotate(
                          angle: 45 * math.pi / 180,
                          child: Icon(
                            Icons.navigation,
                            color: Theme.of(context).primaryColor,
                            size: 24,
                          ),
                        )),
                  ),
                  SizedBox(
                    width: config.App(context).appWidth(1),
                  ),
                  Expanded(
                      child: TextButton(
                    onPressed: () {
                      if (Storage().currentUser != null) {
                        Navigator.of(context)
                            .pushNamed('/Proposal', arguments: ProposalArguments(state.selected.id, DateTime.now(), DateTime.now().hour));
                      } else {
                        Navigator.of(context).pushNamed('/Home');
                        CustomWidgets.buildErrorSnackBar(context, 'Please signup/login');
                      }
                    },
                    style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                        minimumSize: Size(
                          config.App(context).appHeight(50),
                          config.App(context).appHeight(5),
                        ),
                        alignment: Alignment.center),
                    child: Container(
                      alignment: Alignment.center,
                      height: config.App(context).appHeight(5),
                      padding: EdgeInsets.symmetric(
                        vertical: config.App(context).appHeight(1),
                        horizontal: config.App(context).appHeight(1),
                      ),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        "BOOK A SESSION",
                        style: Theme.of(context).textTheme.subtitle1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ))
                ],
              ),
            ],
          ),
        );
      return Container();
    });
  }
}
