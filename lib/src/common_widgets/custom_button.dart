import 'package:flutter/material.dart';

import '../helpers/app_config.dart' as config;

class CustomButton extends StatelessWidget {
  CustomButton({@required this.callback, @required this.string});

  final VoidCallback callback;
  final String string;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: callback,
      child: Container(
        width: config.App(context).appWidth(70),
        padding: EdgeInsets.symmetric(
          vertical: config.App(context).appHeight(3),
          horizontal: 20,
        ),
        decoration: BoxDecoration(
          color: callback == null ? Colors.grey : Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Text(
          string,
          style: Theme.of(context).textTheme.subtitle1,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
