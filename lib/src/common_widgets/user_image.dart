import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/src/helpers/app_config.dart' as config;

class UserImage extends StatelessWidget {
  final String image;
  final double radius;

  UserImage({this.image, this.radius});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(300)),
        child: CachedNetworkImage(
          imageUrl: image ?? Storage().userProfile.profilePicture ?? "",
          fit: BoxFit.cover,
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(
            Icons.person,
            color: Colors.white,
          ),
          width: config.App(context).appWidth(radius ?? 30),
          height: config.App(context).appWidth(radius ?? 30),
        ),
      ),
    );
  }
}
