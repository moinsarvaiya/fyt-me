import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CardMonthInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    var newText = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = new StringBuffer();
    for (int i = 0; i < newText.length; i++) {
      buffer.write(newText[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 2 == 0 && nonZeroIndex != newText.length) {
        buffer.write('/');
      }
    }

    var string = buffer.toString();
    return newValue.copyWith(text: string, selection: new TextSelection.collapsed(offset: string.length));
  }
}

class CardUtils {
  static String validateDate(String value) {
    if (value == null || value.isEmpty) {
      // return Strings.fieldReq;
    }

    int year;
    int month;
    // The value contains a forward slash if the month and year has been
    // entered.
    if (value.contains(new RegExp(r'(/)'))) {
      var split = value.split(new RegExp(r'(/)'));
      // The value before the slash is the month while the value to right of
      // it is the year.
      month = int.parse(split[0]);
      year = int.parse(split[1]);
    } else {
      // Only the month was entered
      month = int.parse(value.substring(0, (value.length)));
      year = -1; // Lets use an invalid year intentionally
    }

    if ((month < 1) || (month > 12)) {
      // A valid month is between 1 (January) and 12 (December)
      return 'Expiry month is invalid';
    }

    var fourDigitsYear = convertYearTo4Digits(year);
    if ((fourDigitsYear < 1) || (fourDigitsYear > 2099)) {
      // We are assuming a valid should be between 1 and 2099.
      // Note that, it's valid doesn't mean that it has not expired.
      return 'Expiry year is invalid';
    }

    if (!hasDateExpired(month, year)) {
      return "Card has expired";
    }
    return null;
  }

  /// Convert the two-digit year to four-digit year if necessary
  static int convertYearTo4Digits(int year) {
    if (year < 100 && year >= 0) {
      var now = DateTime.now();
      String currentYear = now.year.toString();
      String prefix = currentYear.substring(0, currentYear.length - 2);
      year = int.parse('$prefix${year.toString().padLeft(2, '0')}');
    }
    return year;
  }

  static bool hasDateExpired(int month, int year) {
    return isNotExpired(year, month);
  }

  static bool isNotExpired(int year, int month) {
    // It has not expired if both the year and date has not passed
    return !hasYearPassed(year) && !hasMonthPassed(year, month);
  }

  static List<int> getExpiryDate(String value) {
    var split = value.split(new RegExp(r'(/)'));
    return [int.parse(split[0]), int.parse(split[1])];
  }

  static bool hasMonthPassed(int year, int month) {
    var now = DateTime.now();
    // The month has passed if:
    // 1. The year is in the past. In that case, we just assume that the month
    // has passed
    // 2. Card's month (plus another month) is more than current month.
    return hasYearPassed(year) || convertYearTo4Digits(year) == now.year && (month < now.month + 1);
  }

  static bool hasYearPassed(int year) {
    int fourDigitsYear = convertYearTo4Digits(year);
    var now = DateTime.now();
    // The year has passed if the year we are currently is more than card's
    // year
    return fourDigitsYear < now.year;
  }
}

// class ExpirationFormField extends StatefulWidget {
//   //TODO make controller optional
//   ExpirationFormField({
//     this.key,
//     @required this.controller,
//     this.decoration,
//     this.obscureText = false,
//     this.enabled = true,
//   }) : super(key: key);
//
//   final Key key;
//   final TextEditingController controller;
//   final InputDecoration decoration;
//   final bool obscureText;
//   final bool enabled;
//
//   @override
//   _ExpirationFormFieldState createState() => _ExpirationFormFieldState();
// }
//
// class _ExpirationFormFieldState extends State<ExpirationFormField> {
//   @override
//   Widget build(BuildContext context) {
//     return TextField(
//       keyboardType:
//       TextInputType.numberWithOptions(signed: false, decimal: false),
//       controller: widget.controller,
//       decoration: widget.decoration,
//       onChanged: (value) {
//         setState(() {
//           value = value.replaceAll(RegExp(r"\D"), "");
//           switch (value.length) {
//             case 0:
//               widget.controller.text = "MM/YY";
//               widget.controller.selection = TextSelection.collapsed(offset: 0);
//               break;
//             case 1:
//               widget.controller.text = "${value}M/YY";
//               widget.controller.selection = TextSelection.collapsed(offset: 1);
//               break;
//             case 2:
//               widget.controller.text = "$value/YY";
//               widget.controller.selection = TextSelection.collapsed(offset: 2);
//               break;
//             case 3:
//               widget.controller.text =
//               "${value.substring(0, 2)}/${value.substring(2)}Y";
//               widget.controller.selection = TextSelection.collapsed(offset: 4);
//               break;
//             case 4:
//               widget.controller.text =
//               "${value.substring(0, 2)}/${value.substring(2, 4)}";
//               widget.controller.selection = TextSelection.collapsed(offset: 5);
//               break;
//           }
//           if (value.length > 4) {
//             widget.controller.text =
//             "${value.substring(0, 2)}/${value.substring(2, 4)}";
//             widget.controller.selection = TextSelection.collapsed(offset: 5);
//           }
//         });
//       },
//       cursorWidth: 0.0,
//       obscureText: widget.obscureText,
//       enabled: widget.enabled,
//     );
//   }
// }
