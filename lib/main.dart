import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fytme/data/repositories/abstracts/calorie_repository.dart';
import 'package:fytme/data/repositories/abstracts/home_repository.dart';
import 'package:fytme/data/repositories/abstracts/proposal_repository.dart';
import 'package:fytme/data/repositories/abstracts/review_repository.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bloc/authentication/authentication_bloc.dart';
import 'bloc/authentication/authentication_event.dart';
import 'bloc/providers_bloc/chat_bloc.dart';
import 'bloc/providers_bloc/notification_bloc.dart';
import 'data/repositories/abstracts/change_password_repository.dart';
import 'data/repositories/abstracts/profile_respository.dart';
import 'data/repositories/abstracts/user_repository.dart';
import 'data/services/repositories/remote_calorie_repo.dart';
import 'data/services/repositories/remote_change_password_repo.dart';
import 'data/services/repositories/remote_home_repo.dart';
import 'data/services/repositories/remote_profile_repo.dart';
import 'data/services/repositories/remote_proposal_repo.dart';
import 'data/services/repositories/remote_review_repo.dart';
import 'data/services/repositories/remote_user_repo.dart';
import 'route_generator.dart';
import 'src/helpers/app_config.dart' as config;

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

String fcmNewToken = '';
int averageRatings = 0;
int noOfReviews = 0;

class SimpleBlocDelegate extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_messageHandler);
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  runApp(MyApp());
}

Future<void> _messageHandler(RemoteMessage message) async {
  print('background message ${message.notification.body}');
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

Future<void> onSelectNotification(String payload) {
  print("FCM onSelectNotification");
  return Future<void>.value();
}

class _MyAppState extends State<MyApp> {
  FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  Future onSelectNotification(String payload) {
    return Navigator.of(navigatorKey.currentState.context).pushNamed('/About');
  }

  @override
  void initState() {
    super.initState();

    /*var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOs = IOSInitializationSettings();
    var initSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOs);
    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: onSelectNotification);*/

    firebaseMessaging.requestPermission(alert: true, badge: true, sound: true);

    /*Future.delayed(
        Duration(seconds: 15),
        () => showNotification(
              'message.notification.title',
              'message.notification.body',
            ));*/

    firebaseMessaging.getToken().then((token) async {
      print('fcm token : $token');
      fcmNewToken = token;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('fcmToken', token);
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      print("message received");
      print(message.notification.body);
      /*showNotification(
        message.notification.title,
        message.notification.body,
      );*/
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      print("onMessageOpenedApp: ${message.data.toString()}");
      Navigator.of(navigatorKey.currentState.context).pushNamed('/Notification');
    });
  }

  showNotification(String title, String body) async {
    var android = AndroidNotificationDetails('id', 'channel ', 'description', priority: Priority.high, importance: Importance.max);
    var iOS = IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(0, title, body, platform, payload: 'Welcome to the Local Notification demo');
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
        create: (context) => AuthenticationBloc()..add(AppStarted()),
        child: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<UserRepository>(create: (context) => RemoteUserRepository()),
            RepositoryProvider<ProfileRepository>(create: (context) => RemoteProfileRepository()),
            RepositoryProvider<ProposalRepository>(create: (context) => RemoteProposalRepository()),
            RepositoryProvider<CalorieRepository>(create: (context) => RemoteCalorieRepository()),
            RepositoryProvider<HomeRepository>(create: (context) => RemoteHomeRepository()),
            RepositoryProvider<ChangePasswordRepository>(create: (context) => RemoteChangePasswordRepository()),
            RepositoryProvider<ReviewRepository>(create: (context) => RemoteReviewRepository()),
          ],
          child: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => InAppNotificationBloc()),
              ChangeNotifierProvider(create: (_) => InAppChatBloc()),
            ],
            child: MaterialApp(
              navigatorKey: navigatorKey,
              initialRoute: '/Home',
              onGenerateRoute: RouteGenerator.generateRoute,
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primaryColor: config.Colors().mainColor(1),
                cardColor: config.Colors().secondColor(1),
                colorScheme: ColorScheme.fromSwatch().copyWith(secondary: config.Colors().accentColor(1)),
                primaryColorDark: config.Colors().mainDarkColor(1),
                canvasColor: config.Colors().mainDarkColor(1),
                unselectedWidgetColor: Colors.white,
                textTheme: TextTheme(
                  headline1: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
                  subtitle1: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
                  headline4: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal),
                ),
              ),
            ),
          ),
        ));
  }
}
