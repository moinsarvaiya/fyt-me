import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/repositories/abstracts/change_password_repository.dart';

part 'security_event.dart';
part 'security_state.dart';

class SecurityBloc extends Bloc<SecurityEvent, SecurityState> {
  final ChangePasswordRepository passwordRepository;

  SecurityBloc({@required this.passwordRepository})
      : super(SecurityState(
          status: StateStatus(
            status: StateStatuses.success,
          ),
        ));

  @override
  Stream<SecurityState> mapEventToState(SecurityEvent event) async* {
    if (event is ChangePasswordEvent) {
      yield* _mapToChangePassword(event);
    }
  }

  Stream<SecurityState> _mapToChangePassword(ChangePasswordEvent event) async* {
    try {
      yield state.copyWith(status: StateStatus(status: StateStatuses.loading, message: ''));
      var response;
      response =
          await passwordRepository.changePassword(newPassword: event.newPassword, oldPassword: event.oldPassword, token: Storage().currentUser.token);
      yield state.copyWith(status: StateStatus(status: StateStatuses.success, message: jsonDecode(response)['msg']));
    } catch (exception) {
      yield state.copyWith(
        status: state.status.copyWith(
          status: StateStatuses.failure,
          message: getMessage(exception.message),
        ),
      );
    }
  }

  String getMessage(message) {
    message = jsonDecode(message);
    printLog(message);
    if (message['new_password'] != null) {
      return message['new_password'].join(', ');
    } else if (message['new_password'] != null) {
      return message['new_password'].join(', ');
    } else if (message['old_password'] != null) {
      return message['old_password'].join(', ');
    } else {
      return 'Something went wrong, please try again';
    }
  }
}
