part of 'security_bloc.dart';

class SecurityState extends Equatable {
  final StateStatus status;

  const SecurityState({
    this.status = const StateStatus(),
  });

  SecurityState copyWith({
    StateStatus status,
  }) {
    return SecurityState(status: status ?? this.status);
  }

  @override
  List<Object> get props => [status];

  @override
  bool get stringify => true;
}
