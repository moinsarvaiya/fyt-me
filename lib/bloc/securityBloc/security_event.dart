part of 'security_bloc.dart';

class SecurityEvent extends Equatable {
  const SecurityEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class ChangePasswordEvent extends SecurityEvent {
  final String oldPassword;
  final String newPassword;

  ChangePasswordEvent({this.newPassword, this.oldPassword});

  @override
  List<Object> get props => [oldPassword, newPassword];

  @override
  bool get stringify => true;
}
