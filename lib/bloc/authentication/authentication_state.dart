import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

class Uninitialized extends AuthenticationState {}

class Authenticated extends AuthenticationState {}

class Unauthenticated extends AuthenticationState {}

class AccountTypeMissing extends AuthenticationState {}

class ProfileMissing extends AuthenticationState {}

class InstructorVerificationMissing extends AuthenticationState {}

class ForgetPasswordState extends AuthenticationState {}

class EmailVerificationScreen extends AuthenticationState {}

class ForgetFormState extends AuthenticationState {}

class ShowQuestionAnswer extends AuthenticationState {}
