import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {
  final String response;

  LoggedIn(this.response);

  @override
  List<Object> get props => [response];
}

class AccountType extends AuthenticationEvent {
  final String response;

  AccountType(this.response);

  @override
  List<Object> get props => [response];
}

class LoggedOut extends AuthenticationEvent {}

class DeleteAccount extends AuthenticationEvent {}

class ForgetFormEventAuth extends AuthenticationEvent {}

class SetNewPasswordEvent extends AuthenticationEvent {
  final String email;
  final int id;

  SetNewPasswordEvent({this.email, this.id});

  @override
  List<Object> get props => [email, id];
}

class EmailNotVerified extends AuthenticationEvent {
  final String response;

  EmailNotVerified(this.response);

  @override
  List<Object> get props => [response];
}

class SkipVerificationEventAuth extends AuthenticationEvent {}
