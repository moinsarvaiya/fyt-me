import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:fytme/data/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants.dart';
import '../../data/serveraddress.dart';
import 'authentication_event.dart';
import 'authentication_state.dart';

enum EmailPasswordSignInFormType { signIn, register, forgetPassword }

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(Uninitialized());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    // app start
    if (event is AppStarted) {
      bool forInstructorVerification = await _isInsVerified();
      //bool isCompletedQa = await _isQaCompleted();
      var currentUser = await _getToken();
      if (currentUser != '') {
        printLog('======= AppStarted $currentUser');
        print(forInstructorVerification);
        final reBody = jsonDecode(currentUser);
        String authToken = '';
        if (reBody['auth_token'] != null) {
          if (reBody['auth_token']['access_token'] != null) {
            authToken = reBody['auth_token']['access_token'];
          } else {
            authToken = reBody['auth_token'];
          }
        }
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('authToken', authToken);
        Storage().currentUser = User.fromProfile(jsonDecode(currentUser));
        if (!Storage().currentUser.isEmailVerified) {
          yield EmailVerificationScreen();
        } else if (Storage().currentUser.accountType == null) {
          yield AccountTypeMissing();
        } else if (Storage().currentUser.isProfileCompleted == false) {
          yield ProfileMissing();
        } else if (Storage().currentUser.accountType == 'instructor' &&
            forInstructorVerification == null &&
            Storage().currentUser.instructorStatus == null) {
          yield InstructorVerificationMissing();
        } else if ((Storage().currentUser.accountType == 'customer') && Storage().currentUser.isQuestionsAnswered == false) {
          yield ShowQuestionAnswer();
        } else {
          try {
            var res = await getProfile(userId: Storage().currentUser.userId, token: Storage().currentUser.token);
            Storage().userProfile = User();
            Storage().userProfile = User.fromProfile(jsonDecode(res));
            yield Authenticated();
          } catch (e) {
            print('error $e');
            await _deleteToken();
            yield Unauthenticated();
          }
        }
      } else {
        yield Unauthenticated();
      }
    }

    if (event is LoggedIn) {
      bool forInstructorVerification = await _isInsVerified();
      //bool isCompletedQa = await _isQaCompleted();
      Storage().currentUser = User.fromProfile(jsonDecode(event.response));
      await _saveToken(event.response);
      if (!Storage().currentUser.isEmailVerified) {
        yield EmailVerificationScreen();
      } else if (Storage().currentUser.accountType == null) {
        yield AccountTypeMissing();
      } else if (Storage().currentUser.isProfileCompleted == false) {
        yield ProfileMissing();
      } else if (Storage().currentUser.accountType == 'instructor' &&
          forInstructorVerification == null &&
          Storage().currentUser.instructorStatus == null) {
        yield InstructorVerificationMissing();
      } else if ((Storage().currentUser.accountType == 'customer') && Storage().currentUser.isQuestionsAnswered == false) {
        yield ShowQuestionAnswer();
      }
      // else if(
      //   Storage().currentUser.accountType == 'customer' &&
      //   isCompletedQa == null
      // ){
      //     yield ShowQuestionAnswer();
      // }
      else {
        try {
          var res = await getProfile(userId: Storage().currentUser.userId, token: Storage().currentUser.token);
          Storage().userProfile = User();
          Storage().userProfile = User.fromProfile(jsonDecode(res));
          yield Authenticated();
        } catch (e) {
          print('error $e');
          await _deleteToken();
          yield Unauthenticated();
        }
      }
    }

    if (event is SkipVerificationEventAuth) {
      var res = await getProfile(userId: Storage().currentUser.userId, token: Storage().currentUser.token);
      Storage().userProfile = User();
      Storage().userProfile = User.fromProfile(jsonDecode(res));
      yield Authenticated();
    }

    if (event is LoggedOut) {
      await _deleteToken();
      yield Unauthenticated();
    }
    if (event is DeleteAccount) {
      var res = await deleteAccount(token: Storage().currentUser.token);
      if(res != null){
        await _deleteToken();
        yield Unauthenticated();
      }
    }
    if (event is AccountType) {
      Storage().currentUser.accountType = event.response;
      yield Authenticated();
    }
    if (event is EmailNotVerified) {
      await _saveToken(event.response);
      Storage().currentUser = User.fromProfile(jsonDecode(event.response));
      yield EmailVerificationScreen();
    }
    if (event is SetNewPasswordEvent) {
      printLog(event.email);
      printLog(event.id);
      Storage().currentUser = User();
      Storage().currentUser.email = event.email;
      Storage().currentUser.userId = event.id;
      printLog(Storage().currentUser.email);
      printLog(Storage().currentUser.userId);
      printLog('SetNewPasswordEvent');
      yield ForgetPasswordState();
    }
    if (event is ForgetFormEventAuth) {
      yield ForgetFormState();
    }
  }

  /// delete from keystore/keychain
  Future<void> _deleteToken() async {
    Storage().currentUser = null;
    Storage().userProfile = null;
    await Storage().clearToken('currentUser');
  }

  /// write to keystore/keychain
  Future<bool> _saveToken(String token) async {
    // var decode = jsonDecode(token);
    // String profile = await getProfile(token: decode['auth_token'], userId: decode['user_id']);
    return Storage().setToken(token);
  }

  /// read to keystore/keychain
  Future<String> _getToken() async {
    return await Storage().getToken();
  }

  Future<bool> _isInsVerified() async {
    return await Storage().isInstructorVerified();
  }

  void setUser() {
    // Storage().currentUser.token =
  }

  Future<String> getProfile({String token, int userId}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}user-profile/$userId/';
      final client = new http.Client();
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('auth getProfile =========');
      print(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception();
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }

  Future<String> deleteAccount({String token}) async {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      final String url = '${ServerAddress.api_base_url}account-deactivate/';
      final client = new http.Client();
      final response = await client.put(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('auth getProfile =========');
      print(response.body);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception();
      }
    } catch (err) {
      printLog(err);
      rethrow;
    }
  }
}
