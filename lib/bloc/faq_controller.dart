import 'package:flutter/material.dart';
import 'package:fytme/data/services/custom_snackbar.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../data/model/faq.dart';
import '../data/services/repositories/remote_faq_repo.dart' as repository;
import '../src/helpers/helper.dart';

class FaqController extends ControllerMVC {
  Faq faq = new Faq();
  bool loading = false;
  GlobalKey<FormState> formKey;
  GlobalKey<ScaffoldState> scaffoldKey;
  OverlayEntry loader;
  BuildContext context;

  FaqController(BuildContext mContext) {
    context = mContext;
    loader = Helper.overlayLoader(context);
    formKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void sendContactUs() async {
    FocusScope.of(context).unfocus();
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      Overlay.of(context).insert(loader);
      repository.sendContactUs(faq).then((value) {
        if (value == true) {
          Navigator.of(scaffoldKey.currentContext).pop();
          CustomWidgets.buildSuccessSnackBar(context, "Your request has been successfully submitted");
        } else {
          CustomWidgets.buildErrorSnackBar(context, "Failed!");
        }
      }).catchError((e) {
        loader.remove();
        CustomWidgets.buildErrorSnackBar(context, "Failed!");
        /*scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text("Failed!"),
        ));*/
      }).whenComplete(() {
        Helper.hideLoader(loader);
      });
    }
  }
}
