part of 'auth_bloc.dart';

class AuthState extends Equatable {
  final Email email;
  final UserName userName;
  final Password password;
  final String conFirmPassword;
  final bool isUserNameValidate;
  final bool isEmailValid;
  final bool isPasswordValid;
  final EmailPasswordSignInFormType formType;
  final StateStatus status;
  final dynamic socialJson;

  const AuthState({
    this.conFirmPassword = '',
    this.isUserNameValidate = false,
    this.isEmailValid = false,
    this.isPasswordValid = false,
    this.password = const Password.pure(),
    this.email = const Email.pure(),
    this.userName = const UserName.pure(),
    this.status = const StateStatus(),
    this.formType = EmailPasswordSignInFormType.signIn,
    this.socialJson = '',
  });

  AuthState copyWith(
      {Email email,
      UserName userName,
      Password password,
      StateStatus status,
      EmailPasswordSignInFormType formType,
      bool isUserNameValidate,
      bool isEmailValid,
      bool isPasswordValid,
      String conFirmPassword,
      dynamic socialJson}) {
    return AuthState(
      isUserNameValidate: isUserNameValidate ?? this.isUserNameValidate,
      userName: userName ?? this.userName,
      conFirmPassword: conFirmPassword ?? this.conFirmPassword,
      email: email ?? this.email,
      password: password ?? this.password,
      status: status ?? this.status,
      formType: formType ?? this.formType,
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      socialJson: socialJson ?? this.socialJson,
    );
  }

  @override
  List<Object> get props =>
      [conFirmPassword, isUserNameValidate, isEmailValid, isPasswordValid, userName, email, password, status, formType, socialJson];

  @override
  bool get stringify => true;
}
