import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:formz/formz.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_event.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/formz/email.dart';
import 'package:fytme/data/model/formz/username.dart';
import 'package:fytme/data/model/user.dart';
import 'package:fytme/data/repositories/abstracts/user_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants.dart';
import '../../data/model/formz/password.dart';
import '../../main.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthenticationBloc authenticationBloc;
  final UserRepository userRepository;

  AuthBloc({@required this.userRepository, @required this.authenticationBloc})
      : super(AuthState(
          status: StateStatus(
            status: StateStatuses.success,
          ),
        ));

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is LoginForm) {
      yield* _mapLoginFormChanged();
    }
    if (event is RegisterForm) {
      yield* _mapRegisterFormChanged();
    }

    if (event is ForgetPasswordForm) {
      yield* _mapPasswordChanged();
    }

    if (event is AuthEmailChanged) {
      yield* _mapEmailChangedToState(event);
    }

    if (event is AuthUserNameChanged) {
      yield* _mapUserNameChangedToState(event);
    }

    if (event is AuthPasswordChanged) {
      yield* _mapPasswordChangedToState(event);
    }
    if (event is AuthPasswordConfirmChanged) {
      yield* _mapConfirmPasswordChangedToState(event);
    }
    if (event is AuthSubmitted) {
      yield* _mapAuthSubmittedToState(event);
    }
    if (event is ConfirmedEmail) {
      yield* _mapConfirmedEmailWithOtp(event);
    }
    if (event is SocialSignUp) {
      yield* _mapSocialSignUp(event);
    }

    if (event is AuthAccountType) {
      yield* _mapToAccountType(event);
    }

    if (event is ResendOtpEvent) {
      yield* _mapToResendOTP();
    }

    if (event is ResetPasswordEvent) {
      yield* _mapToChangePassword(event);
    }
  }

  Stream<AuthState> _mapLoginFormChanged() async* {
    yield state.copyWith(formType: EmailPasswordSignInFormType.signIn);
  }

  Stream<AuthState> _mapRegisterFormChanged() async* {
    yield state.copyWith(formType: EmailPasswordSignInFormType.register);
  }

  Stream<AuthState> _mapPasswordChanged() async* {
    yield state.copyWith(formType: EmailPasswordSignInFormType.forgetPassword);
  }

  Stream<AuthState> _mapUserNameChangedToState(AuthUserNameChanged event) async* {
    final userName = UserName.dirty(
      externalValidationError: null,
      value: event.userName,
    );
    final formStatus = Formz.validate([userName, state.userName]);
    yield state.copyWith(userName: userName, isUserNameValidate: formStatus);
  }

  Stream<AuthState> _mapEmailChangedToState(AuthEmailChanged event) async* {
    final email = Email.dirty(
      externalValidationError: null,
      value: event.email,
    );
    final formStatus = Formz.validate([email, state.email]);
    yield state.copyWith(email: email, isEmailValid: formStatus);
  }

  Stream<AuthState> _mapPasswordChangedToState(AuthPasswordChanged event) async* {
    final password = Password.dirty(
      passwordValidationError: null,
      value: event.password,
    );
    final formStatus = Formz.validate([password, state.password]);
    yield state.copyWith(password: password, isPasswordValid: formStatus);
  }

  Stream<AuthState> _mapConfirmPasswordChangedToState(AuthPasswordConfirmChanged event) async* {
    yield state.copyWith(
      conFirmPassword: event.passwordConfirm,
    );
  }

  Stream<AuthState> _mapAuthSubmittedToState(AuthSubmitted event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      String response;
      var decode;
      if (state.formType == EmailPasswordSignInFormType.signIn) {
        User user = User();
        user.email = state.email.value;
        user.password = state.password.value;
        //SharedPreferences prefs = await SharedPreferences.getInstance();
        //user.fcmToken = prefs.getString('fcmToken');
        user.fcmToken = fcmNewToken;
        response = await userRepository.login(user: user);

        print("Test login first");
        decode = jsonDecode(response);
        print("Test login");
        if (decode['non_field_errors'] != null) {
          yield state.copyWith(
            status: state.status.copyWith(status: StateStatuses.failure, message: decode['non_field_errors'][0].toString()),
          );
        } else {
          printLog('-------- $decode');
          yield state.copyWith(
            status: state.status.copyWith(status: StateStatuses.failure, message: ''),
          );
          authenticationBloc.add(LoggedIn(response));
        }
      } else if (state.formType == EmailPasswordSignInFormType.register) {
        User user = User();
        user.email = state.email.value;
        user.password = state.password.value;
        user.username = state.userName.value;
        user.confirmPassword = state.conFirmPassword;
        SharedPreferences prefs = await SharedPreferences.getInstance();
        user.fcmToken = prefs.getString('fcmToken');
        //user.fcmToken = fcmToken;
        response = await userRepository.register(user: user);
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
          ),
        );
        printLog(response);
        authenticationBloc.add(LoggedIn(response));
      } else {
        int id = await userRepository.forgotPassword(state.email.value);
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.success,
          ),
        );
        print(state.email.value);
        authenticationBloc.add(SetNewPasswordEvent(email: state.email.value, id: id));
      }
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        printLog(exception.message.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      }
    }
  }

  Stream<AuthState> _mapConfirmedEmailWithOtp(ConfirmedEmail event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      var response = await userRepository.confirmRegistration(event.email, event.pinCode);
      yield state.copyWith(
        status: state.status.copyWith(
          status: StateStatuses.success,
        ),
      );
      authenticationBloc.add(LoggedIn(response));
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        printLog(exception.message.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      }
    }
  }

  Stream<AuthState> _mapSocialSignUp(SocialSignUp event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      String response;
      response = await userRepository.socialSignUp(provider: event.provider, accessToken: event.accessToken);
      yield state.copyWith(
        //socialJson: response,
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
      authenticationBloc.add(LoggedIn(response));
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        printLog(exception.message.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      }
    }
  }

  Stream<AuthState> _mapToResendOTP() async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      await userRepository.resendOtp(userId: Storage().currentUser.userId);
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.success, message: 'Otp sent again'),
      );
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        printLog(exception.message.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      }
    }
  }

  Stream<AuthState> _mapToAccountType(AuthAccountType event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      String response;
      response = await userRepository.setAccountType(
          accountType: event.accountType, userId: Storage().currentUser.userId.toString(), token: Storage().currentUser.token);

      yield state.copyWith(
        status: state.status.copyWith(
          status: StateStatuses.success,
        ),
      );
      print(response);
      authenticationBloc.add(LoggedIn(response));
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        if (exception is SocketException) {
          yield state.copyWith(
            status: state.status.copyWith(
              status: StateStatuses.failure,
              message: 'Check your network connection',
            ),
          );
        } else {
          printLog(exception.message.toString());
          yield state.copyWith(
            status: state.status.copyWith(
              status: StateStatuses.failure,
              message: getMessage(exception.message),
            ),
          );
        }
      }
    }
  }

  Stream<AuthState> _mapToChangePassword(ResetPasswordEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      await userRepository.verificationEmailForResetPassword(event.userId, event.otp, event.newPassword);

      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
      authenticationBloc.add(LoggedOut());
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        printLog(exception.message.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      }
    }
  }

  String getMessage(message) {
    message = jsonDecode(message);
    if (message['email'] != null && message['username'] != null) {
      return message['email'].join(', ') + ' and also ' + message['username'].join(' ');
    } else if (message['email'] != null) {
      return message['email'].join(', ');
    } else if (message['password'] != null) {
      return message['password'].join(', ');
    } else if (message['username'] != null) {
      return message['username'].join(', ');
    } else if (message['non_field_errors'] != null) {
      return message['non_field_errors'].join(', ');
    } else if (message['otp'] != null) {
      return message['otp'].join(', ');
    } else {
      return 'Something went wrong, please try again';
    }
  }
}
