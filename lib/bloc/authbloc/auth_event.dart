part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class AuthUserNameChanged extends AuthEvent {
  final String userName;

  AuthUserNameChanged({@required this.userName});

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class AuthEmailChanged extends AuthEvent {
  final String email;

  AuthEmailChanged({@required this.email});

  @override
  List<Object> get props => [email];
}

class AuthPasswordChanged extends AuthEvent {
  final String password;

  AuthPasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];
}

class AuthPasswordConfirmChanged extends AuthEvent {
  final String passwordConfirm;

  AuthPasswordConfirmChanged({@required this.passwordConfirm});

  @override
  List<Object> get props => [passwordConfirm];
}

class AuthAccountType extends AuthEvent {
  final String accountType;

  AuthAccountType({@required this.accountType});

  @override
  List<Object> get props => [accountType];
}

class ConfirmedEmail extends AuthEvent {
  final String pinCode;
  final String email;

  ConfirmedEmail({@required this.pinCode, @required this.email});

  @override
  List<Object> get props => [pinCode, email];
}

class SocialSignUp extends AuthEvent {
  final String provider;
  final String accessToken;

  SocialSignUp({@required this.provider, @required this.accessToken});

  @override
  List<Object> get props => [provider, accessToken];
}

class AuthSubmitted extends AuthEvent {}

class LoginForm extends AuthEvent {}

class RegisterForm extends AuthEvent {}

class ForgetPasswordForm extends AuthEvent {}

class ResetPasswordEvent extends AuthEvent {
  final String newPassword;
  final int userId;
  final String otp;

  ResetPasswordEvent({this.newPassword, this.userId, this.otp});

  @override
  List<Object> get props => [newPassword, userId, otp];

  @override
  bool get stringify => true;
}

class ResendOtpEvent extends AuthEvent {}
