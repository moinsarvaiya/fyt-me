part of 'home_bloc.dart';

class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class HomeSearchEvent extends HomeEvent {
  final String query;

  HomeSearchEvent({this.query});

  @override
  List<Object> get props => [query];
}

class DashboardEvent extends HomeEvent {
  final String query;

  DashboardEvent({this.query});

  @override
  List<Object> get props => [query];
}

class WithdrawalEvent extends HomeEvent {
  final String amount;

  WithdrawalEvent({this.amount});

  @override
  List<Object> get props => [amount];
}

class HomeSelectedEvent extends HomeEvent {
  final InstructorModel instructorModel;

  HomeSelectedEvent({this.instructorModel});

  @override
  List<Object> get props => [instructorModel];
}
