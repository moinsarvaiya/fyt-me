import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fytme/common/constants.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/dashboard_model.dart';
import 'package:fytme/data/model/search_instructor_model.dart';
import 'package:fytme/data/repositories/abstracts/home_repository.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeRepository homeRepo;

  HomeBloc({this.homeRepo})
      : super(HomeState(
          status: StateStatus(
            status: StateStatuses.success,
          ),
        ));

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is HomeSearchEvent) {
      yield* _mapSearchResult(event);
    }
    if (event is HomeSelectedEvent) {
      yield* _displayUser(event);
    }
    if (event is DashboardEvent) {
      yield* _dashboardResult(event);
    }
    if (event is WithdrawalEvent) {
      yield* _withdrawalResult(event);
    }
  }

  Stream<HomeState> _dashboardResult(DashboardEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      DashboardModel dashboardResult = await homeRepo.dashboardResult(query: event.query, token: Storage().currentUser.token);

      yield state.copyWith(
        dashboardResult: dashboardResult,
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<HomeState> _withdrawalResult(WithdrawalEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      String withdrawalResult = await homeRepo.withdrawalResult(token: Storage().currentUser.token, amount: event.amount);

      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.success, message: withdrawalResult),
      );
    } catch (exception) {
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<HomeState> _mapSearchResult(HomeSearchEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      List<InstructorModel> _listResult = await homeRepo.searchInstructor(query: event.query);
      if (_listResult.isNotEmpty) {
        List<InstructorModel> _list = [];
        _listResult.forEach((element) {
          if (Storage().currentUser != null) {
            if (element.id != Storage().currentUser.userId && element.firstName != null) {
              _list.add(element);
            }
          } else {
            _list.add(element);
          }
        });
        yield state.copyWith(
          result: _list,
          status: state.status.copyWith(status: StateStatuses.success, message: ''),
        );
      } else {
        yield state.copyWith(
          result: _listResult,
          status: state.status.copyWith(status: StateStatuses.success, message: ''),
        );
      }
    } catch (exception) {
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<HomeState> _displayUser(HomeSelectedEvent instructorModel) async* {
    yield state.copyWith(selected: instructorModel.instructorModel);
  }
}
