part of 'home_bloc.dart';

class HomeState extends Equatable {
  final StateStatus status;
  final List<InstructorModel> result;
  final DashboardModel dashboardResult;
  final Set<Marker> markers;
  final InstructorModel selected;

  const HomeState({
    this.status = const StateStatus(),
    this.result = const [],
    this.dashboardResult,
    this.markers = const {},
    this.selected,
  });

  HomeState copyWith(
      {StateStatus status, List<InstructorModel> result, DashboardModel dashboardResult, Set<Marker> markers, InstructorModel selected}) {
    return HomeState(
        selected: selected,
        markers: markers ?? this.markers,
        status: status ?? this.status,
        result: result ?? this.result,
        dashboardResult: dashboardResult ?? this.dashboardResult);
  }

  @override
  List<Object> get props => [status, result, dashboardResult, markers, selected];

  @override
  bool get stringify => true;
}
