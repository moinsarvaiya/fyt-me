import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:fytme/bloc/authentication/authentication_bloc.dart';
import 'package:fytme/bloc/authentication/authentication_event.dart';
import 'package:fytme/bloc/authentication/authentication_state.dart';
import 'package:fytme/data/model/blocs/blocs.dart';
import 'package:fytme/data/model/calender_data.dart';
import 'package:fytme/data/model/formz/username.dart';
import 'package:fytme/data/model/programs.dart';
import 'package:fytme/data/model/user.dart';
import 'package:fytme/data/model/user_comments.dart';
import 'package:fytme/data/repositories/abstracts/profile_respository.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as i;
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/constants.dart';
import '../../data/model/formp/formp.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final AuthenticationBloc authenticationBloc;
  final ProfileRepository profileRepository;

  ProfileBloc({@required this.profileRepository, @required this.authenticationBloc})
      : super(ProfileState(
          status: StateStatus(
            status: StateStatuses.success,
          ),
        ));

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is ProfileCreateEvent) {
      yield* _mapToCreateProfile(event);
    }
    if (event is UserNameEvent) {
      yield* _mapToChangeUserName(event);
    }
    if (event is FirstNameEvent) {
      yield* _mapFirstName(event);
    }
    if (event is LastNameEvent) {
      yield* _mapLastName(event);
    }
    if (event is CountryEvent) {
      yield* _mapCountryName(event);
    }
    if (event is CityEvent) {
      yield* _mapCityName(event);
    }
    if (event is AddressEvent) {
      yield* _mapAddressName(event);
    }
    if (event is DobEvent) {
      yield* _mapDobName(event);
    }
    if (event is SpecialityEvent) {
      yield* _mapSpeciality(event);
    }
    if (event is PostCodeEvent) {
      yield* _mapPostCodeName(event);
    }
    if (event is ContactNumberEvent) {
      yield* _mapPhoneNumber(event);
    }
    if (event is CountryCodeEvent) {
      yield* _mapCountryCode(event);
    }
    if (event is GenderEvent) {
      yield* _mapGender(event);
    }
    if (event is ExpEvent) {
      yield* _mapExperience(event);
    }
    if (event is HrRateEvent) {
      yield* _mapHourlyRate(event);
    }

    if (event is PhotoEvent) {
      yield* _mapPhoto(event);
    }
    if (event is IsImageEditedEvent) {
      yield* _mapIsImageEditedEvent(event);
    }
    if (event is VideoTrainingEvent) {
      yield* _mapVideoTrainingEvent(event);
    }
    if (event is TravelsToClientEvent) {
      yield* _mapTravelsToClientEvent(event);
    }
    if (event is CoordinateEvent) {
      yield* _mapCoordinateName(event);
    }
    if (event is ProfileGetEvent) {
      yield* _mapProfile();
    }
    if (event is UpdateProfile) {
      yield* _mapProfile(update: true);
    }
    if (event is ProfileEditing) {
      yield* _mapProfileEditing(event);
    }
    if (event is UploadImageEvent) {
      yield* _mapToUploadPhoto(event);
    }
    if (event is GetUserImagesEvent) {
      yield* _mapToGetUserImages(event);
    }
    if (event is GetUserProgramsEvent) {
      yield* _mapToGetPrograms(event);
    }
    if (event is GetUserCommentsEvent) {
      yield* _mapToGetUserComments(event);
    }
    if (event is GetCalenderDataEvent) {
      yield* _mapToGetCalenderData(event);
    }
    if (event is UploadUserImageEvent) {
      yield* _mapToUploadUserImage(event);
    }
    if (event is DeleteUserImageEvent) {
      yield* _mapToDeleteUserImage(event);
    }
    if (event is SpecialityEventRe) {
      yield* _mapToRemoveSpeciality(event);
    }
    if (event is BioEvent) {
      yield* _mapToBio(event);
    }
    if (event is DocTypeEvent) {
      yield* _mapDocType(event);
    }

    if (event is DocFrontEvent) {
      yield* _mapDocFront(event);
    }
    if (event is SelectPictureEvent) {
      yield* _mapSelectPicture(event);
    }
    if (event is DocBackEvent) {
      yield* _mapDocBack(event);
    }
    if (event is DocNumberEvent) {
      yield* _mapDocNumber(event);
    }
    if (event is CertificateEvent) {
      yield* _mapCertificate(event);
    }
    if (event is InsuranceEvent) {
      yield* _mapInsurance(event);
    }
    if (event is CprEvent) {
      yield* _mapCpr(event);
    }
    if (event is BankAccountEvent) {
      yield* _mapBankAccount(event);
    }
    if (event is BankSortEvent) {
      yield* _mapBankSortCode(event);
    }

    if (event is SubmitVerificationEvent) {
      yield* _mapSubmitVerification();
    }
    if (event is SubmitVerificationSettingEvent) {
      yield* _mapSubmitVerification(fromSetting: true);
    }

    if (event is SkipVerificationProcess) {
      yield* _mapToVerificationSkip();
    }

    if (event is SubmitQuestionAnswer) {
      yield* _mapToSubmitQuestion(event);
    }
  }

  /// delete from keystore/keychain
  Future<void> _deleteToken() async {
    await Storage().clearToken('currentUser');
  }

  Stream<ProfileState> _mapToSubmitQuestion(SubmitQuestionAnswer event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      var response = await profileRepository.submitQuizQuestionForm(data: event.listAnswer, token: Storage().currentUser.token);
      print(response);
      yield state.copyWith(
        status: state.status.copyWith(
          status: StateStatuses.success,
        ),
      );
      /*Storage().currentUser = User.fromProfile(jsonDecode(response));
      await _saveToken(response);*/
      try {
        var res = await getProfile(userId: Storage().currentUser.userId, token: Storage().currentUser.token);
        Storage().userProfile = User();
        Storage().userProfile = User.fromProfile(jsonDecode(res));
        Storage().currentUser = User.fromProfile(jsonDecode(res));
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.success,
          ),
        );
        print(response);
        authenticationBloc.add(LoggedIn(response));
        //Authenticated();
      } catch (e) {
        print('error $e');
        await _deleteToken();
        Unauthenticated();
      }

      yield state.copyWith(status: StateStatus(status: StateStatuses.success, message: jsonDecode(response)['msg']));
    } catch (exception) {
      printLog('====================== _mapToSubmitQuestion _mapToSubmitQuestion');
      printLog(exception);
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToVerificationSkip() async* {
    try {
      final result = await InternetAddress.lookup("google.com");
      if (!(result.isNotEmpty && result[0].rawAddress.isNotEmpty)) {
        printLog('Internet error');
        throw Exception;
      }
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      await http.post(Uri.parse(ServerAddress.api_base_url + 'skip-verification'), headers: {
        'Authorization': 'Bearer ${Storage().currentUser.token}',
      });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('instructorVerified', false);
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
      authenticationBloc.add(SkipVerificationEventAuth());
    } catch (e) {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.failure, message: e.toString()),
      );
    }
  }

  Stream<ProfileState> _mapDocType(DocTypeEvent event) async* {
    yield state.copyWith(docType: event.value);
  }

  Stream<ProfileState> _mapDocNumber(DocNumberEvent event) async* {
    yield state.copyWith(docNumber: event.value);
  }

  Stream<ProfileState> _mapDocFront(DocFrontEvent event) async* {
    yield state.copyWith(dFrontPic: event.value);
  }

  Stream<ProfileState> _mapSelectPicture(SelectPictureEvent event) async* {
    yield state.copyWith(selectPic: event.value);
  }

  Stream<ProfileState> _mapDocBack(DocBackEvent event) async* {
    yield state.copyWith(docBackPic: event.value);
  }

  ///==========

  Stream<ProfileState> _mapCertificate(CertificateEvent event) async* {
    yield state.copyWith(certificate: event.value);
  }

  Stream<ProfileState> _mapInsurance(InsuranceEvent event) async* {
    yield state.copyWith(insurance: event.value);
  }

  Stream<ProfileState> _mapCpr(CprEvent event) async* {
    yield state.copyWith(cpr: event.value);
  }

  ///==========
  Stream<ProfileState> _mapBankAccount(BankAccountEvent event) async* {
    yield state.copyWith(bankAccountNumber: event.value);
  }

  Stream<ProfileState> _mapBankSortCode(BankSortEvent event) async* {
    yield state.copyWith(bankSortNumber: event.value);
  }

  Stream<ProfileState> _mapToBio(BioEvent event) async* {
    yield state.copyWith(bio: event.bioEvent);
  }

  Stream<ProfileState> _mapToRemoveSpeciality(SpecialityEventRe event) async* {
    List<String> _temp = [];
    _temp.addAll(state.speciality);
    _temp.remove(event.speciality);
    yield state.copyWith(speciality: _temp);
  }

  Stream<ProfileState> _mapProfileEditing(ProfileEditing event) async* {
    yield state.copyWith(isEditing: event.value);
  }

  Stream<ProfileState> _mapToChangeUserName(UserNameEvent userNameEvent) async* {
    final userName = UserName.dirty(
      externalValidationError: null,
      value: userNameEvent.username,
    );
    yield state.copyWith(userName: userName);
  }

  Stream<ProfileState> _mapFirstName(FirstNameEvent event) async* {
    printLog(event.firstName);
    final firstName = FirstName.dirty(
      externalValidationError: null,
      value: event.firstName,
    );
    yield state.copyWith(firstName: firstName);
  }

  Stream<ProfileState> _mapLastName(LastNameEvent event) async* {
    final lastName = LastName.dirty(
      externalValidationError: null,
      value: event.lName,
    );
    yield state.copyWith(lastName: lastName);
  }

  Stream<ProfileState> _mapCityName(CityEvent event) async* {
    final city = CityName.dirty(
      externalValidationError: null,
      value: event.city,
    );
    yield state.copyWith(cityName: city);
  }

  Stream<ProfileState> _mapCountryName(CountryEvent event) async* {
    final country = CountryName.dirty(
      externalValidationError: null,
      value: event.country,
    );
    yield state.copyWith(countryName: country);
  }

  Stream<ProfileState> _mapAddressName(AddressEvent event) async* {
    final address = Address.dirty(externalValidationError: null, value: event.address);
    yield state.copyWith(address: address);
  }

  Stream<ProfileState> _mapDobName(DobEvent event) async* {
    final dob = Dob.dirty(
      externalValidationError: null,
      value: event.dob,
    );
    yield state.copyWith(dob: dob);
  }

  Stream<ProfileState> _mapPostCodeName(PostCodeEvent event) async* {
    final postCode = PostCode.dirty(
      externalValidationError: null,
      value: event.postCode,
    );
    yield state.copyWith(postCode: postCode);
  }

  Stream<ProfileState> _mapPhoneNumber(ContactNumberEvent event) async* {
    final phone = PhoneNumber.dirty(
      externalValidationError: null,
      value: event.contactNumber,
    );
    yield state.copyWith(phoneNumber: phone);
  }

  Stream<ProfileState> _mapCountryCode(CountryCodeEvent event) async* {
    final countryCod = event.countryCode;
    yield state.copyWith(countryCode: countryCod);
  }

  Stream<ProfileState> _mapHourlyRate(HrRateEvent event) async* {
    final hourRate = HourlyRate.dirty(
      externalValidationError: null,
      value: event.hrRate,
    );
    yield state.copyWith(hourlyRate: hourRate);
  }

  Stream<ProfileState> _mapSpeciality(SpecialityEvent event) async* {
    printLog(event.speciality);
    List<String> temp = [];
    temp.addAll(state.speciality);
    if (temp.contains(event.speciality)) {
    } else {
      temp.add(event.speciality);
    }

    yield state.copyWith(speciality: temp);
  }

  Stream<ProfileState> _mapExperience(ExpEvent event) async* {
    yield state.copyWith(exp: event.experience);
  }

  Stream<ProfileState> _mapGender(GenderEvent event) async* {
    final gender = Gender.dirty(
      externalValidationError: null,
      value: event.gender,
    );
    yield state.copyWith(gender: gender);
  }

  Stream<ProfileState> _mapCoordinateName(CoordinateEvent event) async* {
    yield state.copyWith(coordinates: event.coordinates);
  }

  Stream<ProfileState> _mapPhoto(PhotoEvent event) async* {
    yield state.copyWith(image: event.image);
  }

  Stream<ProfileState> _mapIsImageEditedEvent(IsImageEditedEvent event) async* {
    yield state.copyWith(isImageEdited: event.isImageEdited);
  }

  Stream<ProfileState> _mapVideoTrainingEvent(VideoTrainingEvent event) async* {
    yield state.copyWith(isVideoTraining: event.isVideoTraining);
  }

  Stream<ProfileState> _mapTravelsToClientEvent(TravelsToClientEvent event) async* {
    yield state.copyWith(isClientTravels: event.isClientTravels);
  }

  Stream<ProfileState> _mapToUploadPhoto(UploadImageEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      User user;
      User userForUpdate = User();
      printLog('====================== compressImage processing');
      var l = File(event.pathImage).lengthSync();
      var kb = l / 1024;
      var mb = kb / 1024;
      if (mb >= 15) {
        printLog(' $mb ====================== compressImage processing');
        userForUpdate.imagePath = await compressImage(File(event.pathImage));
      } else {
        userForUpdate.imagePath = event.pathImage;
      }
      printLog('====================== compressImage processed');
      user =
          await profileRepository.updateProfileImage(user: userForUpdate, token: Storage().currentUser.token, userId: Storage().currentUser.userId);
      Storage().userProfile = user;
      yield state.copyWith(
        tempUser: user,
        status: state.status.copyWith(status: StateStatuses.success, message: 'profile image updated'),
      );
    } catch (exception) {
      printLog(exception.toString());
      try {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      } catch (e) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToGetPrograms(GetUserProgramsEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      Programs programs = await profileRepository.getPrograms(token: Storage().currentUser.token, userId: event.userId);
      yield state.copyWith(
        programs: programs,
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      printLog(exception.toString());
      try {
        yield state.copyWith(
          status: state.status.copyWith(
              status: StateStatuses.failure,
              message: getMessage(exception.message).contains("Failed host lookup: 'google.com'")
                  ? "Check your network connection"
                  : getMessage(exception.message)),
        );
      } catch (e) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToGetCalenderData(GetCalenderDataEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      CalenderData calenderData = await profileRepository.getCalenderData(
        token: Storage().currentUser.token,
        userId: event.userId,
        startTime: event.startTime,
        endTime: event.endTime,
      );
      yield state.copyWith(
        calenderData: calenderData,
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      printLog(exception.toString());
      try {
        yield state.copyWith(
          status: state.status.copyWith(
              status: StateStatuses.failure,
              message: getMessage(exception.message).contains("Failed host lookup: 'google.com'")
                  ? "Check your network connection"
                  : getMessage(exception.message)),
        );
      } catch (e) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToGetUserComments(GetUserCommentsEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      UserComments userComments = await profileRepository.getUserComments(
        userId: event.userId,
        token: Storage().currentUser.token,
        imageId: int.parse(event.selIndex),
      );

      print('UserComment ::::: ${userComments.totalComments}');

      yield state.copyWith(
        userComments: userComments,
      );

      yield state.copyWith(
        userComments: userComments,
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      printLog(exception.toString());
      try {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      } catch (e) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToGetUserImages(GetUserImagesEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      UserImages userImages = await profileRepository.getUserImages(token: Storage().currentUser.token, userId: event.userId);
      yield state.copyWith(
        isImageEdited: false,
        userImages: getUserImages(userImages),
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      printLog(exception.toString());
      try {
        yield state.copyWith(
          status: state.status.copyWith(
              status: StateStatuses.failure,
              message: getMessage(exception.message).contains("Failed host lookup: 'google.com'")
                  ? "Check your network connection"
                  : getMessage(exception.message)
              //message: getMessage(exception.message),
              ),
        );
      } catch (e) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToUploadUserImage(UploadUserImageEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      printLog('====================== compressImage processing');
      var l = File(event.pathImage).lengthSync();
      var kb = l / 1024;
      var mb = kb / 1024;
      var imagePath = event.pathImage;
      if (mb >= 15) {
        printLog(' $mb ====================== compressImage processing');
        imagePath = await compressImage(File(event.pathImage));
      }
      printLog('====================== compressImage processed');

      UserImages userImages = await profileRepository.uploadUserImage(
          imagePath: File(imagePath),
          token: Storage().currentUser.token,
          userId: Storage().currentUser.userId,
          selIndex: event.selIndex,
          requestType: event.requestType,
          id: event.id);

      yield state.copyWith(
        isImageEdited: true,
        userImages: getUserImages(userImages),
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      printLog('====================== _mapToUploadUserImage _mapToUploadUserImage');
      printLog(exception);
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Stream<ProfileState> _mapToDeleteUserImage(DeleteUserImageEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      /*UserImages userImages = await profileRepository.deleteUserImage(
          token: Storage().currentUser.token,
          userId: Storage().currentUser.userId,
          selIndex: event.selIndex,
          id: event.id);*/
      yield state.copyWith(
        isImageEdited: true,
        //userImages: getUserImages(userImages),
        status: state.status.copyWith(status: StateStatuses.success, message: ''),
      );
    } catch (exception) {
      printLog('====================== _mapToDeleteUserImage _mapToDeleteUserImage');
      printLog(exception);
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  UserImages getUserImages(userImg) {
    return state.userImages.copyWith(
        id: userImg.id,
        memberId: userImg.memberId,
        image1: userImg.image1,
        image2: userImg.image2,
        image3: userImg.image3,
        image4: userImg.image4,
        image5: userImg.image5,
        image6: userImg.image6,
        isImageEdited: userImg.isImageEdited,
        createdOn: userImg.createdOn);
  }

  Stream<ProfileState> _mapToCreateProfile(ProfileCreateEvent event) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      User user = User();
      user.bio = state.bio;
      user.userId = Storage().currentUser.userId;
      //user.contactNumber = state.phoneNumber.value;
      user.contactNumber = '${state.countryCode} ${state.phoneNumber.value}';
      print('crete profile ${state.phoneNumber.value} ${state.countryCode}');
      user.firstName = state.firstName.value;
      user.lastName = state.lastName.value;
      user.city = state.cityName.value;
      user.country = state.countryName.value;
      user.dateOfBirth = state.dob.value;
      if (Storage().currentUser.accountType == 'instructor') {
        user.isClientTravels = state.isClientTravels;
        user.isVideoTraining = state.isVideoTraining;
      }
      user.postCode = state.postCode.value;
      user.address = "ahmedabad";
      user.gender = state.gender.value;
      if (Storage().currentUser.accountType == 'instructor') {
        user.hourlyRate = int.parse(state.hourlyRate.value);
      }
      user.coordinates = state.coordinates;
      if (Storage().currentUser.accountType == 'instructor') {
        if (state.exp != null) {
          user.experience = state.exp;
        }
      }

      user.specialty = state.speciality;
      String response;
      if (state.image != null) {
        printLog('====================== compressImage processing');
        var l = state.image.lengthSync();
        var kb = l / 1024;
        var mb = kb / 1024;
        if (mb >= 15) {
          printLog(' $mb ====================== compressImage processing');
          user.imagePath = await compressImage(state.image);
        } else {
          user.imagePath = state.image.path;
        }
        printLog('====================== compressImage processed');
      }
      response = await profileRepository.createProfile(user: user, token: Storage().currentUser.token, userType: Storage().currentUser.accountType);

      print('user details : $user');
      yield state.copyWith(
        status: state.status.copyWith(
          status: StateStatuses.success,
        ),
      );
      authenticationBloc.add(LoggedIn(response));
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: 'Check your network connection',
          ),
        );
      } else {
        printLog(exception.message.toString());
        try {
          yield state.copyWith(
            status: state.status.copyWith(
              status: StateStatuses.failure,
              message: getMessage(exception.message),
            ),
          );
        } catch (e) {
          yield state.copyWith(
            status: state.status.copyWith(
              status: StateStatuses.failure,
              message: exception.message.toString(),
            ),
          );
        }
      }
    }
  }

  Stream<ProfileState> _mapProfile({bool update = false}) async* {
    try {
      yield state.copyWith(
        tempUser: User(),
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );
      User user;
      if (update) {
        User userForUpdate = User();
        if (state.bio != null) userForUpdate.bio = state.bio;
        if (state.phoneNumber.value.isNotEmpty) {
          //userForUpdate.contactNumber = state.phoneNumber.value;
          List<String> cc = state.phoneNumber.value.split(state.countryCode);
          if (cc.isNotEmpty) {
            if (cc.length > 1) {
              userForUpdate.contactNumber = '${state.countryCode}${cc[1]}';
            } else {
              userForUpdate.contactNumber = '${state.countryCode}${cc[0]}';
            }
          } else {
            userForUpdate.contactNumber = '${state.countryCode}${state.phoneNumber.value}';
          }
        }
        if (state.firstName.value.isNotEmpty) userForUpdate.firstName = state.firstName.value;
        if (state.lastName.value.isNotEmpty) userForUpdate.lastName = state.lastName.value;
        if (state.cityName.value.isNotEmpty) userForUpdate.city = state.cityName.value;
        if (state.countryName.value.isNotEmpty) userForUpdate.country = state.countryName.value;
        if (state.dob.value.isNotEmpty) userForUpdate.dateOfBirth = state.dob.value;
        if (state.postCode.value.isNotEmpty) userForUpdate.postCode = state.postCode.value;
        if (state.address.value.isNotEmpty) userForUpdate.address = state.address.value;
        if (state.gender.value.isNotEmpty) userForUpdate.gender = state.gender.value;
        if (Storage().currentUser.accountType == 'instructor') {
          userForUpdate.isVideoTraining = state.isVideoTraining;
          userForUpdate.isClientTravels = state.isClientTravels;
        }
        if (state.coordinates != null) userForUpdate.coordinates = state.coordinates;
        if (Storage().currentUser.accountType == 'instructor') {
          if (state.hourlyRate.value.isNotEmpty) userForUpdate.hourlyRate = int.parse(state.hourlyRate.value);
        }
        if (Storage().currentUser.accountType == 'instructor') {
          if (state.exp != null) {
            if (state.exp.isNotEmpty) {
              userForUpdate.experience = state.exp;
            }
          }
        }
        if (Storage().currentUser.accountType == 'instructor') {
          if (state.speciality != null) userForUpdate.specialty = state.speciality;
        }

        if (state.image != null) {
          printLog('====================== compressImage processing');
          var l = state.image.lengthSync();
          var kb = l / 1024;
          var mb = kb / 1024;
          if (mb >= 15) {
            printLog(' $mb ====================== compressImage processing');
            userForUpdate.imagePath = await compressImage(state.image);
          } else {
            userForUpdate.imagePath = state.image.path;
          }
          printLog('====================== compressImage processed');
          user = await profileRepository.updateProfile(user: userForUpdate, token: Storage().currentUser.token, userId: Storage().currentUser.userId);
        } else {
          user = await profileRepository.updateProfile(user: userForUpdate, token: Storage().currentUser.token, userId: Storage().currentUser.userId);
        }
      } else {
        user = await profileRepository.getProfile(token: Storage().currentUser.token, userId: Storage().currentUser.userId);
      }
      Storage().userProfile = user;
      final firstName = FirstName.dirty(
        externalValidationError: null,
        value: user.firstName,
      );
      final lastName = LastName.dirty(
        externalValidationError: null,
        value: user.lastName,
      );
      final dob = Dob.dirty(
        externalValidationError: null,
        value: user.dateOfBirth,
      );
      final post = PostCode.dirty(externalValidationError: null, value: user.postCode);
      final city = CityName.dirty(externalValidationError: null, value: user.city);
      final country = CountryName.dirty(externalValidationError: null, value: user.country);
      final contact = PhoneNumber.dirty(externalValidationError: null, value: user.contactNumber);
      final address = Address.dirty(externalValidationError: null, value: user.address);
      var hrRate;
      if (Storage().currentUser.accountType == 'instructor') {
        hrRate = HourlyRate.dirty(externalValidationError: null, value: user.hourlyRate.toString());
      }
      final userName = UserName.dirty(
        externalValidationError: null,
        value: user.username,
      );
      final gender = Gender.dirty(externalValidationError: null, value: user.gender);

      yield state.copyWith(
          tempUser: user,
          status: state.status.copyWith(status: StateStatuses.success, message: update ? 'Profile Updated successfully' : ''),
          bio: user.bio,
          isEditing: false,
          userName: userName,
          speciality: user.specialty,
          coordinates: user.coordinates,
          firstName: firstName,
          lastName: lastName,
          exp: user.experience != null ? user.experience.toString() : '',
          hourlyRate: hrRate,
          isVideoTraining: user.isVideoTraining,
          isClientTravels: user.isClientTravels,
          gender: gender,
          address: address,
          phoneNumber: contact,
          cityName: city,
          countryName: country,
          postCode: post,
          dob: dob);
    } catch (exception) {
      if (exception is SocketException) {
        yield state.copyWith(
          status: state.status.copyWith(status: StateStatuses.failure, message: 'Check your network connection'),
        );
      } else {
        try {
          printLog(exception.toString());
          yield state.copyWith(
            status: state.status.copyWith(
              status: StateStatuses.failure,
              message: getMessage(exception.message),
            ),
          );
        } catch (e) {
          printLog(exception.toString());
          yield state.copyWith(
            status: state.status.copyWith(status: StateStatuses.failure, message: exception.toString()),
          );
        }
      }
    }
  }

  Stream<ProfileState> _mapSubmitVerification({bool fromSetting = false}) async* {
    try {
      yield state.copyWith(
        status: state.status.copyWith(status: StateStatuses.loading, message: ''),
      );

      Map data = <String, dynamic>{
        "document_front_picture": await MultipartFile.fromFile(
          state.dFrontPic,
        ),
        "document_back_picture": await MultipartFile.fromFile(
          state.docBackPic,
        ),
        "certificate": await MultipartFile.fromFile(
          state.certificate,
        ),
        "cpr": await MultipartFile.fromFile(
          state.cpr,
        ),
        "insurance": await MultipartFile.fromFile(
          state.insurance,
        ),
        "bank_account_number": state.bankAccountNumber,
        "bank_sort_code": state.bankSortNumber,
        "document_type": state.docType,
        "document_number": state.docNumber,
        "member": Storage().currentUser.userId,
      };
      var response = await profileRepository.submitInstructorVerificationForm(data: data, token: Storage().currentUser.token);
      if (!fromSetting) {
        yield state.copyWith(
          status: state.status.copyWith(status: StateStatuses.success, message: ''),
        );
        authenticationBloc.add(LoggedIn(response));
      } else {
        User user = User();
        user = await profileRepository.getProfile(token: Storage().currentUser.token, userId: Storage().currentUser.userId);
        Storage().userProfile = user;
        yield state.copyWith(
          status: state.status.copyWith(status: StateStatuses.success, message: ''),
        );
      }
    } catch (exception) {
      printLog('====================== _mapSubmitVerification _mapSubmitVerification');
      printLog(exception);
      try {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: getMessage(exception.message),
          ),
        );
      } catch (e) {
        printLog(exception.toString());
        yield state.copyWith(
          status: state.status.copyWith(
            status: StateStatuses.failure,
            message: exception.toString(),
          ),
        );
      }
    }
  }

  Future<String> compressImage(File image) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    i.Image imageFile = i.decodeImage(image.readAsBytesSync());
    final compressedImageFile = File('$path.jpg')..writeAsBytesSync(i.encodeJpg(imageFile, quality: 80));
    return compressedImageFile.path;
  }

  String getMessage(message) {
    try {
      printLog('=============');
      printLog(message.runtimeType);
      if (message['contact_number'] != null) {
        return message['contact_number'].join(', ');
      } else if (message['city'] != null) {
        return message['city'].join(', ');
      } else if (message['postcode'] != null) {
        return message['postcode'].join(', ');
      } else if (message['country'] != null) {
        return message['country'].join(', ');
      } else if (message['error'] != null) {
        return message['error'];
      } else {
        return 'Something went wrong, please try again';
      }
    } catch (e) {
      return message.toString();
    }
  }

  Future<String> getProfile({String token, int userId}) async {
    try {
      final String url = '${ServerAddress.api_base_url}user-profile/$userId/';
      final client = new http.Client();
      final response = await client.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      printLog('getProfile =========');
      print('${response.body} ${response.statusCode}');
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception();
      }
    } catch (err) {
      printLog('getProfile ========= error');
      printLog(err);
      rethrow;
    }
  }
}
