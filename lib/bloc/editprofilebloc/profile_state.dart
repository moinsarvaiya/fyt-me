part of 'profile_bloc.dart';

class ProfileState extends Equatable {
  final UserName userName;
  final FirstName firstName;
  final LastName lastName;
  final CountryName countryName;
  final String countryCode;
  final Address address;
  final CityName cityName;
  final Dob dob;
  final PhoneNumber phoneNumber;
  final StateStatus status;
  final Gender gender;
  final HourlyRate hourlyRate;
  final List<String> speciality;
  final CalenderData calenderData;
  final String exp;
  final File image;
  final String coordinates;
  final PostCode postCode;
  final bool isEditing;
  final User tempUser;
  final UserImages userImages;
  final bool isImageEdited;
  final bool isVideoTraining;
  final bool isClientTravels;
  final UserComments userComments;
  final Programs programs;
  final String bio;

  //======
  final String docType;

  final String docNumber;
  final String dFrontPic;
  final String docBackPic;

  final String selectPic;

  /// ======
  final String certificate;
  final String insurance;
  final String cpr;

  ///=========

  final String bankAccountNumber;
  final String bankSortNumber;

  //=====
  const ProfileState({
    this.bankAccountNumber,
    this.bankSortNumber,
    this.certificate,
    this.insurance,
    this.cpr,
    this.docType,
    this.docNumber,
    this.dFrontPic,
    this.selectPic,
    this.docBackPic,
    this.tempUser,
    this.userImages = const UserImages(),
    this.isImageEdited = false,
    this.isVideoTraining = false,
    this.isClientTravels = false,
    this.userComments,
    this.programs,
    this.bio,
    this.userName = const UserName.pure(),
    this.isEditing = false,
    this.coordinates,
    this.image,
    this.exp = '1-3',
    this.speciality = const [],
    this.calenderData,
    this.firstName = const FirstName.pure(),
    this.lastName = const LastName.pure(),
    this.address = const Address.pure(),
    this.countryName = const CountryName.pure(),
    this.countryCode,
    this.cityName = const CityName.pure(),
    this.dob = const Dob.pure(),
    this.phoneNumber = const PhoneNumber.pure(),
    this.gender = const Gender.pure(),
    this.hourlyRate = const HourlyRate.pure(),
    this.status = const StateStatus(),
    this.postCode = const PostCode.pure(),
  });

  ProfileState copyWith({
    String bankAccountNumber,
    String bankSortNumber,
    String certificate,
    String insurance,
    String cpr,
    String docType,
    String docNumber,
    String dFrontPic,
    String selectPic,
    String docBackPic,
    String bio,
    User tempUser,
    UserImages userImages,
    bool isImageEdited,
    bool isVideoTraining,
    bool isClientTravels,
    UserComments userComments,
    Programs programs,
    UserName userName,
    bool isEditing,
    String coordinates,
    StateStatus status,
    FirstName firstName,
    LastName lastName,
    CountryName countryName,
    String countryCode,
    Address address,
    CityName cityName,
    Dob dob,
    PhoneNumber phoneNumber,
    Gender gender,
    HourlyRate hourlyRate,
    List<String> speciality,
    CalenderData calenderData,
    String exp,
    File image,
    PostCode postCode,
  }) {
    return ProfileState(
        bankAccountNumber: bankAccountNumber ?? this.bankAccountNumber,
        bankSortNumber: bankSortNumber ?? this.bankSortNumber,
        certificate: certificate ?? this.certificate,
        insurance: insurance ?? this.insurance,
        cpr: cpr ?? this.cpr,
        docType: docType ?? this.docType,
        docBackPic: docBackPic ?? this.docBackPic,
        docNumber: docNumber ?? this.docNumber,
        dFrontPic: dFrontPic ?? this.dFrontPic,
        selectPic: selectPic ?? this.selectPic,
        bio: bio ?? this.bio,
        tempUser: tempUser ?? this.tempUser,
        userImages: userImages ?? this.userImages,
        isImageEdited: isImageEdited ?? this.isImageEdited,
        isVideoTraining: isVideoTraining ?? this.isVideoTraining,
        isClientTravels: isClientTravels ?? this.isClientTravels,
        userComments: userComments ?? this.userComments,
        programs: programs ?? this.programs,
        userName: userName ?? this.userName,
        isEditing: isEditing ?? this.isEditing,
        status: status ?? this.status,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        dob: dob ?? this.dob,
        address: address ?? this.address,
        cityName: cityName ?? this.cityName,
        countryName: countryName ?? this.countryName,
        countryCode: countryCode ?? this.countryCode,
        exp: exp ?? this.exp,
        gender: gender ?? this.gender,
        hourlyRate: hourlyRate ?? this.hourlyRate,
        image: image ?? this.image,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        speciality: speciality ?? this.speciality,
        calenderData: calenderData ?? this.calenderData,
        coordinates: coordinates ?? this.coordinates,
        postCode: postCode ?? this.postCode);
  }

  @override
  List<Object> get props => [
        bankAccountNumber,
        bankSortNumber,
        certificate,
        insurance,
        cpr,
        docType,
        docBackPic,
        docNumber,
        dFrontPic,
        selectPic,
        bio,
        tempUser,
        userImages,
        isImageEdited,
        isVideoTraining,
        isClientTravels,
        userComments,
        programs,
        userName,
        isEditing,
        status,
        firstName,
        lastName,
        dob,
        address,
        countryName,
        countryCode,
        cityName,
        phoneNumber,
        image,
        hourlyRate,
        speciality,
        calenderData,
        exp,
        gender,
        coordinates,
        postCode
      ];

  @override
  bool get stringify => true;
}
