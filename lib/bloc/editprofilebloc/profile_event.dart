part of 'profile_bloc.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class ProfileCreateEvent extends ProfileEvent {}

class UserNameEvent extends ProfileEvent {
  final String username;

  UserNameEvent({this.username});

  @override
  List<Object> get props => [username];
}

class FirstNameEvent extends ProfileEvent {
  final String firstName;

  FirstNameEvent({@required this.firstName});

  @override
  List<Object> get props => [firstName];
}

class ImagesTmpEvent extends ProfileEvent {
  final List<Map<String, String>> images;

  ImagesTmpEvent({@required this.images});

  @override
  List<Object> get props => [images];
}

class UploadTmpImagesEvent extends ProfileEvent {
  final List<String> uploadImages;

  UploadTmpImagesEvent({@required this.uploadImages});

  @override
  List<Object> get props => [uploadImages];
}

class LastNameEvent extends ProfileEvent {
  final String lName;

  LastNameEvent({@required this.lName});

  @override
  List<Object> get props => [lName];
}

class DobEvent extends ProfileEvent {
  final String dob;

  DobEvent({@required this.dob});

  @override
  List<Object> get props => [dob];
}

class PostCodeEvent extends ProfileEvent {
  final String postCode;

  PostCodeEvent({@required this.postCode});

  @override
  List<Object> get props => [postCode];
}

class CityEvent extends ProfileEvent {
  final String city;

  CityEvent({@required this.city});

  @override
  List<Object> get props => [city];
}

class CountryEvent extends ProfileEvent {
  final String country;

  CountryEvent({@required this.country});

  @override
  List<Object> get props => [country];
}

class ContactNumberEvent extends ProfileEvent {
  final String contactNumber;

  ContactNumberEvent({@required this.contactNumber});

  @override
  List<Object> get props => [contactNumber];
}

class CountryCodeEvent extends ProfileEvent {
  final String countryCode;

  CountryCodeEvent({@required this.countryCode});

  @override
  List<Object> get props => [countryCode];
}

class AddressEvent extends ProfileEvent {
  final String address;

  AddressEvent({@required this.address});

  @override
  List<Object> get props => [address];
}

class HrRateEvent extends ProfileEvent {
  final String hrRate;

  HrRateEvent({@required this.hrRate});

  @override
  List<Object> get props => [hrRate];
}

class SpecialityEvent extends ProfileEvent {
  final String speciality;

  SpecialityEvent({this.speciality});

  @override
  List<Object> get props => [speciality];
}

class SpecialityEventRe extends ProfileEvent {
  final String speciality;

  SpecialityEventRe({this.speciality});

  @override
  List<Object> get props => [speciality];
}

class ExpEvent extends ProfileEvent {
  final String experience;

  ExpEvent({this.experience});

  @override
  List<Object> get props => [experience];
}

class BioEvent extends ProfileEvent {
  final String bioEvent;

  BioEvent({this.bioEvent});

  @override
  List<Object> get props => [bioEvent];
}

class GenderEvent extends ProfileEvent {
  final String gender;

  GenderEvent({@required this.gender});

  @override
  List<Object> get props => [gender];
}

class VideoTrainingEvent extends ProfileEvent {
  final bool isVideoTraining;

  VideoTrainingEvent({this.isVideoTraining});

  @override
  List<Object> get props => [isVideoTraining];
}

class TravelsToClientEvent extends ProfileEvent {
  final bool isClientTravels;

  TravelsToClientEvent({this.isClientTravels});

  @override
  List<Object> get props => [isClientTravels];
}

class IsImageEditedEvent extends ProfileEvent {
  final bool isImageEdited;

  IsImageEditedEvent({this.isImageEdited});

  @override
  List<Object> get props => [isImageEdited];
}

class PhotoEvent extends ProfileEvent {
  final File image;

  PhotoEvent({this.image});

  @override
  List<Object> get props => [image];
}

class CoordinateEvent extends ProfileEvent {
  final String coordinates;

  CoordinateEvent({this.coordinates});

  @override
  List<Object> get props => [coordinates];
}

class ProfileGetEvent extends ProfileEvent {}

class UpdateProfile extends ProfileEvent {}

class ProfileEditing extends ProfileEvent {
  final bool value;

  ProfileEditing({this.value});

  @override
  List<Object> get props => [value];
}

class UploadImageEvent extends ProfileEvent {
  final String pathImage;

  UploadImageEvent({this.pathImage});

  @override
  List<Object> get props => [pathImage];
}

class DocTypeEvent extends ProfileEvent {
  final String value;

  DocTypeEvent({this.value});

  @override
  List<Object> get props => [value];
}

class DocNumberEvent extends ProfileEvent {
  final String value;

  DocNumberEvent({this.value});

  @override
  List<Object> get props => [value];
}

class DocFrontEvent extends ProfileEvent {
  final String value;

  DocFrontEvent({this.value});

  @override
  List<Object> get props => [value];
}

class SelectPictureEvent extends ProfileEvent {
  final String value;

  SelectPictureEvent({this.value});

  @override
  List<Object> get props => [value];
}

class DocBackEvent extends ProfileEvent {
  final String value;

  DocBackEvent({this.value});

  @override
  List<Object> get props => [value];
}

class CertificateEvent extends ProfileEvent {
  final String value;

  CertificateEvent({this.value});

  @override
  List<Object> get props => [value];
}

class InsuranceEvent extends ProfileEvent {
  final String value;

  InsuranceEvent({this.value});

  @override
  List<Object> get props => [value];
}

class CprEvent extends ProfileEvent {
  final String value;

  CprEvent({this.value});

  @override
  List<Object> get props => [value];
}

class BankAccountEvent extends ProfileEvent {
  final String value;

  BankAccountEvent({this.value});

  @override
  List<Object> get props => [value];
}

class BankSortEvent extends ProfileEvent {
  final String value;

  BankSortEvent({this.value});

  @override
  List<Object> get props => [value];
}

class SubmitVerificationEvent extends ProfileEvent {}

class SubmitVerificationSettingEvent extends ProfileEvent {}

class SkipVerificationProcess extends ProfileEvent {}

class GetUserImagesEvent extends ProfileEvent {
  final int userId;

  GetUserImagesEvent({this.userId});

  @override
  List<Object> get props => [userId];
}

class GetUserProgramsEvent extends ProfileEvent {
  final int userId;

  GetUserProgramsEvent({this.userId});

  @override
  List<Object> get props => [userId];
}

class GetUserCommentsEvent extends ProfileEvent {
  final String selIndex;
  final int id;
  final int userId;

  GetUserCommentsEvent({this.selIndex, this.id, this.userId});

  @override
  List<Object> get props => [selIndex, id, userId];
}

class GetCalenderDataEvent extends ProfileEvent {
  final String startTime;
  final String endTime;
  final int userId;

  GetCalenderDataEvent({this.startTime, this.endTime, this.userId});

  @override
  List<Object> get props => [startTime, endTime, userId];

  @override
  bool get stringify => true;
}

class UploadUserImageEvent extends ProfileEvent {
  final String selIndex;
  final String pathImage;
  final String requestType;
  final int id;

  UploadUserImageEvent({this.selIndex, this.pathImage, this.requestType, this.id});

  @override
  List<Object> get props => [selIndex, pathImage, requestType, id];
}

class DeleteUserImageEvent extends ProfileEvent {
  final String selIndex;
  final int id;

  DeleteUserImageEvent({this.selIndex, this.id});

  @override
  List<Object> get props => [selIndex, id];
}

class SubmitQuestionAnswer extends ProfileEvent {
  final Map<String, dynamic> listAnswer;

  SubmitQuestionAnswer({this.listAnswer});

  @override
  List<Object> get props => [listAnswer];
}
