import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fytme/data/model/unread_count.dart';
import 'package:fytme/data/serveraddress.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../common/constants.dart';

class InAppNotificationBloc extends ChangeNotifier {
  List<InAppNotificationModel> listInAppNotification = [];

  //List<int> latestIds = [];
  ProposalDetails proposalDetails;
  UnreadCount unreadCount;

  //int totalNotification = 0;

  getNotificationCount() async {
    final String url = '${ServerAddress.api_base_url}notification/?unread_count=true';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $token',
    });
    printLog('======== getNotification total');
    printLog(response.body);
    printLog('token ${Storage().userProfile.email} ${Storage().currentUser.firstName}');
    printLog(response.statusCode);

    var decode = jsonDecode(response.body);
    unreadCount = UnreadCount.fromJson(decode);
    //totalNotification = decode['count'];
    notifyListeners();
    //latestIds.addAll(decode['latest_ids']);
  }

  setCountLength() {
    unreadCount = UnreadCount(count: 0, latestIds: unreadCount.latestIds);
    notifyListeners();
  }

  setLength() {
    unreadCount = UnreadCount(count: 0, latestIds: []);
    notifyListeners();
  }

  getNotification() async {
    final String url = '${ServerAddress.api_base_url}notification/';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });
    printLog('======== getNotification');
    printLog(response.body);
    if (response.statusCode == 200) {
      listInAppNotification = [];
      List<dynamic> _list = jsonDecode(response.body);
      if (_list.isNotEmpty) {
        _list.forEach((element) {
          listInAppNotification.add(InAppNotificationModel.fromJson(element));
        });
      }
    }
  }

  getProposalDetails(int proposalId, BuildContext context) async {
    final String url = '${ServerAddress.api_base_url}proposal-details/?proposal_id=$proposalId';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });
    printLog('======== getProposalDetails');
    printLog(response.body);
    if (response.statusCode == 200) {
      proposalDetails = ProposalDetails.fromJson(jsonDecode(response.body));
      notifyListeners();
    }
  }

  acceptRejectProposal(int proposalId, String action, String typeOfAction) async {
    final String url = '${ServerAddress.api_base_url}${typeOfAction == 'invitation' ? 'invitation-accept-reject' : 'proposal-accept-reject'}';
    final client = new http.Client();
    var token = Storage().currentUser.token;
    print(token);
    final response = await client.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "proposal_id": proposalId,
        "action": action,
        "comments": action,
      }),
    );
    printLog('======== acceptRejectProposal');
    printLog(response.body);
    if (response.statusCode == 200) {}
  }
}

class InAppNotificationModel {
  final int id;
  final int proposalId;
  final int userId;
  final bool isRead;
  final bool isPaid;
  final bool isActive;
  final bool actionable;
  bool isHighLight = false;
  final String createdOn;
  final String proposalType;
  final String typeOfAction;
  final String notificationSubject;
  final String notificationMessage;

  //bool get isHighLight => isHighLight;

  InAppNotificationModel(
      {this.id,
      this.createdOn,
      this.isActive,
      this.actionable,
      this.isRead,
      this.isPaid,
      this.proposalId,
      this.proposalType,
      this.typeOfAction,
      this.notificationMessage,
      this.notificationSubject,
      this.userId});

  //2021-07-08T14:00:07.073916Z
  factory InAppNotificationModel.fromJson(Map json) {
    final df = new DateFormat('dd/MM/yyyy kk:mm a');
    String date = df.format(DateTime.parse(json['created_on']));
    int hours = df.parse(df.format(DateTime.now())).difference(df.parse(df.format(DateTime.parse(json['created_on'])))).inHours;
    return InAppNotificationModel(
        id: json['id'],
        proposalId: json['proposal_id'],
        createdOn: hours > 24
            ? date
            : hours == 0
                ? 'Just Now'
                : '$hours Hours Ago',
        userId: json['user_id'],
        isRead: json['is_read'],
        isPaid: json['is_paid'],
        isActive: json['is_active'],
        actionable: json['actionable'],
        proposalType: json['proposal_type'],
        typeOfAction: json['type_of_action'],
        notificationSubject: json['notification_subject'],
        notificationMessage: json['notification_message']);
  }
}

class ProposalDetails {
  String proposalBy;
  int clientId;
  int instructorId;
  String profilePicture;
  String trainingMode;
  String sessionDate;
  String sessionStartTiming;
  String sessionEndTiming;
  int totalHours;
  int hourlyRate;
  InstructorInfo instructorInfo;
  String videoRoomName;
  String videoAccessToken;
  List<InvitedFriendsInfo> invitedFriendsInfo;

  ProposalDetails(
      {this.proposalBy,
      this.clientId,
      this.instructorId,
      this.profilePicture,
      this.trainingMode,
      this.sessionDate,
      this.sessionStartTiming,
      this.sessionEndTiming,
      this.totalHours,
      this.hourlyRate,
      this.instructorInfo,
      this.videoRoomName,
      this.videoAccessToken,
      this.invitedFriendsInfo});

  ProposalDetails.fromJson(Map<String, dynamic> json) {
    proposalBy = json['proposal_by'];
    clientId = json['client_id'];
    instructorId = json['instructor_id'];
    profilePicture = json['profile_picture'];
    trainingMode = json['training_mode'];
    sessionDate = json['session_date'];
    sessionStartTiming = json['session_start_timing'];
    sessionEndTiming = json['session_end_timing'];
    totalHours = json['total_hours'];
    hourlyRate = json['hourly_rate'];
    instructorInfo = json['instructor_info'] != null ? new InstructorInfo.fromJson(json['instructor_info']) : null;
    videoRoomName = json['video_room_name'];
    videoAccessToken = json['video_access_token'];
    if (json['invited_friends_info'] != null) {
      invitedFriendsInfo = [];
      json['invited_friends_info'].forEach((v) {
        invitedFriendsInfo.add(new InvitedFriendsInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['proposal_by'] = this.proposalBy;
    data['client_id'] = this.clientId;
    data['instructor_id'] = this.instructorId;
    data['profile_picture'] = this.profilePicture;
    data['training_mode'] = this.trainingMode;
    data['session_date'] = this.sessionDate;
    data['session_start_timing'] = this.sessionStartTiming;
    data['session_end_timing'] = this.sessionEndTiming;
    data['total_hours'] = this.totalHours;
    data['hourly_rate'] = this.hourlyRate;
    if (this.instructorInfo != null) {
      data['instructor_info'] = this.instructorInfo.toJson();
    }
    data['video_room_name'] = this.videoRoomName;
    data['video_access_token'] = this.videoAccessToken;
    if (this.invitedFriendsInfo != null) {
      data['invited_friends_info'] = this.invitedFriendsInfo.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InstructorInfo {
  String instructorName;
  String instructorPicture;
  String username;

  InstructorInfo({this.instructorName, this.instructorPicture, this.username});

  InstructorInfo.fromJson(Map<String, dynamic> json) {
    instructorName = json['instructor_name'];
    instructorPicture = json['instructor_picture'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['instructor_name'] = this.instructorName;
    data['instructor_picture'] = this.instructorPicture;
    data['username'] = this.username;
    return data;
  }
}

class InvitedFriendsInfo {
  int quickbloxId;
  String name;
  String username;
  String profilePicture;
  bool isSelected = false;

  InvitedFriendsInfo({this.quickbloxId, this.name, this.username, this.profilePicture});

  InvitedFriendsInfo.fromJson(Map<String, dynamic> json) {
    quickbloxId = json['quickblox_id'];
    name = json['name'];
    profilePicture = json['profile_picture'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['quickblox_id'] = this.quickbloxId;
    data['name'] = this.name;
    data['profile_picture'] = this.profilePicture;
    data['username'] = this.username;
    return data;
  }
}
