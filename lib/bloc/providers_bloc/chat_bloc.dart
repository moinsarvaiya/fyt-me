import 'package:flutter/material.dart';

class InAppChatBloc extends ChangeNotifier {
  int unreadCount = 0;

  updateCount(int count) {
    unreadCount = count;
    notifyListeners();
  }
}
