import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fytme/bloc/calorie_bloc/calorie_bloc.dart';
import 'package:fytme/bloc/reviewbloc/review_bloc.dart';
import 'package:fytme/data/repositories/abstracts/calorie_repository.dart';
import 'package:fytme/data/repositories/abstracts/proposal_repository.dart';
import 'package:fytme/data/repositories/abstracts/review_repository.dart';
import 'package:fytme/src/pages/account_type.dart';
import 'package:fytme/src/pages/contact_us.dart';
import 'package:fytme/src/pages/createditprofile/instructor_createEditProfile.dart';
import 'package:fytme/src/pages/createditprofile/instructor_verification/under_review.dart';
import 'package:fytme/src/pages/createditprofile/proposal.dart';
import 'package:fytme/src/pages/home/chat_messages.dart';
import 'package:fytme/src/pages/home/chat_users.dart';
import 'package:fytme/src/pages/home/comments.dart';
import 'package:fytme/src/pages/home/notifications.dart';
import 'package:fytme/src/pages/home/other_user.dart';
import 'package:fytme/src/pages/home/reviews.dart';
import 'package:fytme/src/pages/home/search_trainers.dart';
import 'package:fytme/src/pages/payment.dart';

import 'bloc/authbloc/auth_bloc.dart';
import 'bloc/authentication/authentication_bloc.dart';
import 'bloc/authentication/authentication_state.dart';
import 'bloc/editprofilebloc/profile_bloc.dart';
import 'bloc/proposalbloc/proposal_bloc.dart';
import 'bloc/securityBloc/security_bloc.dart';
import 'common/constants.dart';
import 'data/repositories/abstracts/change_password_repository.dart';
import 'data/repositories/abstracts/profile_respository.dart';
import 'data/repositories/abstracts/user_repository.dart';
import 'src/pages/about.dart';
import 'src/pages/calorie_tracker.dart';
import 'src/pages/change_password.dart';
import 'src/pages/confirm_forgot_password.dart';
import 'src/pages/confirm_registration.dart';
import 'src/pages/createditprofile/assessment.dart';
import 'src/pages/createditprofile/instructor_verification/form.dart';
import 'src/pages/home.dart';
import 'src/pages/how_it_works.dart';
import 'src/pages/login/login.dart';
import 'src/pages/privacy_policy.dart';
import 'src/pages/programs.dart';
import 'src/pages/quiz_screen.dart';
import 'src/pages/splash_screen.dart';
import 'src/pages/terms_conditions.dart';
import 'src/pages/welcome_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/Welcome':
        return MaterialPageRoute(builder: (_) => WelcomeScreen());
      case '/ChatUsers':
        return MaterialPageRoute(builder: (_) => ChatUsersWidget());
      case '/Quiz':
        return MaterialPageRoute(builder: (_) => QuizScreen());
      case '/AccountType':
        return MaterialPageRoute(builder: (_) => AccountTypeScreen());
      case '/About':
        return MaterialPageRoute(builder: (_) => AboutWidget());
      case '/Programs':
        return MaterialPageRoute(builder: (_) => ProgramsWidget());
      case '/HowItWorks':
        return MaterialPageRoute(builder: (_) => HowItWorksWidget());
      case '/Assessment':
        return MaterialPageRoute(builder: (_) => AssessmentWidget());
      case '/TermsAndConditions':
        return MaterialPageRoute(builder: (_) => TermsAndConditionsWidget());
      case '/PrivacyAndPolicy':
        return MaterialPageRoute(builder: (_) => PrivacyAndPolicyWidget());
      case '/ContactUs':
        return MaterialPageRoute(builder: (_) => ContactUsWidget());
      case '/Notification':
        return MaterialPageRoute(
            builder: (_) => Notifications(
                  latestIds: [],
                ));
      case '/ChatMessages':
        return MaterialPageRoute(builder: (_) {
          ChatMessageArguments argument = args;
          return ChatMessagesWidget(
            otherUserId: argument.otherUserId,
            otherUserName: argument.otherUserName,
            otherUserProfile: argument.otherUserProfile,
          );
        });
      case '/Payment':
        return MaterialPageRoute(builder: (_) {
          PaymentArguments argument = args;
          return PaymentScreen(
            proposalDetails: argument.proposalDetails,
            proposalId: argument.proposalId,
          );
        });
      case '/Pages':
        return MaterialPageRoute(
            builder: (_) => SearchTrainersWidget(
                  isLoggedIn: true,
                ));
      case '/Comments':
        return MaterialPageRoute(builder: (_) {
          CommentsArguments argument = args;
          return Comment(
              currentTab: argument.currentTab,
              userComments: argument.userComments,
              imagePath: argument.imagePath,
              userImagesObj: argument.userImagesObj,
              imageId: argument.imageId,
              userId: argument.userId);
        });

      case '/Login':
        return MaterialPageRoute(
          builder: (_) => BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
              printLog(state.toString());
              if (state is Authenticated) {
                return HomeWidget();
              } else if (state is ForgetPasswordState) {
                printLog('ForgetPasswordState');
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: ConfirmForgotPasswordScreen());
              } else if (state is EmailVerificationScreen) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: ConfirmRegistration());
              } else if (state is AccountTypeMissing) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AccountTypeScreen());
              } else if (state is ProfileMissing) {
                return BlocProvider(
                  create: (context) => ProfileBloc(
                      authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                  child: getPage(Storage().currentUser.accountType),
                );
              } else if (state is ShowQuestionAnswer) {
                return BlocProvider(
                    create: (context) => ProfileBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                    child: QuizScreen());
              } else if (state is InstructorVerificationMissing) {
                return BlocProvider(
                  create: (context) => ProfileBloc(
                      authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                  child: TabsInstructorVerification(),
                );
              } else if (state is ForgetFormState) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AuthWidgets(formType: EmailPasswordSignInFormType.forgetPassword));
              } else {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AuthWidgets(
                      formType: EmailPasswordSignInFormType.signIn,
                    ));
              }
            },
          ),
        );
      case '/SignUp':
        return MaterialPageRoute(
          builder: (_) => BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
              printLog(state.toString());
              if (state is Authenticated) {
                return HomeWidget();
              } else if (state is EmailVerificationScreen) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: ConfirmRegistration());
              } else if (state is ProfileMissing) {
                return BlocProvider(
                  create: (context) => ProfileBloc(
                      authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                  child: getPage(Storage().currentUser.accountType),
                );
              } else if (state is ShowQuestionAnswer) {
                return BlocProvider(
                    create: (context) => ProfileBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                    child: QuizScreen());
              } else if (state is InstructorVerificationMissing) {
                return BlocProvider(
                  create: (context) => ProfileBloc(
                      authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                  child: TabsInstructorVerification(),
                );
              } else if (state is ForgetPasswordState) {
                printLog('ForgetPasswordState');
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: ConfirmForgotPasswordScreen());
              } else if (state is AccountTypeMissing) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AccountTypeScreen());
              } else if (state is ForgetFormState) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AuthWidgets(formType: EmailPasswordSignInFormType.forgetPassword));
              } else {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AuthWidgets(
                      formType: EmailPasswordSignInFormType.register,
                    ));
              }
            },
          ),
        );
      case '/Home':
        return MaterialPageRoute(
          builder: (_) => BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
              printLog(state.toString());
              if (state is Uninitialized) {
                return SplashScreen();
              } else if (state is Authenticated) {
                /*return Storage().userProfile.accountType == 'instructor'
                    ? SearchTrainer(isLoggedIn: true)
                    : HomeWidget();*/
                return HomeWidget();
              } else if (state is EmailVerificationScreen) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: ConfirmRegistration());
              } else if (state is ForgetPasswordState) {
                printLog('ForgetPasswordState');
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: ConfirmForgotPasswordScreen());
              } else if (state is AccountTypeMissing) {
                return BlocProvider(
                    create: (context) => AuthBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), userRepository: RepositoryProvider.of<UserRepository>(context)),
                    child: AccountTypeScreen());
              } else if (state is ProfileMissing) {
                return BlocProvider(
                    create: (context) => ProfileBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                    child: getPage(Storage().currentUser.accountType));
              } else if (state is ShowQuestionAnswer) {
                return BlocProvider(
                    create: (context) => ProfileBloc(
                        authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                    child: QuizScreen());
              } else if (state is InstructorVerificationMissing) {
                return BlocProvider(
                  create: (context) => ProfileBloc(
                      authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
                  child: TabsInstructorVerification(),
                );
              } else {
                return WelcomeScreen();
              }
            },
          ),
        );
      case '/ChangePassword':
        return MaterialPageRoute(builder: (_) {
          return BlocProvider(
            create: (context) => SecurityBloc(passwordRepository: RepositoryProvider.of<ChangePasswordRepository>(context)),
            child: ChangePasswordScreen(),
          );
        });
      case '/EditProfile':
        return MaterialPageRoute(builder: (_) {
          return BlocProvider(
            create: (context) => ProfileBloc(
                authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
            child: InstructorCreatEditProfile(),
          );
        });
      case '/Verification':
        return MaterialPageRoute(builder: (_) {
          Widget defaultWidget = TabsInstructorVerification(
            fromSetting: true,
          );
          if (Storage().userProfile.instructorStatus == 'under_review') {
            defaultWidget = UnderReview();
          }
          return BlocProvider(
              create: (context) => ProfileBloc(
                  authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
              child: defaultWidget);
        });
      case '/Proposal':
        return MaterialPageRoute(builder: (_) {
          ProposalArguments argument = args;
          return BlocProvider(
            create: (context) => ProposalBloc(
                authenticationBloc: context.read<AuthenticationBloc>(), proposalRepository: RepositoryProvider.of<ProposalRepository>(context)),
            child: Proposal(instructorId: argument.instructorId, selectedDate: argument.selectedDate, selectedHours: argument.selectedHours),
          );
        });
      case '/OtherUser':
        return MaterialPageRoute(builder: (_) {
          OtherUserArguments argument = args;
          return BlocProvider(
            create: (context) => ProfileBloc(
                authenticationBloc: context.read<AuthenticationBloc>(), profileRepository: RepositoryProvider.of<ProfileRepository>(context)),
            child: OtherUserWidget(
                userId: argument.userId,
                userProfile: argument.userProfile,
                userName: argument.userName,
                userType: argument.userType,
                isPaid: argument.isPaid),
          );
        });
      case '/Reviews':
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => ReviewBloc(reviewRepository: RepositoryProvider.of<ReviewRepository>(context)),
            child: Reviews(
              instructorDetails: args,
            ),
          ),
        );
      case '/Calorie':
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => CalorieBloc(
                authenticationBloc: context.read<AuthenticationBloc>(), calorieRepository: RepositoryProvider.of<CalorieRepository>(context)),
            child: CalorieTrackerWidget(),
          ),
        );
      default:
        return MaterialPageRoute(builder: (_) => Scaffold(body: SafeArea(child: Text('Route Error'))));
    }
  }

  static Widget getPage(String type) {
    switch (type) {
      case 'instructor':
        return InstructorCreatEditProfile();
        break;
      case 'customer':
      default:
        return InstructorCreatEditProfile();
    }
  }
}
